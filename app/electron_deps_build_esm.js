"use strict";

const babelCore = require("@babel/core");
const fs = require("fs");
const glob = require("glob");
const path = require("path");

function transpile(code) {
	return babelCore.transformSync(code, {
		plugins: [
			require.resolve("@babel/plugin-proposal-export-namespace-from"),
			require.resolve("@babel/plugin-transform-modules-commonjs"),
			require.resolve("@babel/plugin-transform-runtime"),
			require.resolve("babel-plugin-transform-import-meta")
		]
	});
}

{ // ps-list
	let code = fs.readFileSync('./node_modules/ps-list/index.js', "utf8");
	let transpiledCode = transpile(code);
	
	try {fs.mkdirSync('./electron_deps_esm');} catch(e) {}
	fs.writeFileSync('./electron_deps_esm/ps-list.js', transpiledCode.code);
	
	if(process.platform === "win32") {
		try {fs.mkdirSync('./vendor');} catch(e) {}

		// copy vendor fastlist executable
		const fileArch = process.arch === "x64" ? "x64" : "x86";
		const pslistDir = path.resolve('./node_modules/ps-list/vendor');
		const fileToCopy = glob.globSync(`*${fileArch}*.exe`, {cwd:pslistDir, absolute:true, nocase:true})[0];
		const filename = path.basename(fileToCopy);
		fs.copyFileSync(fileToCopy, path.resolve('./vendor/'+filename));
	}
	
}
