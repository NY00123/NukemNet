module.exports = function({}) {

    const iniEdits = [{"cmd":"edit-ini",addIf:(ctx)=>ctx?.GameRoom?.ImHost==true, "file":"nncfg", "edits":[]}]

    const iniEntries = {
        ConnectionName:"'Direct TCP~IP'",
        GameName:"GameRoom.ChanName||'NukemNet'",
        MaxPlayers: "GameRoom.MaxPlayers?.[0]||7",
        RemoteConsolePort:"GameRoom.MyPort",
        ConsolePassword: "'admin'",
        AllowRemoteConsole: "'1'",
        RemoteAdmin: "'1'",
        
        TimeLimit: "GameRoom.Params.timeLimit?.[0]||0",
        KillGoal: "GameRoom.Params.killGoal?.[0]||0",
        MissionName: "GameRoom.Params.mission?.[0]",
        Scriptname: "GameRoom.Params.script?.[0]",
        SetDifficulty: "GameRoom.Params.difficulty?.[0]||0",
        SetLevel: "GameRoom.Params.level?.[0]||1",

        PPS: "GameRoom.Params.pps?.[0]||12",
        RespawnTime: "GameRoom.Params.respawnTime?.[0]||30",
        Peer2Peer:'GameRoom.Params.p2p?.[0]||0'
    }

    const deletableIniEntries = {
        NumTeams:'GameRoom.Params.ntms?.[0]',
        SetTeamDamage:'GameRoom.Params.tmDmg?.[0]',
        AccurateCollisions:'GameRoom.Params.accCol?.[0]',
        SendRotVel:'GameRoom.Params.rotVel?.[0]',
        BrightPlayers:'GameRoom.Params.brightP?.[0]',
        Permissable: 'GameRoom.Params.permiss?.[0]',
        UseSmoothing:'GameRoom.Params.smooth?.[0]',
        RandomizeRespawn:'GameRoom.Params.randSpwn?.[0]',
    }

    for(const key in iniEntries) {
        iniEdits[0].edits.push({"category":"server config file","entry":key, unquote:true, skipNull:true, "value":iniEntries[key]})
    }
    for(const key in deletableIniEntries) {
        iniEdits[0].edits.push({"category":"server config file","entry":key, unquote:true, skipNull:false, "value":deletableIniEntries[key]})
    }

    const runnables = {
        "multiplayer": {
            "file":"main",
            beforeRun: iniEdits,
            hostAlsoRunsAsClient:true
        },
        "settings":{file:"setup"},
        "singleplayer": {"file":"main"}
    };

    return {
        "name": "Descent 3",
        "get": [{
            type:"gog",
            prodId:"1207658657",
            nameId:"descent_3_expansion",
            name:"Descent 3 + Mercenary"
        }],
        "executables": {
            "full": {
                tips: {
                    html: `For more parameter descriptions, refer to http://www.descentforum.net/serverops/d3dedi.htm`
                },
                "name":"Full",
                midGameJoin: true,
                needsTempDir:true,
                "hostInfoOnLaunch": (ctx)=>`Server Console in: telnet ${ctx.GameRoom.DestIp} ${ctx.GameRoom.MyPort} (password: admin)`,
                "files": {
                    "main": {"path":"MAIN.EXE", "runDetachedShell":(obj)=>{return obj?.GameRoom?.ImHost==true}},
                    "setup": {"path":"Descent 3.exe"},
                    "nncfg": {"path":"NukemNet.cfg", "optional":true}
                },
                "runnables": runnables,
                "parameters": {
                    "base": {
                        "modeSupport": ["multiplayer", "singleplayer"],
                        "type": "static",
                        "value": ["'-launched'", "'-nointro'", "'-setdir'+GameDir", "'-tempdir'", "TempDir"],
                        "for":"private"
                    },
                    "client": {
                        "modeSupport": ["multiplayer"],
                        "type": "static",
                        "value": ["'-directip'", "GameRoom.DestIp+':'+GameRoom.DestPort"],
                        "for":"client-only-private"
                    },
                    "server": {
                        "modeSupport": ["multiplayer"],
                        "type": "static",
                        "value": ["'-dedicated'", "'NukemNet.cfg'", "'-useip'", "'0.0.0.0'", "'-useport'", "GameRoom.MyPort"],
                        "for":"host-only-private"
                    },
                    // "pilot": {
                    //     "modeSupport": ["singleplayer"], // Disabled, because it breaks multiplayer game join, and doesn't work properly in singleplayer too
                    //     "type": "dirfiles",
                    //     "value": ["'-pilot'","value[0]"],
                    //     "label": "Pilot",
                    //     "path": "*.plt",
                    //     "stripExt": true,
                    //     optional:true,
                    //     "for":"client-only-private"
                    // },
                    "mission": {
                        "modeSupport": ["multiplayer"],
                        "type": "dirfiles",
                        "value": "value",
                        "label": "Mission",
                        "stripPath": true,
                        "path": "missions/*.mn3",
                        "for":"host-only-shared",
                        syncOnly: true
                    },
                    "level": {
                        "modeSupport": ["multiplayer"],
                        "type": "numrange",
                        "min":1,
                        "max":20,
                        "delta":1,
                        "label": "Level",
                        "value": ["value"],
                        "optional":true,
                        "for":"host-only-shared",
                        syncOnly: true
                    },
                    "script": {
                        "modeSupport": ["multiplayer"],
                        "type": "dirfiles",
                        "value": "value",
                        "label": "Script",
                        "path": "netgames/*.d3m",
                        "stripExt": true,
                        "stripPath": true,
                        "for":"host-only-shared",
                        syncOnly: true
                    },
                    "ae": {
                        "modeSupport": ["multiplayer"],
                        "type": "dirfiles",
                        "value": ["'-autoexec'", "value[0]"],
                        "label": "Autoexec (.dmfc)",
                        "path": "**/*.dmfc",
                        optional:true,
                        "for":"host-only-private"
                    },
                    "timeLimit": {
                        "modeSupport": ["multiplayer"],
                        "type": "numrange",
                        "min":0,
                        "max":120,
                        "delta":1,
                        description:"Time Limit (Minutes)",
                        "label": "Time Limit",
                        "value": ["value"],
                        "optional":true,
                        "for":"host-only-shared",
                        syncOnly: true
                    },
                    "killGoal": {
                        "modeSupport": ["multiplayer"],
                        "type": "numrange",
                        "min":0,
                        "max":120,
                        "delta":1,
                        "label": "Kill Goal",
                        "value": ["value"],
                        "optional":true,
                        "for":"host-only-shared",
                        syncOnly: true
                    },
                    "diffi": {
                        "modeSupport": ["multiplayer"],
                        "type": "numrange",
                        "min":0,
                        "max":4,
                        "delta":1,
                        "label": "Difficulty",
                        "value": ["value"],
                        "optional":true,
                        "for":"host-only-shared",
                        syncOnly: true
                    },

                    "pps": {
                        "modeSupport": ["multiplayer"],
                        "type": "numrange",
                        "min":4,
                        "max":50,
                        "delta":1,
                        description:"Packets Per Second",
                        "label": "PPS",
                        "value": ["value"],
                        "optional":true,
                        "for":"host-only-shared",
                        syncOnly: true,
                        section: "advanced"
                    },
                    "respawnTime": {
                        "modeSupport": ["multiplayer"],
                        "type": "numrange",
                        "min":0,
                        "max":120,
                        "delta":1,
                        "label": "Respawn Time",
                        "value": ["value"],
                        "optional":true,
                        "for":"host-only-shared",
                        syncOnly: true,
                        section: "advanced"
                    },
                    "p2p": {
                        "modeSupport": ["multiplayer"],
                        "type": "boolean",
                        "label": "Peer2Peer",
                        "value": "'1'",
                        "for": "host-only-shared",
                        syncOnly: true,
                        section: "advanced"
                    },
                    "ntms": {
                        "modeSupport": ["multiplayer"],
                        "type": "numrange",
                        "min":0,
                        "max":6,
                        "delta":1,
                        "label": "NumTeams",
                        "value": ["value"],
                        "optional":true,
                        "for":"host-only-shared",
                        syncOnly: true,
                        section: "advanced"
                    },
                    "tmDmg": {
                        "modeSupport": ["multiplayer"],
                        "type": "numrange",
                        "min":0,
                        "max":6,
                        "delta":1,
                        "label": "TeamDmg",
                        "value": ["value"],
                        "optional":true,
                        "for":"host-only-shared",
                        syncOnly: true,
                        section: "advanced"
                    },
                    "accCol": {
                        "modeSupport": ["multiplayer"],
                        "type": "boolean",
                        "label": "AccurateCollisions",
                        "value": "'1'",
                        "for": "host-only-shared",
                        syncOnly: true,
                        section: "advanced"
                    },
                    "rotVel": {
                        "modeSupport": ["multiplayer"],
                        "type": "boolean",
                        "label": "SendRotVel",
                        "value": "'1'",
                        "for": "host-only-shared",
                        syncOnly: true,
                        section: "advanced"
                    },
                    "brightP": {
                        "modeSupport": ["multiplayer"],
                        "type": "boolean",
                        "label": "BrightPlayers",
                        "value": "'1'",
                        "for": "host-only-shared",
                        syncOnly: true,
                        section: "advanced"
                    },
                    "permiss": {
                        "modeSupport": ["multiplayer"],
                        "type": "boolean",
                        "label": "Permissable",
                        "value": "'1'",
                        "for": "host-only-shared",
                        syncOnly: true,
                        section: "advanced"
                    },
                    "smooth": {
                        "modeSupport": ["multiplayer"],
                        "type": "boolean",
                        "label": "UseSmoothing",
                        "value": "'1'",
                        "for": "host-only-shared",
                        syncOnly: true,
                        section: "advanced"
                    },
                    "randSpwn": {
                        "modeSupport": ["multiplayer"],
                        "type": "boolean",
                        "label": "RandomizeRespawn",
                        "value": "'1'",
                        "for": "host-only-shared",
                        syncOnly: true,
                        section: "advanced"
                    }
                },
                paramSections: {
                    "advanced": {
                        name:"Advanced"
                    }
                }
            }
        }
    };
}