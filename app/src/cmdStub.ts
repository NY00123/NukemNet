import { IrcClientExtra } from "../../server/src/common/ircExtend";

// This stub is only to high "cmd" messages coming from 'selfMessage' irc event.
const msgBuffers: {[from:string]: {strBuffer:string, timerId:any}} = {}
export const stub = {
    async hideCommandMessage(to:string, text:string):Promise<boolean> {
        try {
            await IrcClientExtra.handleMultipartCommandBuffer(null, to, text, msgBuffers);
            return true
        } catch(e) {
            return false;
        }
    }
}