declare const nw:any;
function wrequire(str:string) {
    try {
        //NW.js
        return (window as any).require(str)
    } catch(e) {
        //NodeJS
        return require(str);
    }
}

const crypto = wrequire('crypto')


export function createHhash(str:string) {
    return crypto.createHash('md5').update(str).digest("hex").substring(0,8);
}

let counter = Number.MIN_SAFE_INTEGER;
export function createRandomHash() {
    if(counter >= Number.MAX_SAFE_INTEGER) {
        counter = Number.MIN_SAFE_INTEGER
    } else {
        counter++
    }
    const salt = `${Date.now()}:${counter}:${Math.random()}`;
    return createHhash(salt);
}