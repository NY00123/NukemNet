
module.exports = function () {
    const common = require('./quake3_common.i.js')();

    return {
        "name": "Quake 3 Arena",
        "get": [{
            type:"gog",
            prodId:"1443696086",
            nameId:"quake_iii_arena",
            name:"Quake III Arena"
        }],
        executables: {
            "original": {
                name:"Original",
                "maxPlayers":16,
                midGameJoin: true,
                "files": {
                    "main": {"path":"quake3.exe", runShell:true}
                },
                "runnables": {
                    "multiplayer": {"file":"main", beforeRun:common.writeCommandsCfg},
                    "settings": {"file":"main", beforeRun:common.writeCommandsCfg},
                    "singleplayer": {"file":"main", beforeRun:common.writeCommandsCfg}
                },
                parameters:common.getParams()
            },
            "ioquake3": {
                name:"ioquake3",
                "maxPlayers":16,
                midGameJoin: true,
                "files": {
                    "main": {"glob":"ioquake3*"},
                    "ded": {"glob":"ioq3ded*", runDetachedShell:true}
                },
                "runnables": {
                    "multiplayer":{
                        "file":(ctx)=>{return ctx?.GameRoom?.ImHost?'ded':'main'}, 
                        beforeRun:common.writeCommandsCfg, hostAlsoRunsAsClient:true},
                    "settings": {"file":"main", beforeRun:common.writeCommandsCfg},
                    "singleplayer": {"file":"main", beforeRun:common.writeCommandsCfg}
                },
                parameters:common.getParams(undefined, true)
            }
        }
    }
}
