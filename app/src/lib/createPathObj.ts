// https://stackoverflow.com/a/38768912/230637

import path from "path";

export function createPathObj(paths:string[]) {
    const dir = {}
    
    if(paths.length>0) {
        paths.reduce(function(dir:any, path:string) {
            return dir[path] = {}
        }, dir)
    }

    return dir;
}