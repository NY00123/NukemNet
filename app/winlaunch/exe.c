#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
  // We forward the last param, which is the deep linking capability for url schemes (e.g nukemnet://join/mychan)
  char* lastArg = argv[argc-1];
  char* baseCmd = "start .\\node_modules\\electron\\dist\\electron.exe . ";
  int baseCmdLen = strlen(baseCmd);
  int lastArgLen = strlen(lastArg);
  const int totalLen = baseCmdLen + lastArgLen+1; // +1 for null terminator

  char* finalCommand = malloc(sizeof(char) * totalLen);
  strcpy(finalCommand, baseCmd);
  strcat(finalCommand, lastArg);

  popen(finalCommand, "r");
  return 0;
}