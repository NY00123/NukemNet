import { ChildProcess } from "child_process";
import { net } from "../NodeLibs"
import { Client } from "matrix-org-irc";
import storeActions from "../store/storeActions";
import { IrcClientExtra } from "../../../server/src/common/ircExtend";
import { Socket } from "net";
import { mainStore } from "../store/store";
import { isNukemNetBot } from "../../../server/src/common/constants";

const bufferNull = Buffer.from([0x00]);

const reconnectTimeMs=3000;
export async function namedPipeChatSync(proc:ChildProcess, ircClient:Client, pipeName:string, channelLaunched?:string) {
    if(!channelLaunched?.length) return; // must be in a channel to sync chat
    const lchan = channelLaunched.toLowerCase();
    
    const ircChanNicks = new Set(Object.keys(mainStore.joinedChannels[lchan].nicks));

    let client:Socket, destroyed=false;
    function destroy() {
        destroyed = true;
        try {client.removeAllListeners()} catch(e) {}
        try {client.destroy();} catch(e) {}
        IrcClientExtra.customEvents.off('pureMessage', pureMessage);
        ircClient.off('nick', onNick);
        ircClient.off('join', onJoin);
        ircClient.off('part', onLeave);ircClient.off('kick', onLeave);
        ircClient.off('quit', onDisconnect);ircClient.off('kill', onDisconnect);
    }

    proc.once('exit', destroy);
    proc.once('error', destroy);

    // Wait for process to spawn
    await new Promise((resolve)=>{proc.once('spawn', resolve);})

    function connect() {
        try {client.destroy()} catch(e) {}
        try {client.removeAllListeners()} catch(e) {};
        if(destroyed) return;

        try {
            client = net.connect(pipeName);
        } catch(e) {}
        
        client.on('close', ()=>{
            setTimeout(()=>connect(), reconnectTimeMs);
        });
        client.on('error', ()=>{}) // stub to hide exception

        client.on('connect', ()=>{
            console.log(`Pipe Connected ${pipeName}`)
            client.on('data', (buffer)=>{
                try {
                    const messages = buffer.toString().split(bufferNull.toString());
                    for(const message of messages) {
                        const obj = JSON.parse(message)
                        if (obj.event === "message") {
                            const cmd = obj;
                            storeActions.message(`🎮${ircClient.nick}`, lchan, `${cmd.text}`,"", true, false);
                            IrcClientExtra.sendNNCmdTo(ircClient, lchan, {
                                op: 'gameMessage',
                                data: {text: cmd.text}
                            });
                        }
                    }
                } catch(e) {}
            });
        })
    }

    connect();

    function sendToClient(jsonObj:any) {
        client.write(JSON.stringify(jsonObj) + bufferNull);
    }

    function pureMessage(nick:string|null, to:string, text:string, raw:any|null) {
        if(to.toLowerCase() !== lchan) return;
        sendToClient({
            "event":"message",
            "user":nick!=null?nick:ircClient.nick,
            text
        });
    }
    IrcClientExtra.customEvents.on('pureMessage', pureMessage);

    function onJoin(joinedChan:string, nick:string) {
        if(joinedChan.toLowerCase() !== lchan!.toLowerCase()) return;
        ircChanNicks.add(nick.toLowerCase());
        sendToClient({
            "event":"join",
            "user":nick
        });
    }

    function ircUserGone(nick:string) {
        ircChanNicks.delete(nick.toLowerCase())
        if(isNukemNetBot(nick)) return;
        sendToClient({
            "event":"leave",
            "user":nick
        });
        if(ircClient.nick.toLowerCase() === nick.toLowerCase()) {
            destroy(); // TODO verify that it's ok
        }
    }

    function onLeave(leftChan:string, nick:string) {
        if(leftChan.toLowerCase() !== lchan) return;
        ircUserGone(nick);
    }
    function onDisconnect(nick:string) {
        ircUserGone(nick);
    }

    function onNick(oldNick:string, newNick:string) {
        const oldlNick = oldNick.toLowerCase();
        if(ircChanNicks.has(oldlNick)) {
            ircChanNicks.delete(oldlNick);
            ircChanNicks.add(newNick.toLowerCase());
        }
    }

    ircClient.on('nick', onNick);
    ircClient.on('join', onJoin);
    ircClient.on('part', onLeave);ircClient.on('kick', onLeave);
    ircClient.on('quit', onDisconnect);ircClient.on('kill', onDisconnect);
}