
#include <thread>
#include <string>
#include "dl.h"
#include <curl/curl.h>

using namespace std;

long GetFileSize(std::string filename)
{
    struct stat stat_buf;
    int rc = stat(filename.c_str(), &stat_buf);
    return rc == 0 ? stat_buf.st_size : -1;
}

double GetRemoteFileSize(string url) {
    CURL* _curl_handle = curl_easy_init();

    curl_easy_setopt(_curl_handle, CURLOPT_URL, url.c_str());
    curl_easy_setopt(_curl_handle, CURLOPT_HEADER, 1);
    curl_easy_setopt(_curl_handle, CURLOPT_NOBODY, 1);

    double dResult;
    curl_easy_perform(_curl_handle);
    curl_easy_getinfo(_curl_handle, CURLINFO_CONTENT_LENGTH_DOWNLOAD, &dResult);
    return dResult;
}


void checkProgress(string filename) {

    while(true) {
        printf("%d\n", GetFileSize(filename));
        this_thread::sleep_for(chrono::milliseconds(1000));
    }
}

size_t write_data(void* ptr, size_t size, size_t nmemb, FILE* stream) {
    size_t written = fwrite(ptr, size, nmemb, stream);
    return written;
}

void downloadFile(string url, string outfilename) {
    CURL* curl;
    FILE* fp;
    CURLcode res;
    curl = curl_easy_init();
    if (curl) {
        fp = fopen(outfilename.c_str(), "wb");
        curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
        res = curl_easy_perform(curl);
        /* always cleanup */
        curl_easy_cleanup(curl);
        fclose(fp);
    }

}



#include <FL/Fl.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Window.H>

namespace Examples {
    class Main_Window : public Fl_Window {
    public:
        Main_Window() : Fl_Window(200, 100, 300, 300, "Hello world (box)") {
            box1.align(FL_ALIGN_CENTER | FL_ALIGN_INSIDE | FL_ALIGN_CLIP | FL_ALIGN_WRAP);
            box1.labelfont(box1.labelfont() | FL_BOLD | FL_ITALIC);
            box1.labelsize(36);
            box1.labelcolor(fl_rgb_color(0x00, 0x80, 0x00));
            box1.labeltype(FL_SHADOW_LABEL);

            resizable(this);
            position((Fl::w() - w()) / 2, (Fl::h() - h()) / 2);
        }

    private:
        Fl_Box box1{ 0, 0, 300, 300, "Hello, World!" };
    };
}


int main()
{
    /*string filename = "myfile.zip";
    string url = "https://nukemnet.blob.core.windows.net/releases/NukemNet-win32-x64-0.3.13.zip";
    auto res = GetRemoteFileSize(url);

    thread th1(checkProgress, filename);
    downloadFile(url, filename);
    th1.join();*/


    Examples::Main_Window window;
    window.show();
    return Fl::run();
}
