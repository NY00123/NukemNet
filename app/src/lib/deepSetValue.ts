// https://stackoverflow.com/a/6842900/230637

export function deepSetValue(obj:any, value:any, path:string[]) {
    if(!path.length) {
        Object.assign(obj,value)
        return;
    }

    var i;
    for (i = 0; i < path.length - 1; i++)
        obj = obj[path[i]];

    obj[path[i]] = value;
}