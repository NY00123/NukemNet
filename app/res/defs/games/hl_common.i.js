
module.exports = function () {

    const path = require('path');
    const fsPromises = require('fs/promises');

    const NNCfg_HostFile = "NukemNetHost.cfg";
    const NNCfg_ClientFile = "NukemNetClient.cfg";

    const writeCommandsCfg = [{"cmd":"function", async run(ctx, execDef, result, imHost, players, hostIpDetails, useDedicated, othersIpDetails, channelName, hostTunnelChosenPort) {
        const fname = imHost?NNCfg_HostFile:NNCfg_ClientFile;
        const params = ctx.GameRoom.Params;
        let lineCommands = [];
        for(const p in params) {
            const argsArr = params[p];
            for(const line of argsArr) {
                const trm = line.trim();
                if(trm.startsWith("+") && !trm.startsWith("+exec") && !p.startsWith("_") && ctx?.GameRoom?.ParamDefs?.[p]?.syncOnly) {
                    lineCommands.push({id:p, line:trm.substring(1)});
                }
            }
        }
        lineCommands.sort((a,b) => {
            const aOrder = ctx.GameRoom.ParamDefs[a.id]?.order;
            const bOrder = ctx.GameRoom.ParamDefs[b.id]?.order;
            return (aOrder||0)-(bOrder||0);
        });
        const fileContent = lineCommands.map(r=>r.line).join('\r\n');
        await fsPromises.writeFile(path.resolve(ctx.GameDir, fname), fileContent);
    }}];

    
    function getParams(game) {

        const params = {
            _dedicated: {
                "modeSupport": ["multiplayer"],
                "type": "boolean",
                "label": "Dedicated",
                "value": `'Yes'`,
                "for":"host-only-shared",
                syncOnly: true
            },
            "_host": {
                order:-100,
                "modeSupport": ["multiplayer"],
                "type": "static",
                "value": [
                    "'-nomaster'", "'-console'", "'-insecure'",
                    "'+ip'", "'0.0.0.0'",
                    "'-port'", "GameRoom.MyPort", "'-netconport'", "GameRoom.MyPort",
                    "'-netconpassword'", "'nukemnet'", "'-host'", "'NukemNet'"
                ],
                "for":"host-only-private"
            },
            "_game": {
                order:-99,
                "modeSupport": ["multiplayer", "singleplayer"],
                "type": "static",
                "value": ["'-game'", `'${game}'`],
                "for":"private"
            },

            "_cmdshost": {
                order:-99,
                "modeSupport": ["multiplayer", "singleplayer"],
                "type": "static",
                "value": ["'+exec'", `'${NNCfg_HostFile}'`],
                "for":"host-only-private",
            },
            "_cmdsclnt": {
                order:-99,
                "modeSupport": ["multiplayer", "singleplayer"],
                "type": "static",
                "value": ["'+exec'", `'${NNCfg_ClientFile}'`],
                "for":"client-only-private",
            },

            "maxp": {
                order:-98,
                "modeSupport": ["multiplayer"],
                "type": "static",
                "value": ["'+maxplayers 16'"],
                "for":"host-only-private",
                syncOnly:true
            },
            "map": {
                order:-96,
                "modeSupport": ["multiplayer", "singleplayer"],
                "type": "dirfiles",
                "value": ["'+map '+value"],
                "label": "Map",
                "stripPath": true,
                "stripExt": true,
                "path": game+"/maps/*.bsp",
                "for":"host-only-shared",
                syncOnly:true
            },
            "aftermap": {
                order:-94,
                "modeSupport": ["multiplayer"],
                "type": "static",
                "value": ["'+wait'"], // must be added, otherwise some of the next parameters will be ignored in-game (e.g mp_forcerespawn).
                "for":"host-only-private",
                syncOnly:true
            },
            "tmnm": {
                order:-93,
                "modeSupport": ["multiplayer"],
                "type": "text",
                "label": "Team Names",
                "value":[`'+mp_teamlist ' + '"'+value+'"'`],
                "optional":true,
                props: {
                    placeholder: "example: team1;team2"
                },
                syncOnly:true
            },
            foot: {
                order:-93,
                "modeSupport": ["multiplayer"],
                "type": "boolean",
                "label": "Footsteps",
                "value": ["'+mp_footsteps ' + (value?'1':'0')"],
                "addFalse": true,
                "for":"host-only-shared",
                syncOnly:true
            },
            frspawn: {
                order:-93,
                "modeSupport": ["multiplayer"],
                "type": "boolean",
                "label": "Force Respawn",
                "value": ["'+mp_forcerespawn ' + (value?'1':'0')"],
                "addFalse": true,
                "for":"host-only-shared",
                syncOnly:true
            },
            wpnst: {
                order:-93,
                "modeSupport": ["multiplayer"],
                "type": "boolean",
                "label": "Weapon Stay",
                "value": ["'+mp_weaponstay ' + (value?'1':'0')"],
                "addFalse": true,
                "for":"host-only-shared",
                syncOnly:true
            },
            flash: {
                order:-93,
                "modeSupport": ["multiplayer"],
                "type": "boolean",
                "label": game=='gearbox'? "Night Vision Goggles" : "Flashlight",
                "value": ["'+mp_flashlight ' + (value?'1':'0')"],
                "addFalse": true,
                "for":"host-only-shared",
                syncOnly:true
            },
            autocross: {
                order:-93,
                "modeSupport": ["multiplayer"],
                "type": "boolean",
                "label": "Autocrosshair",
                "value": ["'+mp_autocrosshair ' + (value?'1':'0')"],
                "addFalse": true,
                "for":"host-only-shared",
                syncOnly:true
            },
            frndlyfire: {
                order:-93,
                "modeSupport": ["multiplayer"],
                "type": "boolean",
                "label": "Friendly Fire",
                "value": ["'+mp_friendlyfire ' + (value?'1':'0')"],
                "addFalse": true,
                "for":"host-only-shared",
                syncOnly:true
            },
            team: {
                order:-93,
                "modeSupport": ["multiplayer"],
                "type": "boolean",
                "label": "Teamplay",
                "value": ["'+mp_teamplay ' + (value?'1':'0')"],
                "addFalse": true,
                "for":"host-only-shared",
                syncOnly:true
            },
            "frglmt": {
                order:-93,
                "modeSupport": ["multiplayer"],
                "type": "numrange",
                "min":0,
                "max":200,
                "delta":1,
                "label": "Frag Limit",
                "value": ["'+mp_fraglimit ' + value"],
                "optional":true,
                for:'host-only-shared',
                syncOnly:true
            },
            "fdmg": {
                order:-93,
                "modeSupport": ["multiplayer"],
                "type": "choice",
                "label": "Fall Damage",
                "value":["value[0] +' '+ value[1]"],
                "choices": [{
                    "label": "Normal",
                    "value": ["+mp_falldamage", "0"]
                }, {
                    "label": "Realistic",
                    "value": ["+mp_falldamage", "1"]
                }],
                "for":"host-only-shared",
                syncOnly:true
            },
            "tmlmt": {
                order:-93,
                "modeSupport": ["multiplayer"],
                "type": "numrange",
                "min":0,
                "max":60,
                "delta":1,
                "label": "Time Limit",
                "value": ["'+mp_timelimit ' + value"],
                "optional":true,
                for:'host-only-shared',
                syncOnly:true
            },

            "client": {
                "modeSupport": ["multiplayer"],
                "type": "static",
                "value": ["'+connect ' + GameRoom.DestIp + ':' + GameRoom.DestPort"],
                "for":"client-only-private",
                syncOnly:true
            }
        }

        if(game === 'gearbox') {
            params.teamassign ={
                order:-93,
                "modeSupport": ["multiplayer"],
                "type": "boolean",
                "label": "Auto Team Assign",
                "value": ["'+mp_ctf_autoteam ' + (value?'1':'0')"],
                "addFalse": true,
                "for":"host-only-shared",
                syncOnly:true
            };
            params.captlmt = {
                order:-93,
                "modeSupport": ["multiplayer"],
                "type": "numrange",
                "min":0,
                "max":100,
                "delta":1,
                "label": "Capture Limit",
                "value": ["'+mp_ctf_capture ' + value"],
                "optional":true,
                for:'host-only-shared',
                syncOnly:true
            }
        }
        if(game === 'dmc' || game === 'tfc' || game === 'ricochet') {
            ['team', 'frndlyfire', 'autocross', 'flash', 'tmnm', 'fdmg'].forEach(p=>delete params[p]);
        }
        if(game === 'tfc') {
            ['wpnst', 'foot', 'frspawn'].forEach(p=>delete params[p]);
        }
        if(game === 'ricochet') {
            ['wpnst', 'frspawn'].forEach(p=>delete params[p]);
        }
        if(game === 'rewolf') {
            ['foot', 'autocross', 'wpnst'].forEach(p=>delete params[p]);
        }
        if(game === 'dod') {
            params.tklimit = {
                order:-93,
                "modeSupport": ["multiplayer"],
                "type": "numrange",
                "min":0,
                "max":100,
                "delta":1,
                "label": "TeamKill Limit",
                "value": ["'+mp_tkpenalty ' + value"],
                "optional":true,
                for:'host-only-shared',
                syncOnly:true
            };
            ['tmnm', 'frglmt', 'fdmg', 'team', 'wpnst', 'frspawn', 'foot', 'autocross'].forEach(p=>delete params[p]);
        }
        if(game === 'svencoop') {
            
            ['tmnm', 'frglmt', 'tmlmt', 'fdmg', 'team', 'frndlyfire', 'frspawn'].forEach(p=>delete params[p]);

            params.notmlmt = {
                order:-93,
                "modeSupport": ["multiplayer"],
                "type": "boolean",
                "label": "Disable TimeLimits",
                "description": "Disable All Map Timelimits",
                "value": ["'+mp_notimelimit ' + (value?'1':'0')"],
                "addFalse": true,
                "for":"host-only-shared",
                syncOnly:true
            };

            params.skill = {
                order:-93,
                "modeSupport": ["multiplayer"],
                "type": "choice",
                "label": "Difficulty Level",
                "value":["value[0] +' '+ value[1]"],
                "choices": [{
                    "label": "Default",
                    "value": ["+skill", "0"]
                }, {
                    "label": "Easy",
                    "value": ["+skill", "1"]
                }, {
                    "label": "Hard",
                    "value": ["+skill", "2"]
                }, {
                    "label": "Realistic",
                    "value": ["+skill", "3"]
                }],
                "for":"host-only-shared",
                syncOnly:true
            };

            params.gibbing = {
                order:-93,
                "modeSupport": ["multiplayer"],
                "type": "boolean",
                "label": "No Gibbing",
                "description": "Disable Explosion Gibbing",
                "value": ["'+mp_noblastgibs ' + (value?'1':'0')"],
                "addFalse": true,
                "for":"host-only-shared",
                syncOnly:true
            };

            params.intermtm = {
                order:-93,
                "modeSupport": ["multiplayer"],
                "type": "numrange",
                "min":0,
                "max":100,
                "delta":1,
                "label": "InterM.Time(sec)",
                "description": "Intermission Time (sec)",
                "value": ["'+mp_chattime ' + value"],
                for:'host-only-shared',
                syncOnly:true
            };

            params.autoblnc = {
                order:-93,
                "modeSupport": ["multiplayer"],
                "type": "boolean",
                "label": "No Auto-Balance",
                "description": "Disable Low-Player-Count Auto-Balancing",
                "value": ["'+mp_disable_pcbalancing ' + (value?'1':'0')"],
                "addFalse": true,
                "for":"host-only-shared",
                syncOnly:true
            };
        }
        return params;
    }

    return {writeCommandsCfg, getParams};
}