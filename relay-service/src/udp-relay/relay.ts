import udp from 'dgram';

// copy paste.
const regex_v4 = /((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])/;
export function is_ipv4(ip:string){
  return regex_v4.test(ip);
};
export function udp6a(address:string) {
  if(is_ipv4(address) && !address.toLowerCase().startsWith('::ffff:')) {
    return '::ffff:' + address;
  }
  return address;
}

export class Relay {

  constructor(private socket:udp.Socket) {
    let forwardTo:udp.RemoteInfo|null = null;
    socket.on('message', (msg, rinfo)=>{
      if(msg.toString() === '_forward_to_me_') {
        forwardTo = rinfo;
        return;
      }
      if(!forwardTo) return; // If host did not contact first, ignore all messages.

      if(rinfo.address === forwardTo.address && rinfo.port === forwardTo.port) {
        try {
          const obj = parseForwardMessage(msg);
          socket.send(obj.buffer, obj.port, udp6a(obj.ip));
        } catch(e) {}
        return;
      }

      const buf = prepareForwardMessage(rinfo.address, rinfo.port, msg);
      socket.send(buf, forwardTo.port, udp6a(forwardTo.address));
    });
  }

  destroy() {
    this.socket.removeAllListeners();
    try {this.socket.close();} catch(e) {}
  }
}



export function parseForwardMessage(msg:Buffer):{buffer:Buffer,ip:string,port:number} {
  if(msg[0] === '6'.charCodeAt(0))  {
    // forward to ipv6
    const indexOfMetaEnd = msg.indexOf(';');
    const meta = msg.subarray(1,indexOfMetaEnd).toString();
    const [ipv6, port] = meta.split('_');
    const rawBuffer = msg.subarray(indexOfMetaEnd+1);
    return {ip:ipv6, port:parseInt(port), buffer:rawBuffer};
  } else if(msg[0] === '4'.charCodeAt(0))  {
    // forward to ipv4
    const ipv4 = `${msg.readUint8(1)}.${msg.readUint8(2)}.${msg.readUint8(3)}.${msg.readUint8(4)}`;
    const port = msg.readUint16BE(5);
    const rawBuffer = msg.subarray(7);
    return {ip:ipv4, port, buffer:rawBuffer};
  }
  throw 'Unparsable UDP relay message'
}

export function prepareForwardMessage(address:string, port:number, buffer:Buffer) {
  const isIpv6 = address.indexOf(":") != -1;

  if(isIpv6) {
    const metaData = '6'+address+'_'+port+';';
    const metaDataBuffer = Buffer.from(metaData);
    const sendBuffer = Buffer.alloc(metaDataBuffer.length+buffer.length);
    metaDataBuffer.copy(sendBuffer,0,0);
    buffer.copy(sendBuffer,metaDataBuffer.length,0);
    return sendBuffer;
  } else {
    const ipNumParts = address.split('.').map(n=>parseInt(n));
    const sendBuffer = Buffer.alloc(7+buffer.length);
    sendBuffer[0] = '4'.charCodeAt(0);
    sendBuffer.writeUint8(ipNumParts[0],1);
    sendBuffer.writeUint8(ipNumParts[1],2);
    sendBuffer.writeUint8(ipNumParts[2],3);
    sendBuffer.writeUint8(ipNumParts[3],4);
    sendBuffer.writeUInt16BE(port,5);
    buffer.copy(sendBuffer,7,0);
    return sendBuffer;
  }
  
}
