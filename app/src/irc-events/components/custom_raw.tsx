import {Component} from 'solid-js'
import { colorFromStr } from '../../lib/colorFromStr';
import { IRCEventCustom_Raw, IRCEvent_ChangeNick } from '../ircEvents';
import { IrcEventCmp_Base } from './base';

export const IrcEventCmpCustom_Raw:Component<IRCEventCustom_Raw> = (props)=>{
    const text = typeof props.obj === 'object'? JSON.stringify(props.obj, null, 2):props.obj as string;
    return <IrcEventCmp_Base {...props}><pre>{text.trim()}</pre></IrcEventCmp_Base>
}