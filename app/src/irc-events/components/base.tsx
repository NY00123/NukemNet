import {Component, createSignal} from 'solid-js'
import { IRCEvent_BaseEvent } from '../ircEvents';

const blinkTime = 1000;

export const IrcEventCmp_Base: Component<IRCEvent_BaseEvent & {children?:any}> = (props)=>{
    let timeLeftAsNew:number
    if(props.ts) {
        const tsDateNewTime = props.ts+blinkTime;
        timeLeftAsNew = tsDateNewTime-Date.now();
    } else {
        timeLeftAsNew = 0;
    }

    const [isNew, setIsNew] = createSignal(timeLeftAsNew>0);
    if(timeLeftAsNew>0) {
        setTimeout(function(){
            setIsNew(false);
        },timeLeftAsNew);
    }
    
    return <span class={isNew()?'new-event':''}>
        {props.ts?new Date(props.ts).toLocaleString():''}&nbsp
        <span>{props.children}</span>
        </span>
}