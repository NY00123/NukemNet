const v8 = require('v8');

const stringConstructor = "test".constructor;
const arrayConstructor = [].constructor;
const objectConstructor = ({}).constructor;

export function getPrimitiveType(object:any):'Object'|'Array'|'String'|'undefined'|'null'|'other' {
    if (object === null) {
        return "null";
    }
    if (object === undefined) {
        return "undefined";
    }
    if (object.constructor === stringConstructor) {
        return "String";
    }
    if (object.constructor === arrayConstructor) {
        return "Array";
    }
    if (object.constructor === objectConstructor) {
        return "Object";
    }

    return 'other';
}

export const structuredClone = (obj:any) => {
  return v8.deserialize(v8.serialize(obj));
};