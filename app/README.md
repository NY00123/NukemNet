REM:
* first run "npm install"
* then run script "npm run build-deps", you need to re-run this script whenever you update dependencies, after running "npm install" and "npm update"
* when changing interfaces that serve are commands between bot and clients, make sure to rebuild interface checkers using:
`npx ts-interface-builder foo.ts`
This is mandatory, otherwise people can send extra/missing properties in commands without server verification.
* must compile protobuf typescript code in server/src/common/protobuf
* do not do cross compilation, run the make build scripts on each platform, otherwise some stuff won't work.
* move code signing certificate file as app/sign.p12
* create text file app/signpassword which contains the password for app/sign.p12
* Do not do cross complication, always build for the current platform, otherwise there are issues.
  I used a VM per platform, that works fine.