module.exports = function({GameDefsType, utils}) {

    const path = require('path');

    const prepareGameIniAndCommitDat = [{"cmd":"edit-ini","file":"commit_dat", "edits":[
        {"category":"Commit","entry":"SOCKETNUMBER", "value":"34889"},
        {"category":"Commit","entry":"LAUNCHNAME", "value":"'BLOOD.EXE'"},
        {"category":"Commit","entry":"NUMPLAYERS", "value":"GameRoom.NumberOfPlayers"},

        {"category":"Commit","entry":"COMMTYPE", "value":`GameRoom?.Params?.${GameDefsType.DOSBOX_NETTYPE_PARAM_ID}?.[0]==='serial'?1:3`}, // ipx or serial

        // serial entries:
        {"category":"Commit","entry":"COMPORT", "value":"1"},
        {"category":"Commit","entry":"IRQNUMBER", "unquote":true, "value":"'~'"},
        {"category":"Commit","entry":"UARTADDRESS", "unquote":true, "value":"'~'"},
        {"category":"Commit","entry":"PORTSPEED", "unquote":true, skipNull:true, "value":`GameRoom?.Params?.${GameDefsType.DOSBOX_NETTYPE_PARAM_ID}?.[1]`}
    ]},
    {"cmd":"edit-ini","file":"game_ini", "edits":[
        {"category":"options","entry":"Online", "unquote":true, "value":"'commit'"},
        {"category":"options","entry":"GameType", "unquote":true, "value":"GameRoom.Params.gameType[0]"},
        {"category":"options","entry":"EpisodeID", "unquote":true, "value":"GameRoom.Params.level[0]"},
        {"category":"options","entry":"LevelID", "unquote":true, "value":"GameRoom.Params.level[1]"},
        {"category":"options","entry":"GameDifficulty", "unquote":true, "value":"GameRoom.Params.difficulty[0]"},
        {"category":"options","entry":"MonsterSettings", "unquote":true, "value":"GameRoom.Params.monsterSettings[0]"},
        {"category":"options","entry":"WeaponSettings", "unquote":true, "value":"GameRoom.Params.weaponSettings[0]"},
        {"category":"options","entry":"ItemSettings", "unquote":true, "value":"GameRoom.Params.itemSettings[0]"},
        {"category":"options","entry":"RespawnSettings", "unquote":true, "value":"GameRoom.Params.respawnSettings[0]"},

        {"category":"options","entry":"UserMapName", "unquote":true, "value":"GameRoom.Params.map[1]"},
        {"category":"options","entry":"UserMap", "unquote":true, "value":"GameRoom.Params?.map?.[1]?1:0"}
    ]}];

    const sharewareEpisodeOneLevels = [
        {"label": "E1M1: Cradle to Grave","value":          ["0","0"]},
        {"label": "E1M2: Wrong Side of the Tracks","value": ["0","1"]},
        {"label": "E1M3: Phantom Express","value":          ["0","2"]},
        {"label": "E1M4: Dark Carnival","value":            ["0","3"]},
        {"label": "E1M5: Hallowed Grounds","value":         ["0","4"]},
        {"label": "E1M6: The Great Temple","value":         ["0","5"]},
        {"label": "E1M7: Altar of Stone","value":           ["0","6"]},
        {"label": "E1M8: House of Horrors","value":         ["0","7"]}
    ];
    const fullAllLevels = [...sharewareEpisodeOneLevels,
        {"label": "E2M1: Shipwrecked","value":              ["1","0"]},
        {"label": "E2M2: The Lumber Mill","value":          ["1","1"]},
        {"label": "E2M3: Rest for the Wicked","value":      ["1","2"]},
        {"label": "E2M4: The Overlooked Hotel","value":     ["1","3"]},
        {"label": "E2M5: The Haunting","value":             ["1","4"]},
        {"label": "E2M6: The Cold Rush","value":            ["1","5"]},
        {"label": "E2M7: Bowels of the Earth","value":      ["1","6"]},
        {"label": "E2M8: The Lair of Shial","value":        ["1","7"]},
        {"label": "E2M9: Thin Ice (SECRET LEVEL)","value":  ["1","8"]},

        {"label": "E3M1: Ghost Town","value":                       ["2","0"]},
        {"label": "E3M2: The Siege","value":                        ["2","1"]},
        {"label": "E3M3: Raw Sewage","value":                       ["2","2"]},
        {"label": "E3M4: The Sick Ward","value":                    ["2","3"]},
        {"label": "E3M5: Spare Parts","value":                      ["2","4"]},
        {"label": "E3M6: Monster Bait","value":                     ["2","5"]},
        {"label": "E3M7: The Pit of Cerberus (BOSS LEVEL)","value": ["2","6"]},
        {"label": "E3M8: Catacombs (SECRET LEVEL)","value":         ["2","7"]},

        {"label": "E4M1: Butchery Loves Company","value":               ["3","0"]},
        {"label": "E4M2: Breeding Grounds","value":                     ["3","1"]},
        {"label": "E4M3: Charnel House","value":                        ["3","2"]},
        {"label": "E4M4: Crystal Lake","value":                         ["3","3"]},
        {"label": "E4M5: Fire and Brimstone","value":                   ["3","4"]},
        {"label": "E4M6: The Ganglion Depths","value":                  ["3","5"]},
        {"label": "E4M7: In The Flesh","value":                         ["3","6"]},
        {"label": "E4M8: Hall of the Epiphany (BOSS LEVEL)","value":    ["3","7"]},
        {"label": "E4M9: Mall of the Dead (SECRET LEVEL)","value":      ["3","8"]},
		
		{"label": "BB1: The Stronghold (BLOODBATH)","value":			["4","0"]},
		{"label": "BB2: Winter Wonderland (BLOODBATH)","value":			["4","1"]},
		{"label": "BB3: Bodies (BLOODBATH)","value": 					["4","2"]},
		{"label": "BB4: The Tower (BLOODBATH)","value":					["4","3"]},
		{"label": "BB5: Click! (BLOODBATH)","value": 					["4","4"]},
		{"label": "BB6: Twin Fortress (BLOODBATH)","value":				["4","5"]},
		{"label": "BB7: Midgard (BLOODBATH)","value": 					["4","6"]},
		{"label": "BB8: Fun With Heads (BLOODBATH)","value":			["4","7"]},
		{"label": "DM1: Monolith Building 11 (PLASMA PAK)","value":		["4","8"]},
		{"label": "DM2: Power! (PLASMA PAK)","value": 					["4","9"]},
		{"label": "DM3: Area 15 (PLASMA PAK)","value": 					["4","10"]}
    ];

    const commonParamsForAll = {
        "quick": {
            "modeSupport": ["multiplayer", "singleplayer"],
            "type": "static",
            "value": ["'-quick'"],
            "for":"private"
        },
        "handicap": {
            "modeSupport": ["singleplayer", "settings"],
            "type": "numrange",
            "min":0,
            "max":4,
            "delta":1,
            "label": "Handicap (NOT difficulty level)",
            "value": ["'-skill'", "value"],
            "optional":true
        },
        "playback": {
            "modeSupport": ["singleplayer"],
            "type": "file",
            "value": ["'-playback'", "value"],
            "label": "Playback DMO",
            symlink:'forced',
            props: {filters: [{name: 'DMO Files', extensions: ['dmo']},{name: 'All Files', extensions: ['*']}]},
            "optional":true
        },
        "recordDmo": {
            "modeSupport": ["multiplayer", "singleplayer"],
            "type": "text",
            "label": "Record DMO",
            "value": ["'-record'", "value"],
            optional:true,
            props: {
                placeholder: "Demo File Name"
            },
            "for": "private"
        },
        "map": {
            "modeSupport": ["multiplayer", "singleplayer", "settings"],
            "type": "file",
            "res":"maps",
            "value": ["'-map'", "value"],
            "label": "Map",
            symlink:'overlay',
            props: {filters: [{name: 'Map Files', extensions: ['map']}]},
            "optional":true
        },
        "rff": {
            "modeSupport": ["multiplayer", "singleplayer", "settings"],
            "type": "file",
            "res":"mods",
            "value": ["'-rff'", "value"],
            "label": "RFF",
            symlink:'forced',
            props: {filters: [{name: 'RFF Files', extensions: ['rff','pk3','zip']}]},
            "optional":true
        },
        "snd": {
            "modeSupport": ["multiplayer", "singleplayer", "settings"],
            "type": "file",
            "res":"mods",
            "value": ["'-snd'", "value"],
            "label": "Sound",
            symlink:'forced',
            props: {filters: [{name: 'Snd Files', extensions: ['rff']}]},
            "optional":true
        },
        "modini": {
            "modeSupport": ["multiplayer", "singleplayer", "settings"],
            "type": "file",
            "res":"mods",
            "value": ["'-ini'", "value"],
            "label": "Ini",
            symlink:'forced',
            props: {filters: [{name: 'Ini Files', extensions: ['ini']}]},
            "optional":true
        }
    }

    const dosExecParamsCommon = {
        ...commonParamsForAll,
        "netGameIni": {
            "modeSupport": ["multiplayer"],
            "type": "static",
            "value": ["'-getopt'", "'game.ini'"],
            "order":999,
            "for":"host-only-private"
        },
        "netGameIniClient": {
            "modeSupport": ["multiplayer"],
            "type": "static",
            "value": ["'-auto'", "'game.ini'"],
            "order":999,
            "for":"client-only-private"
        },
        "netmode": {
            "modeSupport": ["multiplayer"],
            "type": "choice",
            "label": "NetMode",
            "optional":true,
            "value":"value",
            "description": "Peer2Peer is usually faster with 2-4 players",
            "choices": [
                {"label": "Peer2Peer","value": "-broadcast"},
                {"label": "Master/Slave","value": "-masterslave"}
            ]
        },
        "pname": {
            "modeSupport": ["multiplayer", "singleplayer", "settings"],
            "type": "static",
            "value": ["'-pname'", "MyName"],
            "for":"private"
        },
        "gameType": {
            "modeSupport": ["multiplayer"],
            "syncOnly":true,
            "type": "choice",
            "label": "Game Type",
            "value":"value",
            "optional":true,
            "choices": [{
                "label": "Cooperative",
                "value": "1"
            },{
                "label": "Bloodbath",
                "value": "2"
            }, {
                "label": "Teams",
                "value": "3"
            }]
        },
        "difficulty": {
            "modeSupport": ["multiplayer"],
            "syncOnly":true,
            "type": "choice",
            "label": "Difficulty",
            "value":"value",
            "optional":true,
            "choices": [{
                "label": "Still Kicking",
                "value": "0"
            },{
                "label": "Pink on the Inside",
                "value": "1"
            }, {
                "label": "Lightly Broiled",
                "value": "2"
            }, {
                "label": "Well Done",
                "value": "3"
            }, {
                "label": "Extra Crispy",
                "value": "4"
            }]
        },
        "monsterSettings": {
            "modeSupport": ["multiplayer"],
            "syncOnly":true,
            "type": "choice",
            "label": "Monster Settings",
            "value":"value",
            "optional":true,
            "choices": [{
                "label": "None",
                "value": "0"
            },{
                "label": "Bring 'em on",
                "value": "1"
            }, {
                "label": "Respawn",
                "value": "2"
            }]
        },
        "weaponSettings": {
            "modeSupport": ["multiplayer"],
            "syncOnly":true,
            "type": "choice",
            "label": "Weapon Settings",
            "value":"value",
            "optional":true,
            "choices": [{
                "label": "Do not Respawn",
                "value": "0"
            },{
                "label": "Are Permanent",
                "value": "1"
            }, {
                "label": "Respawn",
                "value": "2"
            }, {
                "label": "Respawn with Markers",
                "value": "3"
            }]
        },
        "itemSettings": {
            "modeSupport": ["multiplayer"],
            "syncOnly":true,
            "type": "choice",
            "label": "Item Settings",
            "value":"value",
            "optional":true,
            "choices": [{
                "label": "Do not Respawn",
                "value": "0"
            },{
                "label": "Respawn",
                "value": "1"
            }, {
                "label": "Respawn with Markers",
                "value": "2"
            }]
        },
        "respawnSettings": {
            "modeSupport": ["multiplayer"],
            "syncOnly":true,
            "type": "choice",
            "label": "Respawn Settings",
            "value":"value",
            "optional":true,
            "choices": [{
                "label": "At Random Locations",
                "value": "0"
            },{
                "label": "Close to Weapons",
                "value": "1"
            }, {
                "label": "Away from Enemies",
                "value": "2"
            }]
        }
    };

    
    const dosExecParamsShareware = {...dosExecParamsCommon,
        "level": {
            "modeSupport": ["multiplayer"],
            "type": "choice",
            "label": "Level",
            "optional":true,
            "value":["value[0]", "value[1]"],
            "choices": sharewareEpisodeOneLevels,
            "syncOnly": true
        }
    };
    
    const dosExecParamsFull = {...dosExecParamsCommon, ...{
            "level": {
                "modeSupport": ["multiplayer"],
                "type": "choice",
                "label": "Episode",
                "optional":true,
                "value":["value[0]", "value[1]"],
                "choices": fullAllLevels,
                "syncOnly": true
            }
        }
    }

    const commands = ['CONFIG -set "cycles=max"','CONFIG -set "core=auto"']
    const vars = {
        "game": {
            "runToolOpts": {"type":"dosbox", "data":{commands, "args":["-machine", "vesa_nolfb"]}},
            "runToolOptsMultiplayer": {"type":"dosbox", "data":{commands, "net":GameDefsType.Dosbox_NetTypesAllSpecific,"args":["-machine", "vesa_nolfb"]}},
            "beforeRun": prepareGameIniAndCommitDat,
            "baseGameEditions": ["shareware","full"]
        }
    };

    /* TODO Add specific support for: One unit whole blood, and cryptic passage */
    const nBloodParams = {
        ...commonParamsForAll,
        "grp": {
            "modeSupport": ["multiplayer", "singleplayer"],
            "type": "file",
            "value": ["'-g'", "value"],
            "label": "GRP",
            "res":"mods",
            symlink:'forced',
            props: {filters: [{name: 'GRP Files', extensions: ['zip','grp','pk3']}]},
            "optional":true
        },
        "setup": {
            "modeSupport": ["settings"],
            "type": "static",
            "value": ["'-setup'"],
            "for":"private"
        },
        "nosetup": {
            "modeSupport": ["multiplayer", "singleplayer"],
            "type": "static",
            "value": ["'-nosetup'", "'-noautoload'"],
            "for":"private"
        },
        "pname": {
            "modeSupport": ["multiplayer", "singleplayer"],
            "type": "static",
            "value": ["'-pname'", "MyName"],
            "for":"private"
        },
        "host_details": {
            "modeSupport": ["multiplayer"],
            "type": "static",
            "order":9999,
            "value": ["'-server'", "GameRoom.NumberOfPlayers", "'-port'", "GameRoom.MyPort"],
            "for":"host-only-private"
        },
        "client_details": {
            "modeSupport": ["multiplayer"],
            "type": "static",
            "order":9999,
            "value": ["'-client'", "GameRoom.DestIp", "'-port'", "GameRoom.DestPort"],
            "for":"client-only-private"
        }
    }

    const patches = require(path.join(__dirname, 'buildmfx.i.js'))(utils, "main");

    return {
        "name": "Blood",
        "get": [{
            type:"gog",
            prodId:"1374469660",
            nameId:"blood_fresh_supply",
            name:"Blood: Fresh Supply"
        }],
        "executables": {
            "shareware": {
                "name":"Shareware (DOS)",
                "files": {
                    "setup": {"path":"SETUP.EXE"},
                    "main": {"path":"BLOOD.EXE"},
                    "commit": {"path":"COMMIT.EXE"},
                    "commit_dat": {"path":"COMMIT.DAT", "optional":true},
                    "game_ini": {"path":"GAME.INI", "optional":true}
                },
                tips: {
                    patches
                },
                "runnables": {
                    "multiplayer": {
                        "file":"commit",
                        "runTool":vars.game.runToolOptsMultiplayer,
                        "beforeRun":vars.game.beforeRun
                    },
                    "settings": {"file":"setup", "runTool":vars.game.runToolOpts},
                    "singleplayer": {"file":"main", "runTool":vars.game.runToolOpts}
                },
                "parameters": dosExecParamsShareware
            },
            "full": {
                "name":"Full (DOS)",
                "files": {
                    "setup": {"path":"SETUP.EXE"},
                    "main": {"path":"BLOOD.EXE"},
                    "commit": {"path":"COMMIT.EXE"},
                    "commit_dat": {"path":"COMMIT.DAT", "optional":true},
                    "game_ini": {"path":"GAME.INI", "optional":true}
                },
                tips: {
                    patches
                },
                "runnables": {
                    "multiplayer": {
                        "file":"commit",
                        "runTool":vars.game.runToolOptsMultiplayer,
                        "beforeRun":vars.game.beforeRun
                    },
                    "settings": {"file":"setup", "runTool":vars.game.runToolOpts},
                    "singleplayer": {"file":"main", "runTool":vars.game.runToolOpts}
                },
                "parameters": dosExecParamsFull
            },
            "nblood": {
                "name":"NBlood",
                "files": {
                    "main": {"path":"nblood.exe"}
                },
                "runnables": {
                    "multiplayer": {"file":"main", "waitForListenPortOnProto": "udp"},
                    "singleplayer": {"file":"main"},
                    "settings": {"file":"main"}
                },
                "parameters": nBloodParams
            },
            "notblood": {
                "name":"NotBlood",
                "namedPipeChatSync": {windows:"\\\\.\\pipe\\NotBlood", unix:"/tmp/notbloodpipe"},
                "files": {
                    "main": {"path":"notblood.exe"}
                },
                "runnables": {
                    "multiplayer": {"file":"main", "waitForListenPortOnProto": "udp"},
                    "singleplayer": {"file":"main"},
                    "settings": {"file":"main"}
                },
                "parameters": nBloodParams
            }
        }
    };
}