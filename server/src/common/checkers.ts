import NNCmd_Checkers_Def from '../common/interface-verifiers/commands-ti';
import Room_Checkers_Def from '../common/interface-verifiers/room-ti';
import NNCmd_Meta_Checkers_Def from '../common/interface-verifiers/commands-meta-ti';
import {createCheckers} from "ts-interface-checker";

export const Checkers = createCheckers(Room_Checkers_Def, NNCmd_Checkers_Def, NNCmd_Meta_Checkers_Def)