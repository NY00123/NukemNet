import {Component} from 'solid-js'
import { colorFromStr } from '../../lib/colorFromStr';
import { IRCEvent_ChangeNick } from '../ircEvents';
import { IrcEventCmp_Base } from './base';

export const IrcEventCmp_ChangeNick:Component<IRCEvent_ChangeNick> = (props)=>{
    const oldStyle = {"background-color":colorFromStr(props.old)}
    const newStyle = {"background-color":colorFromStr(props.new)}

    if(props.isMe) {
        return <IrcEventCmp_Base {...props}>You have changed nick from <a style={oldStyle}>{props.old}</a> to <a style={newStyle}>{props.new}</a></IrcEventCmp_Base>
    }
    return <IrcEventCmp_Base {...props}><a style={oldStyle}>{props.old}</a> is now known as <a style={newStyle}>{props.new}</a></IrcEventCmp_Base>
}