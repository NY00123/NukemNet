module.exports = function() {

    const CommonGameMods = [{
            "label": "Cooperative mode",
            "value": "-COOP"
        }, {
            "label": "Battle",
            "value": "-BATTLE"
        }, {
            "label": "Treasure",
            "value": "-TREASURE"
        }, {
            "label": "Race",
            "value": "-RACE"
        }, {
            "label": "Capture the Flag",
            "value": "-CAPTURE"
        }
    ];

    const MultiplayerMods= [...CommonGameMods, ...[{
        "label": "Roast Tag",
        "value": "-RT"
    }, {
        "label": "Last Rabbit Standing",
        "value": "-LRS"
    }, {
        "label": "Extended Last Rabbit Standing",
        "value": "-XLRS"
    }, {
        "label": "Pestilence",
        "value": "-PEST"
    }, {
        "label": "Team Battle",
        "value": "-TB"
    }, {
        "label": "Death CTF",
        "value": "-DCTF"
    }, {
        "label": "Jailbreak",
        "value": "-JB"
    }, {
        "label": "Flag Run",
        "value": "-FR"
    }, {
        "label": "Team Last Rabbit Standing",
        "value": "-TLRS"
    }, {
        "label": "Domination",
        "value": "-DOM"
    }]]
    
    const runnables = {
        "multiplayer": {"file":"main"},
        "singleplayer": {"file":"main"}
    };

    const params = {
        "adv": {
            "modeSupport": ["multiplayer"],
            "type": "boolean",
            "label": "Advertise",
            "description": "Lists the server on the Internet",
            "value": "'-LIST'",
            "for": "host-only-private"
        },
        "playas": {
            "modeSupport": ["multiplayer", "singleplayer"],
            "type": "choice",
            "label": "Play as",
            "value":"value==='jazz'?null:value",
            "choices": [{
                "label": "Jazz",
                "value": "jazz"
            }, {
                "label": "Lori",
                "value": "-LORI"
            }, {
                "label": "Spaz",
                "value": "-SPAZ"
            }],
            "for":"private"
        },
        "localp": {
            "modeSupport": ["multiplayer", "singleplayer"],
            "type": "numrange",
            "min":1,
            "max":4,
            "delta":1,
            "label": "Local Players",
            "value": ["'-PLAYER'", "value"],
            "optional":true,
            "for":"private"
        },
        "client": {
            "modeSupport": ["multiplayer"],
            "type": "static",
            "value": ["'-CONNECT'", "GameRoom.DestIp+':'+GameRoom.DestPort"],
            "for":"client-only-private"
        },
        "server": {
            "modeSupport": ["multiplayer"],
            "type": "static",
            "value": ["'-SERVER'", "'-port='+GameRoom.MyPort"],
            "for":"host-only-private"
        },
        "smode": {
            "modeSupport": ["singleplayer"],
            "type": "choice",
            "label": "Mode",
            "value":"value",
            "choices": CommonGameMods,
            "for":"host-only-shared"
        },
        "mmode": {
            "modeSupport": ["multiplayer"],
            "type": "choice",
            "label": "Mode",
            "value":"value",
            "choices": MultiplayerMods,
            "for":"host-only-shared"
        },
        "skill": {
            "modeSupport": ["multiplayer", "singleplayer"],
            "type": "choice",
            "label": "Skill",
            "value":"value",
            "choices": [{
                "label": "Easy",
                "value": "-EASY"
            }, {
                "label": "Medium",
                "value": "-MEDIUM"
            }, {
                "label": "Hard",
                "value": "-HARD"
            }],
            optional:true,
            "for":"host-only-shared"
        },
        "count": {
            "modeSupport": ["multiplayer", "singleplayer"],
            "type": "numrange",
            "min":0,
            "max":100,
            "delta":1,
            "description": "Sets the amount of roasts/gems/flags required to win the level",
            "label": "Score Limit",
            "value": ["'-COUNTS'", "value"],
            "optional":true,
            "for":"host-only-shared"
        },
        "map": {
            "modeSupport": ["multiplayer", "singleplayer"],
            "type": "dirfiles",
            "value": ["value[0]"],
            "label": "Map",
            "path": "**/*.j2l",
            "stripExt": true,
            "for":"host-only-shared"
        },
        "levelist": {
            "modeSupport": ["multiplayer"],
            "type": "dirfiles",
            optional:true,
            "value": ["'-LEVELLISTFILE='+value[0]"],
            "label": "LevelListFile",
            "path": "**/*.ini",
            "stripExt": true,
            "for":"host-only-privavte"
        },
        "admin": {
            "modeSupport": ["multiplayer"],
            "type": "dirfiles",
            "value": ["'-ADMIN='+value[0]"],
            "label": "AdminFile",
            optional:true,
            "path": "**/*.ini",
            "stripExt": true,
            "for":"host-only-privavte"
        },
        "j2i": {
            "modeSupport": ["multiplayer", "singleplayer"],
            "type": "dirfiles",
            "value": ["value[0]"],
            "label": "J2I",
            "optional":true,
            "path": "**/*.j2i",
            "stripExt": true,
            "for":"host-only-shared"
        }
    };

    return {
        "name": "Jazz Jackrabbit 2",
        "get": [{
            type:"gog",
            prodId:"1917711239",
            nameId:"jazz_jackrabbit_2_collection",
            name:"Jazz Jackrabbit 2 Collection"
        }],
        "executables": {
            "tsfplus": {
                "name":"The Secret Files (Plus)",
                midGameJoin: true,
                networking: {
                    tcpPort: {port:10052},
                    udpPort: {port:10052}
                },
                tips: {
                    html: `<div>
                    Get Jazz2+ patch on <b><u>https://www.jazz2online.com/wiki/JJ2+</u></b> <br/>
                    if using GOG Galaxy, activate "JJ2+ Mod" by clicking:<br/>
                    * Manage Installation<br/>
                    * Configure...<br/>
                    * Choose under "Beta channels"<br/>
                    </div>`
                },
                "files": {
                    "main": {"path":"JAZZ2.EXE"}
                },
                "runnables": runnables,
                "parameters": params
            }
        }
    };
}