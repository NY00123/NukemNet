import {Component} from 'solid-js'
import { IRCEvent_Registered } from '../ircEvents';
import { IrcEventCmp_Base } from './base';

export const IrcEventCmp_Registered:Component<IRCEvent_Registered> = (props)=>{
    return <IrcEventCmp_Base {...props}>You have connected to the IRC server.</IrcEventCmp_Base>
}