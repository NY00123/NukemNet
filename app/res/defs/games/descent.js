
module.exports = function ({}) {

    const dosboxPreCommands = ['CONFIG -set "cycles=auto 5000 limit 50000"']; /* Descent runs well on these cycles */
    const runToolOpts = {"type":"dosbox", "data":{"commands":dosboxPreCommands, "args":["-machine", "vesa_nolfb"]}};
    const runToolOptsMultiplayer = {"type":"dosbox", "data":{"commands":dosboxPreCommands,"net":"ipx","args":["-machine", "vesa_nolfb"]}};

    const filesSw = {
        "setup": {"path":"SETUP.EXE"},
        "main": {"path":"DCNTSHR.EXE"}
    };

    const filesFull = {
        "setup": {"path":"SETUP.EXE"},
        "main": {"path":"DESCENTR.EXE"}
    };

    const runnables = {
        "multiplayer": {
            "file":"main",
            "runTool":runToolOptsMultiplayer
        },
        "settings": {"file":"setup", "runTool":runToolOpts},
        "singleplayer": {"file":"main", "runTool":runToolOpts}
    };

    const dxxRebirthParams = {
        "host_conn": {
            "modeSupport": ["multiplayer"],
            "type": "static",
            "value": ["'-udp_myport'", "GameRoom.MyPort"],
            "for":"host-only-private"
        },
        "client_conn": {
            "modeSupport": ["multiplayer"],
            "type": "static",
            "value": ["'-udp_myport'", "GameRoom.MyPort", "'-udp_hostaddr'", "GameRoom.DestIp", "'-udp_hostport'", "GameRoom.DestPort"],
            "for":"client-only-private"
        }
    }

    return {
        "name": "Descent",
        "get": [{
            type:"gog",
            prodId:"1207663083",
            nameId:"descent",
            name:"Descent 1"
        }],
        "executables": {
            "shareware1_4": {
                "name":"Shareware 1.4 (DOS)",
                "files": filesSw,
                "runnables": runnables,
                "parameters": {}
            },
            "full": {
                "name":"Full 1.4a (DOS)",
                "files": filesFull,
                "runnables": runnables,
                "parameters": {}
            },
            "dxxrebirth": {
                midGameJoin: true,
                "name":"DXX-Rebirth",
                "files": {
                    "main": {"path":"d1x-rebirth.exe"}
                },
                "runnables": {
                    "multiplayer": {"file":"main"},
                    "settings": {"file":"main"},
                    "singleplayer": {"file":"main"}
                },
                "parameters": dxxRebirthParams
            },
            "dxxretro": {
                midGameJoin: true,
                "name":"DXX-Retro",
                "files": {
                    "main": {"glob":["d1x-rebirth-retro*","d1x-retro-*"]}
                },
                "runnables": {
                    "multiplayer": {"file":"main"},
                    "settings": {"file":"main"},
                    "singleplayer": {"file":"main"}
                },
                "parameters": dxxRebirthParams
            },
            "dxxretroar": {
                midGameJoin: true,
                "name":"DXX-Retro-AR",
                "files": {
                    "main": {"glob":["d1x-rebirth*","d1x-retro-*"]}
                },
                "runnables": {
                    "multiplayer": {"file":"main"},
                    "settings": {"file":"main"},
                    "singleplayer": {"file":"main"}
                },
                "parameters": dxxRebirthParams
            }
        }
    };
}