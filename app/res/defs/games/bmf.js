module.exports = function() {

    const commands = ['CONFIG -set "cycles=max"','CONFIG -set "core=auto"']

    const runToolOpts = {"type":"dosbox", "data":{commands}};
    const runToolOptsMultiplayer = {"type":"dosbox", "data":{commands, "net":"ipx"}};

    const runnables = {
        "multiplayer": {
            "file":"main",
            "runTool":runToolOptsMultiplayer
        },
        "settings": {"file":"setup", "runTool": runToolOpts},
        "singleplayer": {"file":"main", "runTool": runToolOpts}
    };

    return {
        "name": "BMF",
        "executables": {
            "full": {
                "maxPlayers":4,
                "name":"Full (DOS)",
                "files": {
                    "main": {"path":"BMF.EXE"},
                    "setup": {"path":"SETUP.EXE"},
                },
                "runnables": runnables,
                "parameters": {},
            }
        }
    };
}