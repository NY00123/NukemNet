module.exports = function({GameDefsType}) {

    const path = require('path');
    const fs = require('fs/promises')

    const prepareChasmCfg = [{"cmd":"edit-binary","file":"chasm_cfg", "edits":[
        {"offset":0x34, length:1, numType:'setUint8', "value":`GameRoom?.Params?.${GameDefsType.DOSBOX_NETTYPE_PARAM_ID}?.[0]=='ipx'?0:1`},
        {"offset":0x35, length:1, numType:'setUint8', "value":`0x00`}, // Always COM1
        {"offset":0x36, length:1, numType:'setUint8', "value":`(()=>{
            const speed = GameRoom?.Params?.${GameDefsType.DOSBOX_NETTYPE_PARAM_ID}?.[1];
            if(speed == 19200) return 0;
            else if(speed == 28800) return 1;
            else if(speed == 38400) return 2;
            else if(speed == 57600) return 3;
            else if(speed == 115200) return 4;
            return 0;
        })();`},
    ], createMissing: async ()=>{
        return fs.readFile(path.join(__dirname, 'CHASM.CFG'));
    }}];

    const dosboxPreCommands = ['CONFIG -set "core=auto"', 'CONFIG -set "cycles=max"'];
    const runToolOpts = {"type":"dosbox", "data":{"commands":dosboxPreCommands}};
    const runToolOptsMultiplayer = {"type":"dosbox", "data":{"commands":dosboxPreCommands, "net":[
        "ipx",
        "serial:19200",
        "serial:28800",
        "serial:38400",
        "serial:57600",
        "serial:115200"
    ]}};

    const runnables = {
        "multiplayer": {
            "file":"main",
            "runTool":runToolOptsMultiplayer,
            beforeRun: prepareChasmCfg
        },
        "settings": {"file":"main", "runTool": runToolOpts},
        "singleplayer": {"file":"main", "runTool": runToolOpts}
    };

    const fixedByteIndex = 0x1401C;
    const tips = {
        patches: {
            "performancefix": {
                title: "Startup Fix",
                html:`
                    <p>Fixes a startup crash when running in DOSBox with cycles=max.<br/>
                    <b>Runtime error 200 at 0002:1530</b><br/>
                    Source: https://www.moddb.com/games/chasm-the-rift/downloads/patch-1041
                `,
                files:['ps10'],
                apply: async (opts)=>{
                    const file = opts.files['ps10'];
                    const buffer = await fs.readFile(file);
                    const byte = buffer.at(fixedByteIndex);
                    if(byte != 0x74) return {
                        title: "Cancelled",
                        body: "Already patched"
                    }
                    buffer.set([0xEB], fixedByteIndex);
                    await fs.writeFile(file, buffer);
                    return {
                        title: "Success",
                        body: "Patch applied"
                    }
                },
                undo: async (opts)=>{
                    const file = opts.files['ps10'];
                    const buffer = await fs.readFile(file);
                    const byte = buffer.at(fixedByteIndex);
                    if(byte != 0xEB) return {
                        title: "Cancelled",
                        body: "Already unpatched"
                    }
                    buffer.set([0x74], fixedByteIndex);
                    await fs.writeFile(file, buffer);
                    return {
                        title: "Success",
                        body: "Patch removed"
                    }
                }
            }
        }
    };

    return {
        "name": "Chasm: The Rift",
        "get": [{
            type:"gog",
            prodId:"1302753134",
            nameId:"chasmtherift",
            name:"Chasm: The Rift"
        }],
        "executables": {
            "full": {
                midGameJoin: true,
                tips,
                "mountImg": true,
                "name":"Full (DOS)",
                "files": {
                    "main": {"path":"CHASM.EXE"},
                    "ps10": {"path":"PS10.EXE"},
                    chasm_cfg: {path: "CHASM.CFG", optional:true}
                },
                "runnables": runnables
            },
            "full_addon": {
                midGameJoin: true,
                tips,
                "mountImg": true,
                "name":"Addon Pack (DOS)",
                "files": {
                    "main": {"path":"CHASM.EXE"},
                    "ps10": {"path":"PS10.EXE"},
                    chasm_cfg: {path: "CHASM.CFG"}
                },
                "runnables": runnables,
                "parameters": {
                    "addonPack": {
                        "modeSupport": ["multiplayer", "singleplayer", "settings"],
                        "type": "static",
                        "value": ["'-addon:addon1'"],
                        "for":"private"
                    }
                }
            }
        }
    };
}