
// https://brian-horning.com/rtcw-server-setup-guide
module.exports = function ({}) {

    const path = require('path');
    const fsPromises = require('fs/promises');
    const glob = require('glob')

    const NNCfg_HostFile = "NukemNetHost.cfg";
    const NNCfg_ClientFile = "NukemNetClient.cfg";

    const writeCommandsCfg = [{"cmd":"function", async run(ctx, execDef, result, imHost, players, hostIpDetails, useDedicated, othersIpDetails, channelName, hostTunnelChosenPort) {

        const fname = imHost?NNCfg_HostFile:NNCfg_ClientFile;
        const params = ctx.GameRoom.Params;
        let lineCommands = [];
        for(const p in params) {
            const argsArr = params[p];
            for(const line of argsArr) {
                const trm = line.trim();
                if(trm.startsWith("+") && !trm.startsWith("+exec") && !p.startsWith("_") && ctx?.GameRoom?.ParamDefs?.[p]?.syncOnly) {
                    lineCommands.push({id:p, line:trm.substring(1)});
                }
            }
        }
        lineCommands.sort((a,b) => {
            const aOrder = ctx.GameRoom.ParamDefs[a.id]?.order;
            const bOrder = ctx.GameRoom.ParamDefs[b.id]?.order;
            return (aOrder||0)>(bOrder||0);
        });
        const fileContent = lineCommands.map(r=>r.line).join('\r\n');
        await fsPromises.writeFile(path.resolve(ctx.GameDir, 'Main', fname), fileContent);
    }}];
    
    const levelsCalc = [

        ['* Multiplayer *', 'mp_assault'],
        ['Assault', 'mp_assault'],
        ['Base', 'mp_base'],
        ['Beach', 'mp_beach'],
        ['Castle', 'mp_castle'],
        ['Chateau', 'mp_chateau'],
        ['Dam', 'mp_dam'],
        ['Depot', 'mp_depot'],
        ['Destruction', 'mp_destruction'],
        ['Ice', 'mp_ice'],
        ['Keep', 'mp_keep'],
        ['Rocket', 'mp_rocket'],
        ['Sub', 'mp_sub'],
        ['Tram', 'mp_tram'],
        ['TrenchtToast', 'mp_trenchtoast'],
        ['Village', 'mp_village'],

        // ['* Campaign *', 'escape1'],
        // ['Escape!', 'escape1'],
        // ['Castle Keep', 'escape2'],
        // ['Tram Ride', 'tram'],
        // ['Village', 'village1'],
        // ['Catacombs', 'crypt1'],
        // ['Crypt', 'crypt2'],
        // ['The Defiled Church', 'church'],
        // ['The Defiled Church Boss', 'boss1'],
        // ['Forest Compound', 'forest'],
        // ['Rocket Base', 'rocket'],
        // ['Radar Installation', 'baseout'],
        // ['Air Base Assault', 'assault'],
        // ['Kugelstadt', 'sfm'], //EXIST? smf
        // ['The Bombed Factory', 'factory'],
        // ['The Trainyards', 'trainyard'], // EXIST? trainyards
        // ['Secret Weapons Facility', 'swf'],
        // ['Ice Station Norway', 'norway'],
        // ['X-Labs', 'xlabs'],
        // ['Super Soldier', 'boss2'],
        // ['Bramburg Dam', 'dam'],
        // ['Paderborn Village', 'village2'],
        // ['Chateau Schuftaffel', 'chateau'],
        // ['Unhallowed Ground', 'dark'],
        // ['The Dig', 'dig'],
        // ['Return to Castle Wolfenstein', 'castle'],
        // ['Heinrich', 'end']
    ];
    
    const levels = levelsCalc.map(([name, id]) => ({
        "label": name,
        "value": [id]
    }));

    function getParams(sourcePort = false) {

        const dedicatedCmdValue = [
            `'+set'`, `'dedicated'`, `'1'`
        ].concat(sourcePort?[
            `'+set'`, `'sv_master1'`, `'127.0.0.1'`, // preventing ioRTCW from advertising the master server
            `'+set'`, `'sv_master2'`, `'127.0.0.1'`,
            `'+set'`, `'sv_master3'`, `'127.0.0.1'`,
            `'+set'`, `'sv_master4'`, `'127.0.0.1'`,
            `'+set'`, `'sv_master5'`, `'127.0.0.1'`
        ]:[]);

        const params = {
            ...(sourcePort?{
                "_dedicated": { 
                    order:-102,
                    "modeSupport": ["multiplayer"],
                    "type": "static",
                    "value": dedicatedCmdValue, 
                    "for":"host-only-private"
                }
            }:{
                "dedicated": { 
                    order:-102,
                    "modeSupport": ["multiplayer"],
                    "type": "boolean",
                    label:"Dedicated",
                    "value": dedicatedCmdValue,
                    "for":"host-only-private"
                },
            }),
    
            "_netp": { 
                order:-101,
                "modeSupport": ["multiplayer"],
                "type": "static",
                "value": [`'+set'`, `'net_port'`, `GameRoom.MyPort`],
                "for":"host-only-private",
            },
    
            hostbase: {
                order:-101,
                "modeSupport": ["multiplayer"],
                "type": "static",
                "value": [
                    `'+PB_SV_DISABLE'`,
                    `'+set sv_hostname NukemNet'`, `'+set sv_maxclients 16'`, `'+set g_motd "Welcome to NukemNet Server"'`, `'+set rconpassword "nukemnet"'`,
                    `'+set com_hunkMegs 1024'`, `'+set com_soundMegs 32'`, `'+set com_zoneMegs 32'`,
                    `'+set sv_allowDownload 1'`
                ],
                "for":"host-only-private",
                syncOnly:true
            },
    
            "_mod": {
                order:-100,
                "modeSupport": ["multiplayer"],
                "type": "dirfiles",
                "value": ["'+set fs_game '+value[0]"],
                "label": "Mod",
                "path": "!(main)",
                globOpts: {
                    stat: true,
                    ignore: {
                        ignored: (p) => {
                            if(p.isDirectory()) {
                                const files = glob.globSync('*.pk3', {cwd: p.fullpath(), nocase:true});
                                if(files.length>0) {
                                    return false;
                                }
                            }
                            return true;
                        }
                    }
                },
                "stripExt": true,
                "stripPath":true,
                "optional":true
            },
    
            "name": {
                order:-100,
                "modeSupport": ["multiplayer"],
                "type": "static",
                "value": ["'+name '+MyName"],
                "for":"private",
                syncOnly:true,
            },
    
            "_cmdshost": {
                order:-99,
                "modeSupport": ["multiplayer"],
                "type": "static",
                "value": ["'+exec'", `'${NNCfg_HostFile}'`],
                "for":"host-only-private",
            },
            "_cmdsclnt": {
                order:-99,
                "modeSupport": ["multiplayer"],
                "type": "static",
                "value": ["'+exec'", `'${NNCfg_ClientFile}'`],
                "for":"client-only-private",
            },
    
            "_clntcon": {
                order:-99,
                "modeSupport": ["multiplayer"],
                "type": "static",
                "value": ["'+connect'", `GameRoom.DestIp + ':' + GameRoom.DestPort`],
                "for":"client-only-private"
            },

            "gametype": {
                order:-98,
                "modeSupport": ["multiplayer"],
                "type": "choice",
                "label": "Type",
                "value":["'+set g_gametype '+value"],
                "choices": [{
                    "label": "Multiplayer",
                    "value": "5"
                }, {
                    "label": "Stopwatch",
                    "value": "6"
                }, {
                    "label": "Checkpoint",
                    "value": "7"
                }],
                syncOnly:true,
                "for":"host-only-private"
            },
    
            "_map": {
                "modeSupport": ["multiplayer"],
                "type": "dirfiles",
                "value": ["'+map '+value[0]"],
                "label": "Usermap",
                "path": "Main/!(*pak*|mp_bin).pk3",
                "stripExt": true,
                "stripPath":true,
                "optional":true,
                "for":"host-only-shared",
                syncOnly: true
            },
            "_level": {
                "modeSupport": ["multiplayer"],
                "type": "choice",
                "label": "Level",
                "value":["'+map '+value[0]"],
                "choices": levels,
                "optional":true,
                "for":"host-only-shared",
                syncOnly:true
            },
            "inactivity": {
                order:-50,
                "modeSupport": ["multiplayer"],
                "type": "numrange",
                "min":0,
                "max":10000,
                "delta":1,
                "label": "Inactivity",
                "description": "Inactivity Seconds before kickout",
                "value": "'+set g_inactivity '+value",
                "optional":true,
                for:'host-only-shared',
                syncOnly:true
            },
            "tmlmt": {
                order:-50,
                "modeSupport": ["multiplayer"],
                "type": "numrange",
                "min":0,
                "max":1000,
                "delta":1,
                "label": "Time Limit",
                "value": "'+set timelimit '+value",
                "optional":true,
                for:'host-only-shared',
                syncOnly:true
            },
            "ff": {
                order:-50,
                "modeSupport": ["multiplayer"],
                "type": "boolean",
                "label": "Friendly Fire",
                "value": `'+set g_friendlyFire '+(value?'1':'0')`,
                addFalse: true,
                "for":"host-only-shared",
                syncOnly: true
            },
            "map": {
                order:-1,
                "modeSupport": ["multiplayer"],
                "type": "static",
                "value": [`GameRoom?.Params?._map?.[0] || GameRoom?.Params?._level?.[0] || (GameRoom?.Params?._mod?.[0]? undefined:"+map mp_assault")`],
                "for":"host-only-shared",
                syncOnly: true
            }
        }
        return params;
    }

    return {
        "name": "Return to Castle Wolfenstein",
        "get": [{
            type:"gog",
            prodId:"1443699418",
            nameId:"return_to_castle_wolfenstein",
            name:"Return to Castle Wolfenstein"
        }],
        executables: {
            "full": {
                name:"Full",
                "maxPlayers":16,
                midGameJoin: true,
                "files": {
                    "multi": {"path":"WolfMP.exe", runShell:true},
                    "maindir": {"path":"Main"}
                },
                "runnables": {
                    "multiplayer": {"file":"multi", beforeRun:writeCommandsCfg},
                },
                parameters:getParams()
            },
            "iortcw": {
                name:"ioRTCW",
                "maxPlayers":16,
                midGameJoin: true,
                "files": {
                    "dedicated": {glob:"ioWolfDED*", runDetachedShell:true},
                    "multi": {glob:"ioWolfMP*"},
                    "maindir": {"path":"Main"}
                },
                "runnables": {
                    "multiplayer": {
                        "file":(ctx)=>{return ctx?.GameRoom?.ImHost?'dedicated':'multi'},
                        beforeRun:writeCommandsCfg,
                        hostAlsoRunsAsClient:true
                    },
                },
                parameters:getParams(true)
            }
        }
    }
}
