import { Client } from "matrix-org-irc";
import { Button, FloatingLabel, Form, FormControl, InputGroup, Modal, Tab, Tabs } from "solid-bootstrap"
import { Component, createSignal, For, Index, Show } from "solid-js"
import { createMutable } from "solid-js/store";
import { IrcClientExtra } from "../../server/src/common/ircExtend";
import { GameDefsType } from "./GameDefsTypes";
import { deepClone } from "./lib/deepClone";
import { Capitalized, isWindows } from "./lib/utils";
import { Electron, axios, electronRemote, fs, fsSync, path, ps } from "./NodeLibs";
import { DialogMgr } from "./Popups";
import { pathOfExtensions, pathOfRes, pathOfUserDir, Settings, SettingsDef, SoundTypes_All, SoundTypes_All_Types } from "./Settings";
import { mainStore } from "./store/store";
import storeActions from "./store/storeActions";
import { generateLinkForAffiliate } from "./affiliate";
import { getMyCountryCode } from "./lib/cc";
import gogImg from "/src/assets/gog.png"
import zoomPlatformImg from "/src/assets/zoomplatform.webp"

const LauncherSettingsModal:Component<{show:boolean, dismiss?:()=>void, ircClient:Client}> = (props)=>{

    const modifiedGameExecs: {[gidSlashEid:string]: boolean} = {}
    
    const settings = createMutable((()=>{
      // We do a deep clone so we can edit the object freely, then save object to settings file if "Apply" button was clicked.
      let tmpSettings: Partial<SettingsDef> = {};
      try {
        tmpSettings = deepClone(Settings.read(true));
      } catch(e) {}
      if(tmpSettings == null) tmpSettings = {}
      if(tmpSettings.games == null) tmpSettings.games = {};
      if(tmpSettings.dosbox == null) tmpSettings.dosbox = {};
      if(tmpSettings.wine == null) tmpSettings.wine = {};
      return tmpSettings;
    })()) as SettingsDef

    const gameDefs = Settings.gameDefs;

    const [validated, setValidated] = createSignal(false);
    const [selectedGame, _setSelectedGame] = createSignal(null as null|GameDefsType.Game)
    const [selectedGameId, setSelectedGameId] = createSignal(null as null|string)
    const [disableLogging, setDisableLogging] = createSignal(settings.disableLogging||false);
    const [debugDosbox, setDebugDosbox] = createSignal(settings.debugDosbox||false);
    const modifiedFields = createMutable({
      executablePaths: {},
      executableImgPath: {},
      dosBoxPath:false,
      winePath:false,
      wineconsolePath:false,
    } as {
      winePath:boolean,
      wineconsolePath:boolean,
      dosBoxPath:boolean,
      executablePaths:{[execId:string]:boolean}
      executableImgPath:{[execId:string]:boolean}
    });

    const [gameBuyLinksPrices, setGameBuyLinksPrices] = createSignal([] as {price:string, cur:string}[]);
    async function setSelectedGame(obj:null|GameDefsType.Game) {
      setGameBuyLinksPrices([]);
      _setSelectedGame(obj);
      const originalGameDefName = obj?.name;
      if(!obj) return;

      for(let i=0;i<obj?.get?.length||0;i++) { // The index is important here, because we're using it to idetify which link the price belongs to.
        const getObj = obj.get[i];

        let price:string, cur:string;
        
        if(getObj.type === 'gog') {
          const cc = getMyCountryCode() || 'US';
          const res = await axios.get(`https://api.gog.com/products/${getObj.prodId}/prices?countryCode=${cc}`);
          if(originalGameDefName != selectedGame()?.name) return;
          const finalPrice = res?.data?._embedded?.prices?.[0]?.finalPrice as string;
          if(!finalPrice?.length) continue;
          [price, cur] = finalPrice.split(" ");
          if(price != '0') {
            price = price.slice(0, price.length-2) + "." + price.slice(price.length-2);
          }
        } else if(getObj.type==='zoom-platform') {
          const res = await axios.get(`https://www.zoom-platform.com/public/product/${getObj.nameId}`);
          price = res?.data?.product?.free?'0.00':res?.data?.product?.price;
          cur = 'USD';
        } else {
          throw new Error("Unknown Afilliate Platform")
        }

        const arr = structuredClone(gameBuyLinksPrices());
        arr[i] = {cur, price};
        setGameBuyLinksPrices(arr);
      }
    }

    function checkForUnappliedModifications():boolean {
      const hasUnappliedModifications = modifiedFields.winePath || modifiedFields.wineconsolePath || modifiedFields.dosBoxPath || Object.keys(modifiedFields.executablePaths).length || Object.keys(modifiedFields.executableImgPath).length
      if(!hasUnappliedModifications) return false;

      let paths = [] as string[];
      if(modifiedFields.dosBoxPath) {
        paths.push('DosBox path')
      }
      if(modifiedFields.winePath) {
        paths.push('wine path')
      }
      if(modifiedFields.wineconsolePath) {
        paths.push('wineconsole path')
      }
      for(const execId in modifiedFields.executablePaths) {
        const g = selectedGame();
        const e = g!.executables[execId];
        paths.push(`${g?.name}: ${e.name}`)
      }
      for(const execId in modifiedFields.executableImgPath) {
        const g = selectedGame();
        const e = g!.executables[execId];
        paths.push(`${g?.name}: ${e.name} (CD Img)`)
      }

      DialogMgr.addPopup({
        icon: "warning",
        title: "Unapplied Modifications",
        closeButton:true,
        body:()=>{
          const pathsHtml = <div>
            <For each={paths}>
              {p=><div>{p}</div>}
            </For>
          </div>;
          return <><div>Unapplied paths found:</div> {pathsHtml} <div style="font-weight: bolder"> Please click the blinking "Click to Save" button to apply new path</div></>
        },
        buttons:[{body:"ok"}]
      })

      return true;
    }

    const handleSubmit = async (event: SubmitEvent) => {
      event.preventDefault();
      event.stopPropagation();

      setValidated(true);
      const form = event.currentTarget;
      if ((form as HTMLFormElement).checkValidity() === false) { 
        return
      }

      if(checkForUnappliedModifications()) {
        return;
      }

      const ircNick = (document.querySelector('#IrcNick') as HTMLInputElement).value;
      const ircNickPass = (document.querySelector('#IrcNickPass') as HTMLInputElement).value;
      const InGamePlayerName = (document.querySelector('#InGamePlayerName') as HTMLInputElement).value;
      const gamePort = (document.querySelector('#gamePort') as HTMLInputElement).value;

      const oldNick = settings.IrcNick;
      const oldPass = settings.IrcNickPass;

      settings.IrcNick = ircNick;

      if(ircNickPass?.length) {
        settings.IrcNickPass = Buffer.from(ircNickPass).toString('base64')
      } else {
        delete settings.IrcNickPass
      }
      settings.InGamePlayerName = InGamePlayerName;

      // Shortcuts
      if(typeof settings?.shortcutKeys != 'object') settings.shortcutKeys = {};

      const terminateShortCut = termianteProcShortcut()?.trim();
      let shortcutChanged = false;
      if(terminateShortCut !== settings?.shortcutKeys?.terminateProc) {
        shortcutChanged = true;
        if(terminateShortCut?.length) {
          settings.shortcutKeys.terminateProc = terminateShortCut;
        } else {
          delete settings?.shortcutKeys?.terminateProc
        }
      }
      
      let refreshNukemNet = false;
      if(+settings.gamePort != parseInt(gamePort)) {

        if(!Settings.checkIfPortIsAllowed(parseInt(gamePort))) {
          DialogMgr.addPopup({
            closeButton:true,
            title:"Game Port not allowed",
            body: `Game port ${gamePort} is not allowed. Please choose a different port.`
          });
          return;
        }

        const result = await DialogMgr.addPopup({
          icon:"info",
          title: "Game Port Changed",
          body:"Changing game port requires rebooting NukemNet",
          buttons:[{body:'Change and reboot NukemNet', variant:"warning"}, {body:'Revert Game Port', variant:"secondary"}]
        })
        if(result?.buttonIndex===0) {
          settings.gamePort = parseInt(gamePort);
          refreshNukemNet = true;
        }
      }

      (async ()=>{
        await Settings.write(settings);

        if(shortcutChanged) {
          Settings.registerShortcuts();
        }

        if(settings?.IrcNick?.length) {
          const nickChanged = oldNick != settings.IrcNick;
          if(oldNick != settings.IrcNick) {
            props.ircClient.send('NICK', settings.IrcNick!);
          }

          if(settings.IrcNickPass?.length && (nickChanged || settings.IrcNickPass != oldPass)) {
            const pass = Buffer.from(settings.IrcNickPass!, 'base64').toString('utf8');
            IrcClientExtra.writeSaslPlainAuth(settings.IrcNick, pass, props.ircClient.conn!);
          }
        }

        for(const modified in modifiedGameExecs) {
          const parts = modified.split('/');
          const gId = parts[0]
          const eId = parts[1];

          mainStore.refreshIsGameExecMissing_ReturnWhetherUpdateFound(gId, eId, props.ircClient)
        }

        props?.dismiss?.();

        if(refreshNukemNet) {
          location.reload();
        }
      })()
    };

    function onGameSelected(element:any) {
      checkForUnappliedModifications()

      const gameId = element.target.selectedOptions[0].value;
      if(gameId in gameDefs.games) {
        setSelectedGame(gameDefs.games[gameId]);
        setSelectedGameId(gameId);
        for(const prop in modifiedFields.executablePaths) {
          delete modifiedFields.executablePaths[prop];
        }
        for(const prop in modifiedFields.executableImgPath) {
          delete modifiedFields.executableImgPath[prop];
        }
      } else {
        setSelectedGame(null);
        setSelectedGameId(null);
      }
    }

    const setWineExecPath = async (inputId:string, console?:boolean)=>{
      const fullPathInput = document.querySelector('#'+CSS.escape(inputId)) as HTMLInputElement;
      let currentPath = fullPathInput.value;

      const filename = console? 'wineconsole': 'wine';

      if(currentPath?.length) {
        currentPath = currentPath?.length? path.resolve(currentPath):''
        try {
          const s = await fs.stat(currentPath);
          if(!s.isFile()) {
            DialogMgr.addPopup({title:'Error', body:`${filename} path must point to a file, not a directory.`, buttons:[{body:"ok"}]});
            return
          }
        } catch(e) {
          DialogMgr.addPopup({title:'Error', body:`Could not find your ${filename} executable file.`, buttons:[{body:"ok"}]});
          return
        }
      }
      
      if(console) {
        modifiedFields.wineconsolePath = false;
        settings.wine!.console_path_exe = currentPath;
      } else {
        modifiedFields.winePath = false;
        settings.wine!.path_exe = currentPath;
      }
      
    }
    const chooseExeForWine = async (inputId:string, console?:boolean)=>{
      const fullPathInput = document.querySelector('#'+CSS.escape(inputId)) as HTMLInputElement;

      let currentPath = fullPathInput.value;
      currentPath = currentPath?.length? path.resolve(currentPath):''

      const ext = isWindows()? 'exe':'*'
      const result = await electronRemote.dialog.showOpenDialog({
        properties: ['openFile'],
        defaultPath: currentPath,
        filters: [{name:`Wine${console?' Console':''} Executable`, extensions:[ext]}]
      })

      try {
        const newPath = path.resolve(result.filePaths[0]);
        fullPathInput.value = newPath
        if(console) {
          modifiedFields.wineconsolePath = true;
        } else {
          modifiedFields.winePath = true;
        }
      } catch(e) {}
    }

    const setDosboxExecPath = async (inputId:string)=>{
      const fullPathInput = document.querySelector('#'+CSS.escape(inputId)) as HTMLInputElement;
      let currentPath = fullPathInput.value;
      if(currentPath?.length) {
        currentPath = currentPath?.length? currentPath:''
        try {
          const s = await fs.stat(currentPath);
          if(!s.isFile()) {
            DialogMgr.addPopup({title:'Error', body:"DOSBox path must point to a file, not a directory.", buttons:[{body:"ok"}]});
            return
          }
        } catch(e) {
          DialogMgr.addPopup({title:'Error', body:"Could not find your dosbox executable file.", buttons:[{body:"ok"}]});
          return
        }
      }
      
      modifiedFields.dosBoxPath = false;
      settings.dosbox!.path_exe = currentPath;
    }
    const chooseExeForDosbox = async (inputId:string)=>{
      const fullPathInput = document.querySelector('#'+CSS.escape(inputId)) as HTMLInputElement;

      let currentPath = fullPathInput.value;
      currentPath = currentPath?.length? path.resolve(currentPath):''

      const ext = isWindows()? 'exe':'*'
      const result = await electronRemote.dialog.showOpenDialog({
        properties: ['openFile'],
        defaultPath: currentPath,
        filters: [{name:"DOSBox Executable", extensions:[ext]}]
      })

      try {
        const newPath = path.resolve(result.filePaths[0]);
        fullPathInput.value = newPath
        modifiedFields.dosBoxPath = true;
      } catch(e) {}
    }

    const chooseFolderForExec = async (gameId:string, execId:string, inputId:string)=>{
      const fullPathInput = document.querySelector('#'+CSS.escape(inputId)) as HTMLInputElement;

      let currentPath = fullPathInput.value;
      currentPath = currentPath?.length? path.resolve(currentPath):''

      const result = await electronRemote.dialog.showOpenDialog({
        properties: ['openDirectory'],
        defaultPath: currentPath
      })

      try {
        const newPath = path.resolve(result.filePaths[0]);
        fullPathInput.value = newPath

        modifiedFields.executablePaths[execId] = true;
      } catch(e) {}
    }

    const chooseImgFileForExec = async (gameId:string, execId:string, inputId:string)=>{
      const fullPathInput = document.querySelector('#'+CSS.escape(inputId)) as HTMLInputElement;

      let currentPath = fullPathInput.value;
      currentPath = currentPath?.length? path.resolve(currentPath):''

      const result = await electronRemote.dialog.showOpenDialog({
        properties: ['openFile'],
        defaultPath: currentPath,
        filters: [{extensions:['iso','cue','dat','inst'], name:"CD Img"}, {extensions:['*'], name:"All Files"}]
      })

      try {
        const newPath = path.resolve(result.filePaths[0]);
        fullPathInput.value = newPath

        modifiedFields.executableImgPath[execId] = true;
      } catch(e) {}
    }

    const setExecCDRomImgPath = async (gameId:string, execId:string, inputId:string)=>{
      const fullPathInput = document.querySelector('#'+CSS.escape(inputId)) as HTMLInputElement;
      let currentPath = fullPathInput.value;
      currentPath = currentPath?.length? path.resolve(currentPath):''

      const gname = gameDefs.games[gameId].name;
      const ename = gameDefs.games[gameId].executables[execId].name;

      if(currentPath?.length) {
        try {
          await fs.access(currentPath);
        } catch(e) {
          DialogMgr.addPopup({icon:'error', "title":'Error',body:`Could not find CDRom Image for ${gname} @ ${ename}`, buttons:[{body:"ok"}]})
          return
        }
      }

      // Keep in temp settings
      delete modifiedFields.executableImgPath[execId];
      if(settings?.games?.[gameId] == null) (settings as any).games[gameId] = {};
      if(settings?.games?.[gameId][execId] == null) (settings as any).games[gameId][execId] = {};
      if(settings?.games?.[gameId][execId].dosbox == null) (settings as any).games[gameId][execId].dosbox = {};

      settings.games![gameId][execId].dosbox!.imgmount = currentPath;
    }

    const setExecPath = async (gameId:string, execId:string, inputId:string)=>{
      const fullPathInput = document.querySelector('#'+CSS.escape(inputId)) as HTMLInputElement;
      let currentPath = fullPathInput.value;
      currentPath = currentPath?.length? currentPath:''

      if(currentPath?.length) {
        const gname = gameDefs.games[gameId].name;
        const executableDef = gameDefs.games[gameId].executables[execId];
        const ename = executableDef.name;

        async function verifyFilesExistance(tryPath:string) {
          const missingFiles:string[] = [];
          const gameFiles = executableDef.files;
          for(const fileRef in gameFiles) {
            const fileInfo = gameFiles[fileRef];  
            const fullFilePath = Settings.resolveFilePathForFileRef(fileRef, {dir:tryPath, gameId:executableDef._gameId, execId:execId});
            if(!fileInfo.optional && !fullFilePath?.length) {
              missingFiles.push(fileInfo?.glob?.toString() || fileInfo?.path || 'Missing path/glob in file definition');
            }
          }
          return missingFiles;
        }

        let missingFiles = [] as string[];
        let checkPath = currentPath;
        for(let i=0; i<3; i++) { // max 3 tries browsing up on path (useful in games such as GTA which have files on relative paths)
          missingFiles = await verifyFilesExistance(checkPath);
          if(!missingFiles?.length) {
            currentPath = checkPath;
            (document.getElementById(`gamepath-${selectedGameId()}:${execId}`) as HTMLInputElement).value = currentPath;
            break;
          };
          let newCheckPath = path.resolve(checkPath, '..');
          if(newCheckPath != checkPath) checkPath = newCheckPath;
          else break;
        }
        
        if(missingFiles.length) {
          DialogMgr.addPopup({
            icon:'error',
            body:`Files missing for ${gname} @ ${ename}: ${missingFiles.join(', ')}`,
            buttons:[{body:"ok"}]
          })
          return
        }
      }

      // Keep in temp settings
      delete modifiedFields.executablePaths[execId];
      if(settings?.games?.[gameId] == null) (settings as any).games[gameId] = {};
      if(settings?.games?.[gameId][execId] == null) (settings as any).games[gameId][execId] = {};
      settings.games![gameId][execId].path = currentPath;

      modifiedGameExecs[`${gameId}/${execId}`] = true;
    }

    const extensionToAttributes = {} as {[ext:string]:{hasStyles:boolean, hasJs:boolean}};
    const extensions = [] as string[];
    try {
      extensions.push(...fsSync.readdirSync(pathOfExtensions));
    } catch(e) {}

    for(const ext of extensions) {
      let hasStyles = false, hasJs = false;
      
      const extPath = path.resolve(pathOfExtensions, ext);

      // styles
      try {
        const entriesInExtPath = fsSync.readdirSync(extPath);
        const stylesDirName = entriesInExtPath.find(f=>f.toLowerCase()==='styles');
        const stylesDirPath = stylesDirName?.length? path.join(extPath, stylesDirName):undefined;
        if(!stylesDirPath?.length) throw '';
        
        const styleFiles = fsSync.readdirSync(stylesDirPath);
        for(const f of styleFiles) {
          if(f.toLowerCase().endsWith('.css')) {
            hasStyles = true;
            break;
          }
        }
      } catch(e) {}

      // js
      try {
        const entriesInExtPath = fsSync.readdirSync(extPath);
        const jsDirName = entriesInExtPath.find(f=>f.toLowerCase()==='js');
        const jsDir = jsDirName?.length? path.join(extPath, jsDirName):undefined;
        if(!jsDir?.length) throw '';
        
        const jsFiles = fsSync.readdirSync(jsDir);
        for(const f of jsFiles) {
          if(f.toLowerCase().endsWith('.js')) {
            hasJs = true;
            break;
          }
        }
      } catch(e) {}
      
      extensionToAttributes[ext] = {hasStyles, hasJs};
    }

    const indexJsExampleFilePath = path.resolve(pathOfRes, 'example-extension-js', 'index.js');
    const indexJsExampleFilePathFileProto = 'file://'+indexJsExampleFilePath.replaceAll('\\','/');

    function simulateVolumeSound(e:Event, isClick:boolean) {
      const value = (e.target as HTMLInputElement).value;
      const num = parseInt(value)
      if (!isNaN(num)) {
        setSoundVolume(num);
        settings.soundVolume = num;
      }
      Settings.playSoundEffect('room-message', num/100);
    }

    const initialVolume = Settings.read().soundVolume??100;
    const [soundVolume, setSoundVolume] = createSignal(initialVolume);


    function setResPath(execId:string, res:string, newResPath:string) {
      if(!execId?.length || !selectedGameId()?.length) return;
      const gid = selectedGameId()!;
      if(!settings.games) settings.games = {};
      if(!settings.games[gid]) settings.games[gid] = {};
      if(!settings.games[gid][execId]) settings.games[gid][execId] = {};
      if(!settings.games[gid][execId].resPath) settings.games[gid][execId].resPath = {};
      settings.games[gid][execId].resPath![res] = newResPath||'';
    }

    const [termianteProcShortcut, setTerminateProcShortcut] = createSignal(settings?.shortcutKeys?.terminateProc as string|null);

    return <Modal
    onHide={()=>props?.dismiss?.()}
    fullscreen={true}
    scrollable={true}
    show={props.show}
    backdrop="static"
    keyboard={false}
  >
    <Form noValidate validated={validated()} onSubmit={handleSubmit} style={{"overflow-y":'scroll'}}>
      <Modal.Header closeButton>
        <Modal.Title>NukemNet Settings</Modal.Title>
      </Modal.Header>
      <Modal.Body>

        <Tabs defaultActiveKey="general" class="mb-3">
          <Tab eventKey="general" title="General">
            <div style={{display:'flex', "flex-direction":'row'}}>
              <FloatingLabel style={{"flex-grow":1}} controlId="IrcNick" label="NukemNet Nick (IRC)" class="mb-3">
                <Form.Control type="text" value={settings.IrcNick} required minLength={2} />
              </FloatingLabel>
              <FloatingLabel style={{width:'200pt'}} controlId="IrcNickPass" label="Nick Password" class="mb-3">
                <Form.Control type="password" value={Buffer.from(settings.IrcNickPass||'', 'base64').toString('utf8')} minLength={2} />
              </FloatingLabel>
            </div>
            

            <FloatingLabel controlId="InGamePlayerName" label="In-Game Nick" class="mb-3">
              <Form.Control type="text" value={settings.InGamePlayerName} maxLength={20} minLength={2} required />
            </FloatingLabel>

            <FloatingLabel controlId="gamePort" label="Game Port Number" class="mb-3">
              <Form.Control type="number" value={settings.gamePort} required max={65535} min={100} />
            </FloatingLabel>

            <Button onClick={()=>{settings.disableLogging = !settings.disableLogging; setDisableLogging(settings.disableLogging)}} variant={disableLogging()?"danger":"success"} title="Disable logging to files under user directory">Logging {disableLogging()?'Disabled':"Enabled"}</Button><br/><br/>

            <Button onClick={()=>{settings.disableAutoPortChecker = !settings.disableAutoPortChecker}} variant={settings.disableAutoPortChecker?"danger":"success"} title="Toggle Automatic port checking upon creation of a game room">Auto Port Checker {settings.disableAutoPortChecker?'Disabled':"Enabled"}</Button><br/><br/>

            <Button variant="secondary" onClick={()=>Settings.toggleLanMode()}>Switch to {settings.lanOnlyMode?"Standard Online Mode":"Offline LAN Mode"}</Button><br/><br/>

            <Show when={!settings.lanOnlyMode}>
              <Button variant="secondary" onClick={()=>Settings.toggleIpv4Mode()}>Switch to {settings.ipv4Mode?"IPv6/4 Mix (Recommended)":"Strict IPv4"}</Button><br/><br/>
            </Show>

            <InputGroup class="mb-3" title="A keyboard shortcut to terminate the game process quickly">
              <InputGroup.Text>Terminate Game Shortcut ⌨️</InputGroup.Text>
              <InputGroup.Text><a onClick={()=>{Electron.shell.openExternal("https://www.electronjs.org/docs/latest/api/accelerator#available-modifiers");}} href="javascript:;">Formats</a></InputGroup.Text>
              <InputGroup.Checkbox checked={termianteProcShortcut()!=null} onChange={(e)=>{
                const checkbox = e.target as HTMLInputElement;
                if(checkbox?.checked) {
                  setTerminateProcShortcut('CommandOrControl+Shift+X');
                } else {
                  setTerminateProcShortcut(null);
                }
              }} />
              <Show when={termianteProcShortcut()!=null}>
                <FormControl value={termianteProcShortcut()||''} onChange={(e)=>{
                    const input = e.target as HTMLInputElement;
                    if(typeof input.value === 'string' && input?.value?.length) {
                      setTerminateProcShortcut(input.value);
                    } else {
                      setTerminateProcShortcut(null);
                    }
                }} />
              </Show>
            </InputGroup>

            <div>User Directory: {pathOfUserDir}</div>
            <div>Working Directory: {ps.cwd()}</div>
          </Tab>


          <Tab eventKey="games" title="Games">
            
            <Form.Select onChange={onGameSelected}>
              <option>Choose game to setup</option>
              <For each={Object.keys(gameDefs.games).sort((a,b)=>(gameDefs.games[a].name>gameDefs.games[b].name?1:-1))}>
                {gameId=><option value={gameId} style={{"background-color":settings?.games?.[gameId]? 'mediumseagreen':''}}>{gameDefs.games[gameId].name}</option>}
              </For>
            </Form.Select>

            <Show when={selectedGame() != null}>
              <div>
                <div style={{"text-align":"right", "width":"100%"}}>
                  <Index each={selectedGame()?.get}>{(getObj, i)=>
                      <><Button oncontextmenu={()=>{
                        const link = generateLinkForAffiliate(getObj().type, getObj().nameId);
                        Electron.clipboard.writeText(link);
                        DialogMgr.addPopup({
                          noCenter:true,
                          title:"Link Copied",
                          body:()=><div>{getObj().type.toUpperCase()} Link Copied for {getObj().name}</div>,
                          closeButton:true
                        })
                      }} onclick={(e)=>{
                        const url = generateLinkForAffiliate(getObj().type, getObj().nameId);
                        Electron.shell.openExternal(url);
                        e.preventDefault();
                      }} variant="info" style={{
                        display: "table-cell",
                        "vertical-align": "middle",
                        border: "3px groove #1c87c9",
                        "margin-bottom":"2px"
                      }}>
                        <span><b>
                          Get from {getObj().type==='gog'?"GOG":"Zoom-Platform"}&nbsp;
                          <Show when={gameBuyLinksPrices()?.[i]?.price != null}>
                          {parseInt(gameBuyLinksPrices()?.[i]?.price)==0?'Free':(gameBuyLinksPrices()?.[i]?.price + ' ' + gameBuyLinksPrices()?.[i]?.cur)}
                          </Show>
                          </b></span>
                        <img style={{border: "3px solid #2287c9", height: "28px"}} src={getObj().type==='gog'?gogImg:zoomPlatformImg}></img>
                        &nbsp;<span>{getObj()?.name || selectedGame()?.name}</span>
                      </Button><br/></>
                  }</Index>
                </div>
              

              <For each={Object.keys(selectedGame()?.executables || {})}>
                {execId=><div>

                  <h6>
                    {selectedGame()?.executables[execId].name}

                    {selectedGame()?.executables?.[execId]?.tips?<Button style={{"margin-left":"10pt"}} size='sm' onClick={()=>{
                      storeActions.showTipsForDef(selectedGameId()!, execId);
                    }}>Tips</Button>:undefined}
                  </h6>

                  {/* Game Folder Location */}
                  <div style={{display:'flex', "flex-direction":'row'}}>
                    <Button onClick={()=>chooseFolderForExec(selectedGameId()!, execId, `gamepath-${selectedGameId()}:${execId}`)}>Locate Game Folder</Button>
                    <input style={{"flex-grow":1}} onInput={()=>{
                      modifiedFields.executablePaths[execId] = true
                      }} type="text" id={`gamepath-${selectedGameId()}:${execId}`} value={settings?.games?.[selectedGameId() as string]?.[execId]?.path||''}></input>

                    <Show when={modifiedFields.executablePaths[execId] == true}>
                      <Button class="apply-path-button" onClick={()=>{
                        setExecPath(selectedGameId()!, execId, `gamepath-${selectedGameId()}:${execId}`)
                        }}>Click to Save</Button>
                      <Button onclick={()=>{
                          delete modifiedFields.executablePaths[execId];
                          const el = document.querySelector('#'+ CSS.escape(`gamepath-${selectedGameId()}:${execId}`)) as HTMLInputElement;
                          el.value = settings?.games?.[selectedGameId() as string]?.[execId]?.path||''
                        }}>Revert</Button>
                    </Show>
                  </div>

                  {/* Game Cd Image File Location */}
                  <Show when={selectedGame()?.executables?.[execId]?.mountImg}>
                    <div style={{display:'flex', "flex-direction":'row'}}>
                      <Button onClick={()=>chooseImgFileForExec(selectedGameId()!, execId, `gamepath-img-${selectedGameId()}:${execId}`)}>Locate Game CD-IMG</Button>
                      <input style={{"flex-grow":1}} onInput={()=>{
                        modifiedFields.executableImgPath[execId] = true
                        }} type="text" id={`gamepath-img-${selectedGameId()}:${execId}`} value={settings?.games?.[selectedGameId() as string]?.[execId]?.dosbox?.imgmount||''||''}></input>

                      <Show when={modifiedFields.executableImgPath[execId] == true}>
                        <Button class="apply-path-button" onClick={()=>{
                          setExecCDRomImgPath(selectedGameId()!, execId, `gamepath-img-${selectedGameId()}:${execId}`)
                          }}>Click to Save</Button>
                        <Button onclick={()=>{
                            delete modifiedFields.executableImgPath[execId];
                            const el = document.querySelector('#'+ CSS.escape(`gamepath-img-${selectedGameId()}:${execId}`)) as HTMLInputElement;
                            el.value = settings?.games?.[selectedGameId() as string]?.[execId]?.dosbox?.imgmount||''
                          }}>Revert</Button>
                      </Show>
                    </div>
                  </Show>

                  <For each={mainStore.resourcesForExec(selectedGame()?.executables?.[execId])}>
                    {res=>{
                      return <div style={{display:'flex', "flex-direction":'row', height:'22pt'}}>
                        <Button variant="secondary" size="sm" onclick={async ()=>{
                          const resObjPath = `games.${selectedGameId()}.${execId}.resPath.${res}`;
                          const result = await electronRemote.dialog.showOpenDialog({
                            properties: ['openDirectory'],
                            defaultPath: settings?.games?.[selectedGameId()||'']?.[execId]?.resPath?.[res]
                          })
                          const selectedPath = result?.filePaths?.[0];
                          setResPath(execId, res, selectedPath);
                        }}>Locate {Capitalized(res)} Folder</Button>
                        <input onChange={(ev:Event)=>{
                          const newValue = (ev.target as HTMLInputElement).value.trim();
                          if(fsSync.existsSync(newValue) || newValue == '') {
                            setResPath(execId, res, newValue);
                          } else {
                            DialogMgr.addPopup({closeButton:true,buttons:[{body:'Ok'}],
                              title:Capitalized(res)+" Path Error",
                              body:`The path you entered does not exist. Please enter a valid path.`,
                            });
                            (ev.target as HTMLInputElement).value = ''
                            setResPath(execId, res, (ev.target as HTMLInputElement).value);
                          }
                        }} type="text" style={{"flex-grow":1}} placeholder={Capitalized(res)+ ' Folder Path (Optional)'} value={settings?.games?.[selectedGameId() as string]?.[execId]?.resPath?.[res]||''} />
                      </div>;
                    }}
                  </For>
                  
                </div>}
              </For>
            </div>
          </Show>
        </Tab>




          <Tab eventKey="dosbox" title="DOSBox Staging">
              <div>
                <h4 title="NukemNet supports serial emulation only on DOSBox Staging">Please use <a onClick={()=>{Electron.shell.openExternal("https://dosbox-staging.github.io/");}} href="javascript:;">DOSBox Staging</a> for full compatilibity</h4>
                <div style={{display:'flex', "flex-direction":'row'}}>
                  <Button onClick={()=>chooseExeForDosbox(`dosbox-path`)}>Locate</Button>
                  <input style={{"flex-grow":1}} onInput={()=>{
                    modifiedFields.dosBoxPath = true
                    }} type="text" id="dosbox-path" value={settings?.dosbox?.path_exe||''}></input>

                  <Show when={modifiedFields.dosBoxPath == true}>
                    <Button class="apply-path-button" onClick={()=>{
                      setDosboxExecPath(`dosbox-path`)
                      }}>Click to Save</Button>
                    <Button onclick={()=>{
                        modifiedFields.dosBoxPath = false;
                        const el = document.querySelector('#'+ CSS.escape(`dosbox-path`)) as HTMLInputElement;
                        el.value = settings?.dosbox?.path_exe||''
                      }}>Revert</Button>
                  </Show>
                </div>
              </div><br/>
              <Button onClick={()=>{settings.debugDosbox = !settings.debugDosbox; setDebugDosbox(settings.debugDosbox)}} variant={debugDosbox()?"danger":"success"} title='Injecting "pause" statements between each dosbox command'>Debug Dosbox {debugDosbox()?'Enabled':"Disabled"}</Button><br/><br/>
            </Tab>
            
          <Tab eventKey="sounds" title="Sounds">
              <h6>Volume</h6>
              <input id="sound-volume" value={initialVolume} type="range" min="0" max="100" onclick={e=>simulateVolumeSound(e,true)} onInput={e=>simulateVolumeSound(e,false)}></input>
              <label for="sound-volume">{soundVolume()}</label>

              <h6>Customizing sound effects:</h6>
              In your user folder, you can create a "sounds" subfolder, and customize NukemNet's sounds<br/>
              <b>user/sounds/room-message/anyFileName.mp3|wav</b><br/>
              if you put multiple sound files in a specific sound folder, NukemNet will choose randomly from them each time they are played.<br/>
              if you add an empty sound folder, NukemNet will not play any sound for that event.<br/>
              Possible sound names (for your sound folders) are:<br/>
              <ul>
                <For each={SoundTypes_All}>{(name)=><li>{name}</li>}</For>
              </ul>
          </Tab>

          <Tab eventKey="extensions" title="Extensions">
            <b>Custom Extensions allow you to modify NukemNet's appearance and behavior.</b><br/><br/>

            <Show when={extensions?.length}>
              <div style={{"border":'5px solid blue'}}>
                <h4>Active Extensions:</h4>
                <ul>
                  <For each={extensions}>
                    {extension=><li><b>{extension} {extensionToAttributes[extension].hasStyles?'[styles]':''} {extensionToAttributes[extension].hasJs?`[javascript${Settings.loadedJsExtensions[extension]?'':' disabled'}]`:''}</b></li>}
                  </For>
                </ul>
              </div>
              <br/>
            </Show>

            <Show when={!extensions?.length}>
              <h4>No Extensions are currently active.</h4>
              <div>You can add extensions by copying their folder into your <b>user/extensions</b> path.</div>
              <br/>
            </Show>

            <h4>Extension Creation:</h4>
            <div>
              Create a new folder under user/extensions, choose any name you want.<br/><br/>

              <h6>Visual Theming:</h6>
              put your style .css files in user/extensions/my-ext-name/styles, they will be loaded automatically by NukemNet.<br/>
              In your css file, you can refer to files (such as images) in your extension folder using the nnext:// prefix, such as:<br/>
              <pre>
              {`*, someElement.someClassName\n{\n   background: url(nnext://background.png)\n}`}
              </pre>

              <h6>Custom Behaviour (javascript):</h6>
              Create index.js file in user/extensions/my-ext-name/js and make sure it corresponds to the NukemNet official extension object signature<br/>
              <a onClick={async ()=>{
                  const desc = "Save NN Extension Example File";
                  const result = await electronRemote.dialog.showOpenDialog({
                    title:desc,
                    buttonLabel:desc,
                    message:desc,
                    properties: ['openDirectory']
                  });
                  const dlPath = result?.filePaths?.[0];
                  if(!dlPath?.length) return;
                  fs.copyFile(indexJsExampleFilePath, path.resolve(dlPath, path.basename(indexJsExampleFilePath)));
                }} href={indexJsExampleFilePathFileProto} target="_blank">Here's an example of a valid index.js file</a><br/>
              <b>WARNING!</b> if you use an extension that contains js files, you are responsible for making sure it doesn't contain malicious code, so only use extensions provided by people you absolutely trust.<br/>
              <b>IMPORTANT please use your own local irc server when developing extensions, <a onClick={()=>{
                DialogMgr.addPopup({
                  title: "Use Local IRC Server",
                  fullscreen:true,
                  closeButton:true,
                  buttons:[{body:"Close"}],
                  body:()=><pre style={{"text-align":"start"}}>{`
** IMPORTANT **
Please use your own IRC server during extension development
--------------
To enable DevTools, add an empty file named "debug" in your user folder.
When developing extensions, you might need to restart NukemNet, a lot.
to prevent spamming the production irc server with join/quit events,
you should run your own local irc server.
Don't worry it's easy, just download Ergo IRC server,
and using the terminal type "ergo run".
To configure NukemNet to connect to your own local irc server,
(and prevent it from going online - optional),
add these lines in user/server.json
{
    "offline":true, // optional, turns off external ip lookup, bypassing the blocking "Checking Connectivity" popup.
    "irc": {
        "address":"localhost",
        "port":6667,
        "secure":false
    }
}
If you need to run multiple instances of NukemNet concurrently,
add an empty file named "allowMultipleInstances" in the user folder.
If you want to disable lan detection to test connectivity online,
add an empty file named "disableLanDetection" in the user folder.
`}
                  </pre>
                })
              }} href="javascript:;">read this</a></b>
            </div>
          </Tab>

          {isWindows()?<></>:<Tab eventKey="wine" title="Wine">
            <h4>Wine <a onClick={()=>{Electron.shell.openExternal("https://www.winehq.org");}} href="javascript:;">Wine-HQ</a></h4>

            <span><b><u>wine</u></b></span>
            <div style={{display:'flex', "flex-direction":'row'}}>
              <Button onClick={()=>chooseExeForWine(`wine-path`)}>Locate</Button>
              <input style={{"flex-grow":1}} onInput={()=>{
                modifiedFields.winePath = true
                }} type="text" id="wine-path" value={settings?.wine?.path_exe||''}></input>

              <Show when={modifiedFields?.winePath == true}>
                <Button class="apply-path-button" onClick={()=>{
                  setWineExecPath(`wine-path`)
                  }}>Click to Save</Button>
                <Button onclick={()=>{
                    modifiedFields.winePath = false;
                    const el = document.querySelector('#'+ CSS.escape(`wine-path`)) as HTMLInputElement;
                    el.value = settings?.wine?.path_exe||''
                  }}>Revert</Button>
              </Show>
            </div>

            <span><b><u>wineconsole</u></b></span>
            <div style={{display:'flex', "flex-direction":'row'}}>
              <Button onClick={()=>chooseExeForWine(`wineconsole-path`, true)}>Locate</Button>
              <input style={{"flex-grow":1}} onInput={()=>{
                modifiedFields.wineconsolePath = true
                }} type="text" id="wineconsole-path" value={settings?.wine?.console_path_exe||''}></input>

              <Show when={modifiedFields.wineconsolePath == true}>
                <Button class="apply-path-button" onClick={()=>{
                  setWineExecPath(`wineconsole-path`, true)
                  }}>Click to Save</Button>
                <Button onclick={()=>{
                    modifiedFields.wineconsolePath = false;
                    const el = document.querySelector('#'+ CSS.escape(`wineconsole-path`)) as HTMLInputElement;
                    el.value = settings?.wine?.console_path_exe||''
                  }}>Revert</Button>
              </Show>
            </div>
          </Tab>}          

        </Tabs>

      </Modal.Body>
      <Modal.Footer>
        <Button type="submit" variant="primary">Save Settings</Button>
        <Button variant="secondary" onclick={()=>props?.dismiss?.()}>Cancel</Button>
      </Modal.Footer>

    </Form>

  </Modal>
}


export default LauncherSettingsModal;