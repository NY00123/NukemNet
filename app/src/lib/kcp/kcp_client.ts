import { RemoteInfo } from "dgram";
import { Server, Socket as TCPSocket } from "net";
import { KCP } from "node-kcp-x";
import { dgram, kcp, net } from "../../NodeLibs";
import { udp6a } from "../utils";
import { KcpCommon } from "./common";


const udpIndicator = KcpCommon.udpIndicator;
const udpIndicatorBuffer = KcpCommon.udpIndicatorBuffer;

export function createUDPTunnel_client(tunnelDestIp:string, tunnelDestPort:number, gameTcpPort?:number, gameUdpPort?:number) {

    if(gameTcpPort == null) {
        console.log('Error: Missing TCP port');
        return;
    }

    var kcpUdpClient = dgram.createSocket('udp6');

    var interval = 20;

    let kcpobj: KCP;
    let keepAliveCount:number;
    let kcpTunnelConnected = false;

    function kcpTunnelConnectionState(connected:boolean) {
        if(connected) {
            createTcpServer();
        } else {
            destroyTcpServer();
        }
    }

    function recreateKcpStateObj() {
        if(kcpTunnelConnected) {
            kcpTunnelConnected = false;
            kcpTunnelConnectionState(false);
        }
        if(kcpobj) {
            try {kcpobj.release()} catch(e) {}
        }
        kcpobj = new kcp.KCP(123, {address: tunnelDestIp, port: tunnelDestPort});
        kcpobj.nodelay(0, interval, 0, 0);
        kcpobj.output(function(data:Buffer, size:number, context:any) {
            kcpUdpClient.send(data, 0, size, context.port, udp6a(context.address));
        });
        kcpobj.send(Buffer.from(JSON.stringify({type:"keepalive", i:"nnpacket"})));
        keepAliveCount = KcpCommon.keepAliveCounts;
    }
    recreateKcpStateObj();

    const keepAliveInterval = setInterval(()=>{
        if(destroyed) {clearInterval(keepAliveInterval); return;}
        if(kcpobj) {
            kcpobj.send(Buffer.from(JSON.stringify({type:"keepalive", i:"nnpacket"})));
        }
        if(keepAliveCount<=0) {
            recreateKcpStateObj();
        }
        keepAliveCount--;
    },1000);

    kcpUdpClient.on('error', (err) => {console.log(`client error:\n${err.stack}`)});

    kcpUdpClient.on('message', (msg, rinfo) => {
        try {
            // packet starts with nukemnetpack, which means it's UDP, not TCP, so forward this packet to the UDP game port.
            const starter = msg.subarray(0,udpIndicatorBuffer.length).toString();
            if(starter===udpIndicator) {
                const actualPacket = msg.subarray(udpIndicator.length);
                if(localUdpClientLatestRinfo) {
                    localUdpClient.send(actualPacket, localUdpClientLatestRinfo.port, udp6a(localUdpClientLatestRinfo.address));
                }
                return;
            }
        } catch(e) {}

        kcpobj?.input(msg);
    });

    function clientReceivedKcpPacket(data:Buffer) {
        try {
            const metaPacket = data.toString();
            const metaObj = JSON.parse(metaPacket);
            if(!metaObj || !metaObj.type || metaObj.i!=='nnpacket') throw ''; // not a meta packet
            if(metaObj.type === 'closed') {
                try{tcpSocket?.destroy()} catch(e) {}
                tcpSocket = null;
            } else if(metaObj.type === 'keepalive') {
                keepAliveCount = KcpCommon.keepAliveCounts;
                if(!kcpTunnelConnected) {
                    kcpTunnelConnected = true;
                    kcpTunnelConnectionState(true);
                }
            }
            return;
        } catch(e) {}

        if(tcpSocket) {
            tcpSocket.write(data);
        }
    }

    const intervalId = setInterval(() => {
        if(destroyed) {clearInterval(intervalId); return;}
        kcpobj?.update(Date.now());
        do {
            var recv = kcpobj?.recv();
            if (recv) {clientReceivedKcpPacket(recv)}
        } while (recv);
    }, interval);

    // create local TCP listener
    let tcpSocket:TCPSocket|null = null;
    var server:Server;

    function destroyTcpServer() {
        try {server.close()} catch(e) {}
        try {tcpSocket?.destroy()} catch(e) {}
        try {tcpSocket?.removeAllListeners()} catch(e) {}
    }

    function createTcpServer() {
        destroyTcpServer();

        server = net.createServer(function(socket) {
            // if previous socket exists, close it
            if(tcpSocket) {
                try {tcpSocket.destroy()} catch(e) {}
            }
    
            tcpSocket = socket;
    
            kcpobj?.send(Buffer.from(JSON.stringify({type:"connect", i:"nnpacket"})));
            socket.on('data', (msg:Buffer, rinfo:RemoteInfo) => {
                kcpobj?.send(msg);
            });
            socket.on('close', ()=>{
                try {tcpSocket?.destroy()} catch(e) {}
                tcpSocket = null;
                kcpobj?.send(Buffer.from(JSON.stringify({type:"close", i:"nnpacket"})));
            })
        });
        server.listen(gameTcpPort);
    }

    // creat local UDP listener
    const localUdpClient = dgram.createSocket('udp6');
    localUdpClient.bind(gameUdpPort);
    localUdpClient.on('error', (err) => {console.log(`game udp listener error:\n${err.stack}`)});
    let localUdpClientLatestRinfo:RemoteInfo|null = null
    localUdpClient.on('message', (msg, rinfo) => {
        localUdpClientLatestRinfo = rinfo;
        kcpUdpClient.send(Buffer.from([...udpIndicatorBuffer, ...msg]), tunnelDestPort, udp6a(tunnelDestIp));
    });

    const udpPartListening = gameUdpPort ? ` and UDP:${gameUdpPort}` : '';
    console.log(`Listening on TCP:${gameTcpPort}${udpPartListening}, forwarding to UDP:${tunnelDestIp}:${tunnelDestPort}`)

    let destroyed = false;
    return {
        gamePort: null,
        close: function() {
            clearInterval(intervalId);
            clearInterval(keepAliveInterval);
            destroyed = true;
            try {server.close()} catch(e) {}
            try {tcpSocket?.destroy()} catch(e) {}
            try {tcpSocket?.removeAllListeners()} catch(e) {}
            try {localUdpClient.close()} catch(e) {}
            try {localUdpClient.removeAllListeners()} catch(e) {}
            try {kcpUdpClient.close()} catch(e) {}
            try {kcpUdpClient.removeAllListeners()} catch(e) {}
            try {server.removeAllListeners();} catch(e) {}
        }
    }
}

