import { Controller, Delete, Get, UseGuards } from '@nestjs/common';
import { SecretKeyGuard } from './secret-key.guard';
import { UdpRelayService } from './udp-relay/udp-relay.service';
@Controller()
export class AppController {
  
    constructor(private udpRelayService:UdpRelayService) {}

    @Get('noop')
    @UseGuards(SecretKeyGuard)
    noop() {
        return 'noop';
    }

    @Get()
    @UseGuards(SecretKeyGuard)
    async getAll() {
        return {
            "udp": this.udpRelayService.getAll()
        }
    }

    @Delete()
    @UseGuards(SecretKeyGuard)
    async delAll() {
        this.udpRelayService.delAll();
    }
}
