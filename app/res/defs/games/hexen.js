module.exports = function ({deepClone, GameDefsType, utils}) {

    const common = require('./doom_common.i')

    const SUPPORTED_NET_TYPES = GameDefsType.Dosbox_NetTypesAllSpecific.filter(x => x !== "serial:19200");

    const commands = ['CONFIG -set "cycles=max"','CONFIG -set "core=auto"']
    const vars = {
        "game": {
            "runToolOpts": {"type":"dosbox", "data":{commands}},
            "runToolOptsMultiplayer": {"type":"dosbox", "data":{commands, "net":SUPPORTED_NET_TYPES}}
        }
    }


    const Shareware_EpisodeAndLevels = [{
        "label": "Hub 1: Seven Portals",
        "value": ["-warp", "1"]
    }, {
        "label": "H1M1 Winnowing Hall",
        "value": ["-warp", "1"]
    }, {
        "label": "H1M2: Seven Portals",
        "value": ["-warp", "2"]
    }, {
        "label": "H1M3: Guardian of Ice",
        "value": ["-warp", "3"]
    }, {
        "label": "H1M4: Guardian of Fire",
        "value": ["-warp", "4"]
    }];

    const Full_EpisodeAndLevels = [...Shareware_EpisodeAndLevels, {
        "label": "H1M5: Guardian of Steel",
        "value": ["-warp", "5"]
    }, {
        "label": "H1M6: Bright Crucible",
        "value": ["-warp", "6"]
    },
    
    {
        "label": "Hub 2: Shadow Wood",
        "value": ["-warp", "7"]
    }, {
        "label": "H2M1: Shadow Wood (map)",
        "value": ["-warp", "7"]
    }, {
        "label": "H2M2: Darkmere",
        "value": ["-warp", "8"]
    }, {
        "label": "H2M3: Caves of Circe",
        "value": ["-warp", "9"]
    }, {
        "label": "H2M4: Wastelands",
        "value": ["-warp", "10"]
    }, {
        "label": "H2M5: Sacred Grove",
        "value": ["-warp", "11"]
    }, {
        "label": "H2M6: Hypostyle",
        "value": ["-warp", "12"]
    },

    {
        "label": "Hub 3: Heresiarch's Seminary",
        "value": ["-warp", "13"]
    }, {
        "label": "H3M1: Heresiarch's Seminary (map)",
        "value": ["-warp", "13"]
    }, {
        "label": "H3M2: Orchard of Lamentations",
        "value": ["-warp", "17"]
    }, {
        "label": "H3M3: Silent Refectory",
        "value": ["-warp", "18"]
    }, {
        "label": "E3M4: Wolf Chapel",
        "value": ["-warp", "19"]
    }, {
        "label": "H3M5: Dragon Chapel",
        "value": ["-warp", "14"]
    }, {
        "label": "H3M6: Griffin Chapel",
        "value": ["-warp", "15"]
    }, {
        "label": "H3M7: Deathwind Chapel",
        "value": ["-warp", "16"]
    },

    {
        "label": "Hub 4: Castle of Grief",
        "value": ["-warp", "21"]
    }, {
        "label": "E4M1: Castle of Grief (map)",
        "value": ["-warp", "21"]
    },{
        "label": "E4M2: Forsaken Outpost",
        "value": ["-warp", "20"]
    }, {
        "label": "E4M3: Gibbet",
        "value": ["-warp", "22"]
    }, {
        "label": "E4M4: Effluvium",
        "value": ["-warp", "23"]
    }, {
        "label": "E4M5: Dungeons",
        "value": ["-warp", "24"]
    }, {
        "label": "E4M6: Desolate Garden",
        "value": ["-warp", "25"]
    },

/*
Hub 5:  (map)
Hub 5: 
Hub 5: 
Hub 5: 
Hub 5:  - secret map
Hub 5:  - epilogue/final map
*/

    {
        "label": "Hub 4: Necropolis",
        "value": ["-warp", "26"]
    }, {
        "label": "E4M1: Necropolis (map)",
        "value": ["-warp", "26"]
    },{
        "label": "E4M2: Zedek's Tomb",
        "value": ["-warp", "27"]
    }, {
        "label": "E4M3: Menelkir's Tomb",
        "value": ["-warp", "28"]
    }, {
        "label": "E4M4: Traductus' Tomb",
        "value": ["-warp", "29"]
    }, {
        "label": "E4M5: Vivarium",
        "value": ["-warp", "30"]
    }, {
        "label": "E4M6: Dark Crucible",
        "value": ["-warp", "31"]
    }]

    const DOTDC_EpisodeAndLevels = [{
        "label": "Expansion Hub 1: Blight",
        "value": ["-warp", "2"],
    }, {
        "label": "XH1M1: Ruined Village",
        "value": ["-warp", "1"]
    }, {
        "label": "XH1M2: Blight (map)",
        "value": ["-warp", "2"]
    }, {
        "label": "XH1M3: Badlands",
        "value": ["-warp", "5"]
    }, {
        "label": "XH1M4: Brackenwood",
        "value": ["-warp", "6"]
    }, {
        "label": "XH1M5: Catacomb",
        "value": ["-warp", "4"]
    }, {
        "label": "XH1M6: Sump",
        "value": ["-warp", "3"]
    }, {
        "label": "XH1M7: Pyre",
        "value": ["-warp", "7"]
    },

    {
        "label": "Expansion Hub 2: Constable's Gate",
        "value": ["-warp", "8"],
    }, {
        "label": "XH2M1: Constable's Gate (map)",
        "value": ["-warp", "8"]
    }, {
        "label": "XH2M2: Market Place",
        "value": ["-warp", "10"]
    }, {
        "label": "XH2M3: Ordeal",
        "value": ["-warp", "12"]
    }, {
        "label": "XH2M4: Locus Requiescat",
        "value": ["-warp", "11"]
    }, {
        "label": "XH2M5: Treasury",
        "value": ["-warp", "9"]
    }, {
        "label": "XH2M6: Armory",
        "value": ["-warp", "13"]
    },

    {
        "label": "Expansion Hub 3: Nave",
        "value": ["-warp", "14"],
    }, {
        "label": "XH3M1: Nave (map)",
        "value": ["-warp", "14"]
    }, {
        "label": "XH3M2: Chantry",
        "value": ["-warp", "15"]
    }, {
        "label": "XH3M3: Dark Watch",
        "value": ["-warp", "17"]
    }, {
        "label": "XH3M4: Cloaca",
        "value": ["-warp", "18"]
    }, {
        "label": "XH3M5: Abattoir",
        "value": ["-warp", "16"]
    }, {
        "label": "XH3M6: Ice Hold",
        "value": ["-warp", "19"]
    }, {
        "label": "XH3M6: Dark Citadel",
        "value": ["-warp", "20"]
    },

    {
        "label": "Expansion Hub 4: Transit",
        "value": ["-warp", "33"],
    }, {
        "label": "XH4M1: Transit (map)",
        "value": ["-warp", "33"]
    }, {
        "label": "XH4M2: Over N Under",
        "value": ["-warp", "34"]
    }, {
        "label": "XH4M3: Deathfog",
        "value": ["-warp", "35"]
    }, {
        "label": "XH4M4: Castle of Pain",
        "value": ["-warp", "36"]
    }, {
        "label": "XH4M5: Sewer Pit",
        "value": ["-warp", "37"]
    }, {
        "label": "XH4M6: The Rose",
        "value": ["-warp", "38"]
    }];

    const commonExecBase = {
        "files": {
            "main": {"path":"HEXEN.EXE"},
            "setup": {"path":"SETUP.EXE"},
            "ipxsetup": {"path":"IPXSETUP.EXE", optional:true},
            "sersetup": {"path":"SERSETUP.EXE", optional:true}
        },
        "runnables": {
            "multiplayer": {
                "file":(obj)=>{return obj?.GameRoom?.Params?.[GameDefsType.DOSBOX_NETTYPE_PARAM_ID]?.[0]==='serial'?'sersetup':'ipxsetup'},
                "runTool":vars.game.runToolOptsMultiplayer,
                beforeRun: common.ensureSerIpxSetupFiles(utils)
            },
            "settings": {"file":"setup", "runTool":vars.game.runToolOpts},
            "singleplayer": {"file":"main", "runTool":vars.game.runToolOpts}
        },
        "parameters": {
            "nodes": {
                "modeSupport": ["multiplayer"],
                "type": "static",
                "value": ["'-nodes'", "GameRoom.NumberOfPlayers"],
                "for":"private"
            },
            "episodeAndLevel": {
                "modeSupport": ["multiplayer", "singleplayer","settings"],
                "type": "choice",
                "label": "Episode/Level",
                "value":["value[0]", "value[1]"],
                "optional":true,
                "choices": null
            },
            "gameMode": {
                "modeSupport": ["multiplayer"],
                "type": "choice",
                "label": "Game Mode",
                "value":["value[0]", "value[1]"],
                "optional":true,
                "choices": [{
                    "label": "Deathmatch",
                    "value": ["-deathmatch", null]
                }, {
                    "label": "Deathmatch AltDeath",
                    "value": ["-deathmatch", "-altdeath"]
                }, {
                    "label": "Coop",
                    "value": [null, null]
                }]
            },
            "skill": {
                "modeSupport": ["multiplayer","singleplayer","settings"],
                "type": "choice",
                "label": "Skill",
                "value":["value"],
                "optional":true,
                "choices": [{
                    "label": "1",
                    "value": "1"
                }, {
                    "label": "2",
                    "value": "2"
                }, {
                    "label": "3",
                    "value": "3"
                }, {
                    "label": "4",
                    "value": "4"
                }, {
                    "label": "5",
                    "value": "5"
                }]
            },
            "noMonsters": {
                "modeSupport": ["multiplayer", "singleplayer", "settings"],
                "type": "boolean",
                "label": "No Monsters",
                "value": "'-nomonsters'"
            },
            "respawnMonsters": {
                "modeSupport": ["multiplayer", "singleplayer", "settings"],
                "type": "boolean",
                "label": "Respawn Monsters",
                "value": "'-respawn'"
            },
            "turbo": {
                "modeSupport": ["multiplayer", "singleplayer", "settings"],
                "type": "numrange",
                "min":0,
                "max":250,
                "delta":1,
                "label": "Turbo",
                "value": ["'-turbo'", "value"],
                "optional":true
            },
            "levelTimer": {
                "modeSupport": ["multiplayer", "singleplayer", "settings"],
                "type": "numrange",
                "min":0,
                "max":999,
                "delta":1,
                "label": "Timer (Minutes)",
                "value": ["'-timer'", "value"],
                "optional":true
            },
            "class": {
                "for":"private",
                "modeSupport": ["multiplayer","singleplayer","settings"],
                "type": "choice",
                "label": "Class",
                "value":["'-class'", "value"],
                "optional":true,
                "choices": [{
                    "label": "Fighter",
                    "value": "0"
                }, {
                    "label": "Cleric",
                    "value": "1"
                }, {
                    "label": "Mage",
                    "value": "2"
                }]
            },
            "dup": {
                "modeSupport": ["multiplayer"],
                "type": "boolean",
                "label": "TicDup",
                "description": "The -dup parameter duplicates tic commands (see ticdup).\nWhen this is specified, the same tic command is used for two consecutive tics; as a result, the network bandwidth is halved at a cost in movement accuracy.",
                "value": "'-dup'"
            },
            "extratic": {
                "modeSupport": ["multiplayer"],
                "type": "boolean",
                "label": "ExtraTic",
                "description": "The -extratic parameter provides extra data in each data packet sent over the network; this adds redundancy and makes the game more resilient to dropped packets at the possible cost of using more bandwidth (see extratic).",
                "value": "'-extratic'"
            },
            "com": {
                "modeSupport": ["multiplayer"],
                "type": "static",
                "value": [`GameRoom?.Params?.['${GameDefsType.DOSBOX_NETTYPE_PARAM_ID}']?.[0]=='serial'?'-com1':undefined`],
                "for":"private"
            },
            "baud": {
                "modeSupport": ["multiplayer"],
                "type": "static",
                "value": [`GameRoom?.Params?.['${GameDefsType.DOSBOX_NETTYPE_PARAM_ID}']?.[1]?('-'+GameRoom?.Params?.['${GameDefsType.DOSBOX_NETTYPE_PARAM_ID}']?.[1]):undefined`],
                "for":"private"
            }
        }
    }

    const shareware = deepClone(commonExecBase);
    shareware.parameters.episodeAndLevel.choices = Shareware_EpisodeAndLevels;

    const full = deepClone(commonExecBase);
    full.parameters.episodeAndLevel.choices = Full_EpisodeAndLevels;

    const dotdc = deepClone(commonExecBase);
    dotdc.parameters.episodeAndLevel.choices = DOTDC_EpisodeAndLevels;
    dotdc.files.main.path = 'HEXENDK.EXE';

    return {
        "name": "Hexen",
        "get": [{
            type:"gog",
            prodId:"1345971634",
            nameId:"heretic_hexen_collection",
            name:"Heretic + Hexen Collection"
        },{
            type:"gog",
            prodId:"1983497091",
            nameId:"hexen_deathkings_of_the_dark_citadel",
            name:"Hexen: Deathkings of the Dark Citadel"
        }],
        "executables": {
            "shareware": {
                "name":"Shareware (DOS)",
                ...shareware
            },
            "full": {
                "name":"Full (DOS)",
                ...full
            },
            "dotdc": {
                "name":"Deathkings of the Dark Citadel (DOS)",
                ...dotdc
            }
        }
    }
}
