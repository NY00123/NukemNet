export interface Room_CreateRequest {
    name: string,
    gameId: string,
    gameName: string,
    password?: string
    execId: string
    execName: string
    maxPlayers: number,
    playersIn?:number,
    isStarted?:boolean
    channel:string,
}

export interface Room_InMemory extends Room_CreateRequest {
    owner:string,
    createdAt:number
}

export interface Room_Advertisement extends Room_InMemory {
    hasPassword:boolean
    password:undefined // must not exist
}


export interface Room_UpdatePatchFromHost {
    name?: string,
    gameId?: string,
    gameName?: string,
    password?: string|null // null for deleting password
    execId?: string
    execName?: string
    maxPlayers?: number,
    playersIn?:number,
    isStarted?:boolean
}

// Should be almost indentical to "RoomUpdatePatchFromHost" interface
export interface Room_UpdatePatchFromBot extends Room_UpdatePatchFromHost {
    channel:string // channel is the identifier, required
    
    password:undefined // must not exist
    // patchable/deletable fields
    hasPassword?:boolean,
    owner?:string,
}