const {ipcRenderer} = require('electron');
const path = require('path');

const deps = require('./electron_deps');

// safely monkey patching the require call, to require cached prebuilt node modules from vercel ncc.
const Mod = require('module');
const req = Mod.prototype.require;
Mod.prototype.require = function () {
    const res = deps.cachedRequire(arguments[0]);
    if(res!==undefined) return res;
    return req.apply(this, arguments);
};

ipcRenderer.on('open-scheme-url', function (evt, message) {
    window.openUrl = message.url;
    if(window.handleUrl) window.handleUrl();
});

const _setImmediate = setImmediate;
process.once('loaded', async () => {
    global.setImmediate = _setImmediate; // This is required for some npm libs to work properly

    window.require = require;

    window.Buffer = Buffer;
    window.electron = {}
    Object.assign(window.electron, {
        PackageJson: require(path.resolve(__dirname, 'package.json')),
        electronDir: __dirname
    })
})