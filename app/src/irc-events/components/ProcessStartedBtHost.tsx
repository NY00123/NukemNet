import {Component} from 'solid-js'
import { IRCEvent_ProcessStartedByHost, IRCEvent_Registered } from '../ircEvents';
import { IrcEventCmp_Base } from './base';

export const IrcEventCmpCustom_ProcessStartedByHost:Component<IRCEvent_ProcessStartedByHost> = (props)=>{
    return <IrcEventCmp_Base {...props}>Game launched by host.</IrcEventCmp_Base>
}