
module.exports = function({}) {

    const commands = ['CONFIG -set "cycles=max"','CONFIG -set "core=auto"']
    const runToolOpts = {"type":"dosbox", "data":{commands}};
    const runToolOptsMultiplayer = {"type":"dosbox", "data":{commands, "net":"ipx"}};

    runnables = {
        "multiplayer": {
            "file":"main",
            "runTool":runToolOptsMultiplayer
        },
        "settings": {"file":"setup", "runTool": runToolOpts},
        "singleplayer": {"file":"main", "runTool": runToolOpts}
    };
    
    return {
        "name": "Carmageddon",
        "get": [{
            type:"gog",
            prodId:"1207659047",
            nameId:"carmageddon_max_pack",
            name:"Carmageddon Max Pack"
        }],
        "executables": {
            "demo": {
                "name":"Demo (DOS)",
                "files": {
                    "setup": {"path":"SNDSETUP.EXE"},
                    "main": {"path":"CARMA.EXE"}
                },
                "runnables": runnables,
                "parameters": {}
            },
            "full": {
                "name":"Full (DOS)",
                "mountImg": true,
                "files": {
                    "setup": {"path":"SNDSETUP.EXE"},
                    "main": {"path":"CARMA.EXE"}
                },
                "runnables": runnables,
                "parameters": {}
            },
            "splat_full": {
                "mountImg": true,
                "name":"Splat Pack (DOS)",
                "files": {
                    "setup": {"path":"SNDSETUP.EXE"},
                    "main": {"glob":["CARMA.EXE","SPLAT.EXE"]}
                },
                "runnables": {
                    "multiplayer": {
                        "file":"main",
                        "runTool":runToolOptsMultiplayer
                    },
                    "settings": {"file":"setup", "runTool": runToolOpts},
                    "singleplayer": {"file":"main", "runTool": runToolOpts}
                },
                "parameters": {}
            }
        }
    };
}