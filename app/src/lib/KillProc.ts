import { ChildProcess } from "child_process"
import { PsList, TreeKill } from "../NodeLibs";

export async function KillProc(proc:ChildProcess) {
    // kill process running inside cmd (when started with shell: true)
    try {
        const processesList = await PsList();
        const possiblePids = processesList.filter(p=>p.ppid === proc.pid).map(p=>p.pid);
        TreeKill(possiblePids[0]);
    } catch(e) {}
    // normal kill too
    try {proc.kill();} catch(e) {}
}