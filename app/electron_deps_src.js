
import * as axios from 'axios'
import * as psList from './electron_deps_esm/ps-list'

const cachedExports = {
    "matrix-org-irc": require('matrix-org-irc'),
    "fs/promises":require('fs/promises'),
    "fs":require('fs'), 
    "path": require('path'), 
    "child_process":require('child_process'), 
    "netstats":require('netstats'), 
    "process":require('process'), 
    "nat-upnp-2":require('nat-upnp-2'), 
    "nat-puncher": require('nat-puncher'), // Doesn't work on Windows XP, set = false
    "dgram": require('dgram'), 
    "util":require('util'), 
    "zlib":require('zlib'), 
    "ping":require("ping"), 
    "crypto":require('crypto'), 
    "stun":require('stun'),
    "events":require('events'), 
    "net":require('net'), 
    "tree-kill":require('tree-kill'), 
    "json5":require('json5'), 
    "dns":require('dns'), 
    "glob":require('glob'), 
    "systeminformation": require('systeminformation'), // Doesn't work on Windows XP, set = false
    "default-gateway":require('default-gateway'), 
    "nat-pmp":require('nat-pmp'), 
    "stream":require('stream'), 
    "http":require('http'), 
    "os":require('os'), 
    "sudo-prompt":require('sudo-prompt'),
    "node-kcp-x":require('node-kcp-x'),
    "ip2location-nodejs": require("ip2location-nodejs"),

    
    "ps-list":psList,
    "axios":axios,

    "@electron/remote":require('@electron/remote')
}

export function cachedRequire(moduleName) {
    if(cachedExports[moduleName]!==undefined) {
        return cachedExports[moduleName]
    }
}
