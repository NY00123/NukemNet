module.exports = function() {

    const commands = ['CONFIG -set "cycles=max"','CONFIG -set "core=auto"']

    const runToolOpts = {"type":"dosbox", "data":{commands}};
    const runToolOptsMultiplayer = {"type":"dosbox", "data":{commands, "net":"serial"}};


    const runnables = {
        "multiplayer": {
            "file":"main",
            "runTool":runToolOptsMultiplayer
        },
        "singleplayer": {"file":"main", "runTool": runToolOpts}
    };

    // TODO edit binary CONFIG.BIN Direct/COM1/baud on behalf of the user
    const tips = {html: `
    <h3>Both players do the following steps:</h3>
    <ol>
        <li>In game menu, tap Escape key</li>
        <li>Go right twice, to Communications</li>
        <li>tap Enter on "Two players OFF"</li>
        <li>Connection: Direct</li>
        <li>Port: COM1</li>
        <li>Baud rate: choose the same number as the other player</li>
        <li>Tap "Down" key until you reach the lowest entry "DONE?", then tap Enter</li>
        <li>Once the other player completes the above steps too, it will connect.</li>
    </ol>
    `};

    return {
        "name": "Vette!",
        "executables": {
            "full": {
                "maxPlayers":2,
                tips,
                "name":"Full (DOS)",
                "files": {
                    "main": {"path":"VETTE.EXE"}
                },
                "runnables": runnables,
                "parameters": {}
            }
        }
    };
}