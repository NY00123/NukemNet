
module.exports = function ({}) {

    const common = require('./hl_common.i')();

    const game = 'rewolf';

    return {
        "name": "Gunman Chronicles",
        executables: {
            "compat": {
                name:"Compat",
                "maxPlayers":32,
                midGameJoin: true,
                tips: {html:`<div>
                * Get the Steam version of the game from https://www.moddb.com/games/gunman-chronicles/downloads/gunman-chronicles-steam-version<br/>
                * You must launch Steam manually before launching the game.
                </div>`},
                "files": {
                    "dedicated": {glob:["hlds.exe", "hlds_run", "hlds"], runDetachedShell:true, optional:true},
                    "main": {glob:["hl.exe", "hl"]},
                    "maindir": {"path":game}
                },
                "runnables": {
                    "multiplayer": {
                        "file":(ctx)=>ctx?.GameRoom?.Params?._dedicated?.[0]?'dedicated':'main',
                        beforeRun: common.writeCommandsCfg
                    }
                },
                parameters: common.getParams(game)
            }
        }
    }
}