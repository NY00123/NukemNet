


import { Component } from 'solid-js';
const ChangeLog: Component = () => {
  return <div style={{"text-align":"center"}}>

  <h4>0.3.13 Alpha 2023-08-23</h4>
    <div style="font-size: 12pt; margin-left:auto; margin-right:auto; width:fit-content">
      <ul>
      <li>Smaller size, less files: 185MB inflated, 86MB deflated.</li>
      <li>Faster Boot.</li>
      <li>Windows exec code-signed.</li>
      <li>macOS support.</li>
      <li>p2p/ms choice for DOS duke3d, blood, redneck. p2p mode should be faster up to 4 players, thanks Noldor Ranzou for helping me test this switch.</li>
      <li>Patches for build games (mouse fix) and Chasm: The Rift (performance boot fix), in game Tips screen.</li>
      <li>Improved support: Worms Armageddon, Descent 1/2/3, Carmageddon.</li>
      <li>Bot does not need to enter the room to advertise it.</li>
      <li>Support /names IRC command</li>
      <li>List players and game params given a room, without being in it, right click on game room row.</li>
      <li>Show game tips button in more screens.</li>
      <li>Added hide-room toggle when creating a room.</li>
      <li>ip-to-country now uses local logic.</li>
      <li>New welcome sound by ZNukem</li>
      <li>usermaps/mods are now made with file symlinks, instead of junctions (folders), falls back to file copy if symlink failed.</li>
      <li>RoTT user maps</li>
      <li>Can now use relative paths in "Setup" window</li>
      <li>Fixed DOS Blood map support for long file names.</li>
      <li>Added DOSBox debug toggle (pause execution line by line).</li>
      <li>Fixed case sensitivity issues on non-windows systems.</li>
      <li>Support for RoTT custom maps, and RTS files (remote ridicule).</li>
      <li>Added packet dup arguments for dos build-engine games and doom based games.</li>
      <li>NN is now bundled with IPXSETUP.EXE and SERSETUP.EXE, for when doom based games don't have these files in the directory, NN copies it.</li>
      <li>GOG & Zoom-Platform links per game in "Setup" window.</li>
      <li>Many other fixes.</li>
      <li style="text-align:left">
        Game Support:
        <ol>
          <li>Quake 3 & Team Arena, Return to Castle Wolfenstein, Enemy Territory.</li>
          <li>GoldSrc Steam games: Half Life, Opposing Force, Deathmatch Classic, Team Fortress Classic, Ricochet, Day of Defeat, Sven Co-op.</li>
          <li>Theme Hospital</li>
          <li>Wings</li>
          <li>Transport Tycoon Deluxe, Terminator: SkyNET, Thanks DENCHIK for the suggestion.</li>
          <li>Sopwith, Battle Chess, F2 Retaliator, Thanks Fraggle for SDL Sopwith and those game suggestions.</li>
          <li>Armor Alley</li>
          <li>BMF</li>
          <li>Darklight Conflict</li>
          <li>GTA</li>
          <li>Vette!</li>
          <li>Dungeon Keeper</li>
          <li>Eradicator</li>
          <li>The Need for Speed: Special Edition</li>
          <li>Last Rites</li>
          <li>Z.A.R.</li>
          <li>Jagged Alliance (and Deadly Games)</li>
          <li>Cylindrix</li>
          <li>Q.A.D: Quintessential Art of Destruction</li>
        </ol>
      </li>
      </ul>
    </div>

    <h4>0.3.8 Alpha 2023-03-25</h4>
    <div style="font-size: 12pt; margin-left:auto; margin-right:auto; width:fit-content">
      <ul>
        <li>Auto FileSync support, sharing maps/mods.</li>
        <li>IPv6 Tunneling support (works across all IPv4 games too), IPv6 tunnels are auto-established to each game participant, when successful, bypassing the requirement of port-forward at all.</li>
        <li>Can turn IPv6 support off in Settings window.</li>
        <li>Can paste images/files from clipboard into channels.</li>
        <li>User can terminate his own game process.</li>
        <li>A keyboard shortcut can be assigned to terminate game process (Quiver).</li>
        <li>Added support for more Descent 1/2 source ports.</li>
        <li>Game support: Quake 2, Descent 3, Jazz Jackrabbit 2 Plus, Worms Armageddon</li>
        <li>Dedicated server option (no host participation) for applicable games, such as UT99, RoTT, Quake 2.</li>
        <li>Offline LAN Party mode: useful when playing over a LAN, without internet connection.</li>
        <li>Fix for NukemNet extensions style loading on linux systems.</li>
        <li>Hidden webkit devtools and top menubar triggered by Alt key, create an empty file "user/debug" to add devtools back.</li>
        <li>IRC connectivity prefers connecting on IPv6 when possible.</li>
        <li>Added port-forwarding/connectivity-checks for TCP, and ports other than the chosen game port number.</li>
        <li>Relay choices: as Fallback or Forced Mode (fallback make it only used for users who could not establish direct communication to host with NN Tunnels, STUN/IPv6)</li>
        <li>Added webserver admin for Unreal Gold</li>
        <li>Plenty of other Logic/GUI fixes.</li>
      </ul>
    </div>

    <h4>0.2.4 Alpha 2023-01-30</h4>
    <div style="font-size: 12pt; margin-left:auto; margin-right:auto; width:fit-content">
        <ul>
          <li>STUN-UDP Hole Punch support (alternative to port-forwarding), can combine with Relay as fallback.</li>
          <li>Game Support: Unreal Tournament, Unreal (Gold/Classic v226), Wacky Wheels.</li>
          <li>DOSBox Serial emulation with different baudrates (dosbox-staging only).</li>
          <li>Master volume for sound effects.</li>
          <li>Added "terminated" sound effect on process force-termination button.</li>
          <li>If UDP gameport is busy by another process, notifying with details.</li>
          <li>Initial maps/mods NetDuke32 support (temporary solution to avoid manual-args).</li>
          <li>Many other bugfixes and improvements</li>
        </ul>
    </div>

    <h4>0.2.2 Alpha 2023-01-21</h4>
    <div style="font-size: 12pt; margin-left:auto; margin-right:auto; width:fit-content">
        <ul>
          <li>Hotfix for game launching issue.</li>
        </ul>
    </div>

    <h4>0.2.1 Alpha 2023-01-20</h4>
    <div style="font-size: 12pt; margin-left:auto; margin-right:auto; width:fit-content">
        <ul>
          <li>Support for custom user extensions (customizing behaviour and styles).</li>
          <li>Support for playing offline, and using other irc servers, useful for extension development and LAN games with no internet connection.</li>
          <li>Better popup management, one does not close the other (stackable)</li>
          <li>Fixed bug where live room list updates were sometimes inaccurate, and RefreshRooms logic.</li>
          <li>Support for NotBlood (Blood source-port).</li>
          <li>Improved Rise of The Triad latency issue (now host spawns 2 instances, one server and the other client).</li>
          <li>Improved UDP port checker logic (had false-negatives in some cases depending on router).</li>
          <li>Added support for deeplinking, such as nukemnet://join/myChanName</li>
          <li>Added support for DXX-Rebirth (Descent 1&2 source-port).</li>
          <li>Added support for Redneck Rampage.</li>
          <li>Added button for room host, to terminate in-game processs for all participants.</li>
          <li>BugFix: When being kicked from a room, a reason is shown (e.g room is full).</li>
          <li>When joining a room where the game supports mid-game join, showing a modal popup asking to join.</li>
          <li>Added "find-optimal-reley" panel</li>
          <li>Moved all NukemNet logs and generated files under "user" directory.</li>
          <li>"Setup" window categorized by tabs.</li>
          <li>Support for custom user sound files.</li>
          <li>Speed up NukemNet startup logic (external IP finding).</li>
          <li>Chat-Sync with NetDuke32 & NotBlood.</li>
          <li>Can now run NukemNet from protected directories such as "Program Files" (user dir will be on %appdata%)</li>
          <li>Added 'Are you sure' popup before closing.</li>
          <li>Improved 'whois' response readability</li>
          <li>Added some styling.</li>
          <li>Updated all project dependencies.</li>
          <li>Lots of other changes and fixes (too many to write here)</li>
        </ul>
    </div>

    <h4>0.1.8.1 Alpha 2022-09-03</h4>
    <div style="font-size: 12pt; margin-left:auto; margin-right:auto; width:fit-content">
        <ul>
          <li>Support for UDP Relay using p2p mode: hDuke, xDuke, rDuke, nDuke (2 players only)</li>
        </ul>
    </div>

    <h4>0.1.8 Alpha 2022-09-02</h4>
    <div style="font-size: 12pt; margin-left:auto; margin-right:auto; width:fit-content">
        <ul>
          <li>Added mute toggle for sound effects</li>
          <li>Added UDP Relay (workaround when port cannot be forwarded),<br/>
          similar to IPX Relay, just that it works for non-dosbox games (e.g NetDuke32, NBlood, VoidSW).
          </li>
          <li>A list of relays are available for selection, instead of just one.</li>
          <li>On game launch, if dosbox exec location not defined, showing error popup.</li>
          <li>Changing game-port number forces restart of NukemNet now.</li>
          <li>If trying to join a room with game definitions that are missing, a popup will be shown.</li>
          <li>Updated dependencies to latest version, including Electron.</li>
        </ul>
    </div>

    <h4>0.1.7 Alpha 2022-08-21</h4>
    <div style="font-size: 12pt; margin-left:auto; margin-right:auto; width:fit-content">
        <ul>
          <li>BugFix: source-ports (hDuke, NetDuke32) launch with paths containing spaces</li>
          <li>Updated Dependencies</li>
        </ul>
    </div>

    <h4>0.1.6 Alpha 2022-08-17</h4>
    <div style="font-size: 12pt; margin-left:auto; margin-right:auto; width:fit-content">
        <ul>
          <li>Added game support "Z"</li>
          <li>Once hosted game room, if port not accessible, showing warning popup</li>
          <li>BugFix: Fixed UI Freeze that happened in some environments, triggered by "mode" irc event (thanks NY00123 for providing the logs!)</li>
          <li>BugFix: Did not reconnect when network changed (e.g switching wifi network)</li>
        </ul>
    </div>

    <h4>0.1.5 Alpha 2022-08-12</h4>
    <div style="font-size: 12pt; margin-left:auto; margin-right:auto; width:fit-content">
        <ul>
          <li>Added Refresh-Game-Room-List button (usually no need to refresh, it's updated live)</li>
          <li>Added Connection details per participant (for debugging), option when clicking on username</li>
          <li>Added IRC Colors support (read-only), so discord bridge messages will look nicer</li>
          <li>BugFix: Improved local/external IPv4 detection for some IP ranges (some IPs were detected as external instead of local, this could have prevented multiplayer games from starting correctly in rare cases)</li>
          <li>BugFix: Forbidden room name chars are removed when converted to IRC channel name</li>
          <li>BugFix: When connection timed out after 3 minutes, would sometimes do infinite connect-disconnect loop every few seconds</li>
        </ul>
    </div>

    <h4>0.1.4 Alpha 2022-08-09</h4>
    <div style="font-size: 12pt; margin-left:auto; margin-right:auto; width:fit-content">
        <ul>
          <li>Added hide-to-tray button</li>
          <li>BugFix: Sometimes NukemNet would fail to detect your external ip, now it will retry after a few seconds instead of quitting</li>
          <li>BugFix: After editting game details and parameters of existing room, sometimes would not update for all participants</li>
          <li>BugFix: MaxPlayers setting, sometimes would insta-kick players even when there is room left</li>
        </ul>
    </div>

    <h4>0.1.3 Alpha 2022-08-08</h4>
    <div style="font-size: 12pt; margin-left:auto; margin-right:auto; width:fit-content">
        <ul>
          <li>Updated Project Dependencies</li>
        </ul>
    </div>

    <h4>0.1.2 Alpha 2022-08-07</h4>
    <div style="font-size: 12pt; margin-left:auto; margin-right:auto; width:fit-content">
        <ul>
          <li>Added support for Worms United</li>
          <li>Various bug fixes</li>
        </ul>
    </div>

    <h4>0.1.1 Alpha 2022-08-05</h4>
    <div style="font-size: 12pt; margin-left:auto; margin-right:auto; width:fit-content">
        <ul>
          <li>Improved quake support</li>
          <li>Improved netduke32 support</li>
          <li>Added support for "In Pursuit of Greed"</li>
          <li>Improved <a target="_blank" href="https://www.winehq.org/">Wine</a> support, added in Setup menu</li>
          <li>Updated electron to v20 (from v19)</li>
          <li>Various bug fixes</li>
        </ul>
    </div>

    <h4>0.1.0 Alpha 2022-07-31</h4>
    <div style="font-size: 12pt; margin-left:auto; margin-right:auto; width:fit-content">
        <ul>
          <li>First release</li> 
        </ul>
    </div>
  </div>
};

export default ChangeLog;
