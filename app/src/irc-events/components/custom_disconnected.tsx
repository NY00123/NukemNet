import {Component} from 'solid-js'
import { IRCEventCustom_Disconnected, IRCEvent_Registered } from '../ircEvents';
import { IrcEventCmp_Base } from './base';

export const IrcEventCmpCustom_Disconnected:Component<IRCEventCustom_Disconnected> = (props)=>{
    return <IrcEventCmp_Base {...props}>You have been disconnected from IRC server.</IrcEventCmp_Base>
}