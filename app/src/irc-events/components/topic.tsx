import {Component} from 'solid-js'
import { IRCStylizeText } from '../../lib/IrcStyling';
import { IRCEvent_Topic } from '../ircEvents';
import { IrcEventCmp_Base } from './base';

export const IrcEventCmp_Topic:Component<IRCEvent_Topic> = (props)=>{

    const topic = IRCStylizeText(props.topic);
    if(props.command === 'TOPIC') {
        return <IrcEventCmp_Base {...props}> {props.nick} changes topic to {topic}</IrcEventCmp_Base>
    } else if(props.command === 'rpl_topicwhotime') {
        const dt = new Date(props.setAtEpochSeconds! * 1000).toLocaleString()
        return <IrcEventCmp_Base {...props}> Topic is {topic} since {dt}</IrcEventCmp_Base>
    } else {
        return <IrcEventCmp_Base {...props}> Topic is "{topic}" set by {props.nick}</IrcEventCmp_Base>
    }
}
