const child_process = require('child_process');
const fs = require('fs');
const path = require('path');

const thisFileName = path.basename(__filename);

const task = process.argv?.[2] || '';
const restArgs = process?.argv?.slice(3) || [];

if(task === 'build-deps') {
  const fsExtra = require('fs-extra');
  
  child_process.execSync('node electron_deps_build_esm.js');
  child_process.execSync('npx ncc build electron_deps_src.js -e "electron/main" -e electron --no-source-map-register -m -o ncc');
  child_process.execSync('npx ncc build electron_idx_deps_src.js -e "electron/main" -e electron --no-source-map-register -m -o ncce');

  fsExtra.copyFileSync('./ncc/index.js', './electron_deps.js');
  
  fsExtra.copyFileSync('./ncce/index.js', './electron_idx_deps.js');
  
  fsExtra.rmSync('./build', {recursive:true, force:true});
  if(fsExtra.existsSync('./ncc/build')) {
    fsExtra.moveSync('./ncc/build', './build');
  }
  
  fsExtra.rmSync('./ncc', {recursive:true, force:true});
  fsExtra.rmSync('./ncce', {recursive:true, force:true});
  fsExtra.rmSync('./electron_deps_esm', {recursive:true, force:true});

} else if(task === 'zip-pack') {
  const archiver = require('archiver');

  const platformAndArch = process.argv?.[3];
  if(!platformAndArch?.length) {
    return console.warn("Missing platform and arch in second argument.");
  }

  const version = require('./package.json').version;

  const dirnameToZip = `NukemNet-${platformAndArch}`;
  const dirnameWithVersion = `${dirnameToZip}-${version}`;

  const zipDir = path.resolve(__dirname, 'out', dirnameToZip)
  const zipFile = path.resolve(__dirname, 'out', dirnameWithVersion+'.zip')
  const output = fs.createWriteStream(zipFile);
  const archive = archiver('zip', {
    zlib: { level: 9 }
  });
  archive.pipe(output);

  archive.directory(zipDir, dirnameWithVersion);
  archive.finalize();
} else if(task === 'build-release-docker') {
  const buildEnv = restArgs?.[0];
  const platform = restArgs?.[1];

  const proc = child_process.fork(thisFileName, ['build-docker-image', buildEnv]);
  proc.once('exit', (code)=>{
    if(code!==0) return;
    child_process.fork(thisFileName, ['copy-from-docker', buildEnv, platform]);
  })

} else if(task === 'build-docker-image') {
  console.log('Starting snippet for building a NukemNet release via docker build');
  const buildEnv = restArgs?.[0];
  if(!buildEnv?.length) {
    console.error(`Missing build env argument.\nFor possible values, see directory names under /app/src/docker.`);
    return;
  }

  const dockerfilePath = path.resolve(`./src/docker/${buildEnv}/Dockerfile`)
  if(!fs.existsSync(dockerfilePath)) {
    console.error(`No such docker file: ${dockerfilePath}`);
    return;
  }
  
  console.log("Dockerfile found: " + dockerfilePath)
  console.log("Building...");

  const proc = child_process.spawn(`docker build -t nukemnet-${buildEnv} -f ./app/src/docker/${buildEnv}/Dockerfile .`, {
    cwd:'..',
    shell:true,
    detached: true
  })

  proc.on('exit', (code)=>{
    if(code === 0) {
      console.log("Docker build finished.")
    } else {
      console.error("Docker build failed.")
    }
    process.exit(code); // forward code to executor.
  });

  process.on('SIGINT', ()=>{
    proc.kill();
  })

} else if(task === 'copy-from-docker') {
  console.log('Starting snippet for copying NukemNet release from docker build');
  const dockerImageName = restArgs?.[0];
  const platform = restArgs?.[1];
  if(!dockerImageName?.length) {
    console.error("Missing docker image name");
    return;
  }
  if(!platform?.length) {
    console.error("Missing platform");
    return;
  }
  
  const zipFilePath = child_process.execSync(`docker run --platform ${platform} --rm nukemnet-${dockerImageName} find /nn/app/out/make/zip -name "NukemNet*"`).toString().trim();
  if(!zipFilePath?.length) {
    console.error('Release zip file not found in docker image');
  }
  console.log("Found file in docker image: " + zipFilePath);
  child_process.execSync(`docker run --platform ${platform} -v ${process.cwd()}/out/docker:/opt/out --rm nukemnet-${dockerImageName} cp ${zipFilePath} /opt/out`).toString().trim();
  console.log("Release ZIP file copied.")
  
} else if(task === 'use-electron-v18') {
  const pjson = require('./package.json');
  pjson.devDependencies.electron = "^18.3.15";
  pjson.devDependencies["@electron/remote"] = "git+https://github.com/aabluedragon/electron-remote.git";
  fs.writeFileSync('./package.json', JSON.stringify(pjson, null, 2));
} else {
  console.warn("Unknown Task: "+task);
}

