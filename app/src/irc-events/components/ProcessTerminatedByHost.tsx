import {Component} from 'solid-js'
import { IRCEventCustom_Disconnected, IRCEvent_ProcessTerminatedByHost, IRCEvent_Registered } from '../ircEvents';
import { IrcEventCmp_Base } from './base';

export const IrcEventCmpCustom_ProcessTerminatedByHost:Component<IRCEvent_ProcessTerminatedByHost> = (props)=>{
    return <IrcEventCmp_Base {...props}>Host terminated in-game process.</IrcEventCmp_Base>
}