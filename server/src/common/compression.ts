function wrequire(str:string) {
    try {
        // Electron
        return (window as any).require(str)
    } catch(e) {
        // NodeJS
        return require(str);
    }
}

const zlib = wrequire('zlib')
const util = require('util');

export const deflatePromise = util.promisify(zlib.deflate);
export const inflatePromise = util.promisify(zlib.inflate);

export async function compressStr(str:string): Promise<string> {
    const deflated = (await deflatePromise(str, {level:9})).toString('base64') as string;
    return deflated;
}

export async function decompressStr(deflated:string): Promise<string> {
    const inflated = (await inflatePromise(Buffer.from(deflated, 'base64'))).toString();
    return inflated;
}

