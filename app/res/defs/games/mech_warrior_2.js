module.exports = function() {
    const dosboxPreCommands = ['CONFIG -set "cycles=max"','CONFIG -set "core=auto"'];
    const runToolOpts = {"type":"dosbox", "data":{"commands":dosboxPreCommands}};
    const runToolOptsMultiplayer = {"type":"dosbox", "data":{"commands":dosboxPreCommands,"net":["ipx", "serial"]}};

    return {
        "name": "MechWarrior 2: Mercenaries",
        "executables": {
            "netmech": {
                "mountImg": true,
                "name":"NetMech (DOS)",
                "files": {
                    "setup": {"path":"NETSETUP.EXE"},
                    "main": {"path":"NETMECH.EXE"}
                },
                "runnables": {
                    "multiplayer": {
                        "file":"main",
                        "runTool":runToolOptsMultiplayer
                    },
                    "settings": {"file":"setup", "runTool":runToolOpts}
                },
                "parameters": {},
                "resources": {}
            }
        }
    };

}