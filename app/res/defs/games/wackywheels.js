module.exports = function() {

    const commands = ['CONFIG -set "cycles=max"','CONFIG -set "core=auto"']

    const runToolOpts = {"type":"dosbox", "data":{commands}};
    const runToolOptsMultiplayer = {"type":"dosbox", "data":{commands, "net":"serial"}};

    const runnables = {
        "multiplayer": {
            "file":"main",
            "runTool":runToolOptsMultiplayer
        },
        "settings": {"file":"setup", "runTool": runToolOpts},
        "singleplayer": {"file":"main", "runTool": runToolOpts}
    };

    const params = {
        "turbo": {
            "modeSupport": ["multiplayer", "singleplayer"],
            "type": "boolean",
            "label": "Turbo",
            "description": "Activate the turbo key brake + fire",
            "value": "'turbo'"
        },
        "jump": {
            "modeSupport": ["multiplayer", "singleplayer"],
            "type": "boolean",
            "label": "Jump",
            "description": "Activate the jump key brake + accelerate",
            "value": "'jump'"
        },
        "gameplay": {
            "modeSupport": ["multiplayer", "singleplayer"],
            "type": "choice",
            "label": "Gameplay",
            "value":"value",
            "optional":true,
            "choices": [{
                "label": "Fast",
                "value": "/2"
            },{
                "label": "Really Fast",
                "value": "/3"
            }]
        },
        "weapon99": {
            "modeSupport": ["multiplayer", "singleplayer"],
            "type": "choice",
            "label": "Weapon",
            "value":"value",
            "optional":true,
            "choices": [{
                "label": "99 Hedgehogs",
                "value": "hog"
            },{
                "label": "99 Ice Cubes",
                "value": "ice"
            }, {
                "label": "99 Fireballs",
                "value": "fire"
            }]
        },
        "debug": {
            "modeSupport": ["multiplayer", "singleplayer"],
            "type": "boolean",
            "label": "Debug",
            "description": "Create a ERR.LOG file as you play",
            "value": "'/debug'"
        }
    }

    return {
        "name": "Wacky Wheels",
        "get": [{
            type:"gog",
            prodId:"1207665483",
            nameId:"wacky_wheels",
            name:"Wacky Wheels"
        },{
            type:"zoom-platform",
            nameId:"wacky-wheels",
            name:"Wacky Wheels"
        }],
        "executables": {
            "full": {
                "maxPlayers":2,
                "name":"Full (DOS)",
                "files": {
                    "setup": {"path":"SETUP.EXE"},
                    "main": {"path":"WW.EXE"}
                },
                "runnables": runnables,
                "parameters": params,
            },
            "shareware": {
                "maxPlayers":2,
                "name":"Shareware (DOS)",
                "files": {
                    "setup": {"path":"SETUP.EXE"},
                    "main": {"path":"WW.EXE"}
                },
                "runnables": runnables,
                "parameters": params
            }
        }
    };
}