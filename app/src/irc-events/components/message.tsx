import {Component} from 'solid-js'
import { colorFromStr } from '../../lib/colorFromStr';
import { IRCEvent_ChannelMessage, IRCEvent_PrivateMessage } from '../ircEvents';
import { IrcEventCmp_Base } from './base';
import { isImgExtension, REGEX_WEB_URL_PATTERN_RFC1738 } from '../../lib/ImageUpload';
import { IRCStylizeText } from '../../lib/IrcStyling';
import {Electron} from '../../NodeLibs'


function urlify_imgify(textElements:HTMLSpanElement[]) {
    const resElements = [] as HTMLSpanElement[];
    for(const origEl of textElements) {
        const el = origEl.cloneNode(true) as HTMLSpanElement;
        
        const htmlStr = el.innerText.replace(REGEX_WEB_URL_PATTERN_RFC1738, function(url) {
            const isImg = isImgExtension(url);
            return `<a href="javascript:;" >${url}</a>` + (isImg? `<br/><img class="message-image" src="${url}" />`:'');
        })
    
        el.innerHTML = htmlStr;
        try {
            // trying because it might not be an anchor element.
            const anchor = el.children.item(0) as HTMLAnchorElement;
            anchor.onclick = ()=>{
                const address = anchor.innerText.includes('://')?anchor.innerText:`https://${anchor.innerText}`;
                Electron.shell.openExternal(address);
            }
        } catch(e) {}
        
        const images = el.querySelectorAll('img.message-image');
        if(images?.length) {
            images.forEach(img=>{
                img.addEventListener('click', function() {
                    const minified = img.classList.contains('message-image-minified');
                    if(minified) {
                        img.classList.replace('message-image-minified', 'message-image');
                    } else {
                        img.classList.add('message-image', 'message-image-minified');
                    }
                });
            })
        }

        resElements.push(el);
    }
    return resElements;
}


export const IrcEventCmp_Message:Component<IRCEvent_ChannelMessage|IRCEvent_PrivateMessage> = (props)=>{
    const style = {"background-color":colorFromStr(props.nick)};

    const stylizedElements = IRCStylizeText(props.message);
    const message = urlify_imgify(stylizedElements);

    if((props as IRCEvent_ChannelMessage).channel) {
        const cProp = props as IRCEvent_ChannelMessage;
        const fromSegment = props.isHistServ?'':<>{`<`}{cProp.opSign}<a style={style}>{cProp.nick}</a>{`>`}</>;
        // channel message
        // TODO onclick nick name, open message tab.
        return <IrcEventCmp_Base {...cProp}>{fromSegment}{message}</IrcEventCmp_Base>
    }

    // private message
    const fromSegment = props.isHistServ?'':<>{`<`}<a style={style}>{props.nick}</a>{`>`}</>
    return <IrcEventCmp_Base {...props}>{fromSegment}{message}</IrcEventCmp_Base>
}