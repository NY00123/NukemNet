
import * as IRC_Def from 'matrix-org-irc'
import * as FS_Type from 'fs/promises'
import * as FSSync_Type from 'fs'
import * as PATH_Type from 'path'
import * as CHILD_PROCESS_Type from 'child_process'
import * as DGRAM_Type from 'dgram'
import * as ElectronRemote_Type from '@electron/remote'
import * as ElectronType from 'electron';
import * as Util_Type from 'util'
import * as Zlib_Type from 'zlib'
import * as Crypto_TYPE from 'crypto'
import * as Axios_Type from 'axios'
import * as Events_TYPE from 'events'
import * as Net_TYPE from 'net'
import * as TreeKill_type from 'tree-kill'
import * as PsList_Type from 'ps-list'
import * as json5_Type from 'json5'
import * as DNS_Type from 'dns'
import * as GlobType from 'glob'
import * as SI_Type from 'systeminformation'
import * as KCPTYPE from 'node-kcp-x'
import * as AzureStorageBlob_TYPE from '@azure/storage-blob'
import * as Stream_TYPE from 'stream'
import * as http_type from 'http'
import * as os_TYPE from 'os'
import * as SudoPrompt_TYPE from 'sudo-prompt'

export const MatrixOrgIrc = require('matrix-org-irc') as typeof IRC_Def;
export const fs = require('fs/promises') as typeof FS_Type;
export const fsSync = require('fs') as typeof FSSync_Type;
export const path = require('path') as typeof PATH_Type;
export const child_process = require('child_process') as typeof CHILD_PROCESS_Type;
export const netstats = require('netstats') as any;
export const ps = require('process') as any
export const natUpnp2 = require('nat-upnp-2') as any
export const natPuncher = require('nat-puncher') as any
export const dgram = require('dgram') as typeof DGRAM_Type;
export const electronRemote = require('@electron/remote') as typeof ElectronRemote_Type
export const Electron = require('electron') as typeof ElectronType
export const util = require('util') as typeof Util_Type;
export const zlib = require('zlib') as typeof Zlib_Type
export const ping = require("ping") as any;
export const crypto = require('crypto') as typeof Crypto_TYPE
export const stun = require('stun');
export const events = require('events') as typeof Events_TYPE;
export const net = require('net') as typeof Net_TYPE;
export const TreeKill = require('tree-kill') as typeof TreeKill_type.default;
export const json5 = require('json5') as typeof json5_Type;
export const dns = require('dns') as typeof DNS_Type;
export const glob = require('glob') as typeof GlobType;
export const systeminformation = require('systeminformation') as typeof SI_Type;
const kcppath = 'node-kcp-x'; // We gotta do this because of the way node-kcp-x is written, otherwise vite complains during build.
export const kcp = require(kcppath) as typeof KCPTYPE;
export const defaultGateway = require('default-gateway') as any;
export const natPmp = require('nat-pmp') as any
export const AzureStorageBlob = AzureStorageBlob_TYPE;
export const stream = require('stream') as typeof Stream_TYPE;
export const http = require('http') as typeof http_type;
export const os = require('os') as typeof os_TYPE;
export const SudoPrompt = require('sudo-prompt') as typeof SudoPrompt_TYPE;
export const axios = require('axios').default as any as typeof Axios_Type.default;
export const PsList = require('ps-list').default as typeof PsList_Type.default;
export const ip2loc = require('ip2location-nodejs');

// Using nodejs adapter (instead of browser XHR adapter) to prevent CORS issues.
axios.defaults.adapter = ['http'];