import { Injectable, OnModuleInit } from '@nestjs/common';
import * as udp from 'dgram';

@Injectable()
export class AppService implements OnModuleInit {
  #maxRelayLife:number;
  public get maxRelayLife() {return this.#maxRelayLife;}

  private minPort:number
  private maxPort:number
  
  
  onModuleInit() {
    this.#maxRelayLife = parseInt(process.env.MAX_RELAY_LIFEMS);
    this.minPort = parseInt(process.env.PORT_MIN);
    this.maxPort = parseInt(process.env.PORT_MAX);
  }

  
  get amountOfPorts() {
    return this.maxPort-this.minPort;
  }

  private iterationPort:number
  getNextUdpPort() {
    if(!this.iterationPort) this.iterationPort = this.minPort;
    this.iterationPort++;
    if(this.iterationPort>this.maxPort) this.iterationPort=this.minPort;
    return this.iterationPort;
  }

  async getUdpRelayAndBind() {
    
    const newClient = udp.createSocket('udp6');

    let boundPort:number|null = null;
    for(let i=0;i<this.amountOfPorts;i++) {
      const port = this.getNextUdpPort();
      try {
        newClient.bind(port,'::');
        await new Promise((res,rej)=>{
          newClient.once('listening', ()=>{
            newClient.removeAllListeners();
            res(null);
          })
          newClient.once('error', (err)=>{
            newClient.removeAllListeners();
            rej(err)
          })
        });
        boundPort = port;
        break
      } catch(e) {}
    }
    if(!boundPort) {
      try {newClient.close()} catch(e) {}
      throw new Error('Could not bind to any port');
    }

    return newClient;
  }
}
