
const regionNames = new Intl.DisplayNames(
    ['en'], {type: 'region'}
);


export function CountryCodeToName(cc:string) {
    return regionNames.of(cc);
}