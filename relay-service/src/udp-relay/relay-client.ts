import * as UDP_Type from 'dgram';
function wrequire(str:string) {
  try {
      // Electron
      return (window as any).require(str)
  } catch(e) {
      // NodeJS
      return require(str);
  }
}

const udp = wrequire('dgram') as typeof UDP_Type;

import { parseForwardMessage, prepareForwardMessage, udp6a } from "./relay";

interface Male {
  ip:string,
  port:number
}

export class RelayClient {
  
  private connectionFrom?:UDP_Type.RemoteInfo;

  constructor(private address:string, private port:number, private socket:UDP_Type.Socket, private male?:Male) {

    socket.on('message',(msg,rinfo)=>{
      try {
        const obj = parseForwardMessage(msg);
        const key = `${obj.ip}_${obj.port}`;
        let endpoint = this.ipAndPortToEndpoint[key];
        if(!endpoint) {
          endpoint = this.ipAndPortToEndpoint[key] = {
            socket: udp.createSocket('udp6'),
            ip: obj.ip,
            port: obj.port
          };

          endpoint.socket.on('message', (msg,rinfo)=>{
              if(!this.male && (this?.connectionFrom?.port != rinfo.port || this?.connectionFrom?.address != rinfo.address)) {
                this.connectionFrom = rinfo;
              }
              const buf = prepareForwardMessage(endpoint.ip, endpoint.port, msg);        
              socket.send(buf,this.port, udp6a(this.address));
          });
        }

        if(this.male) {
          endpoint.socket.send(obj.buffer, this.male.port, udp6a(this.male.ip));
        } else if (this.connectionFrom) {
          endpoint.socket.send(obj.buffer, this.connectionFrom.port, udp6a(this.connectionFrom.address));
        }

      } catch(e) {}
    });

    this.sendForwardToMe();
    this.intervalTimer = setInterval(()=>{
      this.sendForwardToMe();
    },3000);
  }

  private intervalTimer:any

  destroy() {
    clearInterval(this.intervalTimer);
    try {this.socket.close();} catch(e) {}
    for(const id in this.ipAndPortToEndpoint) {
      const obj = this.ipAndPortToEndpoint[id];
      try {obj.socket.close();} catch(e) {}
      delete this.ipAndPortToEndpoint[id];
    }
  }

  private sendForwardToMe() {
    this.socket.send('_forward_to_me_',this.port, udp6a(this.address));
  }

  setMale(male:Male) {
    this.male = male;
  }

  setFemaleAndGetPort() {
    delete this.male;
    return this.boundPort;
  }

  public get boundPort() {
    return this.socket.address().port;
  }

  private static minPort = 10000;
  private static maxPort = 60000;
  private static iterationStart:number = RelayClient.minPort;

  public static async create(address:string, port:number, exceptLocalPort:number, male?:Male) {
    const relayUdpClient = await RelayClient.getUdpRelayAndBind(exceptLocalPort);
    return new RelayClient(address, port, relayUdpClient, male);
  }

  static async getUdpRelayAndBind(except:number) {
    
    const newClient = udp.createSocket('udp6')
  
    let boundPort:number|null = null;
    const amountPorts = RelayClient.maxPort-RelayClient.minPort;
    for(let port=RelayClient.iterationStart,i=0;
      i<amountPorts;
      port++,i++,RelayClient.iterationStart=port) {

      if(port>RelayClient.maxPort) port = RelayClient.iterationStart = RelayClient.minPort;

      if(port == except) continue;
      try {
        newClient.bind(port,'::');
        await new Promise((res,rej)=>{
          newClient.once('listening', ()=>{
            newClient.removeAllListeners();
            res(null);
          })
          newClient.once('error', (err)=>{
            newClient.removeAllListeners();
            rej(err)
          })
        });
        boundPort = port;
        console.log('UDP Relay bound to local port: '+port);
        break
      } catch(e) {}
    }
    this.iterationStart++;
    if(!boundPort) {
      try {newClient.close()} catch(e) {}
      throw new Error('Could not bind to any port');
    }
  
    return newClient;
  }

  private ipAndPortToEndpoint:{[ip_port:string]:{
    socket: UDP_Type.Socket,
    ip:string,
    port:number
  }} = {}

}

