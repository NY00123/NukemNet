
import { Component } from 'solid-js';

const OpenSource: Component = () => {
  return <div style="margin:35pt">
  <p>
  tl;dr - <a href='https://gitlab.com/aabluedragon/NukemNet' target='_blank'>Get the source code here</a>, <a href='https://gitlab.com/aabluedragon/NukemNet/-/wikis/home' target='_blank'>Read the dev wiki here</a>.
  </p>

  <p>
  Open sourcing a project you've poured your heart & effort into is a tough decision.<br/>
As you create something from the ground up, you begin to develop a deep attachment to it over time—dare I say fall in love with it, along with its ultimate goals accumulated along the way.<br/>
Your desire is to maintain its integrity, ensure it remains bug-free, troll-free, secure, and resilient against issues introduced by anyone who seeks to damage your project, hence why some users urged me not to take the step toward open source.<br/>
<br/>
This concern becomes even more pronounced when you consider the potential for those with malicious intentions to exploit vulnerabilities, troll & DDOS your servers, spawn incompatible clones, or devise "innovative" ways to ruin the experience for all involved, be it players, admins, or developers, which becomes relatively easier upon exposing the codebase of your entire stack.<br/>
<br/>
For those acquainted with the history of YANG (past multiplayer launcher) going open source, the cautionary tale is evident.<br/>
For those unfamiliar, you can refer to the following posts:<br/>
(1) Post by NY00123, YANG's author (click "Show" on "Getting it closed" spoiler).<br/>
<a href="https://forums.duke4.net/topic/700-the-yang-thread/page__view__findpost__p__377925" target='_blank'>https://forums.duke4.net/topic/700-the-yang-thread/page__view__findpost__p__377925</a><br/>
(2) Post by a community veteran named Radar<br/>
<a href="https://forums.duke4.net/topic/6523-eduke32-oldmp/page__view__findpost__p__160708" target='_blank'>https://forums.duke4.net/topic/6523-eduke32-oldmp/page__view__findpost__p__160708</a><br/>
<br/>
However, the flip side reveals the potential for this step to become the best thing that has happened to NukemNet since its inception in 2022.<br/>
The user interface could be refined, features expanded upon, bugs addressed, and all sorts of contributions welcomed.<br/>
and this might turn out well above everyone's expectations, fixated by past events.<br/>
<br/>
A multitude of tasks await attention, and fellow contributors may bridge gaps for aspects I'm less inclined to tackle, such as sleek or legacy user interfaces, animations, and related elements (detailed in the roadmap here: <a href="https://gitlab.com/aabluedragon/NukemNet/-/wikis/Roadmap" target='_blank'>https://gitlab.com/aabluedragon/NukemNet/-/wikis/Roadmap</a> ).<br/>
<br/>
Adopting this perspective has required a change in mentality on my part—a willingness to embrace the notion that control might slip away and unfortunate outcomes could compromise all the effort and hope invested in this project. It's undoubtedly a substantial gamble, not just for me, but for any NukemNet participants. The poll results indicate that the majority of you are in favor, hence the decision to move towards open source (look mom, I can play a democrat too).<br/>
<br/>
This does necessitate a responsible approach, both on my part and on the part of those who intend to take the code for a spin.<br/>
The license will contain additional restrictions to ensure no malicious intent,<br/>
no creation of clones under different trademarks,<br/>
no incompatible clones,<br/>
no significant deviation from mainline NukemNet (preventing community splittage in half, thirds, or more as we previously seen taking place.).<br/>
a mandate that all modifications be openly shared and contributed back to the primary repository—ensuring responsible stewardship.<br/>
<br/>
I'll continue to be the sole financier, taking responsibilities for the various supporting services—Servers, Relays, Azure storage, Code Signing Certificates, Domain, and the like.<br/>
<br/>
I'm choosing to end with the following note, if you truely love something you should [be prepared to] set it free ("borrowing" from Unreal Engine promo, sorry Epic, but you probably taken insparation from others as well).<br/>
  </p>
  </div>
};

export default OpenSource;
