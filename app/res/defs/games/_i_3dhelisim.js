module.exports = function() {

    const commands = ['CONFIG -set "cycles=max"','CONFIG -set "core=auto"']

    const runToolOpts = {"type":"dosbox", "data":{commands}};
    const runToolOptsMultiplayer = {"type":"dosbox", "data":{commands, "net":"serial"}};

    const runnables = {
        "multiplayer": {
            "file":"main",
            "runTool":runToolOptsMultiplayer
        },
        "singleplayer": {"file":"main", "runTool": runToolOpts}
    };

    return {
        "name": "3-D Helicopter Simulator",
        "executables": {
            "full": {
                "maxPlayers":2,
                "name":"Full (DOS)",
                "files": {
                    "main": {"path":"HELI.EXE"}
                },
                "runnables": runnables,
                "parameters": {},
            }
        }
    };
}