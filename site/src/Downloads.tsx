


import { Component } from 'solid-js';
const Downloads: Component = () => {
  return <section style={{"text-align":"center"}}>
    <h4>Various helpful downloads</h4>

    <div style="font-size: 12pt; margin-left:auto; margin-right:auto; width:fit-content;background-color:#06063c">
        <a style="font-weight:bold; font-size:16pt" href="https://nukemnet.blob.core.windows.net/misc/netduke32_complete_fun_pack.zip">NetDuke32 Complete Fun Pack (2023-01-14)</a><br/>
        Includes:<br/>
        <div style="text-align:left">
          <ul>
            <li>NetDuke32 V1.2.1 (2023-08-14)</li>
            <li>Multiplayer Mods: StrikerDM, Duke64 (Nintendo 64), Vacation (Life's a Beach)</li>
            <li>
              Usermaps
              <ul>
                <li>bdbrosmaps: BuildEngine Bros (2023-07-15)</li>
                <li>others (2023-01-14)</li>
              </ul>
            </li>
          </ul>
        </div>
        Just copy <u><b style="background-color:blue">DUKE3D.GRP</b></u> and <u><b style="background-color:blue">DUKE3D.RTS</b></u> from your original copy of Duke3D Atomic.
    </div><br/>

    <div style="font-size: 12pt; margin-left:auto; margin-right:auto; width:fit-content">
        <a href="https://nukemnet.blob.core.windows.net/misc/HDUKE_18K_B64.zip">hDuke Version 18K B64 2022-09-20 (Windows) put DUKE3D.GRP in the folder</a>  
    </div><br/>

    <div style="font-size: 12pt; margin-left:auto; margin-right:auto; width:fit-content">
        <a href="https://nukemnet.blob.core.windows.net/misc/nDuke_v2.0_beta4.zip">nDuke 2.0 Beta 4 (Windows)</a>  
    </div><br/>

    <div style="font-size: 12pt; margin-left:auto; margin-right:auto; width:fit-content">
        <a href="https://nukemnet.blob.core.windows.net/misc/NO_GRP_DukeAtomicDos.zip">
          Duke3D Atomic with Mouse fix and WASD keys+mouse cfg (DOS)
        </a><br/>
        <span style={{"font-weight":"bolder"}}>*It does not include the GRP file*</span>: After extracting the zip, put duke3d.grp file from your original copy, into the extracted folder.
    </div><br/>

    <div style="font-size: 12pt; margin-left:auto; margin-right:auto; width:fit-content">
        <a href="https://dukeworld.com/eduke32/synthesis/latest" target='_blank'>VoidSW</a><br/>
        Shadow Warrior Modern Source-Port (bundled with eduke32 release).
    </div><br/>

    <div style="font-size: 12pt; margin-left:auto; margin-right:auto; width:fit-content">
        <a href="https://lerppu.net/wannabethesis" target='_blank'>NBlood</a> and <a href="https://github.com/clipmove/NotBlood/releases" target='_blank'>NotBlood</a><br/>
        Blood (Game) Modern Source-Ports
    </div><br/>

    <div style="font-size: 12pt; margin-left:auto; margin-right:auto; width:fit-content">
        <a href="https://www.dxx-rebirth.com/download-dxx-rebirth" target='_blank'>DXX-Rebirth</a><br/>
        Descent 1&2 Modern Source-Ports
    </div><br/>

    <div style="font-size: 12pt; margin-left:auto; margin-right:auto; width:fit-content">
        <a href="https://nukemnet.blob.core.windows.net/misc/hyang.zip">
          YANG
        </a><br/>
        an old legacy multiplayer launcher by NY00123, hyang version
    </div><br/>
    
  </section>
};

export default Downloads;
