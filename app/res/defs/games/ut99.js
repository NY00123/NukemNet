module.exports = function() {

    function chosenGameMode(ctx) {
        return ctx.GameRoom.Params.mode[0];
    }

    const iniEdits = [{"cmd":"edit-ini",addIf:(ctx)=>ctx?.GameRoom?.ImHost==true, "originFile":"gameini", "file":"nnini", "edits":[
        {"category":"UWeb.WebServer","entry":"Applications[0]", unquote:true, "value":"'UTServerAdmin.UTServerAdmin'"},
        {"category":"UWeb.WebServer","entry":"ApplicationPaths[0]", unquote:true, "value":"'/ServerAdmin'"},

        {"category":"UWeb.WebServer","entry":"Applications[1]", unquote:true, "value":"'UTServerAdmin.UTImageServer'"},
        {"category":"UWeb.WebServer","entry":"ApplicationPaths[1]", unquote:true, "value":"'/images'"},
        {"category":"UWeb.WebServer","entry":"DefaultApplication", unquote:true, "value":"'0'"},
        {"category":"UWeb.WebServer","entry":"bEnabled", unquote:true, "value":"'True'"},
        {"category":"UWeb.WebServer","entry":"ListenPort", unquote:true, "value":"GameRoom.MyPort"},
        {"category":"UWeb.WebServer","entry":"MaxConnections", unquote:true, "value":"'30'"},

        {"category":"UTServerAdmin.UTServerAdmin","entry":"AdminUsername", unquote:true, "value":"''"},
        {"category":"UTServerAdmin.UTServerAdmin","entry":"AdminPassword", unquote:true, "value":"''"},
        
        {"category":"Engine.GameInfo","entry":"AdminPassword", unquote:true, "value":"''"},
        {"category":"Engine.GameInfo","entry":"GamePassword", unquote:true, "value":"''"},
        
        {"category":"Engine.GameInfo","entry":"MaxPlayers", unquote:true, "value":"16"},

        {"category":chosenGameMode,"entry":"NetWait", unquote:true, "value":"0"},
        {"category":chosenGameMode,"entry":"RestartWait", unquote:true, "value":"0"},
        {"category":chosenGameMode,"entry":"bMultiWeaponStay", unquote:true, "value":"GameRoom?.Params?.wpnstay?.[0]?'True':'False'"},
        {"category":chosenGameMode,"entry":"bForceRespawn", unquote:true, "value":"GameRoom?.Params?.forcerspwn?.[0]?'True':'False'"},
        {"category":chosenGameMode,"entry":"bChangeLevels", unquote:true, "value":"GameRoom?.Params?.chnglvls?.[0]?'True':'False'"},
        {"category":chosenGameMode,"entry":"bUseTranslocator", unquote:true, "value":"GameRoom?.Params?.translocator?.[0]?'True':'False'"},

        {"category":chosenGameMode,"entry":"MinPlayers", unquote:true, "value":"GameRoom?.Params?.minplyrs||0"},
        {"category":chosenGameMode,"entry":"FragLimit", unquote:true, "value":"GameRoom?.Params?.frglmt||0"},
        {"category":chosenGameMode,"entry":"TimeLimit", unquote:true, "value":"GameRoom?.Params?.timelmt||0"}        
    ]}]

    const runnables = {
        "multiplayer": {
            "file":(obj)=>{return obj?.GameRoom?.ImHost?'ucc':'main'},
            "beforeRun":iniEdits,
            hostAlsoRunsAsClient:true
        },
        "settings": {"file":"main"},
        "singleplayer": {"file":"main"}
    };

    const params = {
        "vidsetup": {
            "modeSupport": ["settings"],
            "type": "static",
            "value": [`'-firstrun'`],
            "for":"private"
        },
        "server": {
            "modeSupport": ["multiplayer"],
            "type": "static",
            "value": ["'server'", "GameRoom.Params.map+'?alladmin?game='+GameRoom.Params.mode[0]", "'port='+GameRoom.MyPort", "'multihome=0.0.0.0'", "'ini=NukemNet.ini'"
            ],
            "for":"host-only-private",
        },
        "mode": {
            "modeSupport": ["multiplayer"],
            "type": "choice",
            "label": "Mode",
            "value":"value",
            "choices": [{
                "label": "Deathmatch",
                "value": "Botpack.DeathMatchPlus"
            },{
                "label": "Team DeathMatch",
                "value": "Botpack.TeamGamePlus"
            },{
                "label": "Last Man Standing",
                "value": "Botpack.LastManStanding"
            },{
                "label": "Assault",
                "value": "Botpack.Assault"
            }, {
                "label": "CTF",
                "value": "Botpack.CTFGame"
            }, {
                "label": "Domination",
                "value": "Botpack.Domination"
            }],
            "for":"host-only-shared",
            syncOnly: true
        },
        "map": {
            "modeSupport": ["multiplayer"],
            "type": "dirfiles",
            "value": ["value[0]"],
            "label": "Map",
            "path": "Maps/*.unr",
            "stripExt": true,
            "stripPath":true,
            "for":"host-only-shared",
            syncOnly: true
        },
        "client": {
            "modeSupport": ["multiplayer"],
            "type": "static",
            "value": ["'unreal://' + GameRoom.DestIp + ':'+GameRoom.DestPort"],
            "for":"client-only-private"
        },
        "wpnstay": {
            "modeSupport": ["multiplayer"],
            "type": "boolean",
            "label": "Weapon Stay",
            "description": "bMultiWeaponStay",
            "value": "'True'",
            "for":"host-only-shared",
            syncOnly: true
        },
        "forcerspwn": {
            "modeSupport": ["multiplayer"],
            "type": "boolean",
            "label": "Force Respawn",
            "description": "bForceRespawn",
            "value": "'True'",
            "for":"host-only-shared",
            syncOnly: true
        },
        "chnglvls": {
            "modeSupport": ["multiplayer"],
            "type": "boolean",
            "label": "Change Levels",
            "description": "bChangeLevels",
            "value": "'True'",
            "for":"host-only-shared",
            syncOnly: true
        },
        "translocator": {
            "modeSupport": ["multiplayer"],
            "type": "boolean",
            "label": "Translocator",
            "description": "bUseTranslocator",
            "value": "'True'",
            "for":"host-only-shared",
            syncOnly: true
        },
        "minplyrs": {
            "modeSupport": ["multiplayer"],
            "type": "numrange",
            "min":0,
            "max":16,
            "delta":1,
            "label": "Min Players",
            "description": "Bots",
            "value": "value",
            "optional":true,
            "for":"host-only-shared",
            syncOnly: true
        },
        "frglmt": {
            "modeSupport": ["multiplayer"],
            "type": "numrange",
            "min":0,
            "max":100,
            "delta":1,
            "label": "Frag Limit",
            "value": "value",
            "optional":true,
            "for":"host-only-shared",
            syncOnly: true
        },
        "timelmt": {
            "modeSupport": ["multiplayer"],
            "type": "numrange",
            "min":0,
            "max":100,
            "delta":1,
            "label": "Time Limit",
            "description": "Time Limit (minutes)",
            "value": "value",
            "optional":true,
            "for":"host-only-shared",
            syncOnly: true
        }
    }

    const execDef = {
        "maxPlayers":16,
        "hostInfoOnLaunch": (ctx)=>`Server Management in: http://${ctx.GameRoom.DestIp}:${ctx.GameRoom.MyPort}`,
        midGameJoin: true,
        networking: {
            tcpPort: {port:7777, optional:true},
            udpPort: {port:7777},
            secondaryPorts:[{type:"udp",port:8777, constant:true}]
        },
        "files": {
            "main": {"path":"System/UnrealTournament.exe"},
            "ucc": {"path":"System/ucc.exe", runDetachedShell:true},
            "gameini": {"path":"System/UnrealTournament.ini"},
            "nnini": {"path":"System/NukemNet.ini", optional:true}
        },
        "runnables": runnables,
        "parameters": params,
    }

    return {
        "name": "Unreal Tournament",
        "executables": {
            "full": {
                "name":"Full",
                ...execDef
            },
            "demo": {
                "name":"Demo",
                ...execDef
            }
        }
    };
}