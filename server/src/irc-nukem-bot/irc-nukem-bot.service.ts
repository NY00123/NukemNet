import { Injectable } from '@nestjs/common';

import {BotMetaChannel, BotMetaChannel_lowercase, BotName, BotName_lowercase, IrcUserMod, IrcUserModSigns_High, IrcUserMod_CmdToSign, maxChannelNameLength, NukemNetLobby, NukemNetLobby_lowercase } from '../common/constants'
import { IrcClientExtra } from '../common/ircExtend';
import *  as NNCmd from '../common/commands';
import { RoomMgrService } from '../room-mgr/room-mgr.service';
import { IrcService } from '../irc/irc.service';
import {Checkers} from '../common/checkers'
import { RelayInstanceInfo, RelayService } from '../relay/relay.service';
import * as sleep from 'sleep-promise';
import { NNPB } from '../common/protobuf/export';
import { DiscordService } from '../discord/discord.service';


interface RelayResultWithEndTimestamp {
    endTime: number
}

const __cooldownRelayMs = 1000*10
const relayRequestCooldown = new Set<string>()
@Injectable()
export class IrcNukemBotService {

    nickToProps:{[lnick:string]: {
        relay?: RelayInstanceInfo
    }} = {}

    allPlayersInMetaChannel():string[] {
        const usersMap = new Set<string>();
        const metaChanData = this.irc.client.chans.get(BotMetaChannel_lowercase) || this.irc.client.chans.get(BotMetaChannel);
        if(metaChanData) {
            Array.from(metaChanData.users.keys()).forEach(n=>usersMap.add(n))
        }
        const botNick = this?.irc?.client?.nick;
        if(botNick?.length) {
            usersMap.delete(botNick);usersMap.delete(botNick.toLowerCase());
        }
        return Array.from(usersMap);
    }

    constructor(private roomMgrService:RoomMgrService, private irc:IrcService, private relay:RelayService, private discordService:DiscordService) {

        this.irc.client.on('registered', ()=>{
        })

        const changeMode = (add:boolean, channel:string, by:string, mode:string, setOn:string, message:any)=>{
            const lchan = channel.toLowerCase();
            const lseton = setOn.toLowerCase();
            const destIsUser = !lseton.startsWith('#');
            if(destIsUser) {
              const sign = IrcUserMod_CmdToSign[mode];
              if(sign.length && add && lseton == this.irc.client.nick.toLowerCase() && lchan === BotMetaChannel_lowercase) {
                this.irc.client.send('MODE', BotMetaChannel, '+m'); // Mute everyone except ops on meta channel
              }
            }
          }
        
        this.irc.client.on('+mode', (channel:string, by:string, mode:string, argument:any, message:any)=>{
            changeMode(true, channel, by, mode, argument, message)
        })
    
        this.irc.client.on('-mode', (channel:string, by:string, mode:string, argument:any, message:any)=>{
            changeMode(false, channel, by, mode, argument, message)
        })

        this.irc.client.on('message', (nick:string, to, text, message)=>{
            try {
                if(!nick.startsWith('#') && message.command === 'PRIVMSG' && to === this.irc.client.nick) {
                    this.handlePrivateMessage(nick,text,message);
                }
            } catch(e) {
                console.error('message error: ' + e.stack)
            }
        })

        this.irc.client.on('error', (err)=>{
            console.log(err);
        })

        this.irc.client.on('nick', async (oldnick:string, newnick:string)=>{
            const lOldNick = oldnick.toLowerCase();
            const lNewNick = newnick.toLowerCase();

            if(lOldNick in this.nickToProps) {
                this.nickToProps[lNewNick] = this.nickToProps[lOldNick];
                delete this.nickToProps[lOldNick];
            }

            this.discordService.drawPlayersList(this.allPlayersInMetaChannel());
        });

        this.irc.client.on(`kick`, (channel:string, nick:string)=>{
            this.commonLogicNickDisconnected(nick)
        })
        this.irc.client.on(`part`, (channel:string, nick:string)=>{
            this.commonLogicNickDisconnected(nick)
        })
        this.irc.client.on(`kill`, (nick:string)=>{
            this.commonLogicNickDisconnected(nick)
        })
        this.irc.client.on(`quit`, (nick:string)=>{
            this.commonLogicNickDisconnected(nick)
        })

        this.irc.client.on('join', async (channel:string)=>{

            // Draw ascii table of players in discord channel
            const lchan = channel.toLowerCase();
            if(lchan === BotMetaChannel_lowercase) {
                try {
                    await IrcClientExtra.getNicksInChannel(this.irc.client, lchan);
                    await this.discordService.drawPlayersList(this.allPlayersInMetaChannel());
                } catch(e) {}
            }
        })
    }

    async commonLogicNickDisconnected(nick:string) {
        this.destroyRelayForNickIfHeHasOne(nick);

        // waiting for next iteration, so nick will be removed.
        setTimeout(()=>{
            this.discordService.drawPlayersList(this.allPlayersInMetaChannel());
        });
    }

    destroyRelayForNickIfHeHasOne(nick:string, sendCmdNotify?:boolean) {
        const lnick = nick.toLowerCase();
        const nickProps = this.nickToProps[lnick];
        if(nickProps?.relay) {
            const relay = nickProps.relay;
            delete nickProps.relay;

            try {
                this.relay.stopLease(relay.id);
            } catch(e) {}

            if(sendCmdNotify) {
                const cmd = {
                    op:'relayClosed',
                    data: {}
                } as NNCmd.Cmd_RelayClosed;
                IrcClientExtra.sendNNCmdTo(this.irc.client, nick, cmd);
                return cmd;
            }
        }
    }

    ops = {

        manageRelayRequest: async (from:string, cmd:NNCmd.Cmd_ManageRelayRequest_Base, message:any):Promise<NNCmd.Cmd_Base> =>{

            // verify fields
            if(cmd?.data?.action === 'destroy') {
            } else if(cmd?.data?.action === 'fetch') {
                const fCmd = cmd as NNCmd.Cmd_ManageRelayRequestFetch;
                if(
                    (fCmd?.data?.type != 'udp') ||
                    !fCmd?.data?.id?.length
                    ) {
                        throw new Error('bad relay fetch command structure')
                    }
            } else {
                throw new Error('invalid relay manage command action')
            }

            let retVal:any = null;
            
            if(cmd.data.action === 'fetch') {
                let updatedNick = from;
                {
                    const fCmd = cmd as NNCmd.Cmd_ManageRelayRequestFetch;
                    const lnick = updatedNick.toLowerCase();
                    if(this.nickToProps[lnick]?.relay) {
                        const r = this.nickToProps[lnick]?.relay;
                        if(r.type == fCmd.data.type) {
                            // if requested same type of relay, return existing.
                            return {
                                op:'spawnRelayCreated',
                                data: {
                                    conn: {
                                        ipOrHostname: r.def.host,
                                        port: r.port,
                                        type: r.type,
                                        terminationTimestamp: r.endOfLifeTS
                                    }
                                }
                            } as NNCmd.Cmd_ManageRelayCreated;
                        } else {
                            // if requested different type of relay, delete previous and return new
                            this.relay.stopLease(r.id);
                            delete this.nickToProps[lnick].relay
                        }
                    }

                    if(relayRequestCooldown.has(lnick)) {
                        return {
                            op:'nnerror',
                            error: `Cooldown between relay request has not finished yet ${__cooldownRelayMs/1000} seconds`
                        } as NNCmd.Cmd_ErrorResponse
                    }
                }

                const onNickChange = (oldnick:string, newnick:string)=>{
                    if(oldnick.toLowerCase()===updatedNick.toLowerCase()) {
                        updatedNick = newnick;
                    }
                };
                this.irc.client.on('nick', onNickChange)

                try {
                    const fetchCmd = cmd as NNCmd.Cmd_ManageRelayRequestFetch;
                    const lease = await this.relay.leaseNewRelay(fetchCmd.data.id, fetchCmd.data.type);
                    const lUpdatedNick = updatedNick.toLowerCase();

                    relayRequestCooldown.add(lUpdatedNick)
                    setTimeout(()=>{
                        relayRequestCooldown.delete(lUpdatedNick);
                    }, __cooldownRelayMs)

                    if(!this.nickToProps[lUpdatedNick]) this.nickToProps[lUpdatedNick] = {}
                    this.nickToProps[lUpdatedNick].relay = lease;
    
                    retVal = {
                        op:'spawnRelayCreated',
                        data: {
                            conn: {
                                ipOrHostname: lease.def.host,
                                port: lease.port,
                                type: fetchCmd.data.type,
                                terminationTimestamp: lease.endOfLifeTS
                            }
                        }
                    } as NNCmd.Cmd_ManageRelayCreated;
                } catch(e) {
                    retVal = {
                        op:'nnerror',
                        error: 'Could not create lease'
                    } as NNCmd.Cmd_ErrorResponse;
                } finally {
                    this.irc.client.removeListener('nick', onNickChange);
                }
            } else if(cmd.data.action === 'destroy') {
                retVal = this.destroyRelayForNickIfHeHasOne(from, true);
            }
            return retVal;
        },

        deleteRoom: async (from:string, cmd:NNCmd.Cmd_DeleteRoom, message:any):Promise<NNCmd.Cmd_Base> =>{
            if(!cmd?.data?.channel) return;

            const room = this.roomMgrService.getGameRoomByOwnerAndChannel(from, cmd.data.channel)
            if(room) {
                this.roomMgrService.deleteRooms(room.channel);
            }
        },

        createRoom: async (from:string, cmd:NNCmd.Cmd_CreateRoom, message:any):Promise<NNCmd.Cmd_Base> =>{
            Checkers.Cmd_CreateRoom.strictCheck(cmd)

            if(!cmd?.data?.channel?.length) {
                return {op:'nnerror', error:'Channel name missing'} as NNCmd.Cmd_ErrorResponse;
            }

            if(cmd?.data?.channel?.length > maxChannelNameLength) {
                return {op:'nnerror', error:'Channel name length should be no longer than '+maxChannelNameLength} as NNCmd.Cmd_ErrorResponse;
            }

            if(!cmd.data.channel.startsWith('#')) {
                return {op:'nnerror', error:'Channel must start with #'} as NNCmd.Cmd_ErrorResponse;
            }
            if(this.roomMgrService.channelRoomTaken(cmd.data.channel)) {
                return {op:'nnerror', error:'Channel already advertised as game room'} as NNCmd.Cmd_ErrorResponse;
            }
            const roomChannelName = cmd.data.channel;

            const makeSureHostIsOwnerAtChannel = async ()=>{
                try {
                    // Get names and modes per user, for channel
                    const namesAndModMap = await IrcClientExtra.getNamesForChannel(this.irc.client, roomChannelName);

                    const lhost = from.toLowerCase();

                    // Make sure the user is the only op on that channel
                    let hostIsInChannel = false
                    for(const nick in namesAndModMap) {
                        const mode = namesAndModMap[nick];
                        const lnick = nick.toLowerCase();
                        if(lnick===lhost) {
                            if(!IrcUserModSigns_High.has(mode)) {
                                return {op:'nnerror', error:`You're not an operator on that channel`} as NNCmd.Cmd_ErrorResponse;
                            }
                            hostIsInChannel = true;
                        } else if(lnick == BotName_lowercase) {}
                        else {
                            if(IrcUserModSigns_High.has(mode)) {
                                return {op:'nnerror', error:`There are other operator users on that channel`} as NNCmd.Cmd_ErrorResponse;
                            }
                        }
                    }
                    if(!hostIsInChannel) {
                        return {op:'nnerror', error:`You are not on that channel`} as NNCmd.Cmd_ErrorResponse;
                    }

                    this.roomMgrService.createRoom(cmd.data, from, roomChannelName);
                    
                    return {op:'roomCreated', data:{channel: roomChannelName}} as NNCmd.Cmd_CreateRoomResponse;
                } catch(e) {
                    return {op:'nnerror', error:e.message} as NNCmd.Cmd_ErrorResponse;
                }
            }

            if(this.roomMgrService.ownerHasRoom(from)) {
                return {op:'nnerror', error:'You already have a room'} as NNCmd.Cmd_ErrorResponse;
            }

            const result = await makeSureHostIsOwnerAtChannel();

            return result;
        },
        getAllRooms: async (from:string, cmd:NNCmd.Cmd_GetAllRooms):Promise<NNCmd.Cmd_GetAllRoomsResponse> =>{
            Checkers.Cmd_GetAllRooms.strictCheck(cmd);

            return {
                op: 'getAllRoomsResponse',
                data: {
                    rooms: this.roomMgrService.getAllRooms()
                }
            } as NNCmd.Cmd_GetAllRoomsResponse;
        },
        requestUpdateRoom: async (nick:string, cmd:NNCmd.Cmd_RequestUpdateRoom, message:any) => {
            Checkers.Cmd_RequestUpdateRoom.strictCheck(cmd);
            return await this.roomMgrService.updateRoomFromUser(nick, cmd.data.channel, cmd.data.patch);
        }
    }

    msgBuffers: {[from:string]: {strBuffer:string, timerId:any}} = {}
    async handlePrivateMessage(nick:string, text:string, message:any) {

        try {
            const cmdObj = await IrcClientExtra.handleMultipartCommandBuffer(null, nick, text, this.msgBuffers, undefined, undefined, ()=>{
                IrcClientExtra.sendNNCmdTo(this.irc.client, nick, {op:'nnerror', error: `Timedout multipart message`} as NNCmd.Cmd_ErrorResponse);
            });
            if(cmdObj === 'multipart') return


            if((cmdObj as NNPB.RootCommand).data?.$case != null) {
                // Not support protobuf yet on bot side
                return;
            }
            const cmdObjJson =cmdObj as NNCmd.Cmd_Base;

            const opFunc = this.ops[cmdObjJson.op];

            if(!opFunc) return await IrcClientExtra.sendNNCmdTo(this.irc.client, nick, {op:'nnerror', error: `No such op ${cmdObjJson.op}`, resToReqId:cmdObjJson.id} as NNCmd.Cmd_ErrorResponse);
            const retVal:NNCmd.Cmd_Base = await opFunc(nick, cmdObj, message);
            if(retVal?.op) {
                if(cmdObjJson.id) {
                    retVal.resToReqId = cmdObjJson.id;
                }
                return await IrcClientExtra.sendNNCmdTo(this.irc.client, nick, retVal);
            }
        } catch(e) {
            IrcClientExtra.sendNNCmdTo(this.irc.client, nick, {op:'nnerror', error: e.message} as NNCmd.Cmd_ErrorResponse);
        }
    }

    periodicChecks() {

        const LmetaChannel = BotMetaChannel.toLowerCase();

        // leave redundant channels
        const Lchannels = Object.keys(this.irc.client.chans).map(c=>c.toLowerCase())
        for(const chan of Lchannels) {
            if(chan !== LmetaChannel) {
                this.irc.client.part(chan, '');
            }
        }

        // rejoin meta if not there for some reason.
        if(!Lchannels.includes(LmetaChannel)) {
            this.irc.client.join(LmetaChannel);
        }

        // clear relay tunnel leases, if required
        const now = Date.now();
        for(const nick in this.nickToProps) {
            const props = this.nickToProps[nick];
            if(props.relay) {
                if(props.relay.endOfLifeTS<now) {
                    // If time of the lease is up, destroy and notify
                    this.destroyRelayForNickIfHeHasOne(nick, true)
                }
            }
        }
    }

    async start() {
        IrcClientExtra.connectWithRetry(this.irc.client,
            ()=>{
                // On connect
                if(process?.env?.BOT_PASSWORD?.length) {
                    IrcClientExtra.writeSaslPlainAuth(BotName, process.env.BOT_PASSWORD, this?.irc?.client?.conn);
                }
            },()=>{
                // On disconnect
            });

        while(true) {
            try {
                await this.periodicChecks();;
            } catch(e) {}
            await sleep(10000);
        }
    }
}
