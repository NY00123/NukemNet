import { Controller, Get } from '@nestjs/common';
import { RelayService } from './relay.service';

@Controller('relay')
export class RelayController {

    constructor(private relayService:RelayService) {}

    @Get()
    async getAll() {
        const arr = [];
        for(const id in this.relayService.relayDefinitions) {
            const def = this.relayService.relayDefinitions[id];
            arr.push({id:def.id, label:def.label, host:def.host});
        }
        return arr;
    }
  
}


