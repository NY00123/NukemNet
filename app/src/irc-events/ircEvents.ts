import { IrcModSignOrNone } from "../../../server/src/common/constants"
import { IRCEvent_TypeNames } from "./eventToComponent"

export interface IRCEvent_BaseEvent {
    ts?: number // timestamp
    type: IRCEvent_TypeNames 
}

export interface IRCEventCustom_Raw extends IRCEvent_BaseEvent {
    type: 'custom_raw',
    obj: any
}

export interface IRCEvent_PrivateMessage extends IRCEvent_BaseEvent {
    type: 'message',
    nick: string,
    message: string,
    isMe:boolean,
    isHistServ?: boolean
}

export interface IRCEvent_ChannelMessage extends IRCEvent_PrivateMessage {
    type: 'message',
    channel: string,
    opSign: IrcModSignOrNone
}

export interface IRCEvent_ChangeNick extends IRCEvent_BaseEvent {
    type: "nick",
    old:  string,
    new: string,
    isMe: boolean
}

export interface IRCEvent_Topic extends IRCEvent_BaseEvent {
    type: 'topic',
    nick: string,
    topic: string,
    channel: string,
    command: 'rpl_topicwhotime'|'TOPIC',
    setAtEpochSeconds?: number // Multiply be 1000f or ms. this field is there only when command='rpl_topicwhotime'
}

export interface IRCEvent_Quit extends IRCEvent_BaseEvent {
    type: "quit",
    nick: string,
    userId: string,
    reason: string
}

export interface IRCEvent_Kill extends IRCEvent_BaseEvent {
    type: "kill",
    nick: string,
    userId: string,
    reason: string
}

export interface IRCEvent_Join extends IRCEvent_BaseEvent {
    type: "join",
    nick: string,
    userId: string,
    opSign: IrcModSignOrNone,
    reason: string
    channel: string,
    isMe: boolean
}

export interface IRCEvent_Part extends IRCEvent_BaseEvent {
    type: "part",
    nick: string,
    userId: string,
    opSign: IrcModSignOrNone,
    reason: string
    channel: string
    isMe:boolean
}

export interface IRCEvent_Kick extends IRCEvent_BaseEvent {
    type: "kick",
    kicker: string,
    kicked: string,
    kickerIsMe: boolean,
    kickedIsMe: boolean,
    reason: string,
    channel: string
}

export interface IRCEvent_Mode extends IRCEvent_BaseEvent {
    type: "mode",
    byUser: string,
    on: string,
    byIsMe: boolean,
    onIsMe: boolean,
    mode: string,
    add:boolean
    channel: string
}

export interface IRCEvent_ProcessTerminatedByHost extends IRCEvent_BaseEvent {
    type: "custom_processTerminatedByHost",
    channel: string
}

export interface IRCEvent_ProcessStartedByHost extends IRCEvent_BaseEvent {
    type: "custom_processStartedByHost",
    channel: string
}

export interface IRCEvent_PlusMod extends IRCEvent_BaseEvent {
    type: "+mode",
    mode: string,
    by: string,
    onNick: string,
    onChannel: string
}

export interface IRCEvent_MinusMod extends IRCEvent_BaseEvent {
    type: "-mode",
    mode: string,
    by: string,
    onNick: string,
    onChannel: string
}

export interface IRCEvent_Registered extends IRCEvent_BaseEvent {
    type: "registered"
}

export interface IRCEventCustom_Disconnected extends IRCEvent_BaseEvent {
    type: "custom_disconnected"
}

export interface IRCEventCustom_GameDetails extends IRCEvent_BaseEvent {
    type: "custom_gameDetails"
}

export interface IRCEvent_Motd extends IRCEvent_BaseEvent {
    type: "motd",
    message: string
}

export interface IRCEvent_Invite extends IRCEvent_BaseEvent {
    type: "invite",
    channel: string,
    from:string,
    clickChan:()=>void
}

export interface IRCEvent_Whois extends IRCEvent_BaseEvent {
    type: 'whois',
    info: any
}

export interface IRCEvent_Notice extends IRCEvent_BaseEvent {
    type: 'notice',
    channel:string
    nick?:string
    text:string
}
