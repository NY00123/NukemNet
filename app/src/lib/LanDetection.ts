import { IpDetailsAndPorts } from "../commands-gameroom";
import { Settings } from "../Settings";
import { mainStore } from "../store/store";
import { detectLanIpOfNick } from "./DetectLanIp";

export async function fullLanDetectionLogic(channel:string, participant?:string) {
    
    async function getLanIp(ipDetails: IpDetailsAndPorts, nick:string) {
        try {
            // try to determine net ip of host.
            if(ipDetails?.httpPort) {
                const detectedIp = await detectLanIpOfNick(ipDetails!, ipDetails!.httpPort, nick, false); // For now, not searching IPV6.
                if(detectedIp) {
                    return detectedIp;
                }
            }
        } catch(e) {}
        return null;
    }

    const lchan = channel.toLowerCase();
    const chanData = mainStore?.joinedChannels?.[lchan];
    const game = chanData?.game;
    if(game) {
        const gameDef = Settings?.gameDefs?.games?.[chanData?.game?.publicDetails?.gameId || ''].executables[chanData?.game?.publicDetails?.execId||''];
        
        const detectForNicks:string[] = []
        if(participant?.length) {
            // If specific nick is given, only check the given name
            detectForNicks.push(participant);
        } else {
            if(gameDef.isPeerToPeer) {
                // If its p2p, we need to check with all users, except ourselves.
                detectForNicks.push(...Object.keys(game.participantNicks).filter(n=>n.toLowerCase()!=mainStore.myNick.toLowerCase()))
            } else {
                // If its not p2p, we only need to check with the host
                detectForNicks.push(game.publicDetails.owner.toLowerCase());
            }
        }

        const mylnick = mainStore.myNick?.toLowerCase()
        for(const nick of detectForNicks) {
            const lnick = nick.toLowerCase()
            if(mylnick == lnick) continue; // Don't do lan detection for yourself.

            const data = game.participantNicks[lnick];
            const nickData = chanData.nicks[lnick];
            
            if(nickData.lan == null) nickData.lan = {isDetecting:false}

            if(nickData.lan.isDetecting) continue;
    
            nickData.lan.isDetecting = true;
            const lanIp = await getLanIp(data.ipsAndPorts, lnick)
            if(lanIp != null) {
                nickData.lan.detectedLanIp = lanIp;
            } else {
                delete nickData.lan.detectedLanIp
            }
            nickData.lan.isDetecting = false;
        }
        
    }
}