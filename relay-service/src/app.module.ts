import { Module, OnModuleInit } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UdpRelayService } from './udp-relay/udp-relay.service';
import { ConfigModule } from '@nestjs/config';
import { IpFilter } from 'nestjs-ip-filter';
import { UdpRelayController } from './udp-relay/udp-relay.controller';

@Module({
  imports: [
    ConfigModule.forRoot(),

    IpFilter.registerAsync({
      useFactory: async () => {
        const whitelistObj = JSON.parse(process.env.WHITELIST);
        return {whitelist: whitelistObj}
      },
    }),
  
  
  ],
  controllers: [AppController, UdpRelayController],
  providers: [AppService, UdpRelayService],
})
export class AppModule implements OnModuleInit {

  validateEnvVars() {
    if(!process.env.SECRET_TOKEN?.length) {
      console.error('env SECRET_TOKEN missing');
      process.exit(1);
    }
    
    try {
      const wl = JSON.parse(process.env.WHITELIST);
      for(const regexIp of wl) {
        new RegExp(regexIp); // will throw if not valid regex
      }
    } catch(e) {
      console.error(`env WHITELIST must be json array of regex strings representing ip addresses, e.g [".*127.0.0.1.*","^::1"]`);
      process.exit(1);
    }

    const minPort = parseInt(process.env.PORT_MIN);
    const maxPort = parseInt(process.env.PORT_MAX);
    if(isNaN(minPort) || isNaN(maxPort) ||
    minPort>65535 || minPort<1 || maxPort>65535 || maxPort<1 ||
    (maxPort-minPort)<100) {
      console.error(`env PORT_MIN & PORT_MAX must both be valid port numbers, max must be bigger than min atleast by 100`);
      process.exit(1);
    }

    const httpPort = parseInt(process.env.HTTP_PORT);
    if(isNaN(httpPort) || httpPort>65535 || httpPort<1) {
      console.error(`env HTTP_PORT invalid num`);
      process.exit(1);
    }

    const relayMaxLife = parseInt(process.env.MAX_RELAY_LIFEMS);
    const minimumAllowedMaxLife = 1000*60*15; // 15 minutes min
    if(isNaN(relayMaxLife) || relayMaxLife<minimumAllowedMaxLife) {
      console.error(`env MAX_RELAY_LIFEMS must be bigger than 15 minutes (${minimumAllowedMaxLife}MS)`);
      process.exit(1);
    }
  }

  onModuleInit() {
    this.validateEnvVars();
  }

}
