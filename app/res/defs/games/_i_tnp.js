module.exports = function({}) {

    const runnables = {
        "multiplayer": {
            "file":"main",
        },
        "singleplayer": {"file":"main"}
    };

    return {
        "name": "Take No Prisoners",
        "executables": {
            "full": {
                "name":"Full",
                "clientInfoOnLaunch": (ctx)=>`Connect to ${ctx.GameRoom.DestIp} (copied to clipboard)`,
                tips: {
                    html: `<div>
                    <b>On Launch, connection details are copied to clipboard</b><br/>
                    <u>Click on:</u><br/>
                    1. Multiplayer<br/>
                    2. tcp/ip<br/>
                    3.a <b>Host</b> clicks "Host Game".<br/>
                    3.b <b>Client</b> clicks "Join Game", and paste the IP address from the clipboard.<br/>
                    </div>`
                },
                networking: {
                    tcpPort: {port:2300, constant:true},
                    udpPort: {port:2350, constant:true}
                },
                clientCopyHostAddressToClipboard:true,
                "files": {
                    "main": {"path":"TNP.EXE"}
                },
                "runnables": runnables,
                "parameters": {},
            }
        }
    };
}