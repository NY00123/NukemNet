
module.exports = function () {

    const path = require('path');
    const fsPromises = require('fs/promises');
    const glob = require('glob')    

    const NNCfg_HostFile = "NukemNetHost.cfg";
    const NNCfg_ClientFile = "NukemNetClient.cfg";

    return {
        writeCommandsCfg: [{"cmd":"function", async run(ctx, execDef, result, imHost, players, hostIpDetails, useDedicated, othersIpDetails, channelName, hostTunnelChosenPort) {
            const fname = imHost?NNCfg_HostFile:NNCfg_ClientFile;
            const params = ctx.GameRoom.Params;
            let lineCommands = [];
            for(const p in params) {
                const argsArr = params[p];
                for(const line of argsArr) {
                    const trm = line.trim();
                    if(trm.startsWith("+") && !trm.startsWith("+exec") && !p.startsWith("_") && ctx?.GameRoom?.ParamDefs?.[p]?.syncOnly) {
                        lineCommands.push({id:p, line:trm.substring(1)});
                    }
                }
            }
            lineCommands.sort((a,b) => {
                const aOrder = ctx.GameRoom.ParamDefs[a.id]?.order;
                const bOrder = ctx.GameRoom.ParamDefs[b.id]?.order;
                return (aOrder||0)-(bOrder||0);
            });
            const fileContent = lineCommands.map(r=>r.line).join('\r\n');
            await fsPromises.writeFile(path.resolve(ctx.GameDir, 'baseq3', fname), fileContent);
        }}],

        getParams(ta = false, sourcePort = false) {
            const levelsCalc = (!ta?[]:[
                ["* Quake 3 Team Arena *", "mpteam1"],
                ["MPTEAM1: Base Siege", "mpteam1"],
                ["MPTEAM2: Fallout Bunker 0225", "mpteam2"],
                ["MPTEAM3: Inner Sanctums", "mpteam3"],
                ["MPTEAM4: Scornforge", "mpteam4"],
                ["MPTEAM5: Teamwerkz", "mpteam5"],
                ["MPTEAM6: Vortex Portal", "mpteam6"],
                ["MPTEAM7: Capture Chamber", "mpteam7"],
                ["MPTEAM8: Assassin's Roost", "mpteam8"],
                ["MPTERRA1: Overdose", "mpterra1"],
                ["MPTERRA2: Distant Screams", "mpterra2"],
                ["MPTERRA3: Final Strike", "mpterra3"],
                ["MPTOURNEY1: The House of Decay", "mptourney1"],
                ["MPTOURNEY2: Death Factory", "mptourney2"],
                ["MPTOURNEY3: Temple of Pain", "mptourney3"],
                ["MPTOURNEY4: Evil Playground", "mptourney4"],
                ["MPQ3TOURNEY6: Beyond Reality", "mpq3tourney6"],
                ["MPQ3CTF1: Dueling Keeps 2", "mpq3ctf1"],
                ["MPQ3CTF2: More Trouble", "mpq3ctf2"],
                ["MPQ3CTF3: Return to the Stronghold", "mpq3ctf3"],
                ["MPQ3CTF4: Chaos in Space", "mpq3ctf4"]
            ]).concat([
                ["* Quake 3 Arena *", "q3dm0"],
                ["Q3DM0: Introduction", "q3dm0"],
                ["Q3DM1: Arena Gate", "q3dm1"],
                ["Q3DM2: House of Pain", "q3dm2"],
                ["Q3DM3: Arena of Death", "q3dm3"],
                ["Q3DM4: The Place of Many Deaths", "q3dm4"],
                ["Q3DM5: The Forgotten Place", "q3dm5"],
                ["Q3DM6: The Camping Grounds", "q3dm6"],
                ["PRO-Q3DM6 The Campgrounds II", "pro-q3dm6"],
                ["Q3DM7: Temple of Retribution", "q3dm7"],
                ["Q3DM8: Brimstone Abbey", "q3dm8"],
                ["Q3DM9: Hero's Keep", "q3dm9"],
                ["Q3DM10: The Nameless Place", "q3dm10"],
                ["Q3DM11: Deva Station", "q3dm11"],
                ["Q3DM12: The Dredwerkz", "q3dm12"],
                ["Q3DM13: Lost World", "q3dm13"],
                ["PRO-Q3DM13 Lost World II", "pro-q3dm13"], // does it exist?
                ["Q3DM14: Grim Dungeons", "q3dm14"],
                ["Q3DM15: Demon Keep", "q3dm15"],
                ["Q3DM16: Bouncy Map", "q3dm16"],
                ["Q3DM17: The Longest Yard", "q3dm17"],
                ["Q3DM18: Space Chamber", "q3dm18"],
                ["Q3DM19: Apocalypse Void", "q3dm19"],
                ["Q3TOURNEY1: Power Station 0218", "q3tourney1"],
                ["Q3TOURNEY2: The Proving Grounds", "q3tourney2"],
                ["PRO-Q3TOURNEY2: The Proving Grounds II", "pro-q3tourney2"],
                ["Q3TOURNEY3: Hell's Gate", "q3tourney3"],
                ["Q3TOURNEY4: Vertical Vengeance", "q3tourney4"],
                ["PRO-Q3TOURNEY4: Vertical Vengeance II", "pro-q3tourney4"],
                ["Q3TOURNEY5: Fatal Instinct", "q3tourney5"],
                ["Q3TOURNEY6: The Very End of You", "q3tourney6"],
        
                ["Q3CTF1: Dueling Keeps", "q3ctf1"],
                ["Q3CTF2: Troubled Waters", "q3ctf2"],
                ["Q3CTF3: The Stronghold", "q3ctf3"],
                ["Q3CTF4: Space CTF", "q3ctf4"],
                ["Q3CTFTOURNEY6: Across Space", "q3tourney6_ctf"]
            ]);
            
            const levels = levelsCalc.map(([name, id]) => ({
                "label": name,
                "value": [id]
            }));

            return {

                ...(sourcePort?{
                    "_dedicated": { 
                        order:-103,
                        "modeSupport": ["multiplayer"],
                        "type": "static",
                        "value": [`'+set'`, `'dedicated'`, `'1'`], // Dedicated=1 is unannounced.
                        "for":"host-only-private"
                    }
                }:{
                    "_dedicated": { 
                        order:-103,
                        "modeSupport": ["multiplayer"],
                        "type": "boolean",
                        label:"Dedicated",
                        "value": [`'+set'`, `'dedicated'`, `'1'`], // Dedicated=1 is unannounced.
                        "for":"host-only-private"
                    },
                }),
                
                "_netp": { 
                    order:-102,
                    "modeSupport": ["multiplayer"],
                    "type": "static",
                    "value": [`'+set'`, `'net_port'`, `GameRoom.MyPort`],
                    "for":"host-only-private"
                },
                hostbase: {
                    order:-101,
                    "modeSupport": ["multiplayer", "singleplayer"],
                    "type": "static",
                    "value": [
                        `'+PB_SV_DISABLE'`,
                        `'+set sv_hostname NukemNet'`, `'+set sv_maxclients 16'`, `'+set g_motd "Welcome Q3 NukemNet Server"'`, `'+set rconpassword "nukemnet"'`],
                    "for":"host-only-private",
                    syncOnly:true
                },

                ...(ta?{_ta:{
                    order:-100,
                    "modeSupport": ["multiplayer", "singleplayer"],
                    "type": "static",
                    "value": ["'+set'", "'fs_game'", "'missionpack'"],
                    "for":"private"
                }}:{}),


                ...(!ta?{
                    "_mod": {
                        order:-100,
                        "modeSupport": ["multiplayer", "singleplayer"],
                        "type": "dirfiles",
                        "value": ["'+set'", "'fs_game'", "value[0]"],
                        "label": "Mod",
                        "path": "!(baseq3)",
                        globOpts: {
                            stat: true,
                            ignore: {
                                ignored: (p) => {
                                    if(p.isDirectory()) {
                                        const files = glob.globSync('*.pk3', {cwd: p.fullpath(), nocase:true});
                                        if(files.length>0) {
                                            return false;
                                        }
                                    }
                                    return true;
                                }
                            }
                        },
                        "stripExt": true,
                        "stripPath":true,
                        "optional":true
                    }
                }:{}),

                "name": {
                    order:-100,
                    "modeSupport": ["multiplayer", "singleplayer"],
                    "type": "static",
                    "value": ["'+name '+MyName"],
                    "for":"private",
                    syncOnly:true,
                },

                "_cmdshost": {
                    order:-99,
                    "modeSupport": ["multiplayer", "singleplayer"],
                    "type": "static",
                    "value": ["'+exec'", `'${NNCfg_HostFile}'`],
                    "for":"host-only-private",
                },
                "_cmdsclnt": {
                    order:-99,
                    "modeSupport": ["multiplayer", "singleplayer"],
                    "type": "static",
                    "value": ["'+exec'", `'${NNCfg_ClientFile}'`],
                    "for":"client-only-private",
                },
        
                "_clntcon": {
                    order:-99,
                    "modeSupport": ["multiplayer"],
                    "type": "static",
                    "value": ["'+connect'", `GameRoom.DestIp + ':' + GameRoom.DestPort`],
                    "for":"client-only-private"
                },
        
                
                "gametype": {
                    order:-98,
                    "modeSupport": ["multiplayer", "singleplayer"],
                    "type": "choice",
                    "label": "Type",
                    "value":["'+set g_gametype '+value"],
                    "choices": [{
                        "label": "Free for all",
                        "value": "0"
                    }, {
                        "label": "Tournament",
                        "value": "1"
                    },
                    // {
                    //     "label": "Campaign", // only in single player
                    //     "value": "2"
                    // },
                    {
                        "label": "Team Deathmatch",
                        "value": "3"
                    }, {
                        "label": "Capture the Flag",
                        "value": "4"
                    }].concat(ta?[
                        {
                            "label": "One flag CTF",
                            "value": "5"
                        }, {
                            "label": "Overload",
                            "value": "6"
                        }, {
                            "label": "Harvester",
                            "value": "7"
                        }
                    ]:[]),
                    syncOnly:true,
                    "for":"host-only-private"
                },
        
                "qdf": {
                    order:-97,
                    "modeSupport": ["multiplayer", "singleplayer"],
                    "type": "numrange",
                    "min":0,
                    "max":10,
                    "delta":1,
                    "label": "Quad Factor",
                    "value": "'+set g_quadfactor '+value",
                    description:"Quad Damage Factor",
                    "optional":true,
                    for:'host-only-shared',
                    syncOnly:true
                },
        
                "tmlmt": {
                    order:-96,
                    "modeSupport": ["multiplayer", "singleplayer"],
                    "type": "numrange",
                    "min":0,
                    "max":100,
                    "delta":1,
                    "label": "Time Limit",
                    "value": "'+set timelimit '+value",
                    "optional":true,
                    for:'host-only-shared',
                    syncOnly:true
                },
        
                "frglmt": {
                    order:-95,
                    "modeSupport": ["multiplayer", "singleplayer"],
                    "type": "numrange",
                    "min":0,
                    "max":100,
                    "delta":1,
                    "label": "Frag Limit",
                    "value": "'+set fraglimit '+value",
                    "optional":true,
                    for:'host-only-shared',
                    syncOnly:true
                },
        
                "wpnrspn": {
                    order:-94,
                    "modeSupport": ["multiplayer", "singleplayer"],
                    "type": "numrange",
                    "min":0,
                    "max":100,
                    "delta":1,
                    description:"Weapon Respawn Time (Seconds)",
                    "label": "Weapon Respawn",
                    "value": "'+set g_weaponrespawn '+value",
                    "optional":true,
                    for:'host-only-shared',
                    syncOnly:true
                },
        
                "inactivity": {
                    order:-93,
                    "modeSupport": ["multiplayer", "singleplayer"],
                    "type": "numrange",
                    "min":0,
                    "max":10000,
                    "delta":1,
                    "label": "Inactivity",
                    "description": "Inactivity Seconds before kickout",
                    "value": "'+set g_inactivity '+value",
                    "optional":true,
                    for:'host-only-shared',
                    syncOnly:true
                },
        
                frcrspwn: {
                    order:-92,
                    "modeSupport": ["multiplayer", "singleplayer"],
                    "type": "boolean",
                    "label": "Force Respawn",
                    "value": `'+set g_forcerespawn '+value`,
                    "for": "host-only-shared",
                    syncOnly:true
                },
        
                "_map": {
                    order:-91,
                    "modeSupport": ["multiplayer", "singleplayer"],
                    "type": "dirfiles",
                    "value": ["'+map '+value[0]"],
                    "label": "Usermap",
                    "path": "baseq3/!(pak*).pk3",
                    "stripExt": true,
                    "stripPath":true,
                    "optional":true,
                    "for":"host-only-shared",
                    syncOnly: true
                },
                "_level": {
                    order:-91,
                    "modeSupport": ["multiplayer", "singleplayer"],
                    "type": "choice",
                    "label": "Level",
                    "value":["'+map '+value[0]"],
                    "choices": levels,
                    "optional":true,
                    "for":"host-only-shared",
                    syncOnly:true
                },
                "map": {
                    order:-91,
                    "modeSupport": ["multiplayer", "singleplayer"],
                    "type": "static",
                    "value": [`GameRoom?.Params?._map?.[0] || GameRoom?.Params?._level?.[0] || (GameRoom?.Params?._mod?.[0]? undefined:"+map q3dm1")`],
                    "for":"host-only-shared",
                    syncOnly: true
                },
        
                "bots": {
                    order:-90,
                    "modeSupport": ["multiplayer", "singleplayer"],
                    "type": "numrange",
                    "min":0,
                    "max":16,
                    "delta":1,
                    "label": "Bots",
                    description:"Number of Minimum Players",
                    "value": "'+set bot_minplayers '+value",
                    "optional":true,
                    for:'host-only-shared',
                    syncOnly:true
                }
            }
        }
    }
}
