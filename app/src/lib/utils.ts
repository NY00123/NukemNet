import { ChildProcess } from "child_process";
import { dgram, dns, glob, net } from "../NodeLibs";
import { is_ipv4, is_ipv6 } from "./GetIp";
import { isIpV4Local } from "./IpDetails";

export function getJsonFromUrl(url:string) {
    if(!url) url = location.href;
    var question = url.indexOf("?");
    var hash = url.indexOf("#");
    if(hash==-1 && question==-1) return {};
    if(hash==-1) hash = url.length;
    var query = question==-1 || hash==question+1 ? url.substring(hash) : 
    url.substring(question+1,hash);
    var result = {} as any;
    query.split("&").forEach(function(part) {
      if(!part) return;
      part = part.split("+").join(" "); // replace every + with space, regexp-free version
      var eq = part.indexOf("=");
      var key = eq>-1 ? part.substr(0,eq) : part;
      var val = eq>-1 ? decodeURIComponent(part.substr(eq+1)) : "";
      var from = key.indexOf("[");
      if(from==-1) result[decodeURIComponent(key)] = val;
      else {
        var to = key.indexOf("]",from);
        var index = decodeURIComponent(key.substring(from+1,to));
        key = decodeURIComponent(key.substring(0,from));
        if(!result[key]) result[key] = [];
        if(!index) result[key].push(val);
        else result[key][index] = val;
      }
    });
    return result;
  }


  export function isElement(obj:any) {
    try {
      //Using W3 DOM2 (works for FF, Opera and Chrome)
      return obj instanceof HTMLElement;
    }
    catch(e){
      //Browsers not supporting W3 DOM2 don't have HTMLElement and
      //an exception is thrown and we end up here. Testing some
      //properties that all elements have (works on IE7)
      return (typeof obj==="object") &&
        (obj.nodeType===1) && (typeof obj.style === "object") &&
        (typeof obj.ownerDocument ==="object");
    }
  }

  export function isJqueryElement(obj:any) {
    return obj instanceof jQuery
  }

  export const isWindows = ()=>{
    return navigator.userAgent.includes('Windows');
  }

  // usage e.g: preloadImagesFromBackgroundUrl(game-started-animation frame1, game-started-animation frame2)
  export function preloadImagesFromBackgroundUrl(...params:string[]) {
    for (var i = 0; i < params.length; i++) {
        const div = document.createElement('div');
        div.className = params[i];
        div.style.width='0';
        div.style.height='0';
        document.body.appendChild(div);
        var url = parseUrlFromCssProp(getComputedStyle(div).backgroundImage);
        var img = new Image();
        const timerId = setTimeout(onLoad, 3000);
        function onLoad() {div.remove();clearTimeout(timerId)};
        img.onload = onLoad;
        img.src = url;
        if (img.complete) onLoad();
    }
  }

  export function parseUrlFromCssProp(propValue:string) {
    try {
      return propValue.match(/\((.*?)\)/)![1].replace(/('|")/g,'')!
    } catch(e) {}
    return propValue;
  }

  export async function resolveHostIpv4(name:string):Promise<string> {
    return new Promise((resolve)=>{
      dns.lookup(name, 'A' as any , function(err:any, records){
        if(records?.length) {
          return resolve(Array.isArray(records)?records[0]:records);
        }
        resolve(name)
      });
    });
  }

  export async function resolveHostIpv6(name:string):Promise<string> {
    return new Promise((resolve)=>{
        dns.lookup(name, 6, function(err:any, records) {
        if(records?.length) {
          return resolve(Array.isArray(records)?records[0]:records);
        }
        resolve(name)
      });
    });
  }

  // If hostname is a local/lan ip, prefer ipv4, otherwise try ipv6 first, and then ipv4.
  // Doing both in parallel to speed up process.
  export async function resolveHostIpv6or4(name:string):Promise<string> {
    return new Promise(resolve=>{

      let ipv4ResolvedIp:string|null = null; // if this is set v4 is Done
      let v6Done = false, resolved = false;

      function end(address:string) {
        if(resolved) return;
        resolved = true;
        resolve(address);
      }

      function fallback() {
        if(ipv4ResolvedIp && v6Done) {
          end(ipv4ResolvedIp);
        }
      }

      resolveHostIpv4(name).then(address=>{
        ipv4ResolvedIp = address;
        if(isIpV4Local(address)) return end(address);
        fallback();
      })
      resolveHostIpv6(name).then(address=>{
        v6Done = true;
        if(is_ipv6(address)) return end(address);
        fallback();
      });
    });
  }

  export function randomInteger(min:number, max:number) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  export function randomIntegerExcept(min:number, max:number, except:number[]) {
    const s = new Set(except);
    const allowedOptions = []
    for(let i=min; i<=max; i++) {
      if(s.has(i)) continue;
      allowedOptions.push(i);
    }
    return allowedOptions[Math.floor(Math.random()*allowedOptions.length)]
  }

  export async function testIfUdpPortIsBindable(port:number, ipv6?:boolean) {
    try {
      const newSocket = dgram.createSocket(ipv6?{ipv6Only:true, type:'udp6'}:{type:'udp6'});
      newSocket.bind(port,'::');
      await new Promise((res,rej)=>{
          newSocket.once('listening', ()=>{
              newSocket.close();
              newSocket.removeAllListeners();
              res(null);
          })
          newSocket.once('error', (err)=>{
              newSocket.close();
              newSocket.removeAllListeners();
              rej(err)
          })
      });
      return true;
    } catch(e) {
      return false;
    }
  }

  export async function testIfTcpPortIsBindable(port:number, ipv6?:boolean) {
    try {
      await new Promise((res,rej)=>{
          const newSocket = net.createServer();
          newSocket.once('listening', ()=>{
              newSocket.close();
              newSocket.removeAllListeners();
              res(null);
          })
          newSocket.once('error', (err)=>{
              newSocket.close();
              newSocket.removeAllListeners();
              rej(err)
          })
          newSocket.listen(ipv6?{ipv6Only:true, type:'udp6', port, host:"::", exclusive:true}:{port, host:"::"});
      });
      return true;
    } catch(e) {
      return false;
    }
  }

  // port range must be different than STUN port range
  export async function findFreeUdpTcpPortInRange(min:number=20001, max:number=35000, except:number[]=[], randomize:boolean=true, ipv6?:boolean) {
    const s = new Set(except);
    const allowedOptions = []
    for(let i=min; i<=max; i++) {
      if(s.has(i)) continue;
      allowedOptions.push(i);
    }

    while(allowedOptions.length) {
      const idx = randomize?Math.floor(Math.random()*allowedOptions.length): 0;
      const port = allowedOptions[idx];
      allowedOptions.splice(idx, 1);

      try {
        if(await testIfTcpPortIsBindable(port, ipv6) && await testIfUdpPortIsBindable(port, ipv6)) {
          return port;
        }
      } catch(e) {}
      continue;
    }
    throw new Error('No free ports found');
  }

  export function checkProcessRunSuccess(proc:ChildProcess): Promise<any> {
    return new Promise((resolve, reject)=>{
      let ended = false;
      function end(err:any) {
        clearTimeout(tmr);
        if(ended) return;
        ended = true;
        proc.off('spawn', end);
        proc.off('error', end);
        if(err instanceof Error) {
          reject(err);
        } else {
          resolve(null);
        }
      }
      
      const tmr = setTimeout(()=>{end(null)}, 3000);

      proc.on('spawn', end);
      proc.on('error', end);
    })
  }

  export function udp6a(address:string) {
    if(is_ipv4(address) && !address.toLowerCase().startsWith('::ffff:')) {
      return '::ffff:' + address;
    }
    return address;
  }
  export function parseIpv4(address:string) {
    if(address.includes('.')) {
      const parts = address.toLowerCase().split('::ffff:');
      if(parts.length > 1) {
        return parts[1];
      }
    }
    return address;
  }

  // Normalized IPv6 address to single format (same IPv6 address can be represented in multiple strings)
  export function normalize_IPv6(ip_string:string) {
    // replace ipv4 address if any
    var ipv4 = ip_string.match(/(.*:)([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$)/) as any;
    if (ipv4) {
        ip_string = ipv4[1];
        ipv4 = ipv4[2].match(/[0-9]+/g);
        for (var i = 0;i < 4;i ++) {
            var byte = parseInt(ipv4[i],10);
            ipv4[i] = ("0" + byte.toString(16)).substr(-2);
        }
        ip_string += ipv4[0] + ipv4[1] + ':' + ipv4[2] + ipv4[3];
    }

    // take care of leading and trailing ::
    ip_string = ip_string.replace(/^:|:$/g, '');

    var ipv6 = ip_string.split(':');

    for (var i = 0; i < ipv6.length; i ++) {
        var hex = ipv6[i] as any;
        if (hex != "") {
            // normalize leading zeros
            ipv6[i] = ("0000" + hex).substr(-4);
        }
        else {
            // normalize grouped zeros ::
            hex = [];
            for (var j = ipv6.length; j <= 8; j ++) {
                hex.push('0000');
            }
            ipv6[i] = hex.join(':');
        }
    }

    return ipv6.join(':');
}

// https://stackoverflow.com/a/30331352/230637
export function IPv6_Equal(ipA?:string, ipB?:string) {
  if(!ipA || !ipB) return false;
  return normalize_IPv6(ipA) === normalize_IPv6(ipB);
}


export async function resolveHostFamilyAndAddressPreferIpv6(addressOrHost:string) {
  function getIpFamily(ip:string) {
    if(ip.includes(':')) return 6;
    if(is_ipv4(ip)) return 4;
    return null;
  }

  const resolvedFamilyForIp = getIpFamily(addressOrHost);
  if(resolvedFamilyForIp != null) return {family:resolvedFamilyForIp, address:addressOrHost};

  const resolvedIp = await resolveHostIpv6or4(addressOrHost);
  const family = getIpFamily(resolvedIp);
  return {family, address:resolvedIp};
}

// https://stackoverflow.com/a/4609476/230637
export function SetCaretAtEnd(elem:HTMLInputElement) {
  var elemLen = elem.value.length;
  // For IE Only
  if ((document as any).selection) {
      // Set focus
      elem.focus();
      // Use IE Ranges
      var oSel = (document as any).selection.createRange();
      // Reset position to 0 & then set at end
      oSel.moveStart('character', -elemLen);
      oSel.moveStart('character', elemLen);
      oSel.moveEnd('character', 0);
      oSel.select();
  }
  else if (elem.selectionStart || elem.selectionStart == 0) {
      // Firefox/Chrome
      elem.selectionStart = elemLen;
      elem.selectionEnd = elemLen;
      elem.focus();
  } // if
}

export function Capitalized(word:string) {
  const capitalized = word.charAt(0).toUpperCase() + word.slice(1)
  return capitalized;
}

export function DateNow_YYYYMMDD() {
  const today = new Date();
  const yyyy = today.getFullYear();
  let mm:string|number = today.getMonth() + 1; // Months start at 0!
  let dd:string|number = today.getDate();

  if (dd < 10) dd = '0' + dd;
  if (mm < 10) mm = '0' + mm;

  const formattedToday = `${yyyy}${mm}${dd}`;
  return formattedToday;
}

/** https://stackoverflow.com/a/14919494/230637
* Format bytes as human-readable text.
* 
* @param bytes Number of bytes.
* @param si True to use metric (SI) units, aka powers of 1000. False to use 
*           binary (IEC), aka powers of 1024.
* @param dp Number of decimal places to display.
* 
* @return Formatted string.
*/
export function humanFileSize(bytes:number, si=false, dp=1) {
 const thresh = si ? 1000 : 1024;

 if (Math.abs(bytes) < thresh) {
   return bytes + ' B';
 }

 const units = si 
   ? ['kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'] 
   : ['KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB'];
 let u = -1;
 const r = 10**dp;

 do {
   bytes /= thresh;
   ++u;
 } while (Math.round(Math.abs(bytes) * r) / r >= thresh && u < units.length - 1);


 return bytes.toFixed(dp) + ' ' + units[u];
}

export function clearObj(obj:any) {
  if(typeof obj !== 'object') return;
  for(const key in obj) {
    if(obj.hasOwnProperty(key)) {
      delete obj[key];
    }
  }
}

export function escapeGlob(str:string) {
  return str.replaceAll('?',"\\?").
    replaceAll('*',"\\*").
    replaceAll('[',"\\[").
    replaceAll(']',"\\]").
    replaceAll('!',"\\!");
}

export function containsWhitespace(str:string) {
  return /\s/.test(str);
}

export function escapeShellArg(arg:string, windows:boolean) {
  const escapeChar = windows ? '^' : '\\';
  let escaped = arg;

  if(!windows) {
    // unix
    escaped = escaped.
    replaceAll('"','\\"');
  }

  if(containsWhitespace(escaped)) return `"${escaped}"`;

  if(windows) {
    escaped = escaped.
    replaceAll('^',"^^").
    replaceAll('"','""');
  }

  escaped = escaped.
  replaceAll('&',escapeChar+"&").
  replaceAll('<',escapeChar+"<").
  replaceAll('>',escapeChar+">").
  replaceAll('|',escapeChar+"|");

  return escaped;
}

export async function findPathCaseInsensitive(pth:string) {
  return (await glob.glob(escapeGlob(pth.replaceAll('\\','/')), {nocase:true, absolute:true}))?.[0];
}

export function findPathCaseInsensitiveSync(pth:string) {
  return glob.sync(escapeGlob(pth.replaceAll('\\','/')), {nocase:true, absolute:true})?.[0];
}

export function removeExtFromPath(pth:string) {
  const i = pth.lastIndexOf('.');
  if(i===-1) return pth;
  let lastIndexOfSlash = pth.lastIndexOf('/');

  // If '.' char is in folder, that's not a file extension, so nothing to trim.
  if(lastIndexOfSlash>i) {
    return pth;
  }
  let lastIndexOfBackslash = pth.lastIndexOf('\\');
  if(lastIndexOfBackslash>i) {
    return pth;
  }

  const str = pth.substring(0,i);
  return str;
}

// https://stackoverflow.com/a/69350576/230637
// Modified by Alon Amir at 2023-May-10 to return index instead of boolean
export function subarrayIndex(possibleParent:any[], possibleChild:any[]){
  var [arr,sub] = [possibleParent, possibleChild]
  if(arr.length<sub.length) {return false} //the sub has to be smaller
  for(let i=0;i<arr.length-(sub.length-1);i++){
      var isSub=true //remains true if entire sub pattern matches
      for(let j=0;j<sub.length;j++){
          if(arr[j+i]!=sub[j]) {
              isSub=false
          }
      }
      if(isSub) {return i} //this means a sub pattern matched
  }
  return -1 //this means no sub pattern mashed
}

export function deepFreeze<T>(orig:T) {
  const obj = orig as any;
  Object.keys(obj).forEach(prop => {
    if (typeof obj[prop] === 'object' && !Object.isFrozen(obj[prop])) deepFreeze(obj[prop]);
  });
  return Object.freeze(obj);
};