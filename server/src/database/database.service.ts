import { Injectable } from '@nestjs/common';

import * as postgres from 'postgres'

@Injectable()
export class DatabaseService {
    sql = postgres({database:'nukemnet', host:'localhost', port:25432, username:'postgres', pass:'example'})
}
