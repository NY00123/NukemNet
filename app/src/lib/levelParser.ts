import {fs} from '../NodeLibs'

function stringIsNumber(str:string) {
    return parseInt(str).toString() === str;
}

type LevelDetails = {
    volume: number
    level: number
    label: string
    fileName: string
    timePar: string
    time3dr: string
}

export async function buildEngine_parseLevelsFromFilePath(...filePaths:string[]):Promise<LevelDetails[]> {
    
    const levelMapping:{[key:string]: LevelDetails} = {};

    for(const filePath of filePaths) {
        const file = await fs.readFile(filePath, 'ascii');
        const parts = file.split('definelevelname ');
        
        for(const levelStr of parts) {
            const trimmed = levelStr.trim().split('\r')[0].split('\n')[0];
            const levelParts = trimmed.split(' ');
        
            const volume = levelParts[0];
            if(!stringIsNumber(volume)) continue;
        
            const [,level, mapFileName, timePar, time3dr, ...rest] = levelParts;
            const levelLabel = rest.join(' ');
    
            const key = `${volume},${level}`
            levelMapping[key] = {
                volume: parseInt(volume),
                level: parseInt(level),
                label: levelLabel,
                fileName: mapFileName,
                timePar,
                time3dr
            }
        }
    }

    const levels = Object.values(levelMapping);
    levels.sort((a,b)=>{
        if(a.volume === b.volume) {
            return a.level - b.level;
        } else {
            return a.volume - b.volume;
        }
    });
    
    return levels;
}