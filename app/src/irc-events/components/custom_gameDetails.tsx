import {Component} from 'solid-js'
import { IRCEventCustom_GameDetails } from '../ircEvents';
import { IrcEventCmp_Base } from './base';

export const IrcEventCmpCustom_GameDetails:Component<IRCEventCustom_GameDetails> = (props)=>{
    return <IrcEventCmp_Base {...props}>Game room details changed</IrcEventCmp_Base>
}