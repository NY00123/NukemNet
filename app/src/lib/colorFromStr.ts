import { createHhash } from "../../../server/src/common/hash";

export function colorFromStr(str:string) {
  const hash = createHhash(str||'null');
  const color = `#${hash.substring(0,6)}aa`;
  return color
}