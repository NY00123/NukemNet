module.exports = function({}) {

    const dosboxPreCommands = ['CONFIG -set "core=auto"', 'CONFIG -set "cycles=max"'];
    const runToolOpts = {"type":"dosbox", "data":{"commands":dosboxPreCommands}};
    const runToolOptsMultiplayer = {"type":"dosbox", "data":{"commands":dosboxPreCommands, "net":"serial"}};

    const runnables = {
        "multiplayer": {"file":"main","runTool":runToolOptsMultiplayer},
        "singleplayer": {"file":"main", "runTool": runToolOpts}
    };

    const tips = {html: `
    <h3>Starting up a modem game takes a few steps on both sides</h3>
    <ol>
        <li>Tap Enter a few times to skip into the game screen</li>
        <li>In game menu, click "F1" to reveal the top bar</li>
        <li>Tap RightArrow twice to go to the Settings tab</li>
        <li>One player selects "Modem -- Red"</li>
        <li>Other player selects "Modem -- Blue"</li>
        <li>It does not matter which one is blue or red, but you need to coordinate it before you start.</li>
    </ol>
    <p>Press F2 to send chat message in game</p>
    `};

    return {
        "name": "Battle Chess",
        "get": [{
            type:"gog",
            prodId:"1207663033",
            nameId:"battle_chess",
            name:"Battle Chess"
        }],
        "executables": {
            "full": {
                tips,
                maxPlayers:2,
                "name":"Full (DOS)",
                "files": {
                    "main": {"path":"CHESS.EXE"}
                },
                "runnables": runnables,
                parameters: {}
            }
        }
    };
}