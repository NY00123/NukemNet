import { Button, Col, FloatingLabel, Form, Modal, OverlayTrigger, Row, Spinner, Table, Tooltip } from "solid-bootstrap"
import { Component, createEffect, createSignal, For, Show } from "solid-js"
import { checkPortConnectivityTcpUdp } from "./lib/CheckPortConnectivity";
import { natUpnp2, natPuncher, natPmp, defaultGateway } from "./NodeLibs"
import { DialogMgr } from "./Popups";
import { Settings } from "./Settings";
import { mainStore } from "./store/store";

const natUpnp2Client = natUpnp2.createClient();

async function getAllMappings(localOnly?:boolean) {
  return new Promise((res,rej)=>{
    natUpnp2Client.getMappings(localOnly?{local: true}:{}, (err:any, results:any) => {
      if(err != null) rej(err);
      else res(results)
    });
  })
}

async function deletePortMapping(opts:{private?:string,public:string, protocol:string}) {
  // This methods deletes the forwarding on all tech (upnp, pmp, pcp)


  // Delete mapping using nat-upnp-2 is more accurate, support protocol parameter, but it supports only UPnP deletion.
  // on nat-puncher it is less accurate, supporting only the 'external' port param, but it supports PMP & PCP (no UPNP support)
  
  const promises:Promise<any>[] = [];

  if(natPuncher) {
    promises.push(natPuncher.deleteMappingPcp(opts.public));
    promises.push(natPuncher.deleteMappingPmp(opts.public));
  }
  
  promises.push(new Promise((res,rej)=>{
    natUpnp2Client.portUnmapping(opts, (err:any,response:string)=>{
      if(err) rej(err);
      else res(response);
    });
  }));

  try {
    await Promise.all(promises);
  } catch(e) {}
}

async function upnpAddMapping(opts:{
  public: number,
  private: number,
  ttl: number,
  protocol: string //'udp'|'tcp'
}) {
  return new Promise((res,rej)=>{
    natUpnp2Client.portMapping(opts,(err:any)=>{
      if(err) rej(err);
      else res(null)
    })
  }) 
}

async function pmpOrPcpAddMapping(which:'pcp'|'pmp',opts:{
  public: number,
  private: number,
  ttl: number,
  protocol: string //'udp'|'tcp'
}) {
  // TCP is not supported in nat-puncher
  if(which === 'pcp') {
    if(natPuncher) return natPuncher.addMappingPcp(opts.public, opts.private, opts.ttl);
    return Promise.resolve();
  } else if(which === 'pmp') {
    if(opts.protocol === 'udp') {
      if(natPuncher) return natPuncher.addMappingPmp(opts.public, opts.private, opts.ttl);
      return Promise.resolve();
    } else {
      let client:any;
      try {
        const gatewayRes:{gateway?:string} = await defaultGateway.gateway4async();
        if(!gatewayRes?.gateway?.length) throw '';
        client = natPmp.connect(gatewayRes.gateway);
        await new Promise(res=>{
          client.portMapping({ private: opts.private, public: opts.public, ttl: opts.ttl, type: 'tcp' }, function (err:any, info:any) {
            res(null);
          });
        });
      } catch(e) {}
      try {client.close()} catch(e) {}
    }
  }
}

const one_year = 60*60*60*24*365;

export async function tryForwardOnAllProtocols(port?:number) {
  if(!port) port = Settings.getGamePort();
  if(!port) return;

  const forwardDef = {
    private: port,
    public: port,
    protocol: 'udp',
    ttl: one_year
  };
  const try1 = pmpOrPcpAddMapping('pmp', forwardDef)
  const try2 = pmpOrPcpAddMapping('pcp', forwardDef);
  const try3 = upnpAddMapping(forwardDef);

  const forwardDefTcp = structuredClone(forwardDef);
  forwardDefTcp.protocol = 'tcp';

  const try4 = pmpOrPcpAddMapping('pmp', forwardDefTcp)
  const try5 = pmpOrPcpAddMapping('pcp', forwardDefTcp);
  const try6 = upnpAddMapping(forwardDefTcp);

  return await Promise.all([try1,try2,try3,try4,try5,try6]);
}

interface PortMapRow {
  description:string,
  enabled:boolean,
  local:boolean,
  protocol: string // 'udp'|'tcp',
  private: {host:string, port:number},
  public: {host:string, port:number},
  ttl:number
}

const PortForwardModal:Component<{show:boolean, dismiss?:()=>void}> = (props)=>{

    const [isPending, setIsPending] = createSignal(false);

    const [mappings, setMappings] = createSignal([] as PortMapRow[]);

    const [myGamePort, setMyGamePort] = createSignal(Settings.getGamePort())

    async function wrapInPending(func:()=>any, exception?:()=>any) {
      if(isPending()) return;
      setIsPending(true);

        try {
          await func();
        } catch(e) {
          await exception?.();
        }

      setIsPending(false);
    }

    async function refreshAll() {
      await wrapInPending(()=>{
        return Promise.all([
          queryMappings()
        ])
      })
    }

    // Upon popup show, refresh supported techs
    let lastIsShown = false;
    createEffect(()=>{
      if(props.show) {
        if(lastIsShown) return
        else lastIsShown = true
        setMyGamePort(Settings.getGamePort());
        refreshAll();
      } else {
        lastIsShown = false
      }
    })

    function boolDesc(isSupported: boolean|null) {
      if(isSupported === true) {
        return 'yes'
      } else if(isSupported === false) {
        return 'no'
      } else {
        return 'unknown'
      }
    }

    async function queryMappings(wrapPending?:boolean) {

        async function run() {
            try {
              const res = (await getAllMappings()) as PortMapRow[];
              setMappings(res)
            } catch(e) {
              setMappings([])
            }
        }

        if(wrapPending) {
          await wrapInPending(run)
        } else {
          await run();
        }
    }

    function formatHostAndPort(host:string, port:number) {
      if(host?.length>0) {
        return `${host}:${port}`
      } else {
        return `${port}`
      }
    }

    function deleteMapping(mapping:PortMapRow) {
        wrapInPending(async ()=>{
          await deletePortMapping({
            public: ''+mapping.public.port,
            private: ''+mapping.private.port,
            protocol: mapping.protocol
          })
          await queryMappings()
        }, async ()=>{
          DialogMgr.addPopup({
            title: "Action Failed",
            body: 'Failed to delete mapping',
            icon: 'error',
            buttons:[{body:"ok"}]
          })
        });
    }

    async function addMapping() {
      const elements = {
        tech: document.querySelector('#forward-tech') as HTMLSelectElement,
        protocol: document.querySelector('#forward-protocol') as HTMLSelectElement,
        portPrivate: document.querySelector('#forward-port-private') as HTMLInputElement,
        portPublic: document.querySelector('#forward-port-public') as HTMLInputElement,
        ttl: document.querySelector('#forward-ttl') as HTMLInputElement
      }
      const tech = elements.tech.selectedOptions[0].value as 'upnp'|'upmp'|'pcp';
      const protocolChosen = elements.protocol.selectedOptions[0].value as 'udp'|'tcp'|'tcp&udp';
      const portPrivate = elements.portPrivate.value || elements.portPublic.value;
      const portPublic = elements.portPublic.value || elements.portPrivate.value;
      const ttl = elements.ttl?.value || ''+one_year;

      if(!portPrivate?.length || !portPublic?.length || !ttl?.length) {
        DialogMgr.addPopup({
          title: 'Missing some details',
          icon: 'error',
          buttons:[{body:"ok"}]
        })
        return
      }

      function cleanFields() {
        elements.portPrivate.value = '';
        elements.portPublic.value = '';
        elements.ttl.value = '';
      }

      async function runUpnp(protocol:'udp'|'tcp') {
        try {
          await upnpAddMapping({
            private:parseInt(portPrivate),
            public: parseInt(portPublic),
            protocol,
            ttl: parseInt(ttl)
          })
          cleanFields()
          await queryMappings(false);
        } catch(e) {
          DialogMgr.addPopup({
            title: 'Failed Forwarding',
            icon: 'error',
            buttons:[{body:"ok"}]
          })
        }
      }

      async function runPmpOrPcp(tech:'pcp'|'pmp', protocol:'udp'|'tcp') {
        try {
          await pmpOrPcpAddMapping(tech,{
            private:parseInt(portPrivate),
            public: parseInt(portPublic),
            protocol,
            ttl: parseInt(ttl)
          })
          cleanFields()
          await queryMappings(false);
        } catch(e) {
          DialogMgr.addPopup({
            title: 'Failed Forwarding',
            icon: 'error',
            buttons:[{body:"ok"}]
          })
        }
      }


      const protos = [] as ('udp'|'tcp')[];
      if(protocolChosen === 'udp') protos.push('udp');
      if(protocolChosen === 'tcp') protos.push('tcp');
      if(protocolChosen === 'tcp&udp') protos.push('udp', 'tcp');

      for(const proto of protos) {
        if(tech === 'upnp') {
          await wrapInPending(()=>runUpnp(proto));
        } else {
          await wrapInPending(()=>runPmpOrPcp(tech as any, proto))
        }
      }
      
    }

    async function justForwardForMe() {
      const port = myGamePort();
      await wrapInPending(async ()=>{
        try {await tryForwardOnAllProtocols(port!)} catch(e) {}
        await queryMappings(false);
      })
    }

    async function checkMyConnectivity(port?:number) {
      const checkPort = port || myGamePort();
      const ip = mainStore.myExternalIpV4;
      if(!ip?.length) {
        DialogMgr.addPopup({
          title:"Test Failed",
          body: 'Cannot test, could not find your external ip',
          icon: 'error',
          buttons:[{body:"ok"}]
        })
        return
      }

      await wrapInPending(async ()=>{
        const {tcp,udp} = await checkPortConnectivityTcpUdp(checkPort, ip!, 5000);
        if(tcp || udp) {
          const partial = tcp != udp;
          DialogMgr.addPopup({
            title: partial?'Kind of ok':'Hell Yeah',
            body:()=><>
            Your port {checkPort} is accessible on ({tcp?' TCP ':''}{udp?' UDP ':''})<br/>
            {partial? (<>But not on {!tcp?"TCP":''} {!udp?"UDP":''}<br/></>):''}
            </>,
            buttons:[{body:"Great"}]
          })
        } else {
          DialogMgr.addPopup({
            title: 'Error',
            body:`Your port ${checkPort} is inaccessible`,
            icon: 'error',
            buttons:[{body:"Ok.."}]
          })
        }
      })
    }

    const [selectedTech, setSelectedTech] = createSignal<'upnp'|'upmp'|'pcp'>('upnp');

    return <Modal
    onHide={()=>props?.dismiss?.()}
    fullscreen={true}
    show={props.show}
    backdrop="static"
    keyboard={false}
  >
    <Modal.Header closeButton>
      <Modal.Title>Port Forward (Your game port: {myGamePort()})</Modal.Title>
      <Show when={isPending()}>
        <Spinner style={{"margin-left":'5pt'}} animation="border" variant="primary" />
      </Show>
    </Modal.Header>
    <Modal.Body>

    <Button disabled={isPending()} onClick={()=>justForwardForMe()}>Just forward port for me</Button>
    <Button disabled={isPending()} onClick={()=>checkMyConnectivity()} style={{"margin-left":'10pt'}}>Check My GamePort Connectivity</Button>

    <Button disabled={isPending()} onClick={async ()=>{

      let value:string = ''
      const dialogRes = await DialogMgr.addPopup({
        title:"Choose Port Number",
        body:()=>
          <Form.Control
            placeholder="Port Num"
            type="input"
            max={65535}
            min={50}
            onChange={(e)=>value=(e.target as HTMLInputElement)?.value || ''}
          />,
        buttons:[{body:"Check"}, {body:"Cancel"}]
      })
      if(dialogRes?.buttonIndex !== 0) return;

      const port = parseInt(value.trim());
      if(!port || Number.isNaN(port) || port < 50 || port > 65535) {
        DialogMgr.addPopup({
          title:"Invalid Port Number",
          body: 'Please enter a valid port number',
          closeButton:true, buttons:[{body:"ok"}]
        })
        return;
      }
      checkMyConnectivity(port)
    }} style={{"margin-left":'10pt'}}>Check Other Port Number</Button>
    
    <hr />

    <Button disabled={isPending()} onClick={()=>queryMappings(true)}>Get All Mappings</Button>
    <Table striped bordered hover size="sm">
      <thead>
        <tr>
          <th>Enabled</th><th>IsLocal</th><th>Proto</th><th>Private</th><th>Public</th><th>Description</th><th>TTL(Seconds)</th><th></th>
        </tr>
      </thead>
      <tbody>
        <For each={mappings()}>
          {(item, i)=><tr>
              <td>{boolDesc(item.enabled)}</td>
              <td>{boolDesc(item.local)}</td>
              <td>{item.protocol}</td>
              <td>{formatHostAndPort(item.private.host, item.private.port)}</td>
              <td style={{"background-color": myGamePort()==item.public.port?'lightblue':''}}>
                <OverlayTrigger placement='top' overlay={myGamePort()==item.public.port?<Tooltip>Your Gameport</Tooltip>:''}>
                  <span>{formatHostAndPort(item.public.host, item.public.port)}</span>
                </OverlayTrigger>
              </td>
              <td>{item.description}</td>
              <td>{item.ttl}</td>
              <td><Button disabled={isPending()} onClick={()=>deleteMapping(item)}>❌</Button></td>
            </tr>}
        </For>
      </tbody>

    </Table>

    <div>
      <h4>Add Mapping</h4>
      <Row class="g-2">
        <Col md>
          <FloatingLabel controlId="forward-tech" label="Tech">
            <Form.Select onchange={v=>{setSelectedTech((v.target as any).value)}}>
              <option value="upnp">UPnP/IGD</option>
              <Show when={natPuncher}>
                <option value="pmp">NAT-PMP</option>
                <option value="pcp">PCP</option>
              </Show>
            </Form.Select>
          </FloatingLabel>
        </Col>
        <Col md>
          <FloatingLabel controlId="forward-protocol" label="Protocol">
            <Form.Select>
              <option value="udp">UDP</option>
              <Show when={selectedTech()!='pcp'}>
                <option value="tcp">TCP</option>
                <option value="tcp&udp">TCP&UDP</option>
              </Show>
            </Form.Select>
          </FloatingLabel>
        </Col>
        <Col md>
          <FloatingLabel controlId="forward-port-private" label="Private Port">
            <Form.Control type="text" />
          </FloatingLabel>
        </Col>
        <Col md>
          <FloatingLabel controlId="forward-port-public" label="Public Port">
            <Form.Control type="text" />
          </FloatingLabel>
        </Col>
        <Col md>
          <FloatingLabel controlId="forward-ttl" label="TTL(seconds)">
            <Form.Control type="number" />
          </FloatingLabel>
        </Col>
        <Col md>
          <Button disabled={isPending()} onClick={()=>addMapping()}>➕Add</Button>
        </Col>
      </Row>
      

    </div>

    </Modal.Body>
    <Modal.Footer>
      <Button variant="secondary" onclick={()=>props?.dismiss?.()}>Dismiss</Button>
    </Modal.Footer>
  </Modal>
}


export default PortForwardModal;