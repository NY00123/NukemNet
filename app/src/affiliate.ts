
export type AffiliateType = 'zoom-platform'|'gog'

const gog_affiliate_prefix = `https://track.adtraction.com/t/t?a=1578845458&as=1827400461&t=2&tk=1&url=https://www.gog.com/game/`

const zoom_affiliate_template = `https://www.zoom-platform.com/product/{nameId}?affiliate=ec7ead78-709f-4eb9-b5fd-7c69815d5e09`

export function generateGogAffiliateLink(nameId:string) {
    return `${gog_affiliate_prefix}${nameId}`;;
}

export function generateZoomAffiliateLink(nameId:string) {
    return zoom_affiliate_template.replace('{nameId}', nameId)
}

export function generateLinkForAffiliate(type:AffiliateType, nameId:string) {
    if(type=='gog') {
        return generateGogAffiliateLink(nameId);
    } else if(type=='zoom-platform') {
        return generateZoomAffiliateLink(nameId);
    } else {
        throw new Error('Unknown affiliate: '+type);
    }
}

