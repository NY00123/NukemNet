
const botDefs = [
    '+sv addbot cyborg {skill} cyborg cyborg/ps9000 0',
    '+sv addbot viper {skill} male male/viper 0',
    '+sv addbot venus {skill} female female/venus 0',
    '+sv addbot voodoo {skill} female female/voodoo 0',
    '+sv addbot ultimate_killer {skill} male male/sniper 0',
    '+sv addbot grunt {skill} male male/grunt 0',
    '+sv addbot Dyscord {skill} male male/recon 0',
    '+sv addbot Maul {skill} LordMaul LordMaul/LordMaul 0',
    '+sv addbot Blade {skill} blade blade/blade 0',
    '+sv addbot Han {skill} HanSolo HanSolo/solo_anh 0',
    '+sv addbot McClane {skill} McClane McClane/nakatomi1 0',
    '+sv addbot Ralphael {skill} tmnt tmnt/ralph 0',
    '+sv addbot AT-ST {skill} AT-ST AT-ST/atst 0',
    '+sv addbot Chastity {skill} Chastity Chastity/Chastity1 0',
    '+sv addbot Boba_Fett {skill} BobaFett BobaFett/ROTJ_Fett 0',
    '+sv addbot 00M-9 {skill} bdroid bdroid/00M-9 0',
    '+sv addbot Link {skill} Link Link/link 0'
];

module.exports = function ({}) {

    const path = require('path');
    const fs = require('fs');
    const fsPromises = require('fs/promises');    

    const levelsCalc = [
        
        ['* Deathmatch Maps', 'q2dm1'],
        ['The Edge', 'q2dm1'],
        ["Tokay's Towers", 'q2dm2'],
        ['The Frag Pipe', 'q2dm3'],
        ['Lost Hallways', 'q2dm4'],
        ['The Pits', 'q2dm5'],
        ['Lava Tomb', 'q2dm6'],
        ['The Slimy Place', 'q2dm7'],
        ['Warehouse', 'q2dm8'],

        ['* CTF Maps','q2ctf1'],
        ['McKinley Revival', 'q2ctf1'],
        ['Stronghold Opposition', 'q2ctf2'],
        ['The Smelter', 'q2ctf3'],
        ['Outlands', 'q2ctf4'],
        ['Capture Showdown', 'q2ctf5'],

        ['-- Quake II Campaign', 'base1'],

        ['* Unit 1: Base', 'base1'],
        ['Outer Base', 'base1'],
        ['Installation', 'base2'],
        ['Comm Center', 'base3'],
        ['Lost Station', 'train'],

        ['* Unit 2: Station', 'bunk1'],
        ['Ammo Depot', 'bunk1'],
        ['Supply Station', 'ware1'],
        ['Warehouse', 'ware2'],

        ['* Unit 3: Jail', 'jail1'],
        ['Main Gate', 'jail1'],
        ['Detention Center', 'jail2'],
        ['Security Complex', 'jail3'],
        ['Torture Chambers', 'jail4'],
        ['Guard House', 'jail5'],
        ['Grid Control', 'security'],

        ['* Unit 4: Mine', 'mintro'],
        ['Mine Entrance', 'mintro'],
        ['Upper Mines', 'mine1'],
        ['Borehole', 'mine2'],
        ['Drilling Area', 'mine3'],
        ['Lower Mines', 'mine4'],

        ['* Unit 5: Factory', 'fact1'],
        ['Receiving Center', 'fact1'],
        ['Sudden Death', 'fact2'],
        ['Processing Plant', 'fact3'],

        ['* Unit 6: Power Plant', 'power1'],
        ['The Reactor', 'power2'],
        ['Cooling Facility', 'cool1'],
        ['Toxic Waste Dump', 'waste1'],
        ['Pumping Station 1', 'waste2'],
        ['Pumping Station 2', 'waste3'],

        ['* Unit 7: Big Gun', 'biggun'],

        ['* Unit 8: Hangar', 'hangar1'],
        ['Outer Hangar', 'hangar1'],
        ['Comm Satellite', 'space'],
        ['Research Lab', 'lab'],
        ['Inner Hangar', 'hangar2'],
        ['Launch Command', 'command'],
        ['Outlands', 'strike'],

        ['* Unit 9: Palace', 'city1'],
        ['Outer Courts', 'city1'],
        ['Lower Palace', 'city2'],
        ['Upper Palace', 'city3'],
        
        ['* Unit 10: Final Showdown', 'boss1'],
        ['Inner Chamber', 'boss1'],
        ['Final Showdown', 'boss2'],

        ['-- Mission Pack 1: The Reckoning', 'xswamp'],
        ['* Unit 1: Wilderness', 'xswamp'],
        ['The Sewers', 'xsewer1'],
        ['Waste Sieve', 'xsewer2'],

        ['* Unit 2: Military Compound', 'xcompnd1'],
        ['Outer Compound', 'xcompnd1'],
        ['Inner Compound', 'xcompnd2'],
        ['Core Reactor', 'xreactor'],
        ['The Warehouse', 'xware'],
        ['Intelligence Center', 'xintell'],

        ['* Unit 3: Industrial Region', 'industry'],
        ['Industrial Facility', 'industry'],
        ['Outer Base', 'outbase'],
        ['Refinery', 'refinery'],
        ['Water Treatment Plant', 'w_treat'],
        ['Badlands', 'badlands'],

        ['* Unit 4: Strogg Spaceport', 'xhangar1'],
        ['Lower Hangars', 'xhangar1'],
        ['The Hangars', 'xhangar2'],
        ['Strogg Freighter', 'xship'],

        ['* Unit 5: Moon Base', 'xmoon1'],
        ['Cargo Bay', 'xmoon1'],
        ['Command Center', 'xmoon2'],

        ['-- Mission Pack 2: Ground Zero', 'rmine1'],
        ['* Unit 1: Tectonic Stabilizer', 'rmine1'],
        ['Lower Mines', 'rmine1'],
        ['Thaelite Mines', 'rlava1'],
        ['Tectonic Stabilizer', 'rlava2'],
        ['Mine Engineering', 'rmine2'],

        ['* Unit 2: Base Complex', 'rware1'],
        ['Eastern Warehouse', 'rware1'],
        ['Waterfront Storage', 'rware2'],
        ['Logistics Complex', 'rbase1'],
        ['Tactical Command', 'rbase2'],

        ['* Unit 3: Research Hangars', 'rhangar1'],
        ['Research Hangar', 'rhangar1'],
        ['Waste Processing', 'rsewer1'],
        ['Waste Disposal', 'rsewer2'],
        ['Maintenance Hangars', 'rhangar2'],

        ['* Unit 4: Munitions Installation', 'rammo1'],
        ['Munitions Plant', 'rammo1'],
        ['Ammo Depot', 'p'],
        
        ["* Unit 5: Widow's Lair", 'rboss'],
    ]
    
    const levels = levelsCalc.map(([name, id]) => ({
        "label": name,
        "value": [id]
    }));

    const dmFlagsParams = {};
    const dmFlagsArr = [
        ["nh", "No Health"],
        ["np", "No Powerups"],
        ["ws", "Weapons Stay"],
        ["nfd", "No Falling Damage"],
        ["ip", "Instant Powerups"],
        ["sm", "Same Map"],
        ["tbs", "Teams by Skin"],
        ["tbm", "Teams by Model"],
        ["nff", "No Friendly Fire"],
        ["sf", "Spawn Farthest"],
        ["fr", "Force Respawn"],
        ["na", "No Armor"],
        ["ae", "Allow Exit"],
        ["ia", "Infinite Ammo"],
        ["qd", "Quad Drop"],
        ["ff", "Fixed FOV"]
    ];
    let i=1;

    let calculateDmFlags = '';
    for(const [id, desc] of dmFlagsArr) {
        dmFlagsParams[`df_${id}`] = {
            "modeSupport": ["multiplayer", "singleplayer"],
            "type": "boolean",
            "label": desc,
            "value": i,
            "for": "host-only-shared",
            syncOnly:true,
            section:'dmflags'
        }
        i *= 2;
        calculateDmFlags += `parseInt(GameRoom?.Params?.df_${id}?.[0]||0)+`
    }
    if(calculateDmFlags.endsWith('+')) {
        calculateDmFlags = calculateDmFlags.slice(0, -1);
    }
    const NNCfg_HostFile = "NukemNetHost.cfg";
    const NNCfg_ClientFile = "NukemNetClient.cfg";

    const writeCommandsCfg = [{"cmd":"function", async run(ctx, execDef, result, imHost, players, hostIpDetails, useDedicated, othersIpDetails, channelName, hostTunnelChosenPort) {
        const fname = imHost?NNCfg_HostFile:NNCfg_ClientFile;
        const params = ctx.GameRoom.Params;
        let lineCommands = [];
        for(const p in params) {
            const argsArr = params[p];
            for(const line of argsArr) {
                const trm = line.trim();
                if(trm.startsWith("+") && !trm.startsWith("+exec") && !p.startsWith("_") && ctx?.GameRoom?.ParamDefs?.[p]?.syncOnly) {
                    lineCommands.push({id:p, line:trm.substring(1)});
                }
            }
        }
        lineCommands.sort((a,b) => {
            const aOrder = ctx.GameRoom.ParamDefs[a.id]?.order;
            const bOrder = ctx.GameRoom.ParamDefs[b.id]?.order;

            if(aOrder!= null && bOrder!= null) {
                return aOrder>bOrder;
            } else if(aOrder != null) {
                return -1;
            } else if(bOrder != null) {
                return 1;
            } else {
                return 0;
            }
        });
        const fileContent = lineCommands.map(r=>r.line).join('\r\n');
        await fsPromises.writeFile(path.resolve(ctx.GameDir, fname), fileContent);
    }}];

    const params = {
        "_dedicated": {
            order:-10,
            "modeSupport": ["multiplayer"],
            "type": "boolean",
            "label": "Dedicated",
            "value": ["'+set'", "'dedicated'", "'1'"],
            "for":"host-only-shared"
        },
        "cmdshost": {
            order:-9,
            "modeSupport": ["multiplayer","singleplayer","settings"],
            "type": "static",
            "value": ["'+exec'", `'../${NNCfg_HostFile}'`],
            "for":"host-only-private",
        },
        "cmdsclnt": {
            order:-9,
            "modeSupport": ["multiplayer","singleplayer","settings"],
            "type": "static",
            "value": ["'+exec'", `'../${NNCfg_ClientFile}'`],
            "for":"client-only-private",
        },
        "clientport": {
            order:-9,
            "modeSupport": ["multiplayer"],
            "type": "static",
            "value": ["'+set clientport '+GameRoom.RandomPort"],
            "for":"private",
            syncOnly:true
        },
        "client": {
            order:-8,
            "modeSupport": ["multiplayer"],
            "type": "static",
            "value": ["'+connect '+GameRoom.DestIp+':'+GameRoom.DestPort"],
            "for":"client-only-private",
            syncOnly:true
        },
        "maxclnts": {
            order:-7,
            "modeSupport": ["multiplayer", "singleplayer"],
            "type": "static",
            "value": ["'+set maxclients 16'"],
            "for":"host-only-private",
            syncOnly:true
        },
        "hostport": {
            order:-6,
            "modeSupport": ["multiplayer"],
            "type": "static",
            "value": ["'+set port '+GameRoom.MyPort", "'+set net_port '+GameRoom.MyPort", "'+set hostport '+GameRoom.MyPort", "'+set hostname NukemNet'"],
            "for":"host-only-private",
            syncOnly:true
        },
        "name": {
            order:-5,
            "modeSupport": ["multiplayer", "singleplayer"],
            "type": "static",
            "value": ["'+name '+MyName"],
            syncOnly:true
        },
        "dmflags": {
            order:-4,
            "modeSupport": ["multiplayer", "singleplayer"],
            "type": "static",
            "value": [`'+set dmflags '+(${calculateDmFlags})`],
            syncOnly:true
        },
        game: {
            order:-3,
            "modeSupport": ["multiplayer", "singleplayer", "settings"],
            "type": "dirfiles",
            "value": ["'+set game '+value[0]"],
            "label": "Game",
            "path": "*",
            globOpts: {
                stat: true,
                ignore: {
                    ignored: (p) => {
                        const isGameFolder = p.isDirectory() && (
                            fs.existsSync(path.join(p.fullpath(), 'pak0.pak')) ||
                            fs.existsSync(path.join(p.fullpath(), 'config.cfg'))
                        );
                        return !isGameFolder;
                    }
                }
            },
            "description": "Participants must have a copy of it in their quake2 folder",
            "optional":true,
            "for":"host-only-shared",
            syncOnly:true
        },
        "mode": {
            order:-2,
            "modeSupport": ["multiplayer", "singleplayer"],
            "type": "choice",
            optional:true,
            "label": "Mode",
            "value":["value[0]", "value[1]", "value[2]"],
            "choices": [{
                "label": "Deathmatch",
                "value": ["+set deathmatch 1", "+set coop 0", "+set ctf 0"]
            }, {
                "label": "Cooperative",
                "value": ["+set deathmatch 0", "+set coop 1", "+set ctf 0"]
            }, {
                "label": "CTF",
                "value": ["+set deathmatch 1", "+set coop 0", "+set ctf 1"]
            }],
            "for":"host-only-shared",
            syncOnly:true
        },
        "defmode": {
            order:-2,
            "modeSupport": ["multiplayer"],
            "type": "static",
            "value": ["GameRoom?.Params?.mode?.[0]?undefined:'+set deathmatch 1'"], // if on multiplayer, and forgot to choose mode, default to deathmatch
            "for":"host-only-private",
            syncOnly:true
        },
        "_level": {
            "modeSupport": ["multiplayer", "singleplayer"],
            "type": "choice",
            "label": "Level",
            "value":["'+map '+value[0]"],
            "optional":true,
            "choices": levels,
            "for":"host-only-shared",
            syncOnly:true
        },
        "_gamemap": {
            "modeSupport": ["multiplayer", "singleplayer"],
            "type": "dirfiles",
            "value": ["'+gamemap '+value[0]"],
            "label": "UserMap",
            "path": "baseq2/maps/*.bsp",
            "stripPath": true,
            "stripExt": true,
            "optional":true,
            "for":"host-only-shared",
            syncOnly:true
        },
        "map": {
            order:-1,
            "modeSupport": ["multiplayer", "singleplayer"],
            "type": "static",
            "value": ["GameRoom?.Params?._gamemap?.[0] || GameRoom?.Params?._level?.[0] || (GameRoom?.Mode=='multiplayer'?'+map q2dm1':undefined)"],
            "for":"host-only-private",
            syncOnly:true
        },
        "skill": {
            "modeSupport": ["multiplayer","singleplayer"],
            "type": "choice",
            "label": "Skill",
            "value":["'+set skill '+value"],
            "optional":true,
            "choices": [{
                "label": "Easy",
                "value": "0"
            }, {
                "label": "Medium",
                "value": "1"
            }, {
                "label": "Hard",
                "value": "2"
            }, {
                "label": "Hard+",
                "value": "3"
            }],
            syncOnly:true
        },
        "frglmt": {
            "modeSupport": ["multiplayer", "singleplayer"],
            "type": "numrange",
            "min":0,
            "max":200,
            "delta":1,
            "label": "Frag Limit",
            "value": "'+set fraglimit '+value",
            description:"The score that is necessary for changing of maps",
            "optional":true,
            for:'host-only-shared',
            syncOnly:true
        },
        "tmlmt": {
            "modeSupport": ["multiplayer", "singleplayer"],
            "type": "numrange",
            "min":0,
            "max":200,
            "delta":1,
            "label": "Time Limit",
            description:"The number of minutes before switching to the next level",
            "value": "'+set timelimit '+value",
            "optional":true,
            for:'host-only-shared',
            syncOnly:true
        },
        "cptlmt": {
            "modeSupport": ["multiplayer", "singleplayer"],
            "type": "numrange",
            "min":0,
            "max":100,
            "delta":1,
            "label": "Capture Limit",
            "value": "'+set capturelimit '+value",
            "optional":true,
            for:'host-only-shared',
            syncOnly:true
        },
        "_bots": {
            "modeSupport": ["multiplayer", "singleplayer"],
            "type": "numrange",
            "min":1,
            "max":16,
            "delta":1,
            description:'Must select CRBot "Game" option',
            "label": "Bots",
            "value": "value",
            "optional":true,
            for:'host-only-shared',
            syncOnly:true
        },
        "_botsSkill": {
            "modeSupport": ["multiplayer", "singleplayer"],
            "type": "numrange",
            "min":0,
            "max":10,
            "delta":1,
            "label": "Bots Skill",
            "value": "value",
            "optional":true,
            for:'host-only-shared',
            syncOnly:true
        },
        "bots": {
            "modeSupport": ["multiplayer", "singleplayer"],
            "type": "static",
            "value": `(()=>{
                const botsNum = GameRoom?.Params?._bots?.[0];
                const botsSkill = GameRoom?.Params?._botsSkill?.[0] || 3;
                if (botsNum) {
                    const botDefs = ${JSON.stringify(botDefs)};
                    const botDefsRes = botDefs.sort(() => Math.random() - 0.5).slice(0, botsNum).map(bot=>bot.replace('{skill}',botsSkill));
                    return botDefsRes;
                }
                return undefined;
            })()`,
            "for":"host-only-private",
            syncOnly:true
        },
        "rdmo": {
            "modeSupport": ["multiplayer","singleplayer"],
            "type": "text",
            "label": "Record Demo",
            "value":["'+record ' + value"],
            "optional":true,
            props: {
                placeholder: "Demo File Name"
            },
            syncOnly:true
        },
        // using "freetext" instead of "file" dialog because quake2 does not support absolute paths for demo playback
        "pdmo": {
            "modeSupport": ["singleplayer"],
            "type": "text",
            "label": "Play Demo",
            "value":["'+demomap ' + (value.includes('.')?value:value+'.dm2')"],
            "optional":true,
            "for":"private",
            props: {
                placeholder: "Demo File .dm2"
            },
            syncOnly:true
        },
        ...dmFlagsParams
    }

    const paramSections = {
        dmflags: {
            name: "DmFlags"
        }
    }

    return {
        "name": "Quake 2",
        "get": [{
            type:"gog",
            prodId:"1443696817",
            nameId:"quake_ii_quad_damage",
            name:"Quake II"
        }],
        executables: {
            "full": {
                name:"Full",
                "maxPlayers":16,
                midGameJoin: true,
                tips: {
                    html:`<div>
                    <u>Using the Yamagi Quake 2 client is recommended.</u><br/>
                    https://www.yamagi.org/quake2/<br/>
                    Get the Precompiled Windows binaries, extract them into a directory such as<br/>
                    C:\\games\\quake2, and copy your original game files from your game dir<br/>
                    (e.g C:\\Program Files (x86)\\Steam\\steamapps\\common\\Quake 2)<br/>
                    * Some Quake 2 source ports, including vanilla Quake 2 and Yamagi are compatible for cross-play.<br/>
                    <br/>
                    <u>Playing With Bots</u><br/>
                    To play with bots, you need to download CrBot from<br/>
                    https://www.lonebullet.com/mods/download-the-crbot-quake-2-mod-free-36963.htm<br/>
                    And copy the crbot folder into your quake2 directory.<br/>
                    * You must enable deathmatch Mode, and select "crbot" under "Game" for bots to work.
                    </div>`
                },
                networking: {
                    allocate_RandomPort: true
                },
                "files": {
                    "main": {"path":"quake2.exe"}
                },
                "runnables": {
                    "multiplayer": {"file":"main", beforeRun:writeCommandsCfg},
                    "settings": {"file":"main", beforeRun:writeCommandsCfg},
                    "singleplayer": {"file":"main", beforeRun:writeCommandsCfg}
                },
                parameters:params,
                paramSections
            }
        }
    }
}