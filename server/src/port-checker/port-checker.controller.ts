import { Controller, HttpException, HttpStatus, Param, Put, Req } from '@nestjs/common';
import { isIPv4, isIPv6 } from 'net';
import * as udp from 'dgram';
import * as net from 'net';
import { Request } from 'express';

@Controller('port-checker')
export class PortCheckerController {

    @Put('udp/port/:port')
    testUdpPort(@Req() request: Request, @Param('port') port:number) {
        const ip = request?.socket?.remoteAddress;
        let socket:udp.Socket;
        if(isIPv4(ip)) {
            socket = udp.createSocket('udp4');
        } else if(isIPv6(ip)) {
            socket = udp.createSocket('udp6');
        } else {
            throw new HttpException('Invalid IP address', HttpStatus.BAD_REQUEST);
        }
        socket.send('h', port, ip);
    }

    @Put('tcp/port/:port')
    testTcpPort(@Req() request: Request, @Param('port') port:number) {
        const ip = request?.socket?.remoteAddress;
        let socket = net.createConnection(port, ip);
        socket.once('connect',()=>{try {socket.destroy()} catch(e) {}});
        socket.once('error',()=>{try {socket.destroy()} catch(e) {}});
        socket.once('timeout',()=>{try {socket.destroy()} catch(e) {}});
        socket.once('close',()=>{try {socket.destroy()} catch(e) {}});
    }

    @Put('tcpudp/port/:port')
    testTcpUdpPort(@Req() request: Request, @Param('port') port:number) {
        this.testTcpPort(request, port);
        this.testUdpPort(request, port);
    }
}
