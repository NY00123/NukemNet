import { ChildProcess } from "child_process";
import { RemoteInfo, Socket as UDPSocket } from "dgram";
import { Socket as TCPSocket } from 'net'
import { Client, IrcClientOpts } from "matrix-org-irc";
import { createMutable } from "solid-js/store";
import { RelayClient } from "../../../relay-service/src/udp-relay/relay-client";
import { Cmd_ManageRelayRequestDestroy, LeasedDedicatedServerDetails } from "../../../server/src/common/commands";
import { BotMetaChannel_lowercase, BotName_lowercase, ClientAutojoinChannels, IrcModSignOrNone, NukemNetLobby_lowercase } from "../../../server/src/common/constants";
import { IrcClientExtra } from "../../../server/src/common/ircExtend";
import { LaunchArgType } from "../../../server/src/common/protobuf/main";
import { Room_Advertisement } from "../../../server/src/common/room";
import { CmdGameRoom_ToggleDedicatedServer, FullGameRoomDetails, IpAndPort, IpDetailsAndPorts, LanDetectionDetails, SEND_CmdGameRoom_IsExecMissing, SEND_CmdGameRoom_IsExecRunning } from "../commands-gameroom";
import { GameDefsType } from "../GameDefsTypes";
import { IRCEvent_BaseEvent, IRCEvent_ChangeNick, IRCEvent_ChannelMessage, IRCEvent_Join, IRCEvent_Kick, IRCEvent_PlusMod, IRCEvent_Part, IRCEvent_PrivateMessage, IRCEvent_Quit, IRCEvent_Topic, IRCEvent_Kill } from "../irc-events/ircEvents";
import { LanMode_Activate } from "../LanMode";
import { Eventer } from "../lib/Eventer";
import { IpDetails } from "../lib/IpDetails";
import { StunConnEndpoint, StunController } from "../lib/stun";
import { electronRemote, events, fsSync, json5, path, MatrixOrgIrc } from "../NodeLibs";
import { pathOfUserDir, Settings } from "../Settings";
import storeActions from "./storeActions";
import { setupIpv6Tunnel } from "../ipv6tunnel";
import { KCP } from "node-kcp-x";
import { setupFileSync } from "../filesync";
import { Server } from "http";


export const backlogTab = '@Backlog';
export const backlogTabL = backlogTab.toLowerCase();

export const minimumNukemNetExtensionVersionSupport = '0.2.0';

const bootSettings = Settings.read();

export const {serverHost, serverHttpPort, ircCfg, offline} = (()=>{
    const serverFileName = 'server.json';
    const serverFile = path.resolve(pathOfUserDir, serverFileName);

    const isDev = electronRemote.getGlobal('isDev');
    const defaultServer = isDev?'ircstag.nukemnet.com':'irc.nukemnet.com'
    const cfg = {
        serverHost: defaultServer, serverHttpPort: 9999, ircCfg:{address:defaultServer},offline:false,
    } as {serverHost:string, serverHttpPort:number, ircCfg:IrcClientOpts&{address:string},offline:boolean};

    if(fsSync.existsSync(serverFile)) {
        try {
            const serverCfg = json5.parse(fsSync.readFileSync(serverFile).toString()) as {
                offline?:true,
                api?: {
                    host?: string,
                    port?: number
                },
                irc?: IrcClientOpts
            };
            if(serverCfg?.api?.host) {cfg.serverHost = serverCfg.api.host;}
            if(serverCfg?.api?.port) {cfg.serverHttpPort = parseInt(serverCfg.api.port as any);}
            if(serverCfg?.irc) {cfg.ircCfg = serverCfg.irc as any;}
            if(serverCfg?.offline != null) {cfg.offline = serverCfg.offline}
        } catch(e) {
            alert(`user/${serverFileName} is malformed, fix it, or delete it.\r\n\r\n${(e as any).stack}`);
            electronRemote.process.exit();
        }
    }

    if(bootSettings.lanOnlyMode) {
        cfg.offline = true;
        cfg.ircCfg.address = '254.254.254.1' // temporary invalid IP to prevent connecting
        cfg.ircCfg.port = 6667;
        cfg.ircCfg.secure = false;
        cfg.ircCfg.sasl = false;
        cfg.ircCfg.channels = [NukemNetLobby_lowercase]; // Only join to lobby, not meta channel, because bot won't be there anyway.
    }

    return cfg;
})();

export const ircClient = new MatrixOrgIrc.Client(ircCfg.address, bootSettings.IrcNick!, {
  realName: `NukemNet ${Settings.pjson.version}${Settings?.pjson?.runEnv?` ${Settings?.pjson?.runEnv}`:''}`,
  channels: ClientAutojoinChannels,
  autoConnect:false,
  port:6697,
  secure:true,
//   port:6667,
//   secure:false,
  certExpired: true,
  selfSigned: true,
  retryCount: 0, // disable matrix-org-irc reconnection logic, using a manual reconnection logic instead. see "connectWithRetry()"
  messageSplit: 400, // Without it, the library sometimes loses some characters between the splits (when sender is sending a long string that is being spitted) once it has to split, verified that behaviour in my multi-part support
  encoding: 'binary', // Must put that to prevent the default 'utf8' from being set, it would break Binary messages (such as ProtoBuff with yEnc) on secured TLS connections.
  stripColors: false, // Add colors support and disable it, because stripping might edit cmd messages from protocol-buffers and yEnc, and break multi-part message support.
  sasl:true,
  ...ircCfg
});
IrcClientExtra.fixMatrixOrgIrcBug(ircClient);

if(bootSettings.lanOnlyMode) {LanMode_Activate(ircClient);}

export const nncmdReceivedEmitter = new events.EventEmitter();

export type EventBaseType = 'channel'|'backlog'|'private';

export interface NickIpv6TunnelData {kcp?:KCP, tcpSocket?:TCPSocket, udpSocket?:UDPSocket, udpConnector?:RemoteInfo, r?:boolean, lrh?:number} // address is always localhost, r means if the other side received my handshake, lrh = last received handshake

export interface NickChanData {
    name:string,
    sign:IrcModSignOrNone,
    lan: LanDetectionDetails,
    country?:string,
    ping?:number,
    pingToHost?:number
    stunEndpoint?: StunConnEndpoint,
    localIpv6Tunnel?: NickIpv6TunnelData
}

interface MainStoreRoot {
    connected: boolean,
    myNick: string,
    muteAll?: boolean,
    isLoggedIn?:boolean
    isRegistered?:boolean
    showRoomsList?:boolean
    httpPort:number;
    httpServer:Server|null;
    ipv6udpTunnel:UDPSocket|null;
    ipv6TunnelPort:number;
    localhostIp4:string;
    backlog: {
        events: IRCEvent_BaseEvent[],
        hasUnread?:boolean
    }
    privateChats: { // Private messaging
        [nick:string]: {
            name:string
            events: (IRCEvent_Quit|IRCEvent_Kill|IRCEvent_PrivateMessage|IRCEvent_ChangeNick)[],
            hasUnread?:boolean
        }
    },
    joinedChannels: {
        [channel:string]: {
            name:string
            topic: string
            nicks:{[nick:string]: NickChanData},
            game?: null|FullGameRoomDetails, // theres a separation between "game", and data under "nicks", because the "game" entry is synced to everyone, and data under "nicks" is local.
            stunCtl?: StunController,
            events: (IRCEvent_Quit|IRCEvent_Kill|IRCEvent_ChangeNick|IRCEvent_ChannelMessage|IRCEvent_Join|IRCEvent_Part|IRCEvent_Kick|IRCEvent_PlusMod|IRCEvent_Topic)[]
            hasUnread?:boolean,
            execMissing?:boolean,
            proc?:ChildProcess
            password?:string // Only the room owner keeps the password
        }
    },
    advertisedGameRooms: { // Publicly advertised game rooms list from bot
        [channel:string]: {
            publicDetails: Room_Advertisement
        }
    },
    channelAdvertiseInProgress: {[chanName:string]:boolean},
    ipDetails: IpDetails,
    myLeasedDedicatedServer?: LeasedDedicatedServerDetails_Host, // e.g udp relay
    isRefreshingGameRoomsList?:boolean,
    imAloneInChannel(chan:string):boolean
}

export const mainStore = createMutable({
    // the 5 properties below are set on refreshIpDetails
    httpPort: 0, // used for lan ip detection
    httpServer: null as null|Server,
    ipv6tcpTunnel: null,
    ipv6udpTunnel: null,
    ipv6TunnelPort: 0,
    localhostIp4:'',
    disableLanDetection: fsSync.existsSync(path.join(pathOfUserDir, 'disableLanDetection')),
    connected: false,
    isRegistered: false,
    myNick: '',
    showRoomsList: true,
    muteAll: !!Settings.read().muteAll,
    isRefreshingGameRoomsList:false,
    advertisedGameRooms: {},
    joinedChannels: {},
    privateChats: {},
    backlog: {events: []},
    ipDetails: {localIpV4: [], localIpV6: []},
    channelAdvertiseInProgress: {} as {[chanName:string]:boolean},
    imAloneInChannel(chan:string):boolean  {
        const lchan = chan?.toLowerCase();
        return this?.joinedChannels?.[lchan]?.nicks != null && Object.keys(this.joinedChannels[lchan].nicks).length === 1;
    },
    isChannelGameRoom(chan:string):boolean {
        const lchan = chan?.toLowerCase()
        return this?.joinedChannels?.[lchan]?.game != null
    },
    imHostInChannel(chan:string):boolean {
        const lchan = chan.toLowerCase();
        return this.isChannelGameRoom(lchan) && this.joinedChannels[lchan].game?.publicDetails?.owner?.toLowerCase() === mainStore.myNick.toLowerCase();
    },
    gameForChannel(chan:string) {
        return this?.joinedChannels?.[chan?.toLowerCase()]?.game || null;
    },
    sendAllJoinedGameRooms_ImRunningExec(isRunning:boolean, ircClient:Client) {
        for(const c in mainStore.joinedChannels) {
            const game = this.gameForChannel(c);
            if(game) {
                SEND_CmdGameRoom_IsExecRunning(c, isRunning, ircClient);
            }
        }
    },
    getGameRoom(exceptChan?:string) {
        const lchan = exceptChan?.toLowerCase();
        for(const c in this.joinedChannels) {
            const game = this.gameForChannel(c);
            if(game && c.toLowerCase() != lchan) {
                return {game, chan:c};
            }
        }
        return null;
    },
    imAlreadyInAGameRoom(exceptChan?:string) {
        return this.getGameRoom(exceptChan) != null;
    },
    turnOffDedicatedServer(gameDetails:FullGameRoomDetails, ircClient:Client) {
        const g = gameDetails;
        delete g.dedicatedServer;
        const myDedicated = mainStore.myLeasedDedicatedServer;
        delete mainStore.myLeasedDedicatedServer;

        if(myDedicated) {
            if(myDedicated.type === 'udp') {
                try {myDedicated.rc!.destroy();} catch(e) {}
            }
        }
    
        IrcClientExtra.sendNNCmdTo(ircClient, g.publicDetails.channel, {
          op: 'toggleDedicatedServer',
          data: {}
        } as CmdGameRoom_ToggleDedicatedServer)
    
        IrcClientExtra.sendNNCmdToBot(ircClient, {op:'manageRelayRequest', data:{action:'destroy'}} as Cmd_ManageRelayRequestDestroy)
    },
    addEventTo(type:EventBaseType, dest:string, eventBase:{events:IRCEvent_BaseEvent[], hasUnread?:boolean}, event:IRCEvent_BaseEvent) {
        if(type=='backlog') dest = 'backlog';
        
        eventBase.events.push(event);
        eventBase.hasUnread = true;
        setTimeout(()=>{
            this.tabEvents.emit(type as EventBaseType, {dest, event});
        },0);
    },
    hasGameExec(gameId:string, execId:string) {
        const s = Settings.read();
        const execPath = s?.games?.[gameId]?.[execId]?.path;
        if(execPath?.length) {
            try {
                return fsSync.existsSync(execPath);
            } catch(e) {}
        }
        return false;
    },
    refreshIsGameExecMissing_ReturnWhetherUpdateFound(gameid:string, execid:string, ircClient:Client, channel?:string):boolean {
        const isExecMissing = !mainStore.hasGameExec(gameid, execid);
        let hasUpdate = false;
        const gameRooms:{[chan:string]: FullGameRoomDetails} = {}
        if(channel?.length) {
            const g = mainStore.gameForChannel(channel);
            if(g && g?.publicDetails?.gameId == gameid && g?.publicDetails?.execId == execid) {
                gameRooms[channel.toLowerCase()] = g;
            }
        } else {
            for(const chan in mainStore.joinedChannels) {
                const g = mainStore.gameForChannel(chan);
                if(g && g?.publicDetails?.gameId == gameid && g?.publicDetails?.execId == execid) {
                    gameRooms[chan.toLowerCase()] = g;
                }
            }
        }
        
        for(const chan in gameRooms) {
            const g = gameRooms[chan];
            const prevExecMissing = g?.participantNicks?.[mainStore?.myNick?.toLowerCase()]?.isExecMissing;
            if(prevExecMissing != isExecMissing) {
                const pDetails = g.participantNicks[mainStore.myNick.toLowerCase()];
                pDetails.isExecMissing = isExecMissing;
                SEND_CmdGameRoom_IsExecMissing(chan, isExecMissing, ircClient)
            }
        }

        return hasUpdate;
    },
    async getResolvedIpsInGameRoom(channel:string, exceptNick?:string, noTunnul=false): Promise<{[nick:string]:IpAndPort}> {
        const lExceptNick = exceptNick?.toLowerCase()
        const lchan = channel.toLowerCase();

        const g = this.gameForChannel(lchan);
        if(!g) return {};

        const chanData = this.joinedChannels[lchan];
        if(!chanData) return {};

        const retVal:{[nick:string]:IpAndPort} = {};
        for(const n in g.participantNicks) {
            if(lExceptNick == n.toLowerCase()) continue;

            const pData = g.participantNicks[n];
            const nData = chanData.nicks[n];
            if(nData?.lan?.detectedLanIp?.length) {
                retVal[n] = {ip: nData.lan.detectedLanIp, port: pData.ipsAndPorts.gamePort!};
            } else if(!noTunnul && nData?.localIpv6Tunnel?.r && nData?.localIpv6Tunnel.udpSocket) {
                retVal[n] = {ip: mainStore.localhostIp4, port: nData?.localIpv6Tunnel?.udpSocket?.address()?.port};
            } else if(!noTunnul && nData.stunEndpoint?.otherReceivedMyHandshake) {
                retVal[n] = {ip: mainStore.localhostIp4, port: nData.stunEndpoint.tunnel.address().port};
            } else {
                retVal[n] = {ip: mainStore.userExternalIpV4(n), port: pData.ipsAndPorts.gamePort!};
            }
        }
        return retVal;
    },
    myIpsAndPorts():IpDetailsAndPorts {
        const retVal = {...mainStore.ipDetails, ...{gamePort:Settings.getGamePort(), httpPort: mainStore.httpPort, ipv6tp: mainStore.ipv6TunnelPort}} as IpDetailsAndPorts;
        return retVal;
    },
    execDefForChannel(channel:string):GameDefsType.Executable|null {
        const g =this.gameForChannel(channel);
        if(!g) return null;

        return Settings?.gameDefs?.games?.[g?.publicDetails?.gameId].executables?.[g?.publicDetails?.execId] || null;
    },
    supportsMidGame(channel:string) {
        const supported = this?.execDefForChannel?.(channel)?.midGameJoin === true;
        return supported;
    },
    hostExecIsRunningInChan(channel:string) {
        const lchan = channel.toLowerCase()
        const g = this.gameForChannel(lchan);
        if(!g) return false;
        const running:boolean = g.participantNicks?.[g.publicDetails?.owner?.toLowerCase()||'']?.isExecRunning === true;
        return running;
    },
    myExecIsRunningInChan(channel:string) {
        const lchan = channel.toLowerCase()
        const g = this.gameForChannel(lchan);
        if(!g) return false;
        const running:boolean = g.participantNicks?.[this?.myNick?.toLowerCase()||'']?.isExecRunning === true;
        return running;
    },
    resolveCountryCodeForIpForNick(nick:string|undefined, chan:string) {
        const g = this.gameForChannel(chan);
        if(!g) return;
        const lchan = chan.toLowerCase();

        const chanData = this.joinedChannels[lchan];
        const nicksToDetect = nick?.length? [nick]:Object.keys(chanData.nicks)
        for(const nick of nicksToDetect) {
            const lnick = nick.toLowerCase();
            const ipsAndPorts = g.participantNicks?.[lnick]?.ipsAndPorts
            if(!ipsAndPorts) continue;
    
            const nickData = this?.joinedChannels?.[lchan]?.nicks?.[lnick]
            if(!nickData) continue
    
            let country:string|null = null;
    
            const ipsToTry = [];
            if(ipsAndPorts?.externalIpV4?.length) ipsToTry.push(ipsAndPorts.externalIpV4);
            if(ipsAndPorts?.externalIpV6?.length) ipsToTry.push(ipsAndPorts.externalIpV6);
    
            for(const ip of ipsToTry) {
                const result = Settings.ip2country(ip);
                if(result?.length) {
                    country = result; 
                    break;
                }
            }
    
            if(country?.length) {
                nickData.country = country;
            } 
        }
        
    },
    channelAdvertisedAsGameRoom(chan:string):boolean {
        const lchan = chan.toLowerCase();
        return mainStore.advertisedGameRooms[lchan] != null;
    },
    botIsAvailable():boolean {
        return mainStore?.joinedChannels?.[BotMetaChannel_lowercase]?.nicks?.[BotName_lowercase]!=null;
    },
    gameChosenUdpTunnel(roomDetails?:FullGameRoomDetails, args?:LaunchArgType[]) {
        const _args = roomDetails?.params || args;
        const isUsingUdpTunnel = _args?.find(p=>p.id===GameDefsType.UDPTUNNEL_CHOICE_PARAM_ID)?.valueLabel?.startsWith('UDP4Tunnel')==true;
        return isUsingUdpTunnel;
    },
    gameChosenTCPAndNotOptional(roomDetails:FullGameRoomDetails, args:LaunchArgType[]=roomDetails.params) {
        const isUsingTcpMode = roomDetails.params?.find(p=>p.id===GameDefsType.UDPTUNNEL_CHOICE_PARAM_ID)?.valueLabel?.startsWith('TCP')==true;
        const tcpPortIsOptional = Settings.gameDefs?.games?.[roomDetails?.publicDetails?.gameId]?.executables?.[roomDetails?.publicDetails?.execId]?.networking?.tcpPort?.optional == true;
        return isUsingTcpMode && !tcpPortIsOptional;
    },
    gameIsDosbox (g:FullGameRoomDetails) {
        const execDef = Settings?.gameDefs?.games?.[g?.publicDetails?.gameId]?.executables?.[g?.publicDetails?.execId];
        if(!execDef) return false;

        const runnable = execDef?.runnables?.multiplayer;
        if(!runnable) return false;

        return runnable?.runTool?.type === 'dosbox';
    },
    gameDefIsDosbox(gameId:string, execId:string) {
        const execDef = Settings?.gameDefs?.games?.[gameId]?.executables?.[execId];
        if(!execDef) return false;

        const runnable = execDef?.runnables?.multiplayer;
        if(!runnable) return false;

        return runnable?.runTool?.type === 'dosbox';
    },
    execDefIsDosbox(execDef:GameDefsType.Executable) {
        if(!execDef) return false;

        const runnable = execDef?.runnables?.multiplayer;
        if(!runnable) return false;

        return runnable?.runTool?.type === 'dosbox';
    },
    get finishedDetectingIps() {
        return !storeActions.isRefreshingIpDetails && !!(this.ipDetails.externalIpV4?.length || this.ipDetails.externalIpV6?.length);
    },
    isChanUsingStun(chan:string):boolean {
        return mainStore?.joinedChannels?.[chan?.toLowerCase()]?.stunCtl!=null;
    },
    userExternalIpV4(nick?:string, chanName?:string):string {
        let ip = '';
        const ipDetails = this.userIpDetails(nick,chanName);
        if(ipDetails?.externalIpV4?.length) ip = ipDetails.externalIpV4;
        return ip;  
    },
    userIpDetails(nick?:string, chanName?:string):IpDetails|null {

        let ipDetails:IpDetails|null = null;
        if(!nick?.length) {
            ipDetails = mainStore.ipDetails;
        } else {

            const lnick = nick.toLowerCase();
            if(!chanName?.length) {
                for(const channel in mainStore.joinedChannels) {
                    const chanData = mainStore.joinedChannels[channel];
                    if(chanData.nicks[lnick] && chanData.game) {chanName = channel; break;}
                }
            }

            if(chanName) {
                const g = this.gameForChannel(chanName.toLowerCase());
                if(g) {
                    ipDetails = g.participantNicks?.[lnick]?.ipsAndPorts
                }
            }
        }

        if(ipDetails == null) {console.error('no ip details found for user')};

        return ipDetails; 
    },
    get imOnIpv6():boolean {
        return !!this.ipv6TunnelPort && !Settings.read()?.ipv4Mode && !!this.myIpsAndPorts()?.externalIpV6?.length;
    },
    userOnIpv6(user?:string):boolean {
        const ipDetails = this.userIpDetails(user);
        return !!ipDetails?.externalIpV6?.length && !ipDetails?.ipv4mode;
    },
    get myExternalIpV4():string {
        return this.userExternalIpV4();
    },
    execConstantPort(execDef:GameDefsType.Executable):{udp?:number, tcp?:number} {
        const retVal = {} as {udp?:number, tcp?:number};
        if(execDef?.networking?.tcpPort?.constant) {
            retVal.tcp = execDef.networking.tcpPort.port;
        }
        if(execDef?.networking?.udpPort?.constant) {
            retVal.udp = execDef.networking.udpPort.port;
        }
        return retVal;
    },
    resourcesForExec(execDef:GameDefsType.Executable|undefined):string[] {
        const res = new Set<string>();
        for(const pId in (execDef?.parameters || {})) {
            const param = execDef!.parameters![pId];
            if(param.type === 'file' && param.res?.length) {
                res.add(param.res);
            }
        }
        return Array.from(res);
    },
    tabEvents: new Eventer()
} as MainStoreRoot & {
    resourcesForExec(execDef:GameDefsType.Executable|undefined):string[],
    disableLanDetection:boolean,
    execConstantPort:(execDef:GameDefsType.Executable)=>{udp?:number, tcp?:number},
    isChanUsingStun:(chan:string)=>boolean,
    isChannelGameRoom:(chan:string)=>boolean,
    imHostInChannel:(chan:string)=>boolean,
    gameForChannel:(chan:string)=>FullGameRoomDetails,
    sendAllJoinedGameRooms_ImRunningExec:(isRunning:boolean, ircClient:Client)=>void,
    turnOffDedicatedServer:(gameDetails:FullGameRoomDetails, ircClient:Client)=>void,
    addEventTo:(type:EventBaseType, dest:string, eventBase:{events:IRCEvent_BaseEvent[], hasUnread?:boolean}, event:IRCEvent_BaseEvent)=>void,
    tabEvents:Eventer<EventBaseType, {dest:string, event: IRCEvent_BaseEvent}>,
    hasGameExec:(gameId:string, execId:string)=>boolean,
    refreshIsGameExecMissing_ReturnWhetherUpdateFound:(gameid:string, execid:string, ircClient:Client, channel?:string)=>boolean,
    getResolvedIpsInGameRoom(channel:string, exceptNick?:string, noTunnul?:boolean): Promise<{[nick:string]:IpAndPort}>,
    myIpsAndPorts():IpDetailsAndPorts,
    userExternalIpV4(nick?:string, chanName?:string):string,
    userIpDetails(nick?:string, chanName?:string):IpDetailsAndPorts|null,
    myExternalIpV4:string,
    imOnIpv6:boolean,
    userOnIpv6(user?:string):boolean,
    execDefForChannel(channel:string):GameDefsType.Executable|null,
    supportsMidGame(channel:string):boolean,
    hostExecIsRunningInChan(channel:string): boolean
    myExecIsRunningInChan(channel:string): boolean,
    resolveCountryCodeForIpForNick(nick:string|undefined, chan:string):void,
    channelAdvertisedAsGameRoom(chan:string):boolean,
    botIsAvailable():boolean,
    getGameRoom(exceptChan?:string):{game:FullGameRoomDetails,chan:string}|null,
    imAlreadyInAGameRoom(exceptChan?:string):boolean,
    gameChosenUdpTunnel(roomDetails?:FullGameRoomDetails, args?:LaunchArgType[]):boolean,
    gameChosenTCPAndNotOptional(execDef:FullGameRoomDetails, args?:LaunchArgType[]):boolean,
    gameIsDosbox(g:FullGameRoomDetails):boolean,
    gameDefIsDosbox(gameId:string, execId:string):boolean,
    execDefIsDosbox(execDef:GameDefsType.Executable):boolean,
    finishedDetectingIps:boolean
});

if(!bootSettings.ipv4Mode) {setupIpv6Tunnel(ircClient);}
setupFileSync(ircClient);

export interface LeasedDedicatedServerDetails_Host extends LeasedDedicatedServerDetails {
    rc?: RelayClient
}