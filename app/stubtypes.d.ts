
declare module 'simple-yenc' {
    export function encode(buffer:Uint8Array):string
    export function decode(str:string):Uint8Array
}
