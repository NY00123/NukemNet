import { axios, net, os } from '../NodeLibs'
import { offline } from "../store/store";
import { compareDottedStringNumbers } from "./CompareDottedNumberString";
import { getIPs, is_ipv4, is_ipv6 } from "./GetIp";
import { StunController } from "./stun";
import { resolveHostIpv4 } from "./utils";
import {Address6} from 'ip-address'

export function isIpLocal(ip:string) {
    if(net.isIPv4(ip)) {
        return isIpV4Local(ip)
    } else {
        return isIpV6Local(ip)
    }
}

export function isIpV6Local(ip:string) {
    try {
        const addr6 = new Address6(ip);
        return addr6.isLinkLocal() || addr6.isMulticast() || addr6.isLoopback()
    } catch(e) {}
    return false;
}

export function isIpV4Local(ip:string) {
    if(
        compareDottedStringNumbers(ip, '10.0.0.0') >= 0 &&
        compareDottedStringNumbers(ip, '10.255.255.255') <= 0) {
            return true;
    }

    if(
        compareDottedStringNumbers(ip, '172.16.0.0') >= 0 &&
        compareDottedStringNumbers(ip, '172.31.255.255') <= 0) {
            return true;
    }

    if(
        compareDottedStringNumbers(ip, '192.168.0.0') >= 0 &&
        compareDottedStringNumbers(ip, '192.168.255.255') <= 0) {
            return true;
    }

    if(
        compareDottedStringNumbers(ip, '127.0.0.0') >= 0 &&
        compareDottedStringNumbers(ip, '127.255.255.255') <= 0) {
            return true;
    }

    if(
        compareDottedStringNumbers(ip, '169.254.0.0') >= 0 &&
        compareDottedStringNumbers(ip, '169.254.255.255') <= 0) {
            return true;
    }

    if(
        compareDottedStringNumbers(ip, '224.0.0.0') >= 0 &&
        compareDottedStringNumbers(ip, '239.255.255.255') <= 0) {
            return true;
    }

    return false;
}

export interface IpCollection {
    localIpV6:string[],
    localIpV4:string[],
    externalIpV4?:string,
    externalIpV6?:string,
}

export interface IpDetails extends IpCollection {
    ipv4mode?:boolean
}

export async function getIpCollection() {

    const retVal: IpCollection = {
        localIpV6: [],
        localIpV4: []
    };

    // Lookup on network interfaces
    const localhostIpv4 = await resolveHostIpv4('localhost');
    const networks = os.networkInterfaces();
    for(const adapterName in networks) {
        const adapterAddresses = networks[adapterName];
        if(!adapterAddresses?.length) continue;
        for(const address of adapterAddresses) {
            if(address.family === 'IPv6' && !retVal.externalIpV6?.length && !isIpV6Local(address.address)) {
                retVal.externalIpV6 = address.address;
            }
            if(address.family === 'IPv4' && address.address !== localhostIpv4) {
                if(retVal.localIpV4.includes(address.address)) continue;
                retVal.localIpV4.push(address.address);
            }
        }
    }

    // Lookup on WebRTC ICE
    let iceArray = [] as string[];
    for(let tries=3;tries>0;tries--) {
        try {
            iceArray = await getIPs(800) as string[];
            break;
        } catch(e) {}
    }
    for(const ip of iceArray) {
        if(is_ipv4(ip)) {
            if(!retVal.localIpV4.includes(ip) && isIpV4Local(ip)) {
                retVal.localIpV4.push(ip);
            } else {
                if(retVal.localIpV4.includes(ip)) {continue;} // NOTE! This IPv4 value might contain a VPN given ip such as 25.39.247.4 (Got this example from using Hamachi), so we check if it was already assigned as a local ip by network interfaces lookup.
                retVal.externalIpV4 = ip; 
            }
        } else if(is_ipv6(ip)) {
            if(!isIpV6Local(ip)) {
                retVal.externalIpV6 = ip;
            }
        }
    }

    return retVal;
}

export async function hasNetworkInterfaceWithExternalIpV6() {
    const ipDetails = await getIpCollection();
    return !!ipDetails?.externalIpV6?.length;
}

export async function getStructuredIps(ipv4Mode?:boolean): Promise<{ipDetails: IpDetails, failedIpv6?:boolean}> {

    const retVal:IpDetails = await getIpCollection();    
    if(ipv4Mode) {
        delete retVal?.externalIpV6;
        retVal.ipv4mode = true;
    }

    // if user chose offline mode, prevent getting external ips.
    if(offline) return {ipDetails:retVal};

    const reqTimeout = 3500;

    const mustGetIpv6 = !!(retVal.externalIpV6?.length && !ipv4Mode);

    const attemptFunctionsIpv4 = [] as (() => Promise<any>)[];
    const attemptFunctionsIpv6 = [] as (() => Promise<any>)[];
    const ipv4AbortControllers = [] as AbortController[];
    const ipv6AbortControllers = [] as AbortController[];

    function createAbortController(family:6|4) {
        const newCtl = new AbortController();
        if(family===6) ipv6AbortControllers.push(newCtl);
        else ipv4AbortControllers.push(newCtl);
        return newCtl;
    }

    function abortAll(family:6|4) {
        for(const ctl of family===6 ? ipv6AbortControllers : ipv4AbortControllers) {
            if(!ctl.signal.aborted && !(ctl?.signal as any)?.['closed']) {
                try {ctl.abort();} catch(e) {}
            }
        }
    }

    if(mustGetIpv6) {
        delete retVal.externalIpV6;

        // Using STUN also to try determine external IPv6 address, implementing abort mechanism because UDP does not have one.
        const shuffledStunServers = structuredClone(StunController.stunServersIpv6).sort(() => Math.random() - 0.5);
        for(let i=0;i<shuffledStunServers.length&&i<3;i++) {
            attemptFunctionsIpv6.push(()=>new Promise(resolve=>{
                if(retVal.externalIpV6 == null) {
                    const abortCtl = createAbortController(6);
                    const server = shuffledStunServers[i];

                    let ended = false;
                    function end() {
                        if(ended) return;
                        ended = true;
                        resolve(null);
                    }

                    abortCtl.signal.onabort = end;
                    StunController.getIpv6Address(server, reqTimeout, abortCtl.signal).
                    then(ipRes=>{
                        if(abortCtl?.signal?.aborted) return end();
                        const ip = ipRes?.trim();
                        if (ip?.length && is_ipv6(ip)) {
                            retVal.externalIpV6 = ip
                        }
                    }).
                    then(end).
                    catch(end);
                }
            }));
        }

        attemptFunctionsIpv6.push(async () => {
            if(retVal.externalIpV6 == null) {
                try {
                    const res = await axios.get('https://v6.i-p.show/?plain=true', {timeout:reqTimeout, signal:createAbortController(6).signal});
                    const ip = res.data.trim();
                    if(is_ipv4(ip)) {
                        retVal.externalIpV4 = ip
                    } else if(is_ipv6(ip)) {
                        retVal.externalIpV6 = ip
                    }
                } catch(e) {}
            }
        });
        attemptFunctionsIpv6.push(async () => {
            if(retVal.externalIpV6 == null) {
                try {
                    const res = await axios.get('http://ip6only.me/api', {timeout:reqTimeout, signal:createAbortController(6).signal});
                    const data = res.data.trim();
                    const ip = data.split(',')[1];
                    if(is_ipv4(ip)) {
                        retVal.externalIpV4 = ip
                    } else if(is_ipv6(ip)) {
                        retVal.externalIpV6 = ip
                    }
                } catch(e) {}
            }
        });
        attemptFunctionsIpv6.push(async () => {
            if(retVal.externalIpV6 == null) {
                try {
                    const res = await axios.get('http://v6.ipv6-test.com/api/myip.php', {timeout:reqTimeout, signal:createAbortController(6).signal});
                    const ip = res.data.trim();
                    if(is_ipv4(ip)) {
                        retVal.externalIpV4 = ip
                    } else if(is_ipv6(ip)) {
                        retVal.externalIpV6 = ip
                    }
                } catch(e) {}
            }
        });
        attemptFunctionsIpv6.push(async () => {
            if(retVal.externalIpV6 == null) {
                try {
                    const res = await axios.get('https://v6.ip.nwk.io/ip', {timeout:reqTimeout, signal:createAbortController(6).signal});
                    const ip = res.data.trim();
                    if(is_ipv4(ip)) {
                        retVal.externalIpV4 = ip
                    } else if(is_ipv6(ip)) {
                        retVal.externalIpV6 = ip
                    }
                } catch(e) {}
            }
        });
    }
    attemptFunctionsIpv4.push(async () => {
        if(retVal.externalIpV4 == null) {
            try {
                const res = await axios.get('https://checkip.amazonaws.com', {timeout:reqTimeout, signal:createAbortController(4).signal});
                const ip = res.data.trim();
                if(is_ipv4(ip)) {
                    retVal.externalIpV4 = ip
                } else if(is_ipv6(ip)) {
                    retVal.externalIpV6 = ip
                }
            } catch(e) {}
        }
    })
    attemptFunctionsIpv4.push(async function () {
        if(retVal.externalIpV4 == null) {
            try {
                const res = await axios('https://ipinfo.io/json', {timeout:reqTimeout, signal:createAbortController(4).signal});
                
                const resObj = res.data as {ip:string};
                if(is_ipv4(resObj.ip)) {
                    retVal.externalIpV4 = resObj.ip;
                } else if(is_ipv6(resObj.ip)) {
                    retVal.externalIpV6 = resObj.ip;
                }
            } catch(e) {}
        }
    });
    attemptFunctionsIpv4.push(async ()=>{
        if(retVal.externalIpV4 == null) {
            try {
                const res = await axios.get('https://ipgrab.io', {timeout:reqTimeout, signal:createAbortController(4).signal});
                const ip = res.data.trim();
                if(is_ipv4(ip)) {
                    retVal.externalIpV4 = ip;
                } else if(is_ipv6(ip)) {
                    retVal.externalIpV6 = ip;
                }
            } catch(e) {}
        }
    });
    attemptFunctionsIpv4.push(async ()=>{
        if(retVal.externalIpV4 == null) {
            try {
                const res = await axios.get('https://api.ipify.org?format=json', {timeout:reqTimeout, signal:createAbortController(4).signal});
                const resObj = (res.data) as {ip:string};
                if(is_ipv4(resObj.ip)) {
                    retVal.externalIpV4 = resObj.ip
                } else if(is_ipv6(resObj.ip)) {
                    retVal.externalIpV6 = resObj.ip
                }
            } catch(e) {}
        }
    })
    attemptFunctionsIpv4.push(async ()=>{
        if(retVal.externalIpV4 == null) {
            try {
                const res = await axios.get('https://ident.me', {timeout:reqTimeout, signal:createAbortController(4).signal});
                const ipv4 = (res.data).trim();
                if(is_ipv4(ipv4)) {
                    retVal.externalIpV4 = ipv4;
                }
            } catch(e) {}
        }
    });

    attemptFunctionsIpv4.sort(() => Math.random() - 0.5);
    const ipv4Promises = [] as Promise<void>[];
    for(let i=0;i<attemptFunctionsIpv4.length&&i<4;i++) {
        const attemptFunc = attemptFunctionsIpv4[i];
        ipv4Promises.push(attemptFunc().then(()=>retVal?.externalIpV4?.length?abortAll(4):undefined));
    }

    attemptFunctionsIpv6.sort(() => Math.random() - 0.5);
    const ipv6Promises = [] as Promise<void>[];
    for(let i=0;i<attemptFunctionsIpv6.length&&i<4;i++) {
        const attemptFunc = attemptFunctionsIpv6[i];
        ipv6Promises.push(attemptFunc().then(()=>retVal?.externalIpV6?.length?abortAll(6):undefined));
    }

    await Promise.allSettled([...ipv6Promises, ...ipv4Promises]);

    return {ipDetails: retVal, failedIpv6: true==(mustGetIpv6 && !retVal?.externalIpV6?.length)};
}