const path = require('path');
const fsExtra = require('fs-extra');
const fs = require('fs')
const child_process = require('child_process');
const glob = require('glob');

module.exports = {

  prePackage: async (forgeConfig, options) => {
    console.log('Building SolidJS dist...');
    child_process.execSync('npm run build'); // build SolidJS app
    console.log('Building node dependencies usin ncc...');
    child_process.execSync('node snippets build-deps'); // build electron_deps.js
  },
  
  packageAfterCopy(forgeConfig, buildPath, electronVersion, platform, arch) {
    // Trimming package.json files, and adding required entries.
    const runEnv = buildPath.replaceAll('\\\\','\\').replaceAll('\\','/').split('electron-packager/')[1].split('/')[0];
    const packageJsonPathDest = path.resolve(buildPath, 'package.json');
    const packageJson = require(__dirname+'/package.json');
    const newPackageJson = {
      name:packageJson.name,
      description:packageJson.description,
      version:packageJson.version,
      main:packageJson.main,
      runEnv:runEnv
    }
    fsExtra.writeJsonSync(packageJsonPathDest, newPackageJson, {spaces:2});
  },

  postPackage: async (forgeConfig, options) => {
    
    const outputPaths = options.outputPaths;
    for(const p of outputPaths) {

      // Sign executable
      try {
        const pathOfP12 = path.resolve(__dirname, 'sign.p12');
        const pathOfPassFile = path.resolve(__dirname, 'signpassword');
        if(!fs.existsSync(pathOfP12)) {
          throw 'Sign failed: sign.p12 not found';
        }
        if(!fs.existsSync(pathOfPassFile)) {
          throw 'Sign failed: signpass file not found (password text file)';
        }
        const password = fs.readFileSync(pathOfPassFile, 'utf8').trim();
        if(options.platform == 'win32') {
          const signtoolGlob = `C:/Program Files (x86)/Windows Kits/*/bin/*/*/signtool.exe`
          const signtoolPath = glob.globSync(signtoolGlob, {absolute:true, nocase:true})?.[0];
          if(!signtoolPath?.length) {
            throw `windows signtool.exe not found: ${signtoolGlob}`
          }
          child_process.execFileSync(signtoolPath, ['sign', '/fd', 'SHA256', '/f', pathOfP12, '/p', password, path.resolve(p, 'NukemNet.exe')]);
          console.log("Signed Executable.");
        }
      } catch(e) {
        console.error(e?.message || e);
      }

      try {fs.rmSync(path.resolve(p, 'LICENSE'));} catch(e) {}
      try {fs.rmSync(path.resolve(p, 'LICENSES.chromium.html'));} catch(e) {}

      // Delete some files
      for(const f of root_delete) {
        const entries = glob.globSync(f, {cwd:p, absolute:true});
        for(const entry of entries) {
            try {fsExtra.removeSync(entry);} catch(e) {}
        }
      }

      // Delete unused locales win/unix
      const localesPath = path.resolve(p, 'locales');
      const localesPathEntries = glob.globSync('!(en-US.pak)', {cwd:localesPath, absolute:true});
      for(const entry of localesPathEntries) {
        fsExtra.removeSync(entry);
      }

      // macOS
      if(options.platform === 'darwin') {
        const ResourcesPath = path.join(p, 'NukemNet.app/Contents/Resources');
        const localesPathEntries = glob.globSync('*.lproj', {cwd:ResourcesPath});
        for(const entry of localesPathEntries) {
          if(entry.toLowerCase() != 'en.lproj') {
            fsExtra.removeSync(path.join(ResourcesPath, entry));
          }
        }
      }
    }
  }
  
};

const root_delete = [
  // "libGLESv2.*",
  // "libEGL.*",
  // "d3dcompiler_47.*",
  // "vk_swiftshader.*",
  // "vulkan-1.*",
  // "vk_swiftshader_icd.json",
  // "snapshot_blob.bin",
  // "version",
  // "chrome_100_percent.pak",
  // "chrome_200_percent.pak",

  // // unix
  // "libvulkan.so.1",
  // "libvk_swiftshader.so"
];
