import { BotMetaChannel_lowercase, BotName_lowercase, ClientAutojoinChannels, IrcModSignOrNone, IrcUserModSigns_High, isMetaChannel, isNukemNetBot } from "../../../server/src/common/constants";
import { IRCEventCustom_Disconnected, IRCEvent_ChangeNick, IRCEvent_ChannelMessage, IRCEvent_Invite, IRCEvent_Join, IRCEvent_Kick, IRCEvent_Kill, IRCEvent_Motd, IRCEvent_Notice, IRCEvent_Part, IRCEvent_PrivateMessage, IRCEvent_Quit, IRCEvent_Registered, IRCEvent_Topic } from "../irc-events/ircEvents";
import { ircClient, mainStore, offline } from "./store";
import { Client } from "matrix-org-irc";
import { Cmd_CreateRoom, Cmd_DeleteRoom, Cmd_GetAllRooms, Cmd_GetAllRoomsResponse, Cmd_RequestUpdateRoom } from "../../../server/src/common/commands";
import { CmdGameRoom_SyncSendAllDetails } from "../commands-gameroom"
import { IrcClientExtra } from "../../../server/src/common/ircExtend";
import { LaunchGameModalResult } from "../LaunchGameModal";
import { GameDefsType } from "../GameDefsTypes";
import { Room_CreateRequest } from "../../../server/src/common/room";
import { axios, dgram, fsSync, http, path } from "../NodeLibs";
import { getStructuredIps } from "../lib/IpDetails";
import { ApiUrl, Settings, SettingsDef } from "../Settings";
import { LaunchArgType } from "../lib/LaunchArgType";
import { deepClone } from "../lib/deepClone";
import sleep from "sleep-promise";
import { DialogMgr } from "../Popups";
import { findFreeUdpTcpPortInRange, resolveHostIpv4 } from "../lib/utils";
import { Server } from "http";
import { For, Show } from "solid-js";
import { Accordion, Button } from "solid-bootstrap";

async function destroyLogic(nick:string, reason:string, userId:string, isMe:boolean, eventName:'quit'|'kill', ircClient:Client) {
    const event = {
        type: eventName,
        ts: Date.now(),
        nick,
        reason: reason?.length>0?reason:undefined,
        userId
    } as IRCEvent_Kill|IRCEvent_Quit

    if(isMe) {
        commonLogic_imNotInChannelAnymore(null, ircClient);

        mainStore.addEventTo("backlog",'backlog',mainStore.backlog, event);
        mainStore.joinedChannels = {};
        mainStore.privateChats = {}
        mainStore.advertisedGameRooms = {}
    } else {
        const lnick = nick.toLowerCase();
        for(const lchan in mainStore.joinedChannels) {
            const chanData = mainStore.joinedChannels[lchan];
            if(lnick in chanData.nicks) {
                commonLogic_otherUserNotInChannelAnymore(lnick, lchan, ircClient);

                delete chanData.nicks[lnick];
                mainStore.addEventTo("channel", lchan, chanData, event);
            }
        }
        for(const chatNick in mainStore.privateChats) {
            const prvChat = mainStore.privateChats[chatNick]
            if(chatNick === lnick) {
                mainStore.addEventTo("private", chatNick, prvChat, event);
            }
        }
    }
}

function turnOffDedicatedServerForAllMyRooms(ircClient:Client) {
    for(const c in mainStore.joinedChannels) {
        const g = mainStore.gameForChannel(c);
        if(g) {
            if(g.dedicatedServer) {
                mainStore.turnOffDedicatedServer(g, ircClient);
            }
        }
    }
}

function commonLogic_imNotInChannelAnymore(/*null means all channels*/channel:string|null, ircClient:Client) {
    IrcClientExtra.customEvents.emit('imNotInChannelAnymore', channel, ircClient);

    if(channel) {
        const lchan = channel.toLowerCase();
        if(lchan === BotMetaChannel_lowercase) {
            turnOffDedicatedServerForAllMyRooms(ircClient);
        }
        const g = mainStore.gameForChannel(channel);
        if(g) {
            // if is game-room channel, notify that no longer in any game room
            IrcClientExtra.customEvents.emit('ingameroom', null);
        }
    }
}


function commonLogic_otherUserNotInChannelAnymore(nick:string, channel:string, ircClient:Client) {
    
    IrcClientExtra.customEvents.emit('otherUserNotInChannelAnymore', nick, channel, ircClient);

    const lchan = channel.toLowerCase();

    const lnick = nick.toLowerCase();
    if(lchan === BotMetaChannel_lowercase && lnick === BotName_lowercase) {
        // if bot no longer in meta channel
        turnOffDedicatedServerForAllMyRooms(ircClient);
    }

    const gameDetails = mainStore.joinedChannels[lchan].game
    if(gameDetails) {
        for(const iNick in gameDetails.participantNicks) {
            if(iNick.toLowerCase() === nick) {
                delete gameDetails.participantNicks[iNick];
                break;
            }
        }
        const playersIn = Object.keys(gameDetails.participantNicks).length;
        gameDetails.publicDetails.playersIn = playersIn;

        if(storeActions.imGameHostInChannel(channel)) {
            IrcClientExtra.sendNNCmdToBot(ircClient, {
                op: 'requestUpdateRoom',
                data: {
                    channel,
                    patch: {playersIn: playersIn}
                }
            } as Cmd_RequestUpdateRoom)
        } else if(storeActions.getGameHostNickForChannelImIn(lchan)?.toLowerCase() === nick.toLowerCase()) {
            // If game host leaves, leave the irc channel as well
            ircClient.part(channel, ()=>{
                DialogMgr.addPopup({
                    title: 'Game Room Closed',
                    body:'Game host left the room, parted channel.',
                    icon: 'info',
                    buttons:[{body:"ok"}]
                })
            })
        }
    }
    

    if(isNukemNetBot(nick)) {
        if(lchan === BotMetaChannel_lowercase) {
            // Erasing advertised game rooms list if bot is no longer available in meta channel.
            mainStore.advertisedGameRooms = {}
        }
    } else {
        if(gameDetails) {
            Settings.playSoundEffect('gameroom-part')
        } else {
            Settings.playSoundEffect('room-part');
        }
    }
}

const storeActions = {

    async showStartGameAnimation() {
        const clname = 'game-started-animation'
        const eventsContainer = document.getElementById('events-container') as HTMLDivElement;
        for(let i=1;i<=4;i++) {
            eventsContainer.classList.add(`frame${i}`);
            eventsContainer.classList.add(clname);
            await sleep(250);
            for(let y=1;y<=4;y++) {eventsContainer.classList.remove(`frame${i}`);}
        }
        eventsContainer.classList.remove(clname)
    },

    
    // The purpose of opening an http server is:
    // 1. LAN detection of game-room participants for their local ips.
    // 2. FileSync in offline LAN mode.
    async createHttpListener() {
        const server = http.createServer(function (req, res) {
            if(req.url === '/nick') {
                res.write(ircClient?.nick||'');
                res.end();
            }
        });

        function tryListen(server:Server, port:number) {
            return new Promise((resolve, reject) => {
                function end(err?:any) {
                    try {server.removeListener('error', onError);} catch(e) {}
                    try {server.removeListener('listening', onListening);} catch(e) {}
                    if(err) reject(err);
                    else resolve(null);
                }
                function onError(err:any) {end(err);}
                function onListening() {end();}
                server.once('error', onError);
                server.once('listening', onListening)
                server.listen(port);
            });
        }
        
        async function iterateAndTryListen(server:Server) {
            for(let port=3000; port<20000; port++) {
                const isPortAllowed = Settings.checkIfPortIsAllowed(port);
                if(!isPortAllowed) {continue;}
                try {
                    await tryListen(server, port);
                    return port;
                } catch(e) {
                    continue
                }
            }
            return 0;
        }

        
        const port = await iterateAndTryListen(server);
        return {server, port};
    },

    nickUpdate(oldnick:string, newnick:string, isMe:boolean) {

        const lOldNick = oldnick.toLowerCase();
        const lNewNick = newnick.toLowerCase();

        const nickChangeEvent = {
            old: oldnick, new:newnick,
            ts: Date.now(),
            type: "nick",
            isMe
        } as IRCEvent_ChangeNick

        // Update nick on all joined chans, and add nick change event on chan window
        for(const chan in mainStore.joinedChannels) {
            const chanData = mainStore.joinedChannels[chan];
            if(lOldNick in chanData.nicks) {
                chanData.nicks[lNewNick] = chanData.nicks[lOldNick];
                chanData.nicks[lNewNick].name = newnick;

                if(lNewNick !== lOldNick) {
                    delete chanData.nicks[lOldNick];
                }

                mainStore.addEventTo("channel", chan, chanData, nickChangeEvent);
            }
            if(chanData.game) {
                // rename nick in game participants.
                for(const nick in chanData.game.participantNicks) {
                    if(nick.toLowerCase() === lOldNick) {
                        chanData.game.participantNicks[lNewNick] = chanData.game.participantNicks[nick];
                        delete chanData.game.participantNicks[nick];
                        break;
                    }
                }
                
                
                // Rename nick in owner if applicable.
                if(chanData?.game?.publicDetails?.owner != null && chanData.game.publicDetails.owner.toLowerCase() === lOldNick) {
                    chanData.game.publicDetails.owner = newnick
                }
            }
        }

        if(isMe) {
            mainStore.myNick = newnick;
            mainStore.addEventTo("backlog",'backlog', mainStore.backlog, nickChangeEvent); // push event to backlog if it's you
        } else {
            // Update nick in private chat if exists, and add nick change event to private chat window
            if(lOldNick in mainStore.privateChats) {
                mainStore.privateChats[lNewNick] = mainStore.privateChats[lOldNick];
                mainStore.privateChats[lNewNick].name = newnick;

                if(lNewNick !== lOldNick) {
                    delete mainStore.privateChats[lOldNick];
                }

                mainStore.addEventTo("private", lNewNick, mainStore.privateChats[lNewNick], nickChangeEvent);
            }
        }

        // Renaming owner of game room in advertised list
        for(const chan in mainStore.advertisedGameRooms) {
            const room = mainStore.advertisedGameRooms[chan];
            if(room.publicDetails.owner.toLowerCase() === lOldNick) {
                room.publicDetails.owner = newnick;
            }
        }
    },

    isRefreshingIpDetails:false,
    async refreshIpDetails() {
        if(this.isRefreshingIpDetails) return;

        const settings = Settings.read();
        
        this.isRefreshingIpDetails = true;
        let retrySeconds = 2;
        let popupRef = null;
        const refreshIpDialogId = 'refresh-ip-popup';
        if(popupRef == null) {
            const isLanMode = settings.lanOnlyMode==true;
            popupRef = DialogMgr.addPopup({
                title:"Checking Connectivity",
                persistent:true,
                body: ()=><><span class="spinner-border text-primary" role="status"></span><br/><div><span id={refreshIpDialogId}></span></div></>,
                buttons:[{body:`Switch to ${isLanMode?'Online Mode':'Offline LAN mode'}`, onClick:()=>{
                    Settings.toggleLanMode();
                }}].concat(isLanMode?[]:[{
                    body:settings.ipv4Mode?"Switch to IPv6/4 Mixed":"Switch to Strict IPv4 Mode",
                    onClick:()=>{Settings.toggleIpv4Mode()}
                }])
            })
        }
        do {
            try {
                if(!mainStore.httpPort || !mainStore.httpServer) {
                    const result = await this.createHttpListener();
                    if(result && result.port && result.server) {
                        mainStore.httpPort = result.port;
                        mainStore.httpServer = result.server;
                    }
                }
                if(!mainStore?.localhostIp4?.length) {
                    mainStore.localhostIp4 = await resolveHostIpv4('localhost');
                }

                const {ipDetails, failedIpv6} = await getStructuredIps(settings.ipv4Mode==true);
                // If couldn't find an external ip
                if(!offline && !ipDetails.externalIpV4) {
                    throw "";
                }
                if(Array.isArray(ipDetails?.localIpV4) && mainStore.disableLanDetection) {
                    ipDetails.localIpV4.splice(0, ipDetails.localIpV4.length);
                }

                // if(failedIpv6) {
                //     const msg = `
                //     Your current network seems to support IPv6.
                //     however, NukemNet could not detect your IPv6 address.
                //     Using IPv4 only for now (that's fine!)
                //     (optionally, you can restart the app to try getting IPv6 again)`
                //     console.error(msg);
                //     const htmlMsg = msg.split('\n').join('<br/>');
                //     mainStore.addEventTo('backlog', 'backlog', mainStore.backlog, {
                //         ts: Date.now(),
                //         type: 'message',
                //         isMe:true,
                //         nick:"IPv6 Warning",
                //         message:`<b style="color:yellow"><br/>********************`+htmlMsg+`<br/>********************</b>`
                //     } as IRCEvent_PrivateMessage)
                // }

                mainStore.ipDetails = ipDetails;

                if(!offline && !Settings.read().ipv4Mode && ipDetails.externalIpV6?.length) {
                    // if one is up and the other is down, or vise versa, close both.
                    if(mainStore.ipv6udpTunnel) {
                        try {mainStore?.ipv6udpTunnel?.close()} catch(e) {}
                        mainStore.ipv6udpTunnel = null;
                    }

                    if(!mainStore.ipv6udpTunnel) {
                        const gamePort = Settings.getGamePort();
                        const port = await findFreeUdpTcpPortInRange(gamePort+1,gamePort+3000,[Settings.getGamePort()], false, true);
                        if(port && !mainStore.ipv6udpTunnel) { // checking again after await to make sure.
                            mainStore.ipv6TunnelPort = port;
                            mainStore.ipv6udpTunnel = dgram.createSocket({ipv6Only:true, type:'udp6'});
                            mainStore.ipv6udpTunnel.bind(port, "::");
                        }
                    }
                }

                break;
            } catch(e) {
                for(let i=retrySeconds;i>=0;i--) {
                    document.getElementById(refreshIpDialogId)!.innerText = `Failed to get your external IPv4 address, Retrying in ${i}`;
                    await sleep(1000);
                }
                retrySeconds *= 2;
                if(retrySeconds>10) retrySeconds = 10;
                document.getElementById(refreshIpDialogId)!.innerText = ``;
            }
        } while(true);
        if(popupRef != null) {popupRef.close()}
        this.isRefreshingIpDetails = false;

        console.log(`IP Details Found:\r\n${JSON.stringify(mainStore.ipDetails, null, 2)}`)

        // call extensions
        setTimeout(async ()=>{
            for(const ext in Settings.loadedJsExtensions) {
                try {
                    await Settings?.loadedJsExtensions?.[ext]?.refreshedIpDetails?.(mainStore.ipDetails);
                } catch(e) {
                    console.error(`Error in refreshedIpDetails() for extension ${ext}`, e);
                }
            }
        });
    },

    async connectionStatus(connected: boolean, nick:string, ircClient:Client) {
        mainStore.connected = connected;

        IrcClientExtra.customEvents.emit("connectionStatus", connected, nick, ircClient);

        setTimeout(async ()=>{
            // call extensions to update their connection status
            for(const ext in Settings.loadedJsExtensions) {
                try {
                    await Settings?.loadedJsExtensions?.[ext]?.ircConnection?.(connected);
                } catch(e) {
                    console.error(`Error in ircConnection() for extension ${ext}`, e);
                }
            }
        });

        if(!connected) {
            mainStore.isRegistered = false;
            mainStore.isLoggedIn = false;
            // if disconnected, erase old data
            mainStore.advertisedGameRooms = {};
            mainStore.joinedChannels = {};
            mainStore.channelAdvertiseInProgress = {};
            delete mainStore.myLeasedDedicatedServer;

            mainStore.addEventTo("backlog", 'backlog', mainStore.backlog, {
                ts: Date.now(),
                type: 'custom_disconnected'
            } as IRCEventCustom_Disconnected);
        } else {
            const settings = Settings.read();
            // Login if has username and pass stored
            if(Settings.hasStoredCredentials) {
                const pass = Buffer.from(settings.IrcNickPass!, 'base64').toString('utf8');
                IrcClientExtra.writeSaslPlainAuth(settings.IrcNick!, pass, ircClient.conn!);
            }
        }
    },

    async doLogin(nick:string, password:string, ircClient:Client, onlyIrc=false) {

        let accessToken:string|null = null;
        if(!onlyIrc) {
            try {
                const httpRes = await axios.post(`${ApiUrl}/login`, {nick, password});
                if(httpRes?.data?.access_token) {
                    accessToken = httpRes.data.access_token;
                }
            } catch(e:any) {
                mainStore.isLoggedIn = false;
                const badCreds = e?.response?.status === 401;
                if(badCreds) {
                    Settings.logout();
                }
                DialogMgr.addPopup({
                    icon:'error',
                    title:"Login Failed",
                    body: badCreds? 'Bad Username/Password': e.message,
                    buttons:[{body:"ok"}]
                })
                return;
            }
        }
    
        try {
            IrcClientExtra.writeSaslPlainAuth(nick,password, ircClient.conn!);

            await new Promise((res,rej)=>{
                const callback = (res:any)=>{
                    const response = res.command;
                    if(response === "err_saslalready") {
                        // If already logged in to current user, then it's a success
                        if(nick.toLowerCase() === ircClient.nick.toLowerCase()) {
                            finalize(res, null);
                        } else {
                            finalize(null, res);
                        }
                    } else if (response === 'err_saslfail') {
                        finalize(null, res);
                    } else if (response === "rpl_saslsuccess") {
                        finalize(res,null);
                    }
                };
    
                const timeout = setTimeout(()=>{
                    finalize(null, new Error('Timed out'));
                },5000);
    
                function finalize(succ:any,fail:any) {
                    clearTimeout(timeout);
                    ircClient.removeListener('raw', callback);
    
                    if(succ) res(succ);
                    else rej(fail);
                }
    
                ircClient.on('raw', callback);
            });
            if(!onlyIrc) {
                DialogMgr.addPopup({
                    icon: 'success',
                    title: 'Login Successful',
                    buttons:[{body:"ok"}]
                });
            }
        } catch(err:any) {
            Settings.logout();
            mainStore.isLoggedIn = false;
            const reason = (err?.command && err?.args?.[1]) || err.message;
            DialogMgr.addPopup({
                icon: 'error',
                title: 'Login Failed',
                body: reason || ''
            })
            return;
        }

        if(accessToken) {
            const s = Settings.read();
            s.jwt = accessToken;
            s.IrcNick = nick;
            s.IrcNickPass = Buffer.from(password).toString('base64');
            Settings.write(s);
        }
        mainStore.isLoggedIn = true;
    },

    async registered(ircClient:Client) {
        mainStore.myNick = ircClient.nick;
        mainStore.addEventTo("backlog", 'backlog', mainStore.backlog, {
            ts: Date.now(),
            type: 'registered'
        } as IRCEvent_Registered);

        const settings = Settings.read();
    },

    async kill(nick:string, reason:string, userId:string, isMe:boolean, ircClient: Client) {
        destroyLogic(nick,reason,userId,isMe, 'kill', ircClient);
    },

    async quit(nick:string, reason:string, userId:string, isMe:boolean, ircClient: Client) {
        destroyLogic(nick,reason,userId,isMe, 'quit', ircClient);
    },

    async kick(channel:string, nick:string, by:string, reason:string, kickerIsMe:boolean, kickedIsMe:boolean, ircClient:Client) {
        const event =  {
            type: 'kick',
            ts: Date.now(),
            channel,
            reason,
            kicked:nick,
            kicker:by,
            kickedIsMe,
            kickerIsMe
        } as IRCEvent_Kick

        const lchannel = channel.toLowerCase();
        
        if(kickedIsMe) {
            delete mainStore.joinedChannels[lchannel];
            mainStore.addEventTo("backlog", 'backlog', mainStore.backlog, event)

            DialogMgr.addPopup({
                title:"Kicked from channel",
                body: `Reason: ${reason}`,
                buttons:[{body:"ok"}]
            });

            commonLogic_imNotInChannelAnymore(channel, ircClient)
        } else {
            const lnick = nick.toLowerCase();
            commonLogic_otherUserNotInChannelAnymore(lnick, lchannel, ircClient);
            
            mainStore.addEventTo("channel", lchannel, mainStore.joinedChannels[lchannel], event)
            delete mainStore.joinedChannels[lchannel].nicks[lnick];
        }

        if(mainStore.isChannelGameRoom(lchannel)) {
            Settings.playSoundEffect('gameroom-part')
        } else {
            Settings.playSoundEffect('room-part')
        }
    },

    async partedChannel(channel:string, nick:string, reason:string, isMe:boolean, opSign:string, userId:string, ircClient:Client) {
        const lchannel = channel.toLowerCase();
        const lnick = nick.toLowerCase();

        const event = {
            ts: Date.now(),
            type: 'part',
            channel,
            isMe,
            nick,
            reason,
            opSign,
            userId
        } as IRCEvent_Part;

        if(isMe) {
            commonLogic_imNotInChannelAnymore(channel, ircClient);

            // This check must be done before deleting the channel from joinedChannels.
            const imGameHostInChannel = mainStore.imHostInChannel(lchannel);

            delete mainStore.joinedChannels[lchannel];
            mainStore.addEventTo("backlog", 'backlog', mainStore.backlog, event);

            if(imGameHostInChannel) {
                // Send command to bot, to remove room
                IrcClientExtra.sendNNCmdToBot(ircClient, {
                    op:'deleteRoom',
                    data: {channel:lchannel}
                } as Cmd_DeleteRoom)
            }
        } else {
            commonLogic_otherUserNotInChannelAnymore(lnick, lchannel, ircClient);

            delete mainStore.joinedChannels[lchannel].nicks[lnick]
            mainStore.addEventTo("channel", lchannel, mainStore.joinedChannels[lchannel], event)
        }
    },

    filterOutAndCloneParamDataForClients(origParams: LaunchArgType[]) {
        const params = deepClone(origParams);
        return params.filter(
            p=>{
                delete p.html;
                return !p.for || !GameDefsType.ParamType.ParamFor_Unshared.includes(p.for)
            })
    },


    async refreshGameRoomsList(ircClient:Client, force:boolean = false) {
        if(mainStore.isRefreshingGameRoomsList && !force) return;
        mainStore.isRefreshingGameRoomsList = true;
        try {
            const response = await IrcClientExtra.requestAndResponseBot(ircClient, {op:'getAllRooms'} as Cmd_GetAllRooms) as Cmd_GetAllRoomsResponse;
            mainStore.advertisedGameRooms = {};
            for(const room of response.data.rooms) {
                mainStore.advertisedGameRooms[room.channel!.toLowerCase()] = {publicDetails:room}
            }
        } catch(e) {}
        mainStore.isRefreshingGameRoomsList = false;
    },

    showTipsForDef(gameId:string, execId:string) {
        const gameDef = Settings?.gameDefs?.games?.[gameId];
        const execDef = gameDef?.executables?.[execId];

        async function handlePatch(patchId:string, apply:boolean) {
            const patch = execDef?.tips?.patches?.[patchId];
            const func = apply? patch?.apply: patch?.undo;
            if(!func) return; // Should not happen
            try {
                const gameDirPath = Settings?.read()?.games?.[gameId]?.[execId]?.path;
                if(!gameDirPath?.length || !fsSync.existsSync(gameDirPath)) throw new Error(`Game directory not set or does not exist, please set it's path first.`);
                const fileRefs = patch?.files || [];
                const opts:GameDefsType.ExecutablePatchOpts = {
                    dir:gameDirPath,
                    files:{}
                }
                for(const fr of fileRefs) {
                    const subPath = execDef?.files?.[fr]?.glob || execDef?.files?.[fr]?.path || '';
                    if(!subPath?.length) throw new Error(`FileRef ${fr} not found in executable definition.`);

                    const filePath = await Settings.resolveFilePathForFileRef(fr, {dir:gameDirPath, execId:execDef._execId, gameId:execDef._gameId} );
                    if(!filePath?.length) throw new Error(`File ${subPath} not found in game directory.`);

                    opts.files[fr] = path.resolve(gameDirPath, filePath);
                }
                const result = await func(opts);
                DialogMgr.addPopup({
                    title:result?.title || '',
                    body:result?.body || '',
                    buttons:[{body:"ok"}], closeButton:true
                })
            } catch(e) {
                DialogMgr.addPopup({
                    title:"Patch error",
                    body:`Error while ${apply?'applying':'undoing'} patch ${patchId}: ${(e as any)?.message}`,
                    buttons:[{body:"ok"}], closeButton:true
                })
            }
        }

        const tips = execDef?.tips;
        if(!tips) return;
        DialogMgr.addPopup({
            fullscreen:true, 
            icon:'info',
            noCenter:true,
            title:`Tips for ${gameDef.name} - ${execDef.name}`,
            size:'xl',
            closeButton:true, buttons:[{body:'Close'}],
            body:()=><>
                <Show when={tips?.html?.length}><div innerHTML={tips.html}></div></Show>
                <Show when={Object.keys(execDef?.tips?.patches || {}).length}>
                    <div>
                        <h4>Patches</h4>
                        <For each={Object.keys(execDef?.tips?.patches || {})}>
                            {patchId=>
                            <Accordion defaultActiveKey="-1">
                                <Accordion.Item eventKey="0">
                                <Accordion.Header>{execDef.tips!.patches?.[patchId]?.title || patchId}</Accordion.Header>
                                <Accordion.Body>
                                    <Show when={execDef.tips!.patches?.[patchId]?.apply}><Button variant="primary" onClick={()=>handlePatch(patchId, true)}>Apply</Button></Show>
                                    &nbsp;&nbsp;
                                    <Show when={execDef.tips!.patches?.[patchId]?.undo}><Button variant="secondary" onClick={()=>handlePatch(patchId, false)}>Undo</Button></Show>
                                    <p innerHTML={execDef.tips!.patches?.[patchId]?.html || ''} />
                                </Accordion.Body>
                                </Accordion.Item>
                            </Accordion>}
                        </For>
                    </div>
                </Show>
            </>
        })
    },

    async joinedChannel(channel:string, nick:string, isMe:boolean, userId:string, ircClient:Client) {
        const lchannel = channel.toLowerCase();
        const lnick = nick.toLowerCase();

        if(!(lchannel in mainStore.joinedChannels)) {
            mainStore.joinedChannels[lchannel] = {
                name:channel,
                events:[],
                nicks:{},
                topic:''
            };
        }

        if(!(lnick in mainStore.joinedChannels[lchannel].nicks)) {
            mainStore.joinedChannels[lchannel].nicks[lnick] = {name:nick,sign:'', lan: {isDetecting:false}};
        }

        const event = {
            ts: Date.now(),
            type: 'join',
            channel,
            isMe,
            nick,
            userId,
            opSign:'' // TODO put actual value
        } as IRCEvent_Join;

        if(isMe) {
            mainStore.addEventTo("backlog", 'backlog', mainStore.backlog, event);
        } else {
            if(!isMetaChannel(lchannel) && !isNukemNetBot(nick)) {
                if(mainStore.isChannelGameRoom(lchannel)) {
                    Settings.playSoundEffect('gameroom-join')
                } else {
                    Settings.playSoundEffect('room-join')
                }
            }
        }
        
        mainStore.addEventTo("channel", lchannel, mainStore.joinedChannels[lchannel], event)

        // Refresh game rooms list if needed, fetch from bot.
        if(isMetaChannel(channel)) {
            let shouldRefreshRooms = isNukemNetBot(nick)

            if(!shouldRefreshRooms && isMe) {
                // Check if Its me who joined, and bot is in channel
                const names = await IrcClientExtra.getNicksInChannel(ircClient, channel);
                for(const n in names) {
                    if(n.toLowerCase() === BotName_lowercase) {
                        // Bot is indeed in channel.
                        shouldRefreshRooms = true;
                        break;
                    }
                }
            }
            
            if(shouldRefreshRooms) {
                this.refreshGameRoomsList(ircClient, true);
            }
        }


        // If someone joined a room, and I'm it's game host, and it's not the bot
        if(this.imGameHostInChannel(channel) && !isMe && !isNukemNetBot(lnick)) {
            const chanDetails = mainStore.joinedChannels[lchannel];
            if(chanDetails && chanDetails?.game?.publicDetails != null) {
                const hasRoomLeft = chanDetails.game!.publicDetails.maxPlayers>chanDetails.game!.publicDetails.playersIn!;
                if(!hasRoomLeft) {
                    await IrcClientExtra.kickNick(ircClient,channel,nick, 'Room is full')
                } else {
                    //
                    // Remove private host params.
                    //
                    const gameDetailsForClient = deepClone(chanDetails.game) as typeof chanDetails.game;
                    // Transfer only sharable params.
                    gameDetailsForClient!.params = storeActions.filterOutAndCloneParamDataForClients(gameDetailsForClient!.params);

                    await IrcClientExtra.sendNNCmdTo(ircClient, lnick, {
                        op:'sync_sendAllDetails',
                        data: gameDetailsForClient
                    } as CmdGameRoom_SyncSendAllDetails);

                    // This is not always received correctly for some reason (after Edit Game details modal)
                    // await packAndSendIrcCmd(ircClient, lnick, {
                    //     data: {
                    //         $case: 'cmdSyncSendAllDetails',
                    //         cmdSyncSendAllDetails: gameDetailsForClient
                    //     }
                    // } as NNPB.RootCommand);
                    
                }
                
            }
        }
    },
    
    message(nick:string, to:string, text:string, opSign:string, senderIsMe:boolean, isHistServ:boolean, disableSound?:boolean) {
        if(to.startsWith('#')) {
            const lchannel = to.toLowerCase();
            const event:IRCEvent_ChannelMessage = {
                type:'message',
                channel:to,
                message:text,
                nick,
                opSign: opSign as IrcModSignOrNone,
                isMe: senderIsMe,
                isHistServ
            }
            if(!isHistServ) {
                event.ts = Date.now();
            }

            mainStore.addEventTo('channel', lchannel, mainStore.joinedChannels[lchannel], event)

            if(!disableSound) {
                if(!senderIsMe) {
                    if(this.isChannelGameRoom(lchannel)) {
                        Settings.playSoundEffect('gameroom-message')
                    } else {
                        Settings.playSoundEffect('room-message')
                    }
                }
            }            
        } else {
            const event:IRCEvent_PrivateMessage = {
                type:'message',
                ts:Date.now(),
                message:text,
                nick,
                isMe: senderIsMe
            }

            const otherPerson = senderIsMe? to:nick;
            const lOtherPerson = otherPerson.toLowerCase();
            if(!(lOtherPerson in mainStore.privateChats)) {
                mainStore.privateChats[lOtherPerson] = {name:otherPerson, events:[]}
            }

            mainStore.addEventTo('private', lOtherPerson, mainStore.privateChats[lOtherPerson], event)

            if(!senderIsMe && !disableSound) {
                Settings.playSoundEffect('private-message')
            }
        }
        
    },

    updateNickNamesInChannel: function(channel:string, nickToMod:Map<string,IrcModSignOrNone>) {
        const lchannel = channel.toLowerCase();
        if(!(lchannel in mainStore.joinedChannels)) {
            return
        }

        const currentLowerCaseNicksInChannel = new Set();
        for(const [nick,mod] of nickToMod.entries()) {
            const lnick = nick.toLowerCase();
            currentLowerCaseNicksInChannel.add(lnick);
            
            if(lnick in mainStore.joinedChannels[lchannel].nicks) {
                if(mainStore.joinedChannels[lchannel].nicks[lnick].sign !== mod) {
                    mainStore.joinedChannels[lchannel].nicks[lnick].sign = mod
                }
            } else {
                mainStore.joinedChannels[lchannel].nicks[lnick] = {name:nick, sign: mod, lan: {isDetecting:false}}
            }
        }

        for(const prevNick in mainStore.joinedChannels[lchannel].nicks) {
            if(!currentLowerCaseNicksInChannel.has(prevNick)) {
                delete mainStore.joinedChannels[lchannel].nicks[prevNick];   
            }
        }
    },


    gotTopic(channel:string, topic:string, nick:string, isMe:boolean, command: string, setAtEpochSeconds:string) {
        const lchannel = channel.toLowerCase();
        if(!(lchannel in mainStore.joinedChannels)) {
            return;
        }

        mainStore.joinedChannels[lchannel].topic = topic;

        const event = {
            type: 'topic',
            ts: Date.now(),
            channel,
            command,
            nick,
            topic,
            setAtEpochSeconds: setAtEpochSeconds?parseInt(setAtEpochSeconds):undefined
        } as IRCEvent_Topic

        if(isMe) {
            mainStore.addEventTo("backlog", 'backlog', mainStore.backlog, event);
        }

        mainStore.addEventTo("channel", lchannel, mainStore.joinedChannels[lchannel], event)
    },

    motd(message:string) {
        const event = {
            type: 'motd',
            ts: Date.now(),
            message
        } as IRCEvent_Motd
        mainStore.addEventTo("backlog", 'backlog', mainStore.backlog, event);
    },

    invite(channel:string, from:string, clickChan:()=>void) {
        const event = {
            type: 'invite',
            ts: Date.now(),
            channel,
            from,
            clickChan
        } as IRCEvent_Invite

        mainStore.addEventTo("backlog", 'backlog', mainStore.backlog, event);
    },

    notice(nick:string|undefined, channel:string, text:string) {
        // const isFromServer = nick == null;

        mainStore.addEventTo("backlog", "backlog", mainStore.backlog, {
            type: 'notice',
            ts: Date.now(),
            nick,
            text,
            channel
        } as IRCEvent_Notice);
    },

    createPrivateChat(nick:string) {
        const lnick = nick.toLowerCase();
        if(!(lnick in mainStore.privateChats)) {
            mainStore.privateChats[lnick] = {name:nick, events:[]};
        }
    },

    closeTab(tab:string, ircClient:Client) {
        if(tab.startsWith('@')) return // cannot close backlog tab

        const ltab = tab.toLowerCase();
        if(ltab.startsWith('#')) {
            ircClient.part(ltab, ()=>{});
        } else {
            delete mainStore.privateChats[ltab]
        }
    },

    imInChannelAndHaveHighOp(channel:string) {
        return IrcUserModSigns_High.has(mainStore?.joinedChannels?.[channel]?.nicks?.[mainStore?.myNick?.toLowerCase()]?.sign);
    },

    imInChannel(channel:string) {
        const lchannel = channel.toLowerCase();
        const mylnick = mainStore.myNick.toLowerCase();

        if(!(mylnick in (mainStore?.joinedChannels?.[lchannel]?.nicks || {}))) {
            return false;
        }
        return true;
    },

    imInChannel_othersDontHaveHighOpInChannel(channel:string) {
        const lchannel = channel.toLowerCase();
        const mylnick = mainStore.myNick.toLowerCase();

        if(!storeActions.imInChannel(channel)) return false;

        let otherHasHighOp = false;
        for(const lnick in mainStore.joinedChannels[lchannel].nicks) {
          if(lnick === mylnick) continue;
          if(IrcUserModSigns_High.has(mainStore.joinedChannels[channel].nicks[lnick].sign)) {
            otherHasHighOp = true;
            break;
          }
        }
        return !otherHasHighOp;
    },

    imInChannelAndTheOnlyOp(channel:string) {
        return storeActions.imInChannelAndHaveHighOp(channel) && storeActions.imInChannel_othersDontHaveHighOpInChannel(channel);
    },

    imGameHostInChannel(channel:string) {
        return (mainStore?.joinedChannels?.[channel.toLowerCase()]?.game?.publicDetails?.owner?.toLowerCase() || null) === mainStore.myNick.toLowerCase()
    },

    isChannelGameRoom(channel:string) {
        const lchannel = channel.toLowerCase();
        return (mainStore?.advertisedGameRooms?.[lchannel] != null) || (mainStore?.joinedChannels?.[channel.toLowerCase()]?.game != null)
    },

    getGameHostNickForChannelImIn(channel:string):string|null {
        const lchannel = channel.toLowerCase();
        return mainStore?.joinedChannels?.[lchannel]?.game?.publicDetails?.owner || null;
    },

    imAlreadyHostingAGameRoom() {
        for(const chan in mainStore.joinedChannels) {
            const owner = this.getGameHostNickForChannelImIn(chan);
            if(owner && owner.toLowerCase() === mainStore.myNick.toLowerCase()) {
                return true;
            }
        }
        return false;
    },

    async setGameRoomWithChannel(channel:string, opts: LaunchGameModalResult, ircClient:Client, gameDefs:GameDefsType.Root, _settings?:SettingsDef) {

        const lchannel = channel.toLowerCase();
        if(!storeActions.imInChannel(lchannel)) {
            try {
                await IrcClientExtra.joinChannelAndGetNames(ircClient, channel);
                if(!storeActions.imInChannelAndTheOnlyOp(lchannel)) throw new Error(`You're not in the game channel/not the only operator there`);
                if(opts?.password?.length) {
                    await ircClient.send('MODE', channel, '+k', opts.password)
                }
                if(opts?.maxPlayers != null && opts?.maxPlayers > 0) {
                    await ircClient.send('MODE', channel, '+l', ''+opts.maxPlayers);
                }
            } catch(e) {
                try {
                    // Leave channel if failed
                    await ircClient.part(lchannel,()=>{});
                } catch(e) {
                    throw e;
                }
            }
        }

        // TODO make sure it exists in joinedChannels
        // mainStore.joinedChannels[lchannel]

        const roomPublicDetails:Room_CreateRequest = {
            gameId: opts.game,
            gameName: gameDefs.games[opts.game].name,
            execId: opts.exec,
            execName: gameDefs.games[opts.game].executables[opts.exec].name,
            maxPlayers: opts.maxPlayers!,
            name: opts.roomName!,
            channel,
            password:opts.password,
            isStarted: false,
            playersIn: 1
        }

        mainStore.joinedChannels[lchannel].game = {
            publicDetails: {
                gameId: roomPublicDetails.gameId,
                gameName: roomPublicDetails.gameName,
                channel: roomPublicDetails.channel!,
                execId: roomPublicDetails.execId,
                execName: roomPublicDetails.execName,
                maxPlayers: roomPublicDetails.maxPlayers,
                name: roomPublicDetails.name,
                isStarted: false,
                playersIn: 1,
                password: undefined,
                hasPassword: (opts?.password?.length||0)>0,
                owner: ircClient.nick,
                createdAt: Date.now()
            },
            params: opts.args,
            participantNicks: {[ircClient.nick.toLowerCase()]:{isExecRunning:false, isExecMissing:false, ipsAndPorts: mainStore.myIpsAndPorts()}}
        }

        mainStore.resolveCountryCodeForIpForNick(ircClient.nick.toLowerCase(), channel);
        
        // TODO add event in backlog for new game room
        
        // advertise game room
        if(mainStore.botIsAvailable() && !opts?.hideRoom) {
          const createRoomRequest = {
            op: 'createRoom',
            data: roomPublicDetails
          } as Cmd_CreateRoom
          try {
            await IrcClientExtra.requestAndResponseBot(ircClient, createRoomRequest)
          } catch(e) {}
        }

        return lchannel;
    }

}

export default storeActions;