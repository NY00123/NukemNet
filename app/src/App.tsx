import { Component, onMount, onCleanup, For, createSignal, Show } from 'solid-js';
import { GameDefsType } from './GameDefsTypes';
import { Alert, Button, ButtonGroup, CloseButton, Dropdown, DropdownButton, Form, OverlayTrigger, Spinner, Tab, Table, Tabs, Tooltip } from 'solid-bootstrap';
import { BotMetaChannel, BotName, BotName_lowercase, BotMetaChannel_lowercase, IrcUserMod_CmdToSign, IrcModSign_ToNum, NukemNetLobby_lowercase, isMetaChannel, IrcModSignOrNone } from '../../server/src/common/constants';
import { backlogTab, backlogTabL, EventBaseType, ircClient, mainStore, offline, serverHost, serverHttpPort } from './store/store';
import storeActions from './store/storeActions';
import { eventTypeToComponentMapping } from './irc-events/eventToComponent';
import {IrcEventCmp_Base} from './irc-events/components/base'
import TopBar from './TopBar';
import LaunchGameModal, { LaunchGameModalResult } from './LaunchGameModal';
import { defaultGamePort, pathOfUserTempDir, Settings } from './Settings';
import { IrcClientExtra } from '../../server/src/common/ircExtend';
import { createHhash } from '../../server/src/common/hash';
import NNCommandHandler from './nncmdHandler'
import { CmdGameRoom_LaunchGame, CmdGameRoom_RequestGameDetails, CmdGameRoom_RequestGameDetails_op, CmdGameRoom_SyncSendAllDetails, CmdGameRoom_ToggleDedicatedServer, FullGameRoomDetails } from './commands-gameroom';
import { launchGame } from './launchGame';
import { stub } from './cmdStub';
import { Cmd_CreateRoom, Cmd_DeleteRoom, Cmd_ManageRelayRequestFetch, Cmd_RequestUpdateRoom, Cmd_ManageRelayCreated } from '../../server/src/common/commands';
import { waitUntilPortIsUsedInProto } from './lib/waitForListenPort';
import PortForwardModal, { tryForwardOnAllProtocols } from './PortForwardModal';
import LauncherSettingsModal from './LauncherSettingsModal';
import { createPopup, PopupPickerController } from '@picmo/popup-picker'
import { IRCEventCustom_GameDetails, IRCEventCustom_Raw, IRCEvent_BaseEvent, IRCEvent_Mode, IRCEvent_ProcessStartedByHost, IRCEvent_ProcessTerminatedByHost } from './irc-events/ircEvents';
import { IrcEventTextLogger } from './lib/IrcEventTextLogger';
import { Room_CreateRequest, Room_UpdatePatchFromHost } from '../../server/src/common/room';
import { deepClone } from './lib/deepClone';
import { clientLaunchGame } from './launchGameAsClient';
import { packAndSendIrcCmd } from '../../server/src/common/protobuf/ProtoBufMgr';
import { NNPB } from '../../server/src/common/protobuf/export';
import { axios, electronRemote, path, ping, Electron, systeminformation, fs } from './NodeLibs';
import sleep from 'sleep-promise';
import { CountryCodeToName } from './lib/CountryCodeToName';
import { RoomAdvertisement } from '../../server/src/common/protobuf/main';
import { IRCStylizeText } from './lib/IrcStyling';
import { checkPortConnectivityTcpUdp, checkPortConnectivityTcpUdp_Throw } from './lib/CheckPortConnectivity';
import { compareDottedStringNumbers } from './lib/CompareDottedNumberString';
import { render } from 'solid-js/web';
import { createMutable } from 'solid-js/store';
import Popups, { DialogMgr } from './Popups';
import { ChildProcess } from 'child_process';
import { getJsonFromUrl, isWindows, preloadImagesFromBackgroundUrl, resolveHostFamilyAndAddressPreferIpv6 } from './lib/utils';
import { findOptimalRelay } from './lib/findOptimalRelay';
import { RelayInfo } from './lib/RelayInfo';
import { KillProc } from './lib/KillProc';
import { showStunPanel } from './useStun';
import { showIpv6Panel } from './ipv6tunnel';
import { RelayClient } from '../../relay-service/src/udp-relay/relay-client';
import { hasNetworkInterfaceWithExternalIpV6 } from './lib/IpDetails';
import { genericUploadLogic, PasteHandler_Init } from './lib/fileuploader';
import { terminateGameProcess } from './terminateProc';
import { ParamDisplay } from './ParamDisplay';

// Delete temp dir on startup
fs.rm(pathOfUserTempDir, {recursive:true,force:true});

// preload game-started-animation frames (if we don't preload, first time the animation is played is a bit blinky)
preloadImagesFromBackgroundUrl(...[1,2,3,4].map(i=>`game-started-animation frame${i}`));

function handleUrlSchema() {
  const openUrl = (window as any)?.openUrl;
  if(!openUrl) return;
  delete (window as any).openUrl;

  setTimeout(async ()=>{
    for(let i=0;i<40;i++) { // 20 seconds
      // Wait for seconds to make sure we are connected and have our ip details, and only then join the room.
      // This is to avoid joining the room to early in which case the host won't have our ip details.
      if(mainStore.finishedDetectingIps) {
        try {
          const openUrlAfterSlashes = openUrl.split('://').splice(1).join('').replace(/^\/+|\/+$/g, '');
          const command = openUrlAfterSlashes.split('?')[0];
          const chan = '#'+command.split('/')[1];
          if(chan.startsWith('#')) {
            const str = '?'+openUrlAfterSlashes.split('?').splice(1).join('');
            const params = getJsonFromUrl(str) as {p?:string};
            if(params?.p?.length) {
              ircClient.send('JOIN', chan, params.p);
            } else {
              ircClient.join(chan)
            }
          }
        } catch(e) {}
        break;
      }
      await sleep(500);
    }
  },0);
}
(window as any).handleUrl = handleUrlSchema;


async function checkNewVersion() {
  try {
    const data = await axios.get('https://nukemnet.com/latest-version.json');
    const json = data.data as {version:string, minimumVersion:string, timestamp: number};
    const myVersion = Settings.pjson.version;
    const hasNewVersion = compareDottedStringNumbers(json.minimumVersion, myVersion)>0;
    if(hasNewVersion) {
      const html = ()=><>Update found: {json.version}<br/>{new Date(json.timestamp).toString()}<br/><br/>Please update to keep using NukemNet (prior updating, delete all NukemNet files/folders except "user" folder which contains your personal settings)<br/><br/>Go to website?</>;
      DialogMgr.addPopup({
        title:"Update Found",
        body:html,
        onClose: ()=>{
            electronRemote.process.exit();
        },
        buttons:[{body:"Update", variant:'primary', onClick:()=>{
          const a = document.createElement('a');
          a.setAttribute('href', "https://nukemnet.com");
          a.setAttribute('target', "_blank");
          a.click();
        }},{body:"Cancel", variant:'secondary'}]
      });
    }
  } catch(e) {
    console.error('Failed to check for updates');
  }
}

const App: Component = () => {

  checkNewVersion();

  let messagesContainer: HTMLDivElement;

  const [chosenTab,_setChosenTab] = createSignal('');

  const historyCommands:string[] = [];
  let historyCommandsBrowseIndex = -1;

  function markTabAsRead(tabStr:string) {
    const ltabStr = tabStr;
    if(mainStore?.joinedChannels?.[ltabStr]) {
      mainStore.joinedChannels[ltabStr].hasUnread = false;
    } else if(mainStore?.privateChats?.[ltabStr]) {
      mainStore.privateChats[ltabStr].hasUnread = false
    } else if(ltabStr === backlogTabL) {
      mainStore.backlog.hasUnread = false;
    }
  }

  function setChosenTab(newTabStr:string) {
    const prevTabStr = chosenTabResolve().toLowerCase(); 

    markTabAsRead(prevTabStr);
    markTabAsRead(newTabStr);
    _setChosenTab(newTabStr);
  }

  const [stickScrollToBottom, setStickScrollToBottom] = createSignal(true)
  
  const chosenTabResolve = ()=>{
    const allTabs = allTabNames();
    let chosen = null;
    if(allTabs.includes(chosenTab())) {
      chosen = chosenTab();
    } else {
      chosen = allTabs[allTabs.length-1];
    }

    if(chosen.toLowerCase() === BotMetaChannel_lowercase) { // Cannot select meta channel, it should be hidden to the user
      chosen = backlogTab;
    }
    if(chosen != chosenTab()) {
      _setChosenTab(chosen);
    }
    return chosen;
  }

  const allTabNames = ()=>{
    const tabs: string[] = []

    tabs.push(backlogTab);

    for(const chanId in mainStore.joinedChannels) {
      const chanName = mainStore.joinedChannels[chanId].name;
      tabs.push(chanName);
    }

    for(const nickId in mainStore.privateChats) {
      const nickname = mainStore.privateChats[nickId].name;
      tabs.push(nickname);
    }

    return tabs;
  }

  const eventsForCurrentTabName = (tabname?:string)=>{
    const t = tabname?.length?tabname:chosenTabResolve();
    if(t != null) {
      if(t.startsWith('@')) {
        return mainStore.backlog.events;
      }
      if(t.startsWith('#')) {
        const lchannel = t.toLowerCase();
        return mainStore.joinedChannels?.[lchannel]?.events || [];
      }
      if(t.length>0) {
        const lnick = t.toLowerCase();
        return mainStore.privateChats?.[lnick]?.events || [];
      }
    }
    return [];
  }

  function openPrivateMessageForNick(nick:string) {
      storeActions.createPrivateChat(nick);
      setChosenTab(nick);
  }

  function kickNick(nick:string) {
    ircClient.send("KICK", chosenTabResolve(), nick)
  }

  function clickedOnCloseTab(tab:string) {
    storeActions.closeTab(tab, ircClient);
  }

  onMount(async ()=>{
    document.body.className = ""; // remove loading image

    IrcClientExtra.connectWithRetry(ircClient,
      ()=>{
        // Can request CAP(s) (IRC capabilities) here
        // ircClient.conn!.write('CAP REQ :batch\r\n'); // TODO support batch CAP so I can support history properly without triggering nncmds.
        // ircClient.conn!.write('CAP REQ :message-tags\r\n'); // TODO might be useful in the future

        // On connect
        storeActions.connectionStatus(true, ircClient.nick, ircClient);
        storeActions.refreshIpDetails();
      }, ()=>{
        // On disconnect
        storeActions.connectionStatus(false, ircClient.nick, ircClient);
      }, async (addressOrHost:string)=>{
        if(!Settings?.read()?.ipv4Mode) {
          const hasIpv6Net = await hasNetworkInterfaceWithExternalIpV6();
          if(hasIpv6Net) {
            const {family, address} = await resolveHostFamilyAndAddressPreferIpv6(addressOrHost);
            if(family == 6) {
              try {
                const pingResult = await ping.promise.probe(address, {timeout: 2, extra: isWindows()?[]:['-i', '5']});
                if(pingResult?.alive) {
                  return 6;
                }
              } catch(e) {}
            }
          }
        }
        return 4;
      });
  });

  onCleanup(()=>{
    ircClient.disconnect();  
  });

  ircClient.on('registered', ()=>{
    mainStore.isRegistered = true;
    storeActions.registered(ircClient);
  });

  ircClient.on('notice',function (nick, channel, text, message) {
    storeActions.notice(nick, channel, text);
  });

  const [runGameButtonDisabled, set_runGameButtonDisabled] = createSignal(false);
  let runGameButtonDisabledTimer:any = null;
  function postponeRunGameButton() {
    set_runGameButtonDisabled(true);
    clearInterval(runGameButtonDisabledTimer);
    runGameButtonDisabledTimer = setTimeout(()=>{
      set_runGameButtonDisabled(false);
    },3500);
  }

  IrcClientExtra.customEvents.on('gameRoom_participantAdded', ()=>{
    postponeRunGameButton();
  });

  ircClient.on('join', async (channel:string, nick:string, message)=>{
    
    const isMe = ircClient.nick === nick
    if(isMe && isMetaChannel(channel)) {
      handleUrlSchema();
    }
    const userId = message.nick+'@'+message.host;
    storeActions.joinedChannel(channel, nick, isMe, userId, ircClient);
    const lchan = channel.toLowerCase();
    if(isMe && lchan != BotMetaChannel_lowercase) { // Don't fetch history for meta channel, it will create weird side effects such as ghost rooms list, because it replays previous commands such as "newRoom" (New game room)
      setChosenTab(channel)  
      if(lchan === NukemNetLobby_lowercase) {
        // ircClient.send('HISTORY', channel, '500'); // Fetch history only for lobby channel, it breaks game CMD data if reading history from game channel
      }
    }
  });

  ircClient.on('error', (err)=>{
    if(err.command == "err_channelisfull") {
      const channel = err?.args?.[1] || '';
      DialogMgr.addPopup({
        title: `Channel ${channel} is full`,
        body: $(`<p>
        Could not join channel ${channel}<br/>
        Channel is full
        </p>`),
        closeButton: true, buttons:[{body:"Ok"}]
      })
    }
    mainStore.addEventTo('backlog', 'backlog', mainStore.backlog, {
      ts: Date.now(),
      type: 'custom_raw',
      obj: err?.args
    } as IRCEventCustom_Raw)
  })

  ircClient.on('raw', async (obj)=>{
    if(obj.command === "err_badchannelkey") {
      const formId = 'channel-password-input';
      const room = obj.args[1]
      
      function doJoin() {
        const passInput = document.getElementById(formId) as HTMLInputElement;
        const pass = passInput.value;
        ircClient.send('JOIN', room, pass);
      }

      const popup = DialogMgr.addPopup({
        icon:'info',
        title: "Room Password",
        body: ()=><Form onsubmit={function(e) {
          e.preventDefault();
          doJoin();
          popup.close();
        }}><Form.Control id={formId} size="lg" type="text" placeholder="Channel Password" /></Form>,
        buttons: [{
          variant: 'primary',
          body: "Join",
          onClick: doJoin
        }, {body: "Cancel", variant:'secondary'}]
      });
      document.getElementById(formId)?.focus();

      return;
    }

    if(!["rpl_youreoper"].includes(obj.command!)) return;
    mainStore.addEventTo('backlog', 'backlog', mainStore.backlog, {
      ts: Date.now(),
      type: 'custom_raw',
      obj: obj?.args
    } as IRCEventCustom_Raw)
  })

  ircClient.on('kill', (nick:string, reason:string, channels:string[], message:any)=>{
    const userId = message.nick+'@'+message.host;
    const isMe = nick === ircClient.nick;
    storeActions.kill(nick, reason, userId, isMe, ircClient);
  })

  ircClient.on('quit', (nick:string, reason:string,channelsArr:string[],message:any)=>{
    const isMe = nick === ircClient.nick;
    const userId = message.nick+'@'+message.host;
    storeActions.quit(nick, reason, userId, isMe, ircClient);
  })

  ircClient.on('part', function (channel, nick, reason, message) {
    const isMe = ircClient.nick === nick
    const opSign = ircClient.chans.get(channel)!.users.get(nick)!;
    const userId = message.nick+'@'+message.host;
    storeActions.partedChannel(channel, nick,reason, isMe, opSign, userId, ircClient);
  })

  ircClient.on('kick', function (channel, nick:string, by:string, reason, message) {
    const kickerIsMe = by.toLowerCase() === ircClient.nick.toLowerCase();
    const kickedIsMe = nick.toLowerCase() === ircClient.nick.toLowerCase();
    storeActions.kick(channel, nick, by, reason, kickerIsMe, kickedIsMe, ircClient)
  });

  ircClient.on('message', async function (nick:string, to:string, text, message) {

    let channel = null;
    if(to.startsWith('#')) {
      channel = to;
    }
    const isMe = ircClient.nick === nick;

    const isHistServ = message.nick!.toLowerCase() === 'histserv';
    if(!isHistServ && !isMe) {
      try {
        const isCommand = await NNCommandHandler.handleCommandMessage(channel, nick, text, message, ircClient);
        if(isCommand) return;
      } catch(e) {}
    }
    // Not a command, proceed as normal chat

    if(to.toLowerCase() === BotMetaChannel_lowercase) return;
    if(nick.toLowerCase() === BotName_lowercase) return;

    let opSign;
    if(to.startsWith('#')) {
      opSign = ircClient.chans.get(to)!.users.get(nick)!;
    } else {
      opSign = '';
    }
    storeActions.message(nick,to,text,opSign,isMe, isHistServ);
    transmitPureMessage(nick, to, text, message);
  });

  function anyEvent(origin:EventBaseType, data:{event:IRCEvent_BaseEvent,dest:string}) {
    if(stickScrollToBottom()) {
      scrollMessagesToBottom();
    }

    // Add to logger files
    IrcEventTextLogger(origin, data);
  }

  mainStore.tabEvents.on('channel', (d)=>anyEvent('channel',d))
  mainStore.tabEvents.on('private', (d)=>anyEvent('private',d))
  mainStore.tabEvents.on('backlog', (d)=>anyEvent('backlog',d))

  function scrollMessagesToBottom() {
    messagesContainer.scrollTop = messagesContainer.scrollHeight;
  }

  async function transmitPureMessage(nick:string|null, to:string, text:string, raw:any) {
    IrcClientExtra.customEvents.emit('pureMessage', nick, to, text, raw);

    // also notify extensions
    try {
      for(const extName in Settings.loadedJsExtensions) {
        const ext = Settings.loadedJsExtensions[extName];
        try {
          await ext?.onPureIrcMessage?.(nick, to, text, raw);
        } catch(e) {
          console.error(`Extension ${extName} onPureIrcMessage() error`, e);
        }
      }
    } catch(e) {}
  }

  ircClient.on('selfMessage', async function(to:string, text:string) {
    const lto = to.toLowerCase();
    if(lto === BotName_lowercase || lto == 'nickserv' || lto == 'chanserv' || lto == backlogTabL) return;
    
    const isCommand = await stub.hideCommandMessage(to, text);
    if(isCommand) {
      return // Hide commands
    }

    let opSign;
    if(to.startsWith('#')) {
      opSign = ircClient.chans.get(to)!.users.get(ircClient.nick)!;
    } else {
      opSign = '';
    }
    storeActions.message(ircClient.nick,to,text,opSign,true, false);
    transmitPureMessage(null, to, text, null);
  })

  ircClient.on('motd', function (text:string) {
    storeActions.motd(text)
  })

  function changeMode(add:boolean, channel:string, by:string, mode:string, setOn:string, message:any) {
    const lchan = channel.toLowerCase();
    const lseton = setOn.toLowerCase()

    const byLowercase = by?.toLowerCase();
    mainStore.addEventTo('channel', lchan, mainStore.joinedChannels[lchan], {
      type: 'mode',
      ts: Date.now(),
      add,
      byIsMe: byLowercase==ircClient.nick.toLowerCase(),
      on:setOn,
      byUser: by,
      channel,
      onIsMe: lseton == ircClient.nick.toLowerCase(),
      mode
    } as IRCEvent_Mode)

    const sign = IrcUserMod_CmdToSign[mode];
    const chanData = mainStore?.joinedChannels?.[lchan];
    const destData = chanData?.nicks?.[lseton];
    if(destData && sign.length) {
      if(add) {
        mainStore.joinedChannels[lchan].nicks[lseton].sign = sign;
      } else {
        mainStore.joinedChannels[lchan].nicks[lseton].sign = '';
      }
    }

    if(mode === 'k') {
      // room password
      if(mainStore.imHostInChannel(channel)) {
        const g = mainStore.gameForChannel(channel);
        const chanData = mainStore?.joinedChannels?.[lchan];
        if(g && chanData) {
          let password
          const hasPassword = add;
          if(add) {
            password = setOn;
            chanData.password = password;
          } else {
            password = null // mark as no 
            delete chanData.password;
          }
          const prevHadPassword = g.publicDetails.hasPassword === true;
          if(hasPassword != prevHadPassword) {
            g.publicDetails.hasPassword = !g.publicDetails.hasPassword
            // Update advertised room through bot. TODO: only send this if room is advertised.
            IrcClientExtra.sendNNCmdToBot(ircClient, {
              op: 'requestUpdateRoom',
              data: {
                channel:channel,
                patch: {
                  password
                }
              }
            } as Cmd_RequestUpdateRoom)
          }
        }
      }
    }
  }

  ircClient.on('+mode', function (channel:string, by:string, mode:string, argument:any, message:any) {
    if(!by) by = argument;
    changeMode(true, channel, by, mode, argument, message)
  })

  ircClient.on('-mode', function (channel:string, by:string, mode:string, argument:any, message:any) {
    if(!by) by = argument;
    changeMode(false, channel, by, mode, argument, message)
  })

  ircClient.on('nick', function (oldnick:string, newnick:string, channels, message) {
    const isMe = ircClient.nick.toLowerCase() === newnick.toLowerCase();
    storeActions.nickUpdate(oldnick, newnick, isMe);
  })

  ircClient.on('names', function (channel, nicks) {
    storeActions.updateNickNamesInChannel(channel,nicks as Map<string,IrcModSignOrNone>);
  });

  ircClient.on('topic', function (channel, topic, nick, message) {
    const isMe = ircClient.nick === nick;
    const setAtEpochSeconds = message?.args?.[3];
    storeActions.gotTopic(channel, topic, nick, isMe, message.command!, setAtEpochSeconds);
  });

  ircClient.on('invite', function(channel:string, from:string, message) {
    if(from.toLowerCase() === BotName_lowercase) {
      ircClient.join(channel);
    }
    storeActions.invite(channel, from, ()=>{
      ircClient.join(channel)
    });
  });


  const [showLaunchGameModal, setShowLaunchGameModal] = createSignal(false);
  const handleCloseLaunchGameModal = () => setShowLaunchGameModal(false);

  const onTopBarCommandClicked = async (cmd:string)=>{
    setTimeout(async ()=>{
      for(const extName in Settings.loadedJsExtensions) {
        const ext = Settings.loadedJsExtensions[extName];
        try {
          await ext?.topbarCmd?.(cmd);
        } catch(e) {
          console.error(`Extension ${extName}: error in topbarCmd`, e);
        }
      }
    });

    if(cmd === 'launch_game') {
      openLaunchGameModal();
    } else if(cmd === 'register') {
      
    } else if(cmd === 'logout') {
      Settings.logout();
      ircClient.say('NickServ', `CLIENTS LOGOUT ${Settings.read().IrcNick} ALL`);
    } else if(cmd === 'login') {
      // Not implemented yet
      // const creds = await DialogMgr.addPopup({
      //   icon: 'question',
      //   title: 'Login',
      //   html:
      //     '<input type="text" placeholder="User Name" id="swal-input1" class="swal2-input">' +
      //     '<input type="password" placeholder="Password" id="swal-input2" class="swal2-input">',
      //   focusConfirm: false,
      //   preConfirm: () => {
      //     return [
      //       (document.getElementById('swal-input1')! as HTMLInputElement).value,
      //       (document.getElementById('swal-input2')! as HTMLInputElement).value
      //     ]
      //   }
      // });
      // const [username, password] = creds.value;
      // storeActions.doLogin(username, password, ircClient);
    } else if(cmd === 'port_forward') {
      setShowPortForwardModal(true);
    } else if(cmd === 'nukemnet_settings') {
      setShowLauncherSettingsModal(true);
    } else if(cmd === "toggle_roomlist") {
      mainStore.showRoomsList = !mainStore.showRoomsList;;
    } else if(cmd === 'join_room') {

      const formId = 'join-room-channel-input';
      function doJoin() {
        const input = document.getElementById(formId) as HTMLInputElement;
        let channel = input.value.trim();
        if(!channel.startsWith('#')) channel = '#'+channel;
        ircClient.send('JOIN', channel)
      }
      const popup = DialogMgr.addPopup({
        title: "Join Room",
        body: ()=><Form onsubmit={function(e) {
          e.preventDefault();
          doJoin();
          popup.close();
        }}><Form.Control id={formId} size="lg" type="text" placeholder="Channel Name" /></Form>,
        buttons: [{
          variant: 'primary',
          body: "Join",
          onClick: doJoin
        }, {body: "Cancel", variant:'secondary'}]
      });
      document.getElementById(formId)?.focus();

    } else if(cmd === 'about') {
      DialogMgr.addPopup({
        buttons: [{body:"Close"}],
        fullscreen:true,
        title: "About NukemNet",
        body:()=>
        <div style="font-size: 12pt">
        
          <h4>Version: {Settings.pjson.version}</h4>
          <h5><a onClick={()=>Electron.shell.openExternal('https://nukemnet.com')} href="javascript:;">nukemnet.com</a></h5>
          <div>irc.nukemnet.com</div><br/>

          <div>Join our <a onClick={()=>Electron.shell.openExternal('https://discord.gg/AmJXc8PuNS')} href="javascript:;">Discord</a></div><br/>

          <div>
            NukemNet is brought to you by aaBlueDragon (Alon Amir), author of a previous multiplayer launcher (Dukonnector).<br/>
          </div>
          <div>
            While I do have a lot of fun developing NukemNet, I got server bills to pay (Linux Server, IRC Daemon, Database, MS Azure Storage for file upload, etc..)<br/>
            And an endless amount of coding, supporting new games, fixing bugs, adding features.<br/>
            NukemNet will always be free, but If you enjoy it, consider buying me a beer on<br/>
            <a onClick={()=>Electron.shell.openExternal('https://www.patreon.com/alonamir')} href="javascript:;">Patreon: https://www.patreon.com/alonamir</a> or 
            <a onClick={()=>Electron.shell.openExternal('https://www.paypal.com/donate/?business=6X6AADL3TVMZL&no_recurring=0&currency_code=ILS')} href="javascript:;">PayPal: (aabluedragon@gmail.com)</a> or 
            <a onClick={()=>Electron.shell.openExternal('https://Ko-fi.com/alonamir')} href="javascript:;">Ko-fi: (alonamir)</a>
          </div>
          <br/>
          <h5>Credits go to</h5>
          <div>
            <span style="font-weight:bold">3DRealms Apogee IDSoftware</span> and others, for making such revolutionary iconic games and engines.
          </div>
          <br/>
          <div>
            <span style="font-weight:bold">ZNukem</span> for helping me test NukemNet a whole lot, finding bugs, and gave me motivation to come up with a new launcher, during our first dukematch on (7/May/2022), and for the cool welcome sound.
          </div>
          <br/>
          <div>
            <span style="font-weight:bold">NY00123</span> For all the years of YANG maintainance (along with Turrican and Striker), another cross-platform multiplayer launcher. also technical consultation about linux support, irc discord bridge.<br/>
          </div>
          <br/>
          <div>
            <span style="font-weight:bold">Snake Plissken</span> For testing with me and ZNukem, having fun matches.
          </div>
          <br/>
          <div>
            <span style="font-weight:bold">Ergo IRC Team</span> for developing an awesome IRC daemon.
          </div>
          <br/>
          <div>
            <span style="font-weight:bold">AlinaFK ❤️</span> for not kicking me in the face for being way too much on my laptop all these months, working on NukemNet.
          </div>
          <br/>
          <div>
            <span style="font-weight:bold">sudolinux</span> for the logo, and helping me test NukemNet.
          </div>
          <br/>
          <div>
            <span style="font-weight:bold">THEBaratusII</span> for the top-left logo, and helping me test NukemNet.
          </div>
          <br/>
          <div>
              <span style="font-weight:bold">IP2Location</span>: This site or product includes IP2Location LITE data available from http://www.ip2location.com.<br/>
              IP2Location is a registered trademark of Hexasoft Development Sdn Bhd. All other trademarks are the property of their respective owners.
          </div>
          <br/>
          <div>
              <span style="font-weight:bold">FreakFlags</span>: https://www.freakflagsprite.com Spritesheet containing all country flags.
          </div>
        </div>
      });
    } else if(cmd === 'refresh_rooms') {
      storeActions.refreshGameRoomsList(ircClient);
    }
  }

  async function notifyExtensions_GameRoomDetailsSet(result:LaunchGameModalResult) {
    try {
      for(const extName in Settings.loadedJsExtensions) {
        const ext = Settings.loadedJsExtensions[extName];
        try {
          await ext?.gameRoomDetailsSet?.(result);
        } catch(e) {
          console.error(`Error calling gameRoomDetailsSet for extension ${extName}`, e);
        }
      }
    } catch(e) {}
  }

  const onGameSubmit = async (result:LaunchGameModalResult)=>{
    handleCloseLaunchGameModal();
    await notifyExtensions_GameRoomDetailsSet(result);

    function tipUserOnConstantPorts(execDef:GameDefsType.Executable) {
      const ports = mainStore.execConstantPort(execDef);
      DialogMgr.addPopup({
        title:"Constant Ports",
        closeButton: true,
        buttons: [{body:"Close"}],
        noCenter:true,
        body:()=><>
          {Settings.gameDefs.games[result.game].name} {execDef.name} has constant port(s) that cannot be changed.<br/>
          <Show when={ports.tcp}>
            <b>TCP {ports.tcp}</b><br/>
          </Show>
          <Show when={ports.udp}>
            <b>UDP {ports.udp}</b><br/>
          </Show>
          If you are playing on LAN, you may ignore this message, otherwise, here are a few tips:<br/>
          <ul>
            <li>If you switch to UDP4Tunnel (IPv4 only), it will use your Game Port {Settings.getGamePort()} and allow usage of STUN/Relay instead of port forwarding</li>
            <li>You can port forward the above port(s) in <b>PortConfig</b> window</li>
            <li>If all participants have IPv6, it should also work as is</li>
          </ul>
        </>
      })
    }

    // If editting current game room
    if(result.editChannel) {
      const g = mainStore.gameForChannel(result.editChannel!);
      if(!g) return;

      const lchan = result.editChannel!.toLowerCase();

      // Edit client mode
      if(!mainStore.imHostInChannel(result.editChannel!)) {
        // Update params
        const paramsToAdd = result.args.filter(p=>p.for == 'private');
        const finalParams = g.params.filter(p=>p.for!='private');
        finalParams.push(...paramsToAdd)
        g.params = finalParams;
        return;
      }

      // Prepare patch object for bot and game channel
      const pubDetailsPatch: Room_UpdatePatchFromHost = {};
      if(result.exec != g.publicDetails.execId) {
        pubDetailsPatch.execId = result.exec;
        pubDetailsPatch.execName = Settings.gameDefs.games[result.game].executables[result.exec].name
      }
      if(result.game != g.publicDetails.gameId) {
        pubDetailsPatch.gameId = result.game;
        pubDetailsPatch.gameName = Settings.gameDefs.games[result.game].name
      }
      if(result.maxPlayers != g.publicDetails.maxPlayers) {
        pubDetailsPatch.maxPlayers = result.maxPlayers
      }
      if(result.roomName != g.publicDetails.name) {
        pubDetailsPatch.name = result.roomName
      }
      
      // If choice changed to tcp mode and game has constant ports, tip user
      const prevExecDef = Settings?.gameDefs?.games?.[g.publicDetails?.gameId]?.executables?.[g.publicDetails?.execId];
      const prevChosenUdp4Tunnel = mainStore.gameChosenUdpTunnel(g);
      const prevConstantPorts = mainStore.execConstantPort(prevExecDef);
      const newExecDef = Settings?.gameDefs?.games?.[result?.game]?.executables?.[result?.exec];
      const newChosenUdp4Tunnel = mainStore.gameChosenUdpTunnel(undefined,result.args);
      const newConstantPorts = mainStore.execConstantPort(newExecDef);
      if((JSON.stringify(prevConstantPorts) != JSON.stringify(newConstantPorts) ||
          newChosenUdp4Tunnel != prevChosenUdp4Tunnel ||
          newExecDef != prevExecDef) && 
        Object.keys(newConstantPorts).length && !newChosenUdp4Tunnel)
      {
        tipUserOnConstantPorts(newExecDef)
      }

      // apply for game channel
      Object.assign(g.publicDetails, pubDetailsPatch);
      g.params = result.args;

      // Turn off dedicated server if unapplicable
      if(mainStore.gameChosenTCPAndNotOptional(g, result.args)) {
        if(mainStore.myLeasedDedicatedServer) {
          mainStore.turnOffDedicatedServer(g, ircClient);
        }
      }

      // Update advertised room details
      IrcClientExtra.sendNNCmdToBot(ircClient, {
        op: 'requestUpdateRoom',
        data: {
          channel:result.editChannel,
          patch: pubDetailsPatch
        }
      } as Cmd_RequestUpdateRoom)

      const chanData = mainStore.joinedChannels[lchan];
      if(chanData) {
        if(chanData.password != result.password) {
          if(!result?.password?.length) {
            // Only remove pass if previous setting had a password
            if(chanData?.password?.length) {
              delete chanData.password;
              ircClient.send('MODE', result.editChannel, '-k');
            }
          } else {
            chanData.password = result.password;
            ircClient.send('MODE', result.editChannel, '+k', result.password!);
          }
        }
        if(pubDetailsPatch?.maxPlayers) {
          ircClient.send('MODE', result.editChannel, '+l', ''+result.maxPlayers!);
        }
      }

      const gameDetailsForClient = deepClone(g) as typeof g;
      gameDetailsForClient.params = storeActions.filterOutAndCloneParamDataForClients(gameDetailsForClient.params);

      mainStore.addEventTo('channel', lchan, mainStore.joinedChannels[lchan], {type:'custom_gameDetails', ts:Date.now()} as IRCEventCustom_GameDetails);

      await IrcClientExtra.sendNNCmdTo(ircClient, g.publicDetails.channel, {
        op:'sync_sendAllDetails',
        data: gameDetailsForClient
      } as CmdGameRoom_SyncSendAllDetails);

      IrcClientExtra.customEvents.emit('changedGameroomDetails', result.editChannel, g);
      
      // This is not always received correctly for some reason (after Edit Game details modal)
      // await packAndSendIrcCmd(ircClient, g.publicDetails.channel, {
      //   data: {
      //       $case: 'cmdSyncSendAllDetails',
      //       cmdSyncSendAllDetails: gameDetailsForClient
      //   }
      // } as NNPB.RootCommand);

      return;
    }

    if(result.mode === 'multiplayer') {
        if(!mainStore.connected) {
          DialogMgr.addPopup({
            title: 'Launch Room Failed',
            body: 'Not connected to IRC server',
            icon: 'error',
            buttons:[{body:"Ok"}]
          });
          return;
        }

        if(mainStore.imAlreadyInAGameRoom()) {
          DialogMgr.addPopup({
            title: 'Launch Room Failed',
            body: 'You are already in another game room',
            icon: 'error',
            buttons:[{body:"Ok"}]
          });
          return;
        }

        let channelName:string='';
        try {
          channelName = IrcClientExtra.convertIrcChanNameToValid(`#${result.roomName!.toLowerCase()}`)
        } catch(e) {}

        if(!(channelName?.length>=2)) {
          channelName = '#'+(createHhash(`${Date.now()}`));
        }

        const externalIp = mainStore.myExternalIpV4;
        if(!externalIp?.length && !offline) {
          DialogMgr.addPopup({
            title: 'Launch Room Failed',
            body: 'Missing external ip details',
            icon: 'error',
            buttons:[{body:'Ok'}]
          });
          return;
        }

        const setGameRoomAndChooseTab = async ()=>{

          async function showPortBusyDialog() {
            const gamePort = ''+Settings.getGamePort();
            let procInfoObj = null as null|{pid:number, name?:string, command?:string, path?:string};
            if(systeminformation) { // if we have that node lib available (XP builds don't have it, doesn't work there.)
              try {
                const conns = await systeminformation.networkConnections();
                const connection = conns.filter(c=>c.localPort==gamePort)?.[0];
                if(connection?.pid) procInfoObj = {pid: connection.pid};
                else throw '';
  
                const pss = await systeminformation.processes()
                const busyProc = pss.list.filter(p=>p.pid==connection.pid)?.[0];
                
                if(busyProc?.name) {procInfoObj.name = busyProc.name}
                if(busyProc?.command) {procInfoObj.command = busyProc.command}
                if(busyProc?.path) {procInfoObj.path = busyProc.path.replaceAll('\\\\', '\\')}
              } catch(e) {}
            }
            
            DialogMgr.addPopup({
              title: `Game Port Busy ${gamePort}`,
              noCenter:true,
              body: ()=>{
                const procInfo = <Show when={procInfoObj!=null && procInfoObj?.pid}>
                  <br/>
                  <Table striped bordered hover size="sm" variant="dark">
                    <tbody>
                      <tr><td>Process ID</td><td>{procInfoObj!.pid}</td></tr>
                      <Show when={procInfoObj?.name}><tr><td>Name</td><td>{procInfoObj!.name}</td></tr></Show>
                      <Show when={procInfoObj?.command}><tr><td>Command</td><td>{procInfoObj!.command}</td></tr></Show>
                      <Show when={procInfoObj?.path}><tr><td>Path</td><td>{procInfoObj!.path}</td></tr></Show>
                    </tbody>
                  </Table>
                </Show>
                
                return <>Your game port ({gamePort}) is already in use by another application<br/>{procInfo}Please close the other application.</>;
              },
              closeButton: true,
              buttons: [{body:'Ok'}]
            })
          }

          try {
            const s = Settings.read();
            const joinedChan = await storeActions.setGameRoomWithChannel(channelName, result, ircClient, Settings.gameDefs, result?.settings);
            setChosenTab(joinedChan);
            IrcClientExtra.customEvents.emit('ingameroom', channelName);

            set_NewRoomCheckingPort(true);
            try {
              if(!offline && !s?.disableAutoPortChecker) {
                await checkPortConnectivityTcpUdp_Throw(Settings.getGamePort(), externalIp! , 2500);
                set_NewRoomCheckingPort(false);
              } else {
                // if offline mode, just check local bind
                try {
                  await checkPortConnectivityTcpUdp_Throw(Settings.getGamePort());
                } catch(e) {
                  if((e as any)?.code === 'EADDRINUSE') {
                    showPortBusyDialog();
                  }
                }
                set_NewRoomCheckingPort(false);
              }
            } catch(e) {

              if((e as any)?.code === 'EADDRINUSE') {
                set_NewRoomCheckingPort(false);
                showPortBusyDialog();
                return;
              }

              const dialogRes = await DialogMgr.addPopup({
                title:"Port Not Open",
                body: ()=><>
                Your IPv4 game port ({Settings.getGamePort()}) is not open/forwarded, Therefore multiplayer games might not connect.<br/>
                {mainStore.imOnIpv6?<>If all participants are on IPv6, connectivity might still work.<br/></>:""}
                <br/>
                <b>Would you like NukemNet to try forward for you?</b><br/><br/>
                As an alternative, you may try the Relay/STUN options on the left.<br/>
                If you are hosting a LAN game, you may ignore this message.
                </>,
                icon: 'warning',
                buttons: [{body:'Yes', variant:'primary'},{body:'No', variant:'secondary'}] 
              });
              const tryForward = dialogRes.buttonIndex === 0;
              if(tryForward) {
                let windowClosed = false;
                const port = Settings.getGamePort();
                const window = DialogMgr.addWindow({title:'Forwarding...', body:()=><div id="try-port-forward-modal">Trying to forward port {port}.<br/>This can take some time...</div>,beforeClose:()=>{
                  windowClosed=true;
                  set_NewRoomCheckingPort(false);
                }});
                try {
                  await tryForwardOnAllProtocols();
                } catch(e) {}

                if(windowClosed) {
                  set_NewRoomCheckingPort(false);
                  return;
                };

                try {document.getElementById('try-port-forward-modal')!.innerHTML = 'Checking port connectivity...';} catch(e) {}
                const res = await checkPortConnectivityTcpUdp(port, mainStore.myExternalIpV4, 10000);
                if(!res.tcp && !res.udp) {
                  DialogMgr.addPopup({
                    title:"Could Not Forward",
                    body: ()=><>
                    Your game port ({port}) could not be forwarded.<br/>
                    Please try Relay or STUN.<br/>
                    (Ignore this message if playing on LAN or if you and the participants have IPv6 enabled)
                    </>,
                    icon: 'error'
                  })
                } else {
                  const partial = res.tcp != res.udp;

                  DialogMgr.addPopup({
                    title:`Forward${partial?' Partially ':" "}Successful`,
                    body: ()=><>
                    Your game port ({port}) is ready for {res.tcp?"TCP":""} {res.udp?"UDP":""} connections on IPv4.
                    </>,
                    icon: 'success'
                  })
                }
                set_NewRoomCheckingPort(false);
                window.close();
              } else {
                set_NewRoomCheckingPort(false);
              }
            }
            
          } catch(e) {
            DialogMgr.addPopup({
              title: 'Error',
              body: (e as any)?.message || e,
              icon: 'error',
              buttons:[{body:'Ok'}]
            });

          }   
        }

        if(storeActions.imInChannelAndTheOnlyOp(channelName)) {
          await setGameRoomAndChooseTab();
          return;
        }

        let allChannelNames_lower:string[] = [];
        try {
          allChannelNames_lower = (await IrcClientExtra.getAllChannels(ircClient, 5000)).map(res=>res.name.toLowerCase());
        } catch(e) {
          DialogMgr.addPopup({
            title: 'Error',
            body: 'failed to create room, get all channels timed out.',
            icon: 'error',
            buttons:[{body:"Ok"}]
          })
          return
        }

        const baseChanName = channelName;
        for(let i=0;i<20000;i++) {
          channelName = baseChanName+(i===0?'':i);
          if(allChannelNames_lower.includes(channelName)) continue;
          break;
        }
        await setGameRoomAndChooseTab();

        // tip user on constant ports
        const def = mainStore.execDefForChannel(chosenTabResolve());
        if(def) {
          const constantPorts = mainStore.execConstantPort(def);
          if(Object.keys(constantPorts).length && !mainStore.gameChosenUdpTunnel(game())) {
            tipUserOnConstantPorts(def)
          }
        }
        
        return;
    } else {
      await launchGame(result, true, 1);
    }
  }

  const hasAdvertisedGameRooms = ()=>{
    return mainStore.advertisedGameRooms && Object.keys(mainStore.advertisedGameRooms).length;
  }

  const showGameDetailsForChannel = async (channel:string)=>{
    const lhostNick = mainStore?.advertisedGameRooms?.[channel?.toLowerCase()]?.publicDetails?.owner?.toLowerCase();
    if(!lhostNick?.length || lhostNick === ircClient?.nick?.toLowerCase()) return;

    try {
      const res = await IrcClientExtra.requestAndResponse(ircClient, lhostNick, {
        op: CmdGameRoom_RequestGameDetails_op,
        data: {channel}
      } as CmdGameRoom_RequestGameDetails) as unknown as CmdGameRoom_SyncSendAllDetails;
  
      if(!res?.data) return;

      DialogMgr.addPopup({
        title:"Game Details for "+channel,
        body: ()=><div>
          <ParamDisplay channel={channel} game={res.data} />
        </div>,
        closeButton:true, buttons:[{body:'Ok'}]
      })

    } catch(e) {}
  }

  const getUsersForChannel = async (channel:string)=>{
    if(!channel?.length) return;

    try {
      const res = await IrcClientExtra.getNamesForChannel(ircClient, channel);
      DialogMgr.addPopup({
        title:"Users in "+channel,
        body: ()=><div>
          <For each={Object.keys(res||{})}>
            {(nick:string)=><div>{res[nick]}{nick}</div>}
          </For>
        </div>,
        closeButton:true, buttons:[{body:'Ok'}]
      });
    } catch(e) {}
  }

  const joinGameRoomChannel = (channel:string)=>{
    if(!channel?.length) return;
    const lchannel = channel.toLowerCase();
    if(!mainStore.advertisedGameRooms[lchannel].publicDetails) return;

    if(mainStore.advertisedGameRooms[lchannel].publicDetails.owner.toLowerCase() === ircClient.nick.toLowerCase()) {
      DialogMgr.addPopup({
        title: 'Join Failed',
        body: `You cannot rejoin your own room`,
        icon: 'error',
        buttons:[{body:"Ok"}]
      })
      return;
    }
    
    ircClient.join(channel);
  }

  const startSettingsForGame = async (game:FullGameRoomDetails, channel:string)=>{
    const res = await launchGame({
      args:[],
      game: game.publicDetails.gameId,
      exec: game.publicDetails.execId,
      mode: 'settings'
    },false,1);
    if(!res) return;
    const proc = res.proc;

    const onClose = ()=>{
      const isExecRunning = false
      game.participantNicks[mainStore.myNick.toLowerCase()].isExecRunning = isExecRunning
      mainStore.sendAllJoinedGameRooms_ImRunningExec(isExecRunning, ircClient);
    }

    proc?.once('exit', onClose)
    proc?.once('close', onClose)
    proc?.once('spawn', ()=>{
      const isExecRunning = true;
      game.participantNicks[mainStore.myNick.toLowerCase()].isExecRunning = isExecRunning
      mainStore.sendAllJoinedGameRooms_ImRunningExec(isExecRunning, ircClient);
    })
  }

  Settings.registerShortcuts();

  const destroyGameProcessesDialog = async(channel:string)=>{
    const imHost = mainStore.imHostInChannel(channel);
    DialogMgr.addPopup({title:"Are you sure?", body:()=><>{imHost?"This will terminate everyone's in-game process.":"This will terminate your game process."}</>, buttons:[{body:'Yes', variant:'primary', onClick:async ()=>{
      terminateGameProcess(true, channel, imHost);
    }}, {
      body:'No', variant:'secondary'
    }]});
  }

  const startGameFromRoom = async (channel:string)=>{
    const lchan = channel.toLowerCase();
    const chanDetails = mainStore.joinedChannels[lchan];
    const game = chanDetails.game;
    if(!game) return;

    const execDetails = Settings?.gameDefs?.games?.[game?.publicDetails?.gameId]?.executables?.[game?.publicDetails?.execId];
    if(!execDetails) {
      DialogMgr.addPopup({
        title: 'Start Failed',
        body: 'Could not detect chosen executable for this game, cannot start',
        icon: 'error',
        buttons:[{body:"Ok"}]
      })
      return
    }
    
    if(mainStore.imHostInChannel(channel)) {

      const playersStillInGame:string[] = [];
      const playersMissingExec:string[] = [];
      const playersDownloadingFiles:string[] = [];
      let participants = 0;
      for(const participant in game.participantNicks) {
        participants++;
        const pDetails = game.participantNicks[participant];
        if(pDetails.isExecRunning) {
          playersStillInGame.push(participant);
        }
        if(pDetails.isExecMissing) {
          playersMissingExec.push(participant);
        }
        if(pDetails.fileSyncProgress!=null) {
          playersDownloadingFiles.push(participant);
        }
      }
      if(playersStillInGame.length) {
        DialogMgr.addPopup({
          title: 'Start Failed',
          body: 'Cannot start, some players are still in game:\r\n' + playersStillInGame.join(', '),
          icon: 'error',
          buttons:[{body:"Ok"}]
        })
        return
      }
      if(playersMissingExec.length) {
        DialogMgr.addPopup({
          title: 'Start Failed',
          body: 'Cannot start, some players missing game exec configuration:\r\n' + playersMissingExec.join(', '),
          icon: 'error',
          buttons:[{body:"Ok"}]
        })
        return
      }
      if(playersDownloadingFiles.length) {
        DialogMgr.addPopup({
          title: 'Start Failed',
          body: 'Cannot start, some players are still downloading files:\r\n' + playersDownloadingFiles.join(', '),
          icon: 'error',
          buttons:[{body:"Ok"}]
        })
        return
      }

      if(execDetails.minPlayers != null && participants<execDetails.minPlayers) {
        // cannot start multiplayer game without mimimum amount of players (if defined for executable)
        DialogMgr.addPopup({
          title: 'Start Failed',
          body: `Must have at least ${execDetails.minPlayers} players for ${execDetails.name} multiplayer mode.`,
          icon: 'error',
          buttons:[{body:"Ok"}]
        })
        return;
      }
      
      if(participants>2 && execDetails.isPeerToPeer && game.dedicatedServer) {
        // Cannot use relay in peer2peer mode, with more than 2 players
        DialogMgr.addPopup({
          title: 'Start Failed',
          body: `Cannot Use Relay Server feature in peer2peer executables such as ${game.publicDetails.execName} with more than 2 players.<br/>either use a non-peer2peer executable, or disable relay to play with more than 2 players.`,
          icon: 'error',
          buttons:[{body:"Ok"}]
        })
        return;
      }

      async function launchAndGetProc(asHost:boolean=true) {
        const g = game!;
        const launchRes = await launchGame({
          args: g.params, 
          exec: g.publicDetails.execId, 
          game: g.publicDetails.gameId,
          mode: 'multiplayer'
        },
        asHost,
        Object.keys(g.participantNicks).length,
        {ip: mainStore.localhostIp4, port: Settings.getGamePort()},
        false,
        execDetails.isPeerToPeer?Object.values(await mainStore.getResolvedIpsInGameRoom(channel, ircClient.nick)): [],
        channel,
        !asHost,
        !asHost?hostLaunchTunnelPort:undefined)
        return launchRes;
      }

      const hostLaunchRes = await launchAndGetProc(); // Launch as host
      if(!hostLaunchRes) return;

      const hostLaunchTunnelPort = hostLaunchRes?.hostTunnelPort as any;

      const proc = hostLaunchRes.proc;

      let clientProc:ChildProcess;
    
      const onClose = async ()=>{
        try {await KillProc(clientProc)} catch(e) {} // If launched as client as well, close it.
        try {delete chanDetails?.proc;} catch(e) {}

        isExecRunning = false;

        IrcClientExtra.sendNNCmdToBot(ircClient, {
          op: 'requestUpdateRoom',
          data: {
            channel,
            patch: {isStarted:isExecRunning}
          }
        } as Cmd_RequestUpdateRoom)

        game.participantNicks[mainStore.myNick.toLowerCase()].isExecRunning = isExecRunning
        mainStore.sendAllJoinedGameRooms_ImRunningExec(isExecRunning, ircClient);
      }

      let isExecRunning = null;
      proc?.once('close', onClose);
      proc?.once('exit', onClose);
      proc?.once('spawn', ()=>{
        isExecRunning = true;
        chanDetails.proc = proc;

        IrcClientExtra.sendNNCmdToBot(ircClient, {
          op: 'requestUpdateRoom',
          data: {
            channel,
            patch: {isStarted:isExecRunning}
          }
        } as Cmd_RequestUpdateRoom)

        game.participantNicks[mainStore.myNick.toLowerCase()].isExecRunning = isExecRunning
        mainStore.sendAllJoinedGameRooms_ImRunningExec(isExecRunning, ircClient);
      })

      const launchAsClientDef = execDetails?.runnables?.multiplayer?.hostAlsoRunsAsClient;
      const launchAsClient = typeof launchAsClientDef === 'function'?launchAsClientDef(hostLaunchRes.evalArgs):launchAsClientDef

      const dedicatedModeParam = game?.params?.find(p=>p.id === GameDefsType.DEDICATEDMODE_PARAM_ID);
      const dedicatedServerMode = dedicatedModeParam?.args?.[0] != null;
    
      const waitForListenPortProto = execDetails?.runnables?.multiplayer?.waitForListenPortOnProto;
      if(waitForListenPortProto != null) {
        await waitUntilPortIsUsedInProto(Settings.getGamePort(), waitForListenPortProto)
      }
      const waitForMS = execDetails?.runnables?.multiplayer?.waitForMS;
      if(waitForMS && !Number.isNaN(waitForMS)) {
        await sleep(waitForMS);
      }
      const waitForFunc = execDetails?.runnables?.multiplayer?.waitForFunc;
      if(waitForFunc) {
        try {
          await waitForFunc(hostLaunchRes.evalArgs);
        } catch(e) {}
      }

      if(isExecRunning === null || isExecRunning === true) {
        // null is also ok, because 'spawn' event did not rise up yet.

        // init clients only if launched successfully
        if(proc) {
          // BEGIN launch as client if needed
          if(launchAsClient && !dedicatedServerMode) {
            // In cases such as Rise of the triad, we need to run 2 instances, one as dedicated server, and one as client, even as a host of the game room.
            const res = await launchAndGetProc(false); // Launch as client
            if(!res) return;

            clientProc = res.proc;
            async function onClientClose() {
              try {await KillProc(proc)} catch(e) {}
            }
            clientProc?.once('close', onClientClose);
            clientProc?.once('exit', onClientClose);
          }
          // END launch as client if needed
          
          mainStore.addEventTo('channel', lchan, mainStore.joinedChannels[lchan], {
            type: 'custom_processStartedByHost',
            ts: Date.now(),
            channel
          } as IRCEvent_ProcessStartedByHost);
          storeActions.showStartGameAnimation();

          IrcClientExtra.sendNNCmdTo(ircClient, channel, {
            op: 'launchGame',
            data: {}
          } as CmdGameRoom_LaunchGame)
        }
        
      }

    }
  }

  const game = ()=>{
    return mainStore.gameForChannel(chosenTabResolve());
  }

  const [newRoomCheckingPort, set_NewRoomCheckingPort] = createSignal(false);

  const [isFetchingDedicatedServer, setIsFetchingDedicatedServer] = createSignal(false);

  const fetchDedicatedServer = async ()=>{
    if(!mainStore.botIsAvailable()) {
      DialogMgr.addPopup({
        title: 'Relay Lease Failed',
        body: `Cannot lease relay server: ${BotName} is unavailable`,
        icon: 'error',
        buttons:[{body:"Ok"}]
      })
      return;
    }

    const g = game();
    const execDef = mainStore.execDefForChannel(chosenTabResolve());
    if(!execDef) return; // should not happen

    const participantsCount = Object.keys(g.participantNicks).length;

    if(execDef.isPeerToPeer && participantsCount>2) {
      DialogMgr.addPopup({
        title: 'Relay Lease Failed',
        body: `Cannot use relay in ${execDef.name} for more than 2 players.`,
        icon: 'error',
        buttons:[{body:"Ok"}]
      })
      return;
    }

    const sendToggleToParticipants = ()=>{
      IrcClientExtra.sendNNCmdTo(ircClient, g.publicDetails.channel, {
        op: 'toggleDedicatedServer',
        data: {
          details: g.dedicatedServer
        }
      } as CmdGameRoom_ToggleDedicatedServer)
    }

    const relayType = 'udp';
    if(mainStore.myLeasedDedicatedServer && mainStore.myLeasedDedicatedServer.type == relayType) {
      g.dedicatedServer = mainStore.myLeasedDedicatedServer;
      sendToggleToParticipants();
    } else {

      try {
        const res = await axios.get(`http://${serverHost}:${serverHttpPort}/relay`);
        const relayOptions = res.data as RelayInfo[];

        const relays: (RelayInfo&{pingDesc?:string})[] = relayOptions;

        const popupPromise = DialogMgr.addPopup({
          icon:'info',
          title: 'Select Relay',
          body:()=><>A relay allows you to host without port-forwaring<br/>
          Depending on the situation, might add some lag, or make the connection more stable.<br/><br/><div id="choose-relay"></div></>,
          buttons:[{body:"Cancel"}, {body:"Find Optimal", onClick: ()=>{
            findOptimalRelay(ircClient, g.publicDetails.channel, relays, (relayId)=>{
              if(relayId==null) return;
              useRelayById(relayId);
            });
          }}]
        });

        const relaysStore = createMutable({
          relays
        });
        let chosenRelay = null as RelayInfo|null;
        const el = <Table striped bordered hover variant="dark" class="supported-games-table">
          <thead>
            <tr><td>Name</td><td>Ping</td></tr>
          </thead>
          <tbody>
            <For each={relaysStore.relays}>
              {(item)=>{
                return <tr style={{cursor:'crosshair'}} onclick={()=>{
                  chosenRelay = item;
                  popupPromise.close();
                }}><td>{item.label}</td><td>{item.pingDesc || 'Pinging..'}</td></tr>;
              }}
            </For>
          </tbody>
        </Table>

        let stopPinging = false;
        const timer = setTimeout(()=>{stopPinging=true},30000); // Force stop pinging after some time
        for(const relay of relaysStore.relays) {
          (async ()=>{
            while(!stopPinging) {
              const res = await ping.promise.probe(relay.host, {timeout: 3, extra: isWindows()?[]:['-i', '1']});
              const time = res.times?.[0];
              relay.pingDesc = time!=null? `${time}`:'No Response';
              await sleep(800);
            }
          })();
        }
        render(() => el, document.getElementById('choose-relay') as HTMLElement);

        await popupPromise;
        clearTimeout(timer);
        stopPinging = true;

        if(chosenRelay==null) return;
        await useRelayById(chosenRelay.id);

      } catch(e) {}      
    }
  }

  async function useRelayById(relayId:string) {
    setIsFetchingDedicatedServer(true);

    const userChoiceDialog = await DialogMgr.addPopup({
      title:"Choose Relay Mode",
      body:"Use only as fallback for players that cannot have a direct connection to you, or forced either way?",
      buttons:[{body:"Forced"}, {body:"As Fallback"}, {body:"Cancel"}]
    })
    if(userChoiceDialog.buttonIndex===2) return setIsFetchingDedicatedServer(false);
    const forced = userChoiceDialog.buttonIndex===0;

    try {
      const res = await IrcClientExtra.requestAndResponseBot(ircClient, {
        op: 'manageRelayRequest',
        data: {action:'fetch', id:relayId, type: 'udp'}
      } as Cmd_ManageRelayRequestFetch) as Cmd_ManageRelayCreated
      
      // If success
      if(res.op === 'spawnRelayCreated' && res?.data?.conn?.ipOrHostname?.length) {
        const srvCfg = {...res.data.conn, ...{forced}}

        mainStore.myLeasedDedicatedServer = srvCfg;

        if(srvCfg?.type==='udp') {
            try {
                // create local udp forward tunnel
                mainStore.myLeasedDedicatedServer!.rc = await RelayClient.create(srvCfg.ipOrHostname, srvCfg.port, Settings.getGamePort(), {
                    ip: mainStore.localhostIp4, // local tunnel has to be ipv4, because most games don't support ipv6
                    port: Settings.getGamePort()
                });
            } catch(e) {}
        }

        for(const chan in mainStore.joinedChannels) {
          const g = mainStore.gameForChannel(chan);
          if(g != null && mainStore.imHostInChannel(chan)) {
              g.dedicatedServer = srvCfg;
              IrcClientExtra.sendNNCmdTo(ircClient, chan, {
                  op:'toggleDedicatedServer',
                  data: {details: srvCfg}
              } as CmdGameRoom_ToggleDedicatedServer)
          } 
        }
      }

    } catch(e) {
      const msg = (e as any).error || (e as any).message
      DialogMgr.addPopup({
        title: 'Relay Lease Failed',
        body: 'Failed to lease relay server: ' + msg,
        icon: 'error',
        buttons:[{body:"Ok"}]
      })
    } finally {
      setIsFetchingDedicatedServer(false);
    }
  }

  const usersInChosenTab = ()=>{
    const tabData = mainStore?.joinedChannels?.[chosenTabResolve()];
    if(!tabData) return [];

    const users = Object.keys(tabData?.nicks||{});
    users.sort((a,b)=>{
      const aSign = tabData.nicks[a].sign||'';
      const bSign = tabData.nicks[b].sign||'';
      if(aSign!=bSign) {
        const aSignNum = IrcModSign_ToNum[aSign]||-1;
        const bSignNum = IrcModSign_ToNum[bSign]||-1;
        return bSignNum-aSignNum;
      } else {
        if(b==a) return 0;
        else return b>a?-1:1;
      }
    });
    return users;
  }

  const tabIsDirty = (ltab:string)=>{
    const hasUnread = mainStore?.joinedChannels?.[ltab]?.hasUnread ||
    mainStore?.privateChats?.[ltab]?.hasUnread ||
    (ltab === backlogTab.toLowerCase() && mainStore?.backlog?.hasUnread)

    return hasUnread && chosenTabResolve().toLowerCase() != ltab;
  }

  const [showPortForwardModal, setShowPortForwardModal] = createSignal(false);
  const [showLauncherSettingsModal, setShowLauncherSettingsModal] = createSignal(false);

  const nickDisplay = (nick:string)=>{
    const lnick = nick.toLowerCase();
    const fullNick = (mainStore?.joinedChannels?.[chosenTabResolve()]?.nicks?.[lnick]?.name||'');
    const nickData = mainStore?.joinedChannels?.[chosenTabResolve()]?.nicks?.[lnick];
    const detectedLanIp = nickData?.lan?.detectedLanIp;
    const isOnSameLan = detectedLanIp?.length;
    const isP2p = mainStore.execDefForChannel?.(chosenTabResolve())?.isPeerToPeer;
    const fileSyncProgress = game()?.participantNicks?.[nick?.toLowerCase()]?.fileSyncProgress;
    
    let flagImg = null
    if(nickData?.country?.length) {
      flagImg = <span class={"user-flag-img fflag ff-md fflag-" + nickData.country.toUpperCase()} title={nickData.country? CountryCodeToName(nickData.country):''} />
    }


    let pingContainer = null;

    (()=>{
      let ping = null;
      let pingToHost = null
      let foundPing = false;
      if(nickData.pingToHost!=null) {
        foundPing = true;
        pingToHost = <span title={`Ping to host: ${nickData.pingToHost} MS`} style={{flex:1, "font-size":'10pt', "color":"darkred", "font-weight":'bolder', "background-color":"#FFCCCB", "border":"1pt solid darkred", "border-radius":"10pt"}}>{nickData.pingToHost}</span>
      }
      if(nickData.ping!=null) {
        foundPing = true;
        ping = <span title={`Ping between you and ${fullNick}: ${nickData.ping} MS`} style={{flex:1, "font-size":'10pt', "color":"darkblue", "font-weight":'bolder', "background-color":"lightblue", "border":"1pt solid darkblue", "border-radius":"10pt"}}>{nickData.ping}</span>
      }
      if(foundPing) {
        pingContainer = <span style={{display:'flex', 'flex-direction': 'row', 'align-items': 'center', "align-content": 'center'}}>
          {pingToHost}
          {ping}
        </span>
      }
    })();

    return <span style={{display:'flex', "flex-direction":'row', 'align-items': 'center'}}>
        {pingContainer}
        <Show when={fileSyncProgress!=null}><span title='Syncing Files...' style={{"font-size":"9pt"}}>{fileSyncProgress}%</span></Show>
        <span title="User is still in game (process still running)">{(mainStore?.joinedChannels?.[chosenTabResolve()]?.game?.participantNicks?.[lnick]?.isExecRunning?'🎮':'')}</span>
        <span title="Chosen game executable was not setup by this user">{(mainStore?.joinedChannels?.[chosenTabResolve()]?.game?.participantNicks?.[lnick]?.isExecMissing?'❌':'')}</span>
        <span style={{"background-color":'green'}} title={"User is in same LAN as you "+detectedLanIp}>{(isOnSameLan?'🖧':'')}</span>
        <Show when={isP2p}><span title="Checking if user is in same LAN as you">{(mainStore?.joinedChannels?.[chosenTabResolve()]?.nicks?.[lnick]?.lan?.isDetecting?'⏳️':'')}</span></Show>
        {(mainStore?.joinedChannels?.[chosenTabResolve()]?.nicks?.[lnick]?.sign||'')}{fullNick}
        &nbsp;{flagImg}
      </span>
  }

  function focusMessageInput() {
    try {
      const input = document.getElementById('message-input') as HTMLInputElement;
      input.focus();
    } catch(e) {}
  }

  let popupPickerController:PopupPickerController;
  function showEmojiPicker() {
    if(popupPickerController) {
      if(popupPickerController.isOpen) {
        popupPickerController.close()
      } else {
        popupPickerController.open()
      }
    }
    focusMessageInput();
  }

  const [isDragginFile,SET_isDragginFile] = createSignal(false);

  const emojiContainer = document.createElement('span')
  emojiContainer.id = 'emoji-picker';
  document.body.appendChild(emojiContainer);
  setTimeout(()=>{

    popupPickerController = createPopup({}, {    
      referenceElement: emojiContainer,    
      position: 'bottom-start',
      hideOnClickOutside: false,
      hideOnEmojiSelect: false,
      hideOnEscape: false,
      showCloseButton: false
    });

    popupPickerController.addEventListener('picker:open', focusMessageInput);
    popupPickerController.addEventListener('data:ready', focusMessageInput);    
    popupPickerController.addEventListener('emoji:select', event => {
      const input = document.getElementById('message-input') as HTMLInputElement;
      input.value += event.emoji
      focusMessageInput()
    });

    const holder = document.getElementById('events-container') as HTMLDivElement;
    holder.ondragover = () => {
      SET_isDragginFile(true);
      return false;
    };
    holder.ondragleave = () => {
      SET_isDragginFile(false);
      return false;
    };
    holder.ondragend = () => {
      SET_isDragginFile(false);
      return false;
    };
    holder.ondrop = (e) => {
      SET_isDragginFile(false);
      e.preventDefault();
      const fpath = (e as any)?.dataTransfer?.files?.[0]?.path as string;
      if(fpath?.length) {
        genericUploadLogic(fpath, path.basename(fpath), chosenTabResolve(), ircClient);
      }
      return false;
    };
  });

  const openLaunchGameModal = (preConfig?:LaunchGameModalResult)=>{
    if(preConfig) {
      SET_LaunchGamePreConfig(preConfig);
    } else {
      SET_LaunchGamePreConfig(null);
    }
    // Refresh modal view with updated values
    setShowLaunchGameModal(false);
    setShowLaunchGameModal(true);
  }

  const [LaunchGamePreConfig, SET_LaunchGamePreConfig] = createSignal(null as null|LaunchGameModalResult);

  async function showChooseUploadFileDialog() {
    const result = await electronRemote.dialog.showOpenDialog({
      properties: ['openFile'],
      filters: [{name:"File", extensions:['*']}]
    })
    try {
      const newPath = path.resolve(result.filePaths[0]);
      genericUploadLogic(newPath, path.basename(newPath), chosenTabResolve(), ircClient);
    } catch(e) {}
  }

  async function doWhoIs(nick: string) {
    try {
      const result = await IrcClientExtra.whois(ircClient, nick);
      mainStore.addEventTo('backlog', 'backlog', mainStore.backlog, {
        ts: Date.now(),
        type: 'custom_raw',
        obj: result
      } as IRCEventCustom_Raw)
    } catch(e) {}
  }

  async function sendMessage() {
    try {
      const input = document.getElementById('message-input') as HTMLInputElement;

      historyCommandsBrowseIndex = -1;
  
      const msg = input.value.trim();
      input.value = '';
  
      if(!historyCommands.includes(msg)) {
        historyCommands.push(msg);
        if(historyCommands.length>10) {
          historyCommands.splice(0,1);
          // Max 10 history commands
        }
      }
  
      const lmsg = msg.toLowerCase();
      
      if(lmsg.startsWith('/')) {
        const lMsgParts = lmsg.split(/\s+/);
        const msgParts = msg.split(/\s+/);
        const lCmd = lMsgParts[0];
        if(lCmd == '/leave' || lCmd == '/part') {
          ircClient.send('PART', msgParts.slice(1)?.[0] || chosenTabResolve()); // NOTE: giving wrong params to PART command can cause the app to stop being able to join channels.
        } else if(lCmd == '/join') {
          const parts = msgParts.slice(1);
          ircClient.send('JOIN', ...parts);
        } else if(lCmd == '/rehash') {
          ircClient.send('REHASH');
        } else if(lCmd == '/tag') {
          ircClient.conn!.write(`@example-tag=example-value PRIVMSG #lobby :Message\r\n`);
        } else if(lCmd == '/history') {
          // TODO use CHATHISTORY extension instead, less problematic. the simpler "HISTORY" issue is that it re-triggers old NukemNet commands (both protobuf and json)
          
          const parts = msgParts.slice(1);
          // ircClient.send('CHATHISTORY', ...parts);
          // ircClient.send('CHATHISTORY', 'LATEST', '#lobby', '*', '100');
        } else if(lCmd == '/whois') {
          await doWhoIs(msgParts[1]);        
        } else if(lCmd == '/names') {
          const json = await IrcClientExtra.getNamesForChannel(ircClient, msgParts?.[1] || chosenTabResolve());
          mainStore.addEventTo('backlog', 'backlog', mainStore.backlog, {
            ts: Date.now(),
            type: 'custom_raw',
            obj: json
          } as IRCEventCustom_Raw)
        } else if(lCmd == '/nick') {
          const newNick = msgParts[msgParts.length-1];
          if(newNick.length>1) {
            ircClient.send('NICK', newNick);
          }
        } else if(lCmd == '/msg') {
          if(msgParts.length<3) return;
          const target = msgParts[1];
          const contents = msgParts.slice(2).join(' ');
          ircClient.say(target, contents);
        } else if(lCmd == '/kick') {
  
          if(msgParts.length<3) return;
          const channel = msgParts[1];
          const who = msgParts[2];
          const reason = msgParts?.[3];
          if(reason?.length) {
            ircClient.send('KICK', channel, who, reason)
          } else {
            ircClient.send('KICK', channel, who)
          }
  
        } else if(lCmd == '/ns') {
          if(msgParts.length<2) return;
          const target = 'NickServ'
          const contents = msgParts.slice(1).join(' ');
          ircClient.say(target, contents);
        } else if(lCmd == '/cs') {
          if(msgParts.length<2) return;
          const target = 'ChanServ'
          const contents = msgParts.slice(1).join(' ');
          ircClient.say(target, contents);
  
        } else if(lCmd =='/op') {
          const target = msgParts?.[1];
          ircClient.send('MODE', chosenTabResolve(), "+o", target?.length? target:ircClient.nick);
  
        } else if(lCmd =='/mode') {
          const parts = msgParts.slice(1);
          ircClient.send('MODE', ...parts);
  
        } else if(lCmd =='/oper') {
          const parts = msgParts.slice(1);
          ircClient.send('OPER', ...parts);
  
        } else if(lCmd =='/topic') {
          const channel = msgParts[1];
          const text = msgParts.slice(2).join(' ');
          ircClient.send('TOPIC', channel, text);
  
        } else if(lCmd =='/list') {
          try {
            const res = await IrcClientExtra.getAllChannels(ircClient, 5000);
            mainStore.addEventTo('backlog', 'backlog', mainStore.backlog, {
              ts: Date.now(),
              type: 'custom_raw',
              obj: res
            } as IRCEventCustom_Raw)
          } catch(e) {}
          
        }
      } else {
        if(!chosenTabResolve().startsWith('@')) { // Not backlog
          ircClient.say(chosenTabResolve(), msg);
        }
      }
      input.focus();
    } catch(e) {}
  }

  const isDetectingLan = ()=>{
    const t = chosenTabResolve();
    if(!t) return false;

    const allNicks = mainStore?.joinedChannels?.[t?.toLowerCase()]?.nicks || {};
    for(const n in allNicks) {
      if(allNicks?.[n]?.lan?.isDetecting == true) {
        return true;
      }
    }
    return false;
  }

  const lanMode = ()=>{
    const t = chosenTabResolve();
    if(!t) return false;

    const isP2p = mainStore.execDefForChannel(t)?.isPeerToPeer;
    if(mainStore.imHostInChannel(t) && !isP2p) {
      return <></>
    }

    if(isP2p) {
      let foundWithLan = false;
      let foundWithWan = false;
      const allNicks = mainStore?.joinedChannels?.[t?.toLowerCase()]?.nicks || {};
      for(const n in allNicks) {
        if(n.toLowerCase() === ircClient.nick.toLowerCase()) continue;
  
        if(allNicks?.[n]?.lan?.detectedLanIp != null) {
          foundWithLan = true;
        } else {
          foundWithWan = true
        }
      }
  
      if(foundWithLan) {
        if(foundWithWan) {
          return <span title="Some players are on the same LAN as you">Partial LAN</span>
        } else {
          return <span title="All players are on the same LAN as you">LAN</span>
        }
      } else {
        if(foundWithWan) {
          return <span title="None of the players are on the same LAN as you">WAN</span>
        } else {
          return <></>
        }
      }
    } else {
      const chanData = mainStore?.joinedChannels?.[t?.toLowerCase()];
      const owner = chanData.game?.publicDetails?.owner?.toLowerCase();
      if(owner === ircClient.nick.toLowerCase()) {
        return <></>
      } else {
        const detectedLanIp = chanData?.nicks?.[owner||'']?.lan?.detectedLanIp;
        const foundOnLan = detectedLanIp != null
        if(foundOnLan) {
          return <span title="You and the host are on the same LAN">LAN {detectedLanIp}</span>
        } else {
          return <span title="You and the host are not on the same LAN">WAN</span>
        }
      }
      
    }
    
  }

  async function copyLaunchUrl() {
    async function showPopup_SavedToClipboard(launchUrl:string) {
      try {
        Electron.clipboard.writeText(launchUrl);
        DialogMgr.addPopup({title:"Copied to clipboard", body:()=>
        <div>
          {launchUrl}
          <br/>
          To provide a clickable link on discord, use the following format:<br/>
          <b>{"<"+launchUrl+">"}</b>
        </div>,
          buttons:[{variant:'primary', body:"Ok"}]});
      } catch(e) {}
    }

    try {
      const tab = chosenTabResolve();
      const g = mainStore.gameForChannel(tab);
      const chan = g.publicDetails.channel.split('#')[1];
      if(g.publicDetails.hasPassword) {
        const formId = 'launch-url-password-input';
        DialogMgr.addPopup({
          title: "Provide Password in Invite Link?",
          body: ()=><><div>Provide pasword for link (can leave empty)</div><Form.Control id={formId} size="lg" type="text" placeholder="Password" /></>,
          buttons: [{
            variant: 'primary',
            body: "Ok",
            onClick: async ()=>{
              const input = document.getElementById(formId) as HTMLInputElement;
              const password = input.value.trim();
              if(password?.length) {
                showPopup_SavedToClipboard(`nukemnet://join/${chan}?p=${password}`);
              } else {
                showPopup_SavedToClipboard(`nukemnet://join/${chan}`);
              }
            }
          }]
        });
      } else {
        showPopup_SavedToClipboard(`nukemnet://join/${chan}`);
      }
    } catch(e) {}
  }

  async function toggleRoomAdvertisement() {
    const g = mainStore.gameForChannel(chosenTabResolve());
    if(!g) return;

    const lchan = g.publicDetails.channel.toLowerCase();
    const isAdvertised = mainStore.channelAdvertisedAsGameRoom(lchan);

    mainStore.channelAdvertiseInProgress[lchan] = true;
    setTimeout(async ()=>{
      const timeout = 10*1000;
      const checkIntervals = 500;
      const iterations = timeout/checkIntervals;
      for(let i=0;i<iterations;i++) {
        const newIsAdvertised = mainStore.channelAdvertisedAsGameRoom(lchan);
        if(newIsAdvertised != isAdvertised) {
          mainStore.channelAdvertiseInProgress[lchan] = false;
          return
        }
        await sleep(checkIntervals);
      }
      mainStore.channelAdvertiseInProgress[lchan] = false;
    });

    try {
      if(!isAdvertised) {
        const pubDetailsWithPassMaybe = deepClone(g.publicDetails);
        const pass = mainStore?.joinedChannels?.[lchan]?.password;
        if(pass?.length) {
          (pubDetailsWithPassMaybe as Room_CreateRequest).password = pass;
        } else {
          delete pubDetailsWithPassMaybe.password;
        }
  
        // delete redundant fields, otherwise bot will ignore message
        const delRef = pubDetailsWithPassMaybe as Partial<RoomAdvertisement>
        delete delRef.owner;
        delete delRef.createdAt;
        delete delRef.hasPassword;
        
        const createRoomRequest = {
          op: 'createRoom',
          data: pubDetailsWithPassMaybe
        } as Cmd_CreateRoom
        try {
          await IrcClientExtra.sendNNCmdToBot(ircClient, createRoomRequest)
        } catch(e) {}
      } else {
        const createRoomRequest = {
          op: 'deleteRoom',
          data: {channel: g.publicDetails.channel}
        } as Cmd_DeleteRoom
        try {
          await IrcClientExtra.sendNNCmdToBot(ircClient, createRoomRequest)
        } catch(e) {}
      }  
    } catch(e) {}
  }

  // Ping users in chosen tab
  (async()=>{
    while(true) {
      try {
        const lchan = chosenTabResolve()?.toLowerCase()
        const g = mainStore?.gameForChannel?.(lchan);
        const myExecRunning = mainStore.myExecIsRunningInChan(lchan);
        const hostExecRunning = mainStore.hostExecIsRunningInChan(lchan);

        if(!g) throw null;
    
        const nickIpAndPorts = await mainStore.getResolvedIpsInGameRoom(lchan, ircClient.nick, true)
        const promises = [];
        let pingToHost:number|null = null
        for(const n in nickIpAndPorts) {
          const chanNickData = mainStore.joinedChannels[lchan].nicks[n];
          if(myExecRunning || hostExecRunning || g.participantNicks[n].isExecRunning) {
            delete chanNickData.ping;
            delete chanNickData.pingToHost;
            continue;
          }

          const ip = nickIpAndPorts[n].ip;
          const p = ping.promise.probe(ip, {timeout: 10, extra: isWindows()?[]:['-i', '1']});
          promises.push(p);
          
          (async()=>{
            const res = await p;
            const msDelay = res.times?.[0];
            if(msDelay != null) {
              chanNickData.ping = msDelay;
              if(n === g.publicDetails.owner.toLowerCase()) {
                pingToHost = msDelay;
              }
            }
          })()
          
        }
        await Promise.all(promises);
        
        if(pingToHost != null) {
          // set my ping to host for myself too
          try {mainStore.joinedChannels[lchan].nicks[ircClient.nick.toLowerCase()].pingToHost = pingToHost;} catch(e) {}
          // broadcase ping to host to channel
          await packAndSendIrcCmd(ircClient, g.publicDetails.channel, {
            data: {
                $case: 'cmdPingToHost',
                cmdPingToHost: {ping: pingToHost}
            }
          } as NNPB.RootCommand);
        }

        await sleep(1);
      } catch(e) {}
      await sleep(6000);
    }
  })();

  const StunButton = <Show when={!offline}><Button onClick={()=>{showStunPanel(chosenTabResolve(), ircClient)}}>STUN</Button></Show>;

  function showIpInfo(nick:string) {
    const ipDetails = mainStore?.joinedChannels?.[chosenTabResolve()]?.game?.participantNicks?.[nick]?.ipsAndPorts;
    DialogMgr.addPopup({
      icon:'info',
      title:`Ip Details`,
      size:'xl',
      closeButton:true,
      body:()=><><pre style="text-align:left;">{JSON.stringify(ipDetails, null, 2)}</pre><div style="color:orange">Local IP details are for LAN/VPN detection</div></>
    })    
  }

  const channelListForLanMode = createMutable({channels:[]} as {channels:string[]});
  if(Settings.read().lanOnlyMode) {
    setTimeout(async ()=>{
      while(true) {
        if(ircClient?.conn && !ircClient.conn?.closed && !ircClient.conn?.destroyed && !ircClient.conn?.connecting) {
          try {
            const chans = await IrcClientExtra.getAllChannels(ircClient, 5000);
            const chanNames = chans.map(c=>c.name).filter(c=>!isMetaChannel(c) && c!=NukemNetLobby_lowercase);
            const foundSet = new Set(channelListForLanMode.channels);
            for(const c of chanNames) {
              if(!foundSet.has(c)) {
                channelListForLanMode.channels.push(c);
              }
              foundSet.delete(c);
            }
            for(const c of Array.from(foundSet)) {
              channelListForLanMode.channels.splice(channelListForLanMode.channels.indexOf(c), 1);
            }
          } catch(e) {}
        }
        await sleep(3000);
      }
    },0);
  }

  setTimeout(async ()=>{
    const gamePort = Settings.getGamePort();
    const allowed = Settings.checkIfPortIsAllowed(gamePort);
    if(!allowed) {
      await DialogMgr.addPopup({
        closeButton:true,
        title:"Game Port not allowed",
        body:`Your game port ${gamePort} is used as a constant in some games, changed back to default ${defaultGamePort}.`
      });
      const s = Settings.read();
      s.gamePort = defaultGamePort;
      Settings.write(s);
      location.reload();
    }
  },0)

  PasteHandler_Init(document, chosenTabResolve, ircClient);

  return (<div style={{height:'100%',display:"flex", "flex-direction": "column"}}>

    <Popups/>

    <TopBar onCommandClick={onTopBarCommandClicked} />

    <Show when={showLaunchGameModal() && Settings.gameDefs?.games != null}>
      <LaunchGameModal preConfig={LaunchGamePreConfig()} show={showLaunchGameModal()} onCancel={handleCloseLaunchGameModal} onSubmit={onGameSubmit} gameDefs={Settings.gameDefs as GameDefsType.Root} />
    </Show>

    <Show when={showLauncherSettingsModal()}>
      <LauncherSettingsModal ircClient={ircClient} show={showLauncherSettingsModal()} dismiss={()=>{setShowLauncherSettingsModal(false)}}></LauncherSettingsModal>
    </Show>
    
    <PortForwardModal show={showPortForwardModal()} dismiss={()=>{setShowPortForwardModal(false)}}></PortForwardModal>

    <Show when={!mainStore.connected}>
      Not connected to irc server (connecting...)
    </Show>

    <div style={{height:'100%',display:"flex", "flex-direction": "row"}}>

    <Show when={mainStore.isChannelGameRoom(chosenTabResolve())}>
        <div style={{height:'100%', 'background-color':"black"}}>
        <div class='side-panel-paramcontainer' style={{"overflow-y":"auto", height:"87vh", "text-align":'center'}}>
          <div>
            <Button onClick={()=>{
                const g = game();
                openLaunchGameModal({
                  game:g.publicDetails.gameId,
                  exec:g.publicDetails.execId,
                  args: g.params,
                  mode: 'multiplayer',
                  maxPlayers: g.publicDetails.maxPlayers,
                  password: mainStore?.joinedChannels?.[chosenTabResolve()]?.password,
                  roomName: g.publicDetails.name,
                  editChannel: chosenTabResolve()
                })
            }}>Edit</Button>
            
            <Show when={!mainStore.imHostInChannel(chosenTabResolve()) && mainStore.supportsMidGame(chosenTabResolve()) && mainStore.hostExecIsRunningInChan(chosenTabResolve()) && !mainStore.myExecIsRunningInChan(chosenTabResolve())}>
              <Button disabled={runGameButtonDisabled()} onClick={()=>{clientLaunchGame(chosenTabResolve(), mainStore.gameForChannel(chosenTabResolve()).publicDetails.owner, ircClient);}}>Join Game</Button>
            </Show>
            
            <Show when={!mainStore.imHostInChannel(chosenTabResolve()) && mainStore.isChanUsingStun(chosenTabResolve())}>
              {StunButton}
            </Show>

            <Show when={!offline && !Settings?.read()?.ipv4Mode && !mainStore.gameChosenUdpTunnel(game())}>
              <Button onClick={()=>{showIpv6Panel(chosenTabResolve(), ircClient)}}>IPv6</Button>
            </Show>

            <Button variant="danger" title={mainStore.imHostInChannel(chosenTabResolve())?"Terminate Process for all players":"Terminate your own game processs"} onClick={()=>{destroyGameProcessesDialog(chosenTabResolve())}}>💣</Button>
            <Show when={mainStore.imHostInChannel(chosenTabResolve())}>
              <Button disabled={newRoomCheckingPort() || runGameButtonDisabled()} variant='success' onClick={()=>{startGameFromRoom(chosenTabResolve())}}>Start🚀</Button>
              <div style={{"background-color":'lightblue'}}>
                {/* If NOT playing TCP game */}
                <Show when={!mainStore.gameChosenTCPAndNotOptional(game()) && !offline}>
                  
                    <span>Port Problems? Try</span><br/>
                    <Show when={!game().dedicatedServer}>
                      <Button disabled={isFetchingDedicatedServer()} onClick={()=>{fetchDedicatedServer()}}>Relay</Button>
                    </Show>
                    {StunButton}
                  

                  <Show when={isFetchingDedicatedServer()}>
                    <Spinner animation="border" variant="primary" />
                  </Show>
                </Show>
              </div>
            </Show>

            <Show when={game().dedicatedServer!=null}>
              <div style={{"background-color": 'lightgrey'}}>
                <span>Using relay server{game().dedicatedServer?.forced?" (Forced)":" (Fallback)"}: {game().dedicatedServer?.ipOrHostname}:{game().dedicatedServer?.port} {game().dedicatedServer?.type}</span><br/>
                <span>Lease ends: {new Date(game().dedicatedServer?.terminationTimestamp || 0).toLocaleString()}</span>
                <Show when={mainStore.imHostInChannel(chosenTabResolve())}>
                  <Button onClick={()=>{mainStore.turnOffDedicatedServer(game(), ircClient)}}>Disable Relay Server</Button>
                </Show>
              </div>
            </Show>

            <Show when={(!mainStore.imHostInChannel(chosenTabResolve()) || mainStore.execDefForChannel(chosenTabResolve())?.isPeerToPeer) && game().dedicatedServer==null}>
              <div style={{"text-align":'center'}}>
                <Alert variant="primary">
                  {isDetectingLan()?<><div>Detecting LAN</div><br/></>:null}
                  {lanMode()}
                </Alert>
              </div>
            </Show>
            
            <Show when={Settings?.gameDefs?.games?.[game()?.publicDetails?.gameId]?.executables?.[game()?.publicDetails?.execId]?.runnables?.settings != null}>
              <Button onClick={()=>{startSettingsForGame(game(), chosenTabResolve())}}>Launch Settings</Button>
            </Show>

            
          <Show when={!Settings.read().lanOnlyMode}>
            <div style={{"background-color":"lightgray"}}>{mainStore.channelAdvertisedAsGameRoom(chosenTabResolve())?'Room Advertised':("Room Unadvertised"+(mainStore.botIsAvailable()?'':' (Bot Offline)'))}</div>
          </Show>
          
            
          <Show when={mainStore.imHostInChannel(chosenTabResolve()) && mainStore.botIsAvailable()}>
            <Show when={!mainStore.channelAdvertiseInProgress[chosenTabResolve()]}>
              <Show when={mainStore.channelAdvertisedAsGameRoom(chosenTabResolve())}>
                <Button onClick={()=>{toggleRoomAdvertisement()}}>Unadvertise</Button>
              </Show>
              <Show when={!mainStore.channelAdvertisedAsGameRoom(chosenTabResolve())}>
                <Button onClick={()=>{toggleRoomAdvertisement()}}>Advertise</Button>
              </Show>
            </Show>
          </Show>

          <Button onClick={()=>{copyLaunchUrl()}}>Invite Link</Button>
          <Show when={mainStore.execDefForChannel(chosenTabResolve())?.tips}>
            <Button variant='info' onClick={()=>{
              const pd = mainStore.gameForChannel(chosenTabResolve()).publicDetails;
              storeActions.showTipsForDef(pd.gameId, pd.execId);
              }}>Tips</Button>
          </Show>
          <Show when={mainStore?.gameForChannel?.(chosenTabResolve())?.participantNicks?.[mainStore?.myNick?.toLowerCase()]?.fileSyncProgress!=null}>
              <Button variant='info' onClick={()=>{IrcClientExtra.customEvents.emit('fileSyncClicked')}}>FileSync</Button>
          </Show>

          </div>          
          <ParamDisplay game={game()} channel={chosenTabResolve()} />
        </div>
        </div>
      </Show>

    <div class="tabs-container" style={{"flex-grow":1, display:"flex", "flex-direction":"column"}}>

      <Show when={mainStore.connected && (mainStore?.joinedChannels[BotMetaChannel_lowercase]!=null) && !mainStore.botIsAvailable()}>
        Bot is unavailable
      </Show>

      <Show when={mainStore.botIsAvailable() && mainStore.showRoomsList}>
          <Show when={!hasAdvertisedGameRooms()!}>
            No advertised game rooms.
          </Show>
          <Show when={hasAdvertisedGameRooms()}>
            <table class="game-rooms-table">
              <thead style={{"background-color":'cadetblue', 'text-decoration': 'underline', 'font-weight':'bold'}}>
                <tr>
                  <td>Host</td>
                  <td>RoomName</td>
                  <td>Game</td>
                  <td>Exec</td>
                  <td>Players</td>
                  <td>Started</td>
                  <td>Password</td>
                </tr>
              </thead>
              <tbody>
                <For each={Object.keys(mainStore.advertisedGameRooms)}>
                  {(chan=>{
                    const details = mainStore.advertisedGameRooms[chan].publicDetails;
                    return <tr style={{cursor:'pointer'}} onMouseDown={e=>{
                      e = e || window.event;
                      const isRightMB = e?.button === 2;
                      if(!isRightMB) return;

                      DialogMgr.addPopup({
                        title:details.channel,
                        body:()=><div>Choose action for {details?.name||''} ({details?.channel})</div>,
                        buttons:[{body:"Join", onClick:()=>joinGameRoomChannel(details?.channel)}, {body:'Users', onClick:()=>getUsersForChannel(details?.channel)}, {body:"Game Details", onClick:()=>showGameDetailsForChannel(details?.channel)}, {body:"Cancel", variant:'secondary'}]
                      })

                    }} onDblClick={()=>{joinGameRoomChannel(details?.channel)}}>
                      <td>{details.owner}</td>
                      <OverlayTrigger placement="bottom" delay={{ show: 100, hide: 750 }}overlay={<Tooltip id="button-tooltip">{details.channel}</Tooltip>}>
                        <td>{details.name||details.channel}</td>
                      </OverlayTrigger>
                      <td>{details.gameName}</td>
                      <td>{details.execName}</td>
                      <td>{details.playersIn}/{details.maxPlayers}</td>
                      <td>{details.isStarted?'yes':'no'}</td>
                      <td>{details.hasPassword?'yes':'no'}</td>
                    </tr>
                  })}
                </For>
              </tbody>
            </table>
          </Show>
      </Show>

      <Show when={Settings.read().lanOnlyMode}>
        <table class="game-rooms-table">    
          <tbody>
            <For each={channelListForLanMode.channels}>
              {(channel)=><tr><td style={{cursor:'pointer'}} onClick={()=>{ircClient.join(channel)}}>{channel}</td></tr>}
            </For>
          </tbody>
        </table>
      </Show>

      <Tabs
        id="controlled-tab-example"
        activeKey={chosenTabResolve()}
        onSelect={(k) => {
          if(!k) return;
          setChosenTab(k);
          scrollMessagesToBottom();
        }}
        class="mb-3"
      >
        <For each={allTabNames()}>
          {(tab,i)=>{
            const ltab = tab.toLowerCase();
            if(ltab === BotMetaChannel.toLowerCase()) return <></>;
            else return <Tab eventKey={tab} title={
              <span style={tabIsDirty(ltab)?{
                'background-color': 'lightblue',
                "border-radius":"3pt",
                'font-weight': 'bold',"color":'orange', 'text-decoration': 'underline'
              }:{}}>{storeActions.isChannelGameRoom(tab)?'🎮':''}{tab}<Show when={!tab.startsWith('@')}> <CloseButton style={{"background-color":"darkgray"}} onClick={()=>{clickedOnCloseTab(tab)}} /></Show></span>
            }></Tab>
          }}
        </For>
      </Tabs>

      <Show when={mainStore?.joinedChannels?.[chosenTabResolve()?.toLowerCase()]?.topic?.length}><div style={{"background-color":'lightgray', "text-align":'center', 'font-weight': 'bold'}}>Topic: {IRCStylizeText(mainStore.joinedChannels[chosenTabResolve().toLowerCase()].topic)}</div></Show>
      <div id="events-container" ref={messagesContainer!} style={{"flex":'1 1 0', "overflow-y":'scroll', "background-color":isDragginFile()?"lightblue":''}}>
        <For each={eventsForCurrentTabName()}>
            {(event,id)=>{
              const ComponentTypeOfEvent = eventTypeToComponentMapping[event.type] as typeof IrcEventCmp_Base;
              if(!ComponentTypeOfEvent) return '';

              return <div style={{'word-break': 'break-all'}}><ComponentTypeOfEvent {...event}/></div>
            }}
        </For>
      </div>
      
        <div style={{display:"flex", "flex-direction":"row"}}>

          <input id="message-input" style={{"flex-grow":1}} type="text" placeholder='Send message' onKeyUp={
            function(e){
              if (e.key === 'Enter' || e.keyCode === 13) {
                sendMessage();
              } else if (e.key == 'ArrowUp' || e.key == 'ArrowDown') {
                const input = document.getElementById('message-input') as HTMLInputElement;
                (()=>{
                  if(historyCommandsBrowseIndex === -1) historyCommandsBrowseIndex = historyCommands.length;
                  
                  const isUp = e.key == 'ArrowUp';
                  const sign = isUp? -1:1;
                  const newIndex = historyCommandsBrowseIndex+sign;
                  const histCommand = historyCommands[newIndex];
                  if(histCommand == undefined) return;

                  historyCommandsBrowseIndex = newIndex;
                  input.value = histCommand;
                })()
                // Go to end of input
                const end = input.value.length;
                input.setSelectionRange(end, end);
              }
            }
          }></input>
          <Button title="Emoji Picker" id="show-emoji-toggle" onClick={()=>showEmojiPicker()}>😎</Button>
          <Button title="Send Message" onClick={()=>sendMessage()}>✉️</Button>
          <Button title="Upload File" onClick={()=>showChooseUploadFileDialog()}>📎</Button>

          <span title="Lock Scroll To Bottom" style={{display:'flex', "flex-direction":'row'}}>
            <label for="scroll-to-bottom">⬇</label>
            <input onChange={(el:any)=>{setStickScrollToBottom(el.target.checked); if(el.target.checked) scrollMessagesToBottom();}} checked={stickScrollToBottom()} style={{height:'100%'}} id="scroll-to-bottom" type="checkbox"/>
          </span>
          
        </div>
      
      </div>

      <Show when={Object.keys(mainStore?.joinedChannels?.[chosenTabResolve()]?.nicks||{}).length}>
      <div style={{height:'100%', 'background-color':"lightgrey"}}>
        <div style={{"overflow-y":"auto", "overflow-x": 'hidden', "height":'87vh', 'max-width':"fit-content", "min-width":"fit-content"}}>
          <h4 style={{"text-align":'center'}}>Users</h4>
          <ButtonGroup vertical>
            <For each={usersInChosenTab()}>
              {(nick)=>
                  <DropdownButton class="user user-button" style={{"margin-bottom":'2pt'}} as={ButtonGroup} title={nickDisplay(nick)}>
                    <Dropdown.Item  onClick={()=>{openPrivateMessageForNick(nick)}} eventKey="1">Private</Dropdown.Item>
                    <Dropdown.Item  onClick={()=>{kickNick(nick)}} eventKey="2">Kick</Dropdown.Item>
                    <Dropdown.Item  onClick={()=>{doWhoIs(nick)}} eventKey="3">WhoIs</Dropdown.Item>

                    {/* <Show when={mainStore?.joinedChannels?.[chosenTabResolve()]?.game?.participantNicks?.[nick] != null}>
                      <Dropdown.Item onClick={()=>{showIpInfo(nick)}} eventKey="4">Ip Details</Dropdown.Item>
                    </Show> */}

                    <Dropdown.Item onClick={async ()=>{
                      const channels = [] as string[];
                      for(const lchan in mainStore.joinedChannels) {
                        if(isMetaChannel(lchan)) continue;
                        channels.push(lchan);
                      }

                      const selectId = `dlg-invite-channel-select`;
                      const dlgPromise = DialogMgr.addPopup({
                        icon:'question',
                        title: 'Invite To Channel',
                        body: ()=><><h4>Invite {nick}</h4><br/>
                          <Form.Select id={selectId}>
                            <For each={channels}>
                              {(chan)=><option value={chan}>{chan}</option>}
                            </For>
                          </Form.Select>
                        </>,
                        buttons:[{body:"Invite", variant:"primary"},{body:"Cancel", variant:"secondary"}]
                      });
                      const selectElement = (document.getElementById(selectId) as HTMLSelectElement);
                      const res = await dlgPromise;
                      if(res.buttonIndex===0) {
                        const chan = selectElement.value;
                        if(chan?.length) {
                          ircClient.send('INVITE', nick, chan);
                        }
                      }
                    }} eventKey="5">Invite</Dropdown.Item>
                  </DropdownButton>
                }
            </For>
          </ButtonGroup>
        </div>
      </div>
      </Show>
      
    </div>
</div>)
};

export default App;
