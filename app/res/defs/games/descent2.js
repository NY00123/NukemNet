
module.exports = function({}) {

    const dosboxPreCommands = ['CONFIG -set "cycles=auto 5000 limit 50000"']; /* Descent runs well on these cycles */
    const runToolOpts = {"type":"dosbox", "data":{"commands":dosboxPreCommands, "args":["-machine", "vesa_nolfb"]}};
    const runToolOptsMultiplayer = {"type":"dosbox", "data":{"commands":dosboxPreCommands,"net":"ipx","args":["-machine", "vesa_nolfb"]}};

    const filesSw = {
        "setup": {"path":"SETUP.EXE"},
        "main": {"path":"D2DEMO.EXE"}
    };

    const filesFull = {
        "setup": {"path":"SETUP.EXE"},
        "main": {"path":"DESCENT2.EXE"}
    };

    const runnables = {
        "multiplayer": {
            "file":"main",
            "runTool":runToolOptsMultiplayer
        },
        "settings": {"file":"setup", "runTool":runToolOpts},
        "singleplayer": {"file":"main", "runTool":runToolOpts}
    };

    const dxxRebirthParams = {
        "host_conn": {
            "modeSupport": ["multiplayer"],
            "type": "static",
            "value": ["'-udp_myport'", "GameRoom.MyPort"],
            "for":"host-only-private"
        },
        "client_conn": {
            "modeSupport": ["multiplayer"],
            "type": "static",
            "value": ["'-udp_myport'", "GameRoom.MyPort", "'-udp_hostaddr'", "GameRoom.DestIp", "'-udp_hostport'", "GameRoom.DestPort"],
            "for":"client-only-private"
        }
    }

    return {
        "name": "Descent 2",
        "get": [{
            type:"gog",
            prodId:"1207663093",
            nameId:"descent_2",
            name:"Descent 2"
        }],
        "executables": {
            "demo": {
                "name":"Demo (DOS)",
                "files": filesSw,
                "runnables": runnables,
                "parameters": {}
            },
            "full": {
                "mountImg": true,
                "name":"Full (DOS)",
                "files": filesFull,
                "runnables": runnables,
                "parameters": {}
            },
            "dxxrebirth": {
                midGameJoin: true,
                "name":"DXX-Rebirth",
                "files": {
                    "main": {"path":"d2x-rebirth.exe"}
                },
                "runnables": {
                    "multiplayer": {"file":"main"},
                    "settings": {"file":"main"},
                    "singleplayer": {"file":"main"}
                },
                "parameters": dxxRebirthParams
            },
            "dxxretro": {
                midGameJoin: true,
                "name":"DXX-Retro",
                "files": {
                    "main": {"glob":["d2x-rebirth-retro*","d2x-retro-*"]}
                },
                "runnables": {
                    "multiplayer": {"file":"main"},
                    "settings": {"file":"main"},
                    "singleplayer": {"file":"main"}
                },
                "parameters": dxxRebirthParams
            },
            "dxxretroar": {
                midGameJoin: true,
                "name":"DXX-Retro-AR",
                "files": {
                    "main": {"glob":["d2x-rebirth*","d2x-retro-*"]}
                },
                "runnables": {
                    "multiplayer": {"file":"main"},
                    "settings": {"file":"main"},
                    "singleplayer": {"file":"main"}
                },
                "parameters": dxxRebirthParams
            }
        }
    };
}