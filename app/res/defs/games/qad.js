module.exports = function() {

    const commands = ['CONFIG -set "cycles=max"','CONFIG -set "core=auto"']

    const runToolOpts = {"type":"dosbox", "data":{commands}};
    const runToolOptsMultiplayer = {"type":"dosbox", "data":{commands, "net":["ipx","serial"]}};

    const runnables = {
        "multiplayer": {
            "file":"main",
            "runTool":runToolOptsMultiplayer
        },
        "settings": {"file":"snd", "runTool": runToolOpts},
        "singleplayer": {"file":"main", "runTool": runToolOpts}
    };

    // TODO QAD.CFG

    return {
        "name": "Q.A.D: Quintessential Art of Destruction",
        "executables": {
            "full": {
                "maxPlayers":4,
                "name":"Full (DOS)",
                "mountImg": true,
                "files": {
                    "snd": {"path":"SETSOUND.EXE"},
                    "main": {"path":"QAD.EXE"}
                },
                "runnables": runnables,
                "parameters": {}
            }
        }
    };
}