


import { Component, createSignal, Show } from 'solid-js';
import DownloadLinks from './DownloadLinks';
import SupportedGames from './SupportedGames';

const Home: Component = () => {

  
  const [latestVersion, SET_latestVersion] = createSignal(null as null|{version:string,timestamp:number});
  (async ()=>{
    const res = await fetch('./latest-version.json?uncache='+Date.now());
    const body = await res.json()
    SET_latestVersion(body);
  })();


  return <div>
  <div class="welcome-title logo-container">
      <img src={'./res/nukemnet.png'} style={{width:'100%', "max-width":'500pt'}}/>
      <div class="centered" style={{width:'100%'}}>
        <div>Welcome to NukemNet!</div>
        <div style={{"margin-top":'-5pt'}}>A multiplayer launcher for retro games</div>
      </div>
  </div>

  <Show when={latestVersion()!=null}>
    <div class="latest-version">Latest Version: {latestVersion()?.version} <span style={{color:"orange"}}>Alpha</span><br/>Date: {new Date(latestVersion()!.timestamp).toString()}</div>
    <DownloadLinks version={latestVersion()!.version} timestamp={latestVersion()!.timestamp}/>
  </Show>
  
  <SupportedGames />
  
</div>
};

export default Home;
