
import * as SimpleYenc from 'simple-yenc';
import { Client } from 'matrix-org-irc';
import { deflatePromise, inflatePromise } from '../compression';
import { NNPB } from './export';
import { nncmdSentEmitter } from '../ircExtend';

function isSet(value:number, bitPos:number) {
    var result = Math.floor(value / Math.pow(2, bitPos)) % 2;
    return result == 1;
}

function bufferYencEncode(bytes:Uint8Array) {
    const encodedStr = SimpleYenc.encode(bytes) as string
    const arr = [] as number[];
    for(let i=0;i<encodedStr.length;i++) {
        const byte = encodedStr.charCodeAt(i);
        arr.push(byte);
    }
    return new Uint8Array(arr);
}

function bufferYencDecode(bytes:Uint8Array) {
    let strArr = '';
    for(let i=0;i<bytes.length;i++) {
        const byte = bytes[i];
        strArr += String.fromCharCode(byte);
    }

    const decoded = SimpleYenc.decode(strArr) as Uint8Array
    return decoded
}

function toBytesInt32(num:number) {
    // https://stackoverflow.com/a/24947000/230637
    // ALON: I changed it to big endian
    const arr = new ArrayBuffer(4); // an Int32 takes 4 bytes
    const view = new DataView(arr);
    view.setUint32(0, num, true); // byteOffset = 0; litteEndian = true
    return arr;

    // Example Usage:
    // --------------
    // const msgLength = 4294444295;
    // const bufMessageLength = toBytesInt32(msgLength);
    // const originalNumber = Buffer.from(bufMessageLength).readUint32LE();
}

export function encodeForIrc(data:any, protocolBuffersType:any) {
    const b = protocolBuffersType.encode(data)
    const buffer = b.finish();
    return buffer;
}

export async function packAndSendIrcCmd(ircClient: Client, target:string, data:NNPB.RootCommand) {
    const e = encodeForIrc(data, NNPB.RootCommand);
    const pack = await prepareNukemNetPackage(Buffer.from(e))
    IRC_sendBufferToTarget(ircClient, target, pack);

    setTimeout(()=>{
        nncmdSentEmitter.emit('*', data.data.$case, 'protobuf', target, data);
        nncmdSentEmitter.emit(data.data.$case, data.data.$case, 'protobuf', target, data);
    });
}

export function decodeForIrc<T>(encoded: Uint8Array, protocolBuffersType:any) {    
    return (protocolBuffersType as any).decode(encoded) as T;
}

export function IRC_ParseBufferFromPRIVMSG(buffer:Buffer) {
    try {
        const trimmedBuffer = [] as number[];
        let countSpace = 0;
        for(let i=0;i<buffer.byteLength;i++) {
        const byte = buffer[i];
        if(countSpace<3) {
            if(byte===32) {
            countSpace++;
            }
        } else {
            trimmedBuffer.push(byte);
        }
        }      
        if(trimmedBuffer[0]===58) trimmedBuffer.splice(0,1);
        if(trimmedBuffer[trimmedBuffer.length-1]===10) trimmedBuffer.splice(trimmedBuffer.length-1,1)
        if(trimmedBuffer[trimmedBuffer.length-1]===13) trimmedBuffer.splice(trimmedBuffer.length-1,1)
    
        const byteArray = Buffer.from(trimmedBuffer);
        return byteArray;
    } catch(e) {
        return Buffer.from([]);
    }
}


export function myNUH(ircClient:Client) {
    const mask = (ircClient as any).hostMask as string;
    if(!mask?.length) throw "Could not find my NUH";
    
    const NUH = `:${ircClient.nick}!${(ircClient as any).hostMask}`;
    return NUH;
}

const maxBytesPerLine = 500 // (actual value is 512, but we take spare space, so no byte is cut-off) Sending more bytes than that will lose a few characters or discard the entire message completely
export function IRC_Build_PRIVMSG_Messages(ircClient:Client, target:string, buffer:Uint8Array) {
    // WARNING, this function builds the buffer as is, it does not escape NUL, CR, LF.
    
    const NUH = myNUH(ircClient);
    const NuhLength = Buffer.from(NUH).byteLength+1; // +1 because of space

    const prefix = new Uint8Array(Buffer.from(`PRIVMSG ${target} :`,'utf8'));
    const commandSizeWithoutMessage = NuhLength + prefix.length + 2 //+2 because \r\n.
    
    function completeMessage(numArr:number[]) {
        return new Uint8Array([...prefix, ...new Uint8Array(numArr), 13, 10]);
    }

    const maxMessageContentSize = maxBytesPerLine-commandSizeWithoutMessage;

    const messages = [] as Uint8Array[];
    let accumulated:number[] = [];
    for(let i=0;i<buffer.length;i++) {
        if(i%maxMessageContentSize === 0) {
            if(i>0) messages.push(completeMessage(accumulated));
            accumulated = [];
        }
        const byte = buffer[i];
        accumulated.push(byte);
    }
    messages.push(completeMessage(accumulated));
    
    return messages;
}

/**
 * 
 * @param num plain byte value between 0 and 252
 * @returns irc safe byte all between 0-255 except 0,10,13
 */
 function byteToIrcCompatibleByte(num:number) {
    if(num<0 || num>252) throw 'Incompatible Plain Byte:' + num; // 252 is the highest num, we lose 3 decimal numbers because of 3 forbidden IRC nums, CR LF NUL

    if(num+1<10) return num+1;
    if(num+2<13) return num+2;
    return num+3;
}

/**
 * 
 * @param num irc safe byte all between 0-255 except 0,10,13
 * @returns plain byte value between 0 and 252
 */
function ircCompatibleByteToPlainByte(num:number) {
    if(num<=0 || num===10 || num===13 || num>255) throw 'Incompatible Irc Byte: '+num

    if(num<10) return num-1;
    if(num<13) return num-2;
    return num-3;
}

const L_StartPackage = 5 // ENQ

export function isNukemNetPackageBuffer(buf:Buffer) {
    return buf[0]===L_StartPackage 
}

export async function prepareNukemNetPackage(buf:Buffer) {
    const compressedBuffer = await deflatePromise(buf, {level:9});

    const shouldCompress = compressedBuffer.length<buf.length;

    let flagBits = 0b00000000;
    if(shouldCompress) {
        flagBits += 1 // First bit (LSB) flag indicates whether compression is ON for this package.
    }

    const encodedContentsBuffer = bufferYencEncode(shouldCompress? compressedBuffer:buf);
    const encodedContentsBufferLen = toBytesInt32(encodedContentsBuffer.length);

    // Headers contains 1 byte of 8 flags (only first one in use for now, 7 reserved), the 4 bytes which is a 32bit unsigned int that specifies the message content length.
    const encodedHeader = bufferYencEncode(new Uint8Array([flagBits, ...new Uint8Array(encodedContentsBufferLen)]));
    const encodedHeaderLen = encodedHeader.length;
    const encodedHeaderLen_SafeIrcNum = byteToIrcCompatibleByte(encodedHeaderLen);

    const packageBuf = new Uint8Array([
        L_StartPackage, // 5 (ENQ) mark start of NukemNet package
        encodedHeaderLen_SafeIrcNum, // 1 byte: specifies the length of the yEnc-encoded-header.
        ...encodedHeader, // encoded header: specifieds first byte (8 flags) and then 4 bytes for 32bit unsigned num, size of the encoded contents
        ...encodedContentsBuffer
    ]); 

    return Buffer.from(packageBuf);
}

export async function unpackNukemNetPackage(buffer:Buffer): Promise<{isCompressed:boolean, fullLength:number, content:Buffer|null}> {
    if(buffer[0]!==L_StartPackage) throw `Wrong package first byte (expected ${L_StartPackage})`;
    const headerLength = ircCompatibleByteToPlainByte(buffer[1]);

    const index_startOfHeader = 2;
    const index_endOfHeader = index_startOfHeader+headerLength;

    const headerEncoded = buffer.subarray(2,index_endOfHeader);
    const header = bufferYencDecode(headerEncoded);

    const flagsByte = header[0];
    const isCompressed = isSet(flagsByte, 0); // first bit indicates whether the contents are compressed
    const contentsLength = Buffer.from(header.subarray(1,5)).readUint32LE(); // 4 bytes (32bit uint, length of contents)

    const index_startOfContents = index_endOfHeader;

    const entireBufferLength = index_startOfContents+contentsLength;

    if(buffer.length>=entireBufferLength) {
        const encodedContents = buffer.subarray(index_startOfContents, entireBufferLength);
        const decodedContentsPossiblyCompressed = bufferYencDecode(encodedContents);
    
        const pureBuffer = isCompressed? (await inflatePromise(decodedContentsPossiblyCompressed)) : Buffer.from(decodedContentsPossiblyCompressed);
        return {
            content: pureBuffer,
            isCompressed,
            fullLength: entireBufferLength
        }
    } else {
        return {
            content: null,
            isCompressed,
            fullLength: entireBufferLength
        }
    }
}

export function IRC_sendBufferToTarget(ircClient:Client, target:string, buf:Buffer) {
    // WARNING, this function sends the buffers as is, it does not escape NUL, CR, LF.
    const buffers = IRC_Build_PRIVMSG_Messages(ircClient, target, buf);
    for(let i=0;i<buffers.length;i++) {
      const buf = buffers[i];
      ircClient.conn!.write(buf);
    }
}

// Example
// -------
// const e = encodeForIrc({author:"אלון",isbn:1313131,title:"אלון"}, Book);
// const d = decodeForIrc<Book>(e, Book);

// Sending
// -------
// const pack = await prepareNukemNetPackage(Buffer.from('hello אני אלון :}'))
// IRC_sendBufferToTarget(ircClient, '#lobby', pack);

// Receiving
// ---------
// const buf = IRC_ParseBufferFromPRIVMSG(message.buffer);
// const pack = await unpackNukemNetPackage(buf);

// IRC Batching (might need later)
// -------------------------------
// ircClient.send('BATCH', '+123', 'draft/multiline', '#lobby')
// ircClient.send(`@batch=123`, `PRIVMSG`, `#Lobby`, `hello`)
// ircClient.send(`@batch=123`, `privmsg`, `#Lobby`, `how is `)
// ircClient.send('BATCH', '-123')