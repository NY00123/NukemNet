const { app, BrowserWindow, shell, Tray, Menu, dialog, ipcMain, nativeImage } = require('electron')
const path = require('path')
const fs = require('fs');
const remote = require('./electron_idx_deps').cachedRequire('@electron/remote/main');

// on Windows XP, must disable some security checks for https requests to work (untested)
// https://stackoverflow.com/questions/44658269/electron-how-to-allow-insecure-https
// app.commandLine.appendSwitch('ignore-certificate-errors')
// app.commandLine.appendSwitch('allow-insecure-localhost', 'true');
// app.on('certificate-error', (event, webContents, url, error, certificate, callback) => {
//    // Prevent having error
//   event.preventDefault()
//   // and continue
//   callback(true)
// })

global.paths = {}

remote.initialize()

function convertToPosixPath(path) {
  return path.replace(/\\/g, '/');
}

const appPath = app.getAppPath();
const iconPath = path.resolve(appPath, 'res', 'favicon.png');

const isDev = fs.existsSync(path.resolve(appPath, 'src', 'index.ts'));
global.isDev = isDev;

const createWindow = () => {
  const win = new BrowserWindow({
    icon:iconPath,
    minWidth: 920,
    minHeight: 500,
    autoHideMenuBar: true,
    webPreferences: {
      preload: path.resolve(appPath, 'electron_preload.js'),
      contextIsolation:false,
      sandbox: false,
      webSecurity: false // Must be disabled for file:// resources to work
    }
  })
  remote.enable(win.webContents)
  win.webContents.on('new-window', function(event, url){
    event.preventDefault();
    shell.openExternal(url);
  });

  if(isDev) {
    win.loadURL('http://localhost:3001') // Used during development
  } else {
    win.loadFile(path.resolve(__dirname, 'dist', 'index.html'), {}) // Used in release
  }

  return win;
}

app.disableHardwareAcceleration();

function checkWriteAccess(chkWriteAccessPath) {
  const chkDir = path.resolve(chkWriteAccessPath, '__chkWriteAccessDir__');
  try {
    try {fs.mkdirSync(chkDir)} catch(e) {}
    fs.rmdirSync(chkDir);
    return true;
  } catch(e) {
    try {fs.rmdirSync(chkDir);} catch(e) {}
    return false;
  }
}

const execPath = convertToPosixPath(path.dirname(process.execPath));
const {runDir, programDir} = (()=>{
  let runDir;
  if(isDev) {
    runDir = appPath;
  } else {
    if(execPath.includes('node_modules/electron/dist')) {
      const baseDir = path.resolve(execPath, '..', '..', '..');
      runDir = baseDir;
    } else {
      runDir = execPath;
    }
  }

  if(checkWriteAccess(runDir)) return {runDir, programDir:runDir};
  
  // fallback to use safe accessible path for user files.
  const appdataDir = process.env.APPDATA || (process.platform == 'darwin' ? process.env.HOME + '/Library/Preferences' : process.env.HOME + "/.local/share")
  const safeRunDir = path.resolve(appdataDir, 'NukemNet');
  if(!fs.existsSync(safeRunDir)) fs.mkdirSync(safeRunDir);
  return {runDir:safeRunDir, programDir:runDir};
})();

global.paths.programDir = programDir;

process.chdir(runDir);

if(process.defaultApp) {
  if(process.argv.length >= 2) {
    app.setAsDefaultProtocolClient('nukemnet', process.execPath, [path.resolve(process.argv[1])]);
  }
} else {
  app.setAsDefaultProtocolClient('nukemnet');
}

app.on('window-all-closed', function () {
  app.quit()
})

let mainWindow;

function openedFromUrlScheme(url) {
  mainWindow?.webContents?.send('open-scheme-url', {url});
}

const forceSingleInstance = !(()=>{
  try {
    return fs.existsSync(path.join(process.cwd(), 'user', 'allowMultipleInstances'));
  } catch(e) {}
  return false;
})();

const allowDebugging = (()=>{
  try {
    return fs.existsSync(path.join(process.cwd(), 'user', 'debug'));
  } catch(e) {}
  return false;
})();
if(!allowDebugging) {
  Menu.setApplicationMenu(null)
}

const gotTheLock = app.requestSingleInstanceLock()
if (!gotTheLock && forceSingleInstance) {
  app.quit()
} else {
  if(forceSingleInstance) {
    app.on('second-instance', (event, commandLine, workingDirectory) => {
      // Someone tried to run a second instance, we should focus our window.
      if (mainWindow) {
        if (mainWindow.isMinimized()) mainWindow.restore()
        mainWindow.focus()
      }
      const urlScheme = commandLine[commandLine.length-1];
      if(urlScheme?.toLowerCase?.().startsWith?.('nukemnet://')) {
        openedFromUrlScheme(urlScheme);
      }
    });
  }

  // macOS dock menu
  const dockMenu = Menu.buildFromTemplate([
    {
      label: 'Show',
      click () { mainWindow.show() }
    }
  ])
  
  app.commandLine.appendSwitch('lang', 'en-US');
  // Create mainWindow, load the rest of the app, etc...
  app.whenReady().then(() => {

    if (process.platform === 'darwin') {
      app.dock.setMenu(dockMenu)
    }

    const tray = (()=>{
      if(process.platform === 'darwin') {
        const smallerImage = nativeImage.createFromPath(iconPath).resize({width:18,height:18})
        return new Tray(smallerImage);
      } else {
        return new Tray(iconPath);
      }
    })();
    const contextMenu = Menu.buildFromTemplate([
      {
        label: 'Show',
        click() {mainWindow.show();}
      },
      {
        label: 'Hide to tray',
        click() {mainWindow.hide();}
      },
      {
        label: 'Exit',
        click() {app.quit();}
      },
    ]);
    tray.setToolTip('NukemNet');
    tray.setContextMenu(contextMenu);

    mainWindow = createWindow();
    mainWindow.on('close', function(e) {
      const choice = dialog.showMessageBoxSync(this, {
        type: 'question',
        buttons: ['Yes', 'No'],
        title: 'Confirm',
        message: 'Are you sure you want to quit?'
      });
      if (choice === 1) {e.preventDefault();}
    });


    // keep track and kill leftover tunneler (UDPTunnel with KCP) instance.
    const tunnelerPidsSet = new Set();
    ipcMain.on('subprocess-pids', (event, obj) => {
      if(obj.action == 'add') {
        tunnelerPidsSet.add(obj.value);
      } else if(obj.action == 'kill') {
        if(tunnelerPidsSet.has(obj.value)) {
          try {process.kill(obj.value)} catch(e) {}
          tunnelerPidsSet.delete(obj.value);
        }
      } else if(obj.action == 'killall') {
        const pids = Array.from(tunnelerPidsSet);
        for(const pid of pids) {
          try {process.kill(pid)} catch(e) {}
        }
        tunnelerPidsSet.clear();
      }
    })

    tray.on('click', ()=>{
      mainWindow.show();
    });

    const urlScheme = process.argv[process.argv.length-1];
    if(urlScheme?.toLowerCase?.().startsWith?.('nukemnet://')) {
      openedFromUrlScheme(urlScheme);
    }
  })
  
  // macOS: click on dock to reveal window, if hidden.
  app.on('activate', () => {
    mainWindow.show();
  })
  
  // macOS & linux deep linking
  app.on('open-url', (event, url) => {
    openedFromUrlScheme(url);
  });
}