module.exports = function() {
    const commands = ['CONFIG -set "cycles=max"','CONFIG -set "core=auto"'];
    const runToolOpts = {"type":"dosbox", "data":{commands}};
    const runToolOptsMultiplayer = {"type":"dosbox", "data":{commands, "net":["ipx","serial"]}};

    const runnables = {
        "multiplayer": {
            "file":"main",
            "runTool":runToolOptsMultiplayer
        },
        "settings": {"file":"setupsound", "runTool": runToolOpts},
        "singleplayer": {"file":"main", "runTool": runToolOpts}
    };

    const params = {
        "cd": {
            "modeSupport": ["singleplayer","multiplayer"],
            "type": "static",
            "value": ["'/CD'"],
            "for":"private"
        }
    };

    return {
        "name": "Z",
        "get": [{
            type:"gog",
            prodId:"1207664893",
            nameId:"z",
            name:"Z: The Game"
        },{
            type:"zoom-platform",
            nameId:"z-the-game",
            name:"Z: The Game"
        }],
        "executables": {
            "dos": {
                "name":"(DOS)",
                "files": {
                    "setupsound": {"path":"SETSOUND.EXE"},
                    "main": {"path":"ZED.EXE"}
                },
                "runnables": runnables,
                "parameters": params
            }
        }
    };
}