module.exports = function({utils}) {

    const fs = require('fs/promises');
    const path = require('path');

    const commands = ['CONFIG -set "cycles=max"','CONFIG -set "core=auto"',
        'DRVLOAD.COM NAMEDEV.SYS', 'DRVLOAD.COM SERIAL.SYS'
    ];

    const loadDrv = [{"cmd":"function", async run(ctx) {
        for(const fileName of ['DRVLOAD.COM','NAMEDEV.SYS','SERIAL.SYS']) {
            const destination = path.resolve(ctx.GameDir, fileName);
            try {
                if(!utils.findPathCaseInsensitiveSync(destination)) {
                    await fs.copyFile(path.resolve(__dirname, fileName), destination, fs.constants.COPYFILE_EXCL);
                }
            } catch(e) {}
        }
    }}];

    const runToolOpts = {"type":"dosbox", "data":{commands}};
    const runToolOptsMultiplayer = {"type":"dosbox", "data":{commands, "net":"serial"}};

    const runnables = {
        "multiplayer": {
            "file":"main",
            "runTool":runToolOptsMultiplayer,
            "beforeRun":loadDrv
        },
        "singleplayer": {"file":"main", "runTool": runToolOpts}
    };

    const tips = {html: `
    For multiplayer, in game, select option "A - 2 players on asynchronous line"<br/>
    Download links: https://fragglet.github.io/sdl-sopwith/original.html
    `}


    const sdlTips = {html: `Get it here: https://fragglet.github.io/sdl-sopwith`}

    return {
        "name": "Sopwith",
        "executables": {
            "author": {
                maxPlayers:2,
                "name":"Author's Edition (DOS)",
                tips,
                "files": {
                    "main": {"path":"swauth.exe"}
                },
                "runnables": runnables,
                "parameters": {},
            },
            "network": {
                maxPlayers:2,
                "name":"Network Edition (DOS)",
                tips,
                "files": {
                    "main": {"path":"swnet.exe"}
                },
                "runnables": runnables,
                "parameters": {}
            },
            "sdl": {
                maxPlayers:2,
                "name":"SDL",
                tips: sdlTips,
                "files": {
                    "main": {"path":"sopwith.exe"}
                },
                "runnables": {
                    "multiplayer": {"file":"main"},
                    "singleplayer": {"file":"main"}
                },
                networking: {
                    tcpPort: {port:3847},
                },
                "parameters": {
                    "server": {
                        "modeSupport": ["multiplayer"],
                        "type": "static",
                        "value": ["'-l'","'-p'", "GameRoom.MyPort"],
                        "for":"host-only-private",
                    },
                    "client": {
                        "modeSupport": ["multiplayer"],
                        "type": "static",
                        "value": ["'-j'", "GameRoom.DestIp+':'+GameRoom.DestPort"],
                        "for":"client-only-private"
                    },
                    "smode": {
                        "modeSupport": ["singleplayer"],
                        "type": "choice",
                        "label": "Mode",
                        "value":"value",
                        "choices": [{
                            "label": "Novice",
                            "value": "-n"
                        },{
                            "label": "Expert",
                            "value": "-s"
                        },{
                            "label": "Versus Computer",
                            "value": "-c"
                        }]
                    },
                    "dif": {
                        "modeSupport": ["multiplayer", "singleplayer"],
                        "type": "numrange",
                        "min":0,
                        "max":2,
                        "delta":1,
                        "label": "Difficulty",
                        "value": "'-g'+value"
                    }
                }
            }
        }
    };
}