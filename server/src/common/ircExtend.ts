// NOTICE!
// This file has common code with app and server, so careful when importing & calling stuff from non-require() syntax

// import { EventEmitter } from "stream";
import * as NNCmd from "./commands";
import { compressStr, decompressStr } from "./compression";
import { BotName, IrcModSignCmd, IrcModSignOrNone, IrcModSigns, IrcUserMod_CmdToSign, IrcUserMod_SignToCmd } from "./constants";
import { getPrimitiveType } from "./utils";
const JSON5 = require('json5');
import { createRandomHash, createHhash } from "./hash";
import { Client, Message } from "matrix-org-irc";
import { IRC_ParseBufferFromPRIVMSG, isNukemNetPackageBuffer, unpackNukemNetPackage, decodeForIrc } from "./protobuf/ProtoBufMgr";
import { NNPB } from "./protobuf/export";
import { removeNonAlphaNumChars } from "./StringUtils";
import * as net from 'net'
import EventEmitter from "events";

const events = require('events')
export const nncmdSentEmitter = new events.EventEmitter();

export const getFinishedPayload = {
    uncompressedWithoutSliceSign(existingMsgStrPart:string) {
        const fittingFormat = existingMsgStrPart.startsWith(`${NNCmd.attrs.prefix}{`) && existingMsgStrPart.endsWith('}');
        if(fittingFormat) {
            const lastIndexOfJsonEnd = existingMsgStrPart.lastIndexOf('}');
            const firstIndexOfJsonStart = existingMsgStrPart.indexOf('{');
            const innerPayload = existingMsgStrPart.substring(firstIndexOfJsonStart, lastIndexOfJsonEnd+1);
            return innerPayload;
        }
        return null
    },

    withSliceSign(existingMsgStrPart:string) {
        try {
            const initialPrefix = `${NNCmd.attrs.prefix}.`;
            const indexOfEndHashSignSlice = existingMsgStrPart.indexOf('.',initialPrefix.length);
            const sliceSign = existingMsgStrPart.substring(initialPrefix.length, indexOfEndHashSignSlice);
            
            const fullPrefix = `${NNCmd.attrs.prefix}.${sliceSign}.`;
            if(existingMsgStrPart.startsWith(fullPrefix) && existingMsgStrPart.endsWith(sliceSign)) {
                const payload = existingMsgStrPart.substring(fullPrefix.length, existingMsgStrPart.length-sliceSign.length);
                return payload;
            }
        } catch(x) {}
        return null;
    },

    tryAll(existingMsgStrPart:string) {
        let payload;

        payload = getFinishedPayload.uncompressedWithoutSliceSign(existingMsgStrPart);
        if(payload != null) return payload;

        payload = getFinishedPayload.withSliceSign(existingMsgStrPart);
        if(payload != null) return payload;

        return null
    }
}

export namespace IrcClientExtra {

    export const customEvents = new events.EventEmitter() as EventEmitter;
    
    export async function invite(client: any, nick:string, channel:string) {
        return await client.send(`INVITE`, nick, channel);
    }

    // TODO add history request
    // TODO maybe implement search in history by externally keeping track of all messages, maybe reading from unrealircd history database


    export async function modeUserOnChannel(client: any, channel:string, flags:string, nick:string) {
        return await client.send(`MODE`, channel, flags, nick);
    }

    export async function getAllChannels(client: Client, timeout:number) {
        const chanList = [] as {name:string, users:string, topic?:string}[];
        function onNewItem(item: {name:string, users:string, topic?:string}) {
            chanList.push(item);
        }
        client.on('channellist_item', onNewItem)
        try {
            await IrcClientExtra.execAndWaitEvent(client, 'channellist',()=>{
                client.list();
            },timeout);
            client.off('channellist_item', onNewItem)
        } catch(e) {
            client.off('channellist_item', onNewItem)
            throw e;
        }
        
        return chanList;
    }

    export function isConnected(client:any) {
        return client.conn && client.conn?.connecting===false && client.conn?.destroyed === false;
    }

    
    export function waitForAllReplyCommands(ircClient:Client, commands:string[], endOnCommand?:string, isPartialOk?:boolean, timeout?:number, isRelevant?:(msg:Message)=>boolean): Promise<{[key:string]:Message}> {
        return new Promise((resolve, reject)=>{
            const commandToResponse = {} as {[key:string]:Message};
            function finalize(success:boolean) {
                clearTimeout(timerId);
                ircClient.removeListener('raw', rawListener);
                if(success) resolve(commandToResponse);
                else reject(new Error('Timeout'));
            }
            function rawListener(message:Message) {
                if (isRelevant != null && !isRelevant(message)) return;
                if(commands.includes(message!.command!)) {
                    commandToResponse[message.command!] = message;
                }
                if(Object.keys(commandToResponse).length === commands.length) {finalize(true)}
                if(endOnCommand!=null && endOnCommand == message.command) {finalize(true)}
            }
            const timerId = setTimeout(()=>{
                finalize(isPartialOk===true);
            }, timeout||5000);
            ircClient.on('raw', rawListener);
        });
    }

    export async function waitForAllReplyCommandsAndMergeArgs(ircClient:Client, commands:string[], endOnCommand?:string, isPartialOk?:boolean, timeout?:number, isRelevant?:(msg:Message)=>boolean):Promise<{[key:string]:string}>  {
        const response = await IrcClientExtra.waitForAllReplyCommands(ircClient, commands, endOnCommand, isPartialOk, timeout);
        const mergedObj = Object.keys(response).reduce((acc, key)=>{
            acc[key] = response[key].args.slice(1).join(' ');
            return acc;
        }, {} as any);
        return mergedObj
    }

    export async function whois(ircClient:Client, nick:string) {
        ircClient.whois(nick);
        return await IrcClientExtra.waitForAllReplyCommandsAndMergeArgs(ircClient, ['rpl_whoisuser', "rpl_whoischannels", "rpl_whoisactually", "379", "671", "rpl_whoisidle"], 'rpl_endofwhois', true, 2000, (msg:Message)=>{
            return msg.args[1] === nick;
        });
    }

    export async function execAndWaitEvent(client:any, event:string, doWhat:()=>any=()=>false, timeout:number = 10000, verifyResult:(...args:any)=>Promise<boolean>|boolean = ()=>true): Promise<any> {
        return new Promise(async (res:(...args)=>void,rej)=>{

            const funcOnEvent = async function(){
                try {
                    const isFittingResult = await verifyResult(...arguments);
                    if(isFittingResult) {
                        client.off(event, funcOnEvent);
                        clearTimeout(timer);
                        return res(arguments);
                    }
                } catch(e) {
                    rej(e);
                    client.off(event, funcOnEvent);
                    clearTimeout(timer);
                    return
                }
            }

            const timer = setTimeout(function(){
                client.off(event, funcOnEvent);
                clearTimeout(timer);
                rej(new Error('Timed out'));
            }, timeout);

            try {
                client.on(event, funcOnEvent);
                await doWhat();
            } catch(e) {
                client.off(event, funcOnEvent);
                clearTimeout(timer);
                rej(e);
            }
        })
    }

    const msgBuffers: {[from:string]: {strBuffer:string, timerId:any}} = {}
    export async function requestAndResponse(client:Client, to:string, cmd:NNCmd.Cmd_Base) {
        const chan = to.startsWith('#')? to:null;
        const nick = to.startsWith('#')? client.nick:to;

        let responseCmd: NNCmd.Cmd_Base;
        await execAndWaitEvent(client, 'message', async ()=>{
            cmd.id = createRandomHash();
            await sendNNCmdTo(client, to, cmd);
        },undefined, async (_nick:string, _to:string, _text:string, _message)=>{
            if(_nick?.toLowerCase() !== to?.toLowerCase()) return false; // Ignore messages by other users, not from the destination of the request

            const resCmdObj = await IrcClientExtra.handleMultipartCommandBuffer(chan, nick, _text, msgBuffers);
            if(resCmdObj === 'multipart') return false

            try {
                if(cmd.id && (resCmdObj as NNCmd.Cmd_Base).resToReqId === cmd.id) { // TODO: suport new protobuf req&res: line1
                    (responseCmd as any)  = resCmdObj; // TODO: suport new protobuf req&res: line2
                    return true;
                }
            } catch(e) {}
            
            return false;
        })

        delete responseCmd.id;
        delete responseCmd.resToReqId;

        // If response is error, throw the error (like http)
        if((responseCmd as NNCmd.Cmd_ErrorResponse).op === 'nnerror' || (responseCmd as NNCmd.Cmd_ErrorResponse).error) {
            throw responseCmd;
        }
        return responseCmd;
    }

    export async function requestAndResponseBot(client:any, cmd:NNCmd.Cmd_Base) {
        return await requestAndResponse(client, BotName, cmd);
    }

    export async function sendNNCmdTo(client:any, nickOrChan:string, cmd:NNCmd.Cmd_Base) {
        const isJsonObj = getPrimitiveType(cmd) === 'Object';
        if(!isJsonObj) throw new Error('cmd must be a json obj, not array or anything else.')

        const msg = JSON5.stringify(cmd);
        const compressedMsg = await compressStr(msg);

        // Use shortest string, compete with zlib and plaintext
        const chosenMsg = msg.length<compressedMsg.length? msg:compressedMsg;

        // create a hash as a slice sign
        let sliceSign;
        let nonce = 0;
        const uuid = createRandomHash();
        do {
            nonce++;
            sliceSign = await createHhash(chosenMsg+uuid+nonce);
            if(nonce >= 20) {throw new Error('Could not create hash for slice signing')}
        } while(chosenMsg.indexOf(sliceSign) !== -1); // Just in case the hash exists in the content, recreate the hash.. although this would be extremely rare.

        const finalPayload = `${NNCmd.attrs.prefix}.${sliceSign}.${chosenMsg}${sliceSign}`;

        const retVal = await client.say(nickOrChan, finalPayload);
        
        setTimeout(()=>{
            nncmdSentEmitter.emit('*', cmd.op, 'normal', nickOrChan, cmd);
            nncmdSentEmitter.emit(cmd.op, cmd.op, 'normal', nickOrChan, cmd);
        });

        return retVal;
    }

    export async function getNicksInChannel(client:any, channel:string) {
        const [,namesAndModMap ]:[undefined,Map<string, IrcModSignOrNone>] = await IrcClientExtra.execAndWaitEvent(client, `names`, async ()=>{await client.names(channel)},10000,
        (chan:string)=>{
            return channel.toLowerCase() === chan.toLowerCase();
        })
        return Object.fromEntries(namesAndModMap) as {[nick:string]:IrcModSignOrNone};
    }

    export async function kickNick(client:Client, channel:string, nick:string, reason?:string) {
        if(reason?.length) {
            await client.send(`KICK`, channel, nick, reason)
        } else {
            await client.send(`KICK`, channel, nick)
        }
    }

    export async function handleMultipartCommandBuffer(channel:string|null, nick:string, text:string, msgBuffers:{[from:string]: {strBuffer:string, timerId:any}}, buffer?:Buffer, msgBuffersProtoBuf?: {[from:string]: {buffer:Buffer, length:number, timerId?:any}}, timeoutCb:()=>void = ()=>undefined): Promise<NNCmd.Cmd_Base|NNPB.RootCommand|'multipart'> {
        const bufIndex = channel!=null? `${channel}%${nick}` : `${nick}`;
        // New multi-part logic
        const protoPackResult = await (async function newMultiPartLogic(): Promise<false|true|NNPB.RootCommand> {
            try {
                if(msgBuffersProtoBuf == null || buffer == null) return false;
                buffer = IRC_ParseBufferFromPRIVMSG(buffer);
    
                let existingMsgPart = msgBuffersProtoBuf?.[bufIndex]?.buffer;
                let pureBuffer:Buffer|null = null;
                let isFirstPart = false;
                if(existingMsgPart) {
                    // multipart accumulated message
                    msgBuffersProtoBuf[bufIndex].buffer =
                    existingMsgPart =
                    Buffer.from([...existingMsgPart, ...buffer]);
                    const isPackageComplete = existingMsgPart.length>=msgBuffersProtoBuf[bufIndex].length;
                    if(isPackageComplete) {
                        pureBuffer = (await unpackNukemNetPackage(existingMsgPart)).content!; 
                    }
                } else {
                    // first part or only part
                    if(!isNukemNetPackageBuffer(buffer)) return false;
    
                    isFirstPart = true;
                    
                    existingMsgPart = buffer;
                    const pack = await unpackNukemNetPackage(buffer);
    
                    msgBuffersProtoBuf[bufIndex] = {
                        buffer,
                        length: pack.fullLength
                    };
    
                    if(pack.content != null) pureBuffer = pack.content;
                }
                // If received first part of the message, but it's not complete yet, set a timer for the rest of the message.
                if(pureBuffer === null && isFirstPart) {
                    const timerId = setTimeout(()=>{
                        delete msgBuffersProtoBuf[bufIndex];
                        setTimeout(()=>{timeoutCb()},0)
                    },8000);
                    msgBuffersProtoBuf[bufIndex].timerId = timerId;
                } else if(pureBuffer) {
                    // Received complete package
                    clearTimeout(msgBuffersProtoBuf?.[bufIndex]?.timerId);
                    delete msgBuffersProtoBuf[bufIndex]
                    // Finally decoded actual protobuf message, return for app handling
                    return decodeForIrc<NNPB.RootCommand>(pureBuffer, NNPB.RootCommand);
                }
    
                return true; // true means it's the new package format, but unfinished (multipart)
            } catch(e) {
                return false;
            }
        })();

        // New protobuf pack detected, don't continue to old packing logic
        if(protoPackResult !== false) {
            if(protoPackResult === true) return 'multipart';
            return protoPackResult;
        }

        // Old multi-part text logic
        const msgPart = text.trim();
        let existingMsgStrPart = msgBuffers?.[bufIndex]?.strBuffer;
        if(existingMsgStrPart) {
            // multipart accumulated message
            existingMsgStrPart += msgPart;
        } else {
            // first part or only part
            if(!msgPart.startsWith(NNCmd.attrs.prefix)) throw new Error(`Unknown command syntax`);
            existingMsgStrPart = msgPart;
        }

        let innerPayload:string;
        // Check if received last part of message
        try {
            innerPayload = getFinishedPayload.tryAll(existingMsgStrPart);
            if(innerPayload != null) {
                clearTimeout(msgBuffers?.[bufIndex]?.timerId)
                delete msgBuffers[bufIndex];

                // Check if payload is compressed, if so: decomress it.
                if(!innerPayload.startsWith('{')) {
                    innerPayload = innerPayload = await decompressStr(innerPayload);
                }
            } else {
                // Multipart message unfinished
                if(!msgBuffers[bufIndex]) {
                    // First part of a multipart message is received, create message str buffer, and init timeout timer.
                    const timerId = setTimeout(()=>{
                        delete msgBuffers[bufIndex];
                        setTimeout(()=>{timeoutCb()},0)
                    },8000);
                    msgBuffers[bufIndex] = {strBuffer:existingMsgStrPart, timerId};
                } else {
                    // Keep accumulating
                    msgBuffers[bufIndex].strBuffer = existingMsgStrPart
                }
                return 'multipart'
            }
        } catch(e) {
            throw new Error(`Failed parsing cmd inner payload`);
        }

        let cmdObj;
        try {
            cmdObj = JSON5.parse(innerPayload);
        } catch(e) {
            throw new Error(`command json5 could not be parsed`);
        }

        return cmdObj;
    }

    export async function sendNNCmdToBot(client:any, cmd:NNCmd.Cmd_Base) {
        return sendNNCmdTo(client, BotName, cmd);
    }

    export function validIrcChanName(channel:string) {
        const rg = /^(((![A-Z0-9]{5})|([#+&][^\x00\x07\r\n ,:]+))(:[^\x00\x07\r\n ,:]+)?)$/
        return rg.test(channel);
    }

    export function convertIrcChanNameToValid(channel:string) {
        const fixedChanName = `#${removeNonAlphaNumChars(channel)}`;
        
        if(fixedChanName.length>3) {
            return fixedChanName;
        } else {
            throw new Error('Given chan name is completely invalid');
        }
    }

    export async function joinChannelAndGetNames(ircClient:Client, roomChannelName:string, password?:string) {
        const [,namesAndModMap ]:[undefined,Map<string, IrcModSignOrNone>] = await IrcClientExtra.execAndWaitEvent(ircClient, `names`, async ()=>{
            if(!password?.length) {
                await ircClient.join(roomChannelName)
            } else {
                await ircClient.send('JOIN', roomChannelName, password)
            }            
        }, 10000, (chan:string)=>{
            return roomChannelName.toLowerCase() === chan.toLowerCase();
        })
        return Object.fromEntries(namesAndModMap) as {[nick:string]:IrcModSignOrNone};
    }

    export async function changeIrcConnection(ircClient:Client, newServer:string, newPort:number, family:number) {
        ircClient['server'] = ircClient['opt'].address = newServer;
        ircClient['opt'].port = newPort;
        ircClient['opt'].family = family;
        try {ircClient.disconnect();} catch(e) {}
    }
    
    // matrix-org-irc retry reconnection logic has a flaw, after 3 minutes timeout, if your computer becomes connected, it would do like an endless loop of connect-disconnect every few seconds.
    // so using a manual reconnetion logic instead.
    export async function connectWithRetry(ircClient:Client, onConnected:()=>void, onDisconnected:()=>void, addressToIpFamilyResolver?:(address:string)=>Promise<6|4|null>) {
        console.log('Connecting to IRC...');

        const connTimeout = 5000;

        // Set family, we do that manually to prefer IPv6 address when possible, and IPv4 as fallback, because that possibly may extend life of IPv6 temp address, if there's a TCP connection in the background for IRC.
        if(addressToIpFamilyResolver) {
            const addr = ircClient?.['opt']?.address ?? ircClient?.['server'];            
            const family = (await addressToIpFamilyResolver(addr) ?? 4);
            ircClient['opt'].family = family;
        } else {
            delete ircClient['opt'].family;
        }

        ircClient.connect();

        const timerId = setTimeout(()=>{
            onClose();
        }, connTimeout);

        let isConnected = false;

        let pingIntervalId:any = null;
        
        function onConnect() {
            console.log('IRC Connected');
            clearTimeout(timerId);
            isConnected = true;

            if(pingIntervalId) clearInterval(pingIntervalId);
            pingIntervalId = setInterval(()=>{
            // If connection is destroyed, but no event was triggered, we trigger the logic manually
            if(ircClient.conn?.connecting && isConnected) {
                onClose();
            }
            },30*1000);

            setTimeout(()=>{
                onConnected();
            });
        }

        function onClose() {
            if(pingIntervalId) clearInterval(pingIntervalId);

            if(isConnected) {
            isConnected = false;
            
            setTimeout(()=>{
                onDisconnected();
            });
            }
            console.log('IRC connection not open, retrying...');

            clearTimeout(timerId);
            removeListeners();
            ircClient.disconnect();
            setTimeout(()=>{
                connectWithRetry(ircClient, onConnected, onDisconnected, addressToIpFamilyResolver);
            },2000);
        }

        function removeListeners() {
            ircClient.conn?.removeListener('connect', onConnect);
            ircClient.conn?.removeListener('end', onClose);
            ircClient.conn?.removeListener('timeout', onClose);
        }

        ircClient.conn!.once('connect', onConnect);
        ircClient.conn!.once('end', onClose);
        ircClient.conn!.once('timeout', onClose);
    }

    export function writeSaslPlainAuth(nick:string, pass:string, socket:net.Socket) {
        if(!socket) return;

        const accountNameBuf = Buffer.from(nick);
        const passBuf = Buffer.from(pass);
        const msg = Buffer.from([
            ...accountNameBuf,
            0,
            ...accountNameBuf,
            0,
            ...passBuf
        ]);
        
        const b64creds = msg.toString('base64');
        socket.write('AUTHENTICATE PLAIN\n');
        socket.write(`AUTHENTICATE ${b64creds}\n`);
    }

    export function getNamesForChannel(ircClient:Client, channel:string): Promise<{[nick:string]:IrcModSignOrNone}> {
        return new Promise((resolve, reject)=>{
            try {
                const namesOpMap = {} as {[nick:string]:IrcModSignOrNone};

                function onRaw(res:Message) {
                    try {
                        if(res?.command == 'rpl_namreply') {
                            const names = res.args[3].split(/\s+/);
                            for(const name of names) {
                                const modSign = name[0];
                                if(modSign in IrcUserMod_SignToCmd) {
                                    namesOpMap[name.substring(1)] = modSign as IrcModSigns;
                                } else {
                                    namesOpMap[name] = '' as IrcModSignOrNone;
                                }
                            }
                        } else if(res?.command == 'rpl_endofnames') {
                            done(namesOpMap);
                        }
                    } catch(e) {}
                }

                const timer = setTimeout(()=>{
                    done(undefined, new Error('Timeout'));
                }, 1000*10);

                function done(res?:any, err?:Error) {
                    clearTimeout(timer);
                    ircClient.removeListener('raw', onRaw);
                    if(res) {
                        resolve(res);
                    } else {
                        reject(err);
                    }
                }

                ircClient.send("NAMES", channel);
                ircClient.on('raw', onRaw);
            } catch(e) {
                reject(e);
            }
        });
        
    }

    export function fixMatrixOrgIrcBug(ircClient:Client) {
        // On latest matrix-org-irc, the library is not removing the user from the channel when he leaves, so we have to do it manually.
        const deleteNickFromChannel = (nick:string, channel?:string)=>{
            try {
                for (const chan of ircClient.chans.keys()) {
                    if(channel?.length && chan.toLowerCase() !== channel.toLowerCase()) continue;

                    const chanData = ircClient.chans.get(chan);
                 
                    for (const user of chanData.users.keys()) {
                        if(user.toLowerCase() === nick.toLowerCase()) {
                            chanData.users.delete(user)
                        }
                    }
                    if(channel?.length) break;
                }
            } catch(e) {
                console.error("Exception on matrix-org-irc bug fix", e);
            }
        }

        ircClient.on(`kick`, (channel:string, nick:string)=>{
            deleteNickFromChannel(nick, channel)
        })
        ircClient.on(`part`, (channel:string, nick:string)=>{
            deleteNickFromChannel(nick, channel)
        })
        ircClient.on(`kill`, (nick:string)=>{
            deleteNickFromChannel(nick)
        })
        ircClient.on(`quit`, (nick:string)=>{
            deleteNickFromChannel(nick)
        })
        ircClient.on('nick', async (oldnick:string, newnick:string)=>{
            for (const chan of ircClient.chans.keys()) {
                const chanData = ircClient.chans.get(chan);
             
                for (const user of chanData.users.keys()) {
                    if(user.toLowerCase() === oldnick.toLowerCase()) {
                        const userData = chanData.users.get(user);
                        chanData.users.set(newnick, userData);
                        chanData.users.delete(user)
                        break;
                    }
                }
            }
        })
    }

}