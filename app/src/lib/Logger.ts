
import { fs, path } from '../NodeLibs'
import { pathOfUserDir, Settings } from '../Settings';

const pathOfLogFile = path.resolve(pathOfUserDir, 'nn.log');

class AppLogger {

    async log(str:string|object) {
        if(Settings.read().disableLogging) return;

        if(typeof str === 'object') {
            str = JSON.stringify(str, null, 2);
        }
        fs.appendFile(pathOfLogFile, str+'\n')
    }
}

export const appLogger = new AppLogger()