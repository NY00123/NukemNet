import { defineConfig } from 'vite';
import solidPlugin from 'vite-plugin-solid';

export default defineConfig({
  base: '',
  server: {
    port: 3002
  },
  plugins: [solidPlugin()],
  optimizeDeps: {
    esbuildOptions: {
      target: 'es2020'
    }
  }
});
