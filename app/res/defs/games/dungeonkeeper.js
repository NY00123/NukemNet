module.exports = function() {

    const commands = ['CONFIG -set "cycles=max"','CONFIG -set "core=auto"']

    // must cd into the "SOUND" folder when configuring sound device settings
    const runToolOpts = {"type":"dosbox", "data":{commands, cdInto:true}};
    const runToolOptsMultiplayer = {"type":"dosbox", "data":{commands, cdInto:true, "net":["ipx","serial"]}};

    const runnables = {
        "multiplayer": {
            "file":"main",
            "runTool":runToolOptsMultiplayer
        },
        "settings": {"file":"snd", "runTool": runToolOpts},
        "singleplayer": {"file":"main", "runTool": runToolOpts}
    };

    // TODO edit binary config file to COM1, and baud rate on behalf of the user

    return {
        "name": "Dungeon Keeper",
        "get": [{
            type:"gog",
            prodId:"1207658934",
            nameId:"dungeon_keeper",
            name:"Dungeon Keeper Gold™"
        }],
        "executables": {
            "full": {
                "maxPlayers":4,
                "name":"Full (DOS)",
                "files": {
                    "snd": {"path":"SOUND/SETSOUND.EXE"},
                    "main": {"path":"KEEPER.EXE"}
                },
                "runnables": runnables,
                "parameters": {}
            },
            "deeper": {
                "maxPlayers":4,
                "name":"Deeper Dungeons (DOS)",
                "files": {
                    "snd": {"path":"SOUND/SETSOUND.EXE"},
                    "main": {"path":"DEEPER.EXE"}
                },
                "runnables": runnables,
                "parameters": {}
            },
        }
    };
}