module.exports = function() {

    const commands = ['CONFIG -set "cycles=max"','CONFIG -set "core=auto"']

    const runToolOpts = {"type":"dosbox", "data":{commands}};
    const runToolOptsMultiplayer = {"type":"dosbox", "data":{commands, "net":"ipx"}};

    const runnables = {
        "multiplayer": {
            "file":"main",
            "runTool":runToolOptsMultiplayer
        },
        "settings": {"file":"setup", "runTool": runToolOpts},
        "singleplayer": {"file":"main", "runTool": runToolOpts}
    };

    return {
        "name": "Darklight Conflict",
        "executables": {
            "full": {
                "maxPlayers":6,
                "name":"Full (DOS)",
                midGameJoin: true,
                "files": {
                    "main": {"path":"DLEXEC.EXE"},
                    "setup": {"path":"DRIVERS/SETSOUND.EXE"},
                },
                "runnables": runnables,
                "parameters": {},
            }
        }
    };
}