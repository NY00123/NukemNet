import { crypto, fs, path } from "../NodeLibs";
import { Client } from "matrix-org-irc";
import axios from "axios";
import { AppWindowReturn, DialogMgr } from "../Popups";
import { ProgressBar } from "solid-bootstrap";
import { createMutable } from "solid-js/store";
import { backlogTabL } from "../store/store";


export namespace AzureUpload {

    const SAS_AddOnly = {
        comp:"blocklist",
        sp:"acw",
        st:"2022-07-24T17:15:29Z",
        se:"2040-07-25T01:15:29Z",
        sip: "0.0.0.0-255.255.255.255",
        sv: "2021-06-08",
        sr: "c",
        sig: "ZgSKkUuXO85alNImnA8l9aZQcq1O5PfCeMR/PaUF7xo=",
    }
    
    const statusElId = 'upload-file-dialog-progress';

    export async function putSingeBlobBlock(encodedUrl:string, bytes: Uint8Array, abortController: AbortController, blockIdBase64:string, onUploadProgress:(p:any)=>void) {
        return await axios.request({
            url: encodedUrl,
            params: {...SAS_AddOnly, comp:"block", blockid: blockIdBase64},
            data: bytes,
            method:"PUT",
            signal: abortController.signal,
            onUploadProgress
        })
    }
    export async function putBlockList(url:string, blocklistXml:string, abortController: AbortController) {
        const encodedUrl = encodeURI(url);

        const obj = {
            url: encodedUrl,
            params: {...SAS_AddOnly, comp: "blocklist" },
            data: blocklistXml,
            method: "PUT",
            signal: abortController.signal,
            headers: {"Content-Type": "text/plain; charset=UTF-8" }
        }
        await axios.request(obj)

        return url;
    }
    const BIGGEST_FILE_UPLOAD_BYTES = 500000000

    export async function fullUpload(filepathOrBuffer:string|Buffer, fileName:string, target:string, ircClient:Client, skipQuestion:boolean = false) {
        if(target.toLowerCase() === backlogTabL) return
        
        const buffer = typeof filepathOrBuffer === "string" ?
        (
            await (async()=>{
            try {
                const b = await fs.readFile(filepathOrBuffer);
                return b;
            } catch(e) {
                DialogMgr.addPopup({
                    title:'File Upload',
                    icon:"error",
                    body: "File is too large to read and upload.",
                    closeButton:true
                });
                return null;
            }
            })()
        ) : filepathOrBuffer as Buffer;

        if(buffer==null) return;
        const bytes = new Uint8Array(buffer);
        if(!skipQuestion) {
            if(bytes.length>BIGGEST_FILE_UPLOAD_BYTES) {
                DialogMgr.addPopup({
                    title:'File Upload',
                    icon:"error",
                    body: "Uploading files larger than " + BIGGEST_FILE_UPLOAD_BYTES + " bytes is not supported yet",
                    closeButton:true
                });
                return;
            }
        
            const dialogRes = await DialogMgr.addPopup({
                title: 'Upload File',
                icon: "question",
                body: `Upload file ${fileName}?\r\n`,
                buttons:[{body:"Upload", variant:"primary",},{
                    body:"Cancel", variant:"secondary",
                    onClick:()=>{try {controller.abort();} catch(e) {}}
                }]
            })

            if(dialogRes.buttonIndex!==0) return;
        }

        const controller = new AbortController()

        const status = createMutable({percent:0});

        let pendingDialog:AppWindowReturn|null = null;
        try {
            pendingDialog = DialogMgr.addWindow({
                close:()=>{controller.abort();},
                title:"Uploading...",
                body: ()=><><ProgressBar now={status.percent} label={`${status.percent}%`} /><div id={statusElId}></div></>,
                buttons:[{text:"Abort",click:()=>{
                    pendingDialog!.close();
                }}]
            });
            
            const maxBytesPerBlock = BIGGEST_FILE_UPLOAD_BYTES; // it should be smaller than max size, preferrably 500,000 bytes. but multiple blocks fail in PUT blocklist request for some reason, so meanwhile we submit one big block.
            const blocksNum = parseInt(bytes.length/maxBytesPerBlock as any)+1;
            const blockIds = [] as string[];

            let i:number;
            function onUploadProgress(p:{loaded:number, total:number}) {
                const statusEl = document.getElementById(statusElId)!;    
                const loaded = (i*maxBytesPerBlock)+p.loaded;
                const percent = (parseInt((loaded / bytes.length)*1000 as any))/10;
                status.percent = percent;
                statusEl.innerText = `Uploading File: ${percent}% (${loaded}/${bytes.length})`
            }
            
            const uniqePrefix = crypto.createHash('md5').update(bytes).digest("hex").substring(0,8)
            const url = `https://nukemnet.blob.core.windows.net/nnuploads/${uniqePrefix}/${fileName}`;
            const encodedUrl = encodeURI(url);

            // Check if exists
            try {
                await axios.head(encodedUrl);
                ircClient.say(target, encodedUrl);
                if(pendingDialog) pendingDialog.close();
                return // If no exception (404 not found)
            } catch(e) {}

            for(i=0;i<blocksNum;i++) {
                const startIndex = maxBytesPerBlock*i;
                // const blockBytes = new Uint8Array(bytes.subarray(startIndex, startIndex+maxBytesPerBlock))
                const blockBytes = bytes.subarray(0, 1); // I absolutely have no idea why it works.
                
                if(blockBytes.length) {
                    const blockId = Buffer.from([i]).toString('base64');
                    blockIds.push(blockId);
                    await putSingeBlobBlock(encodedUrl, blockBytes, controller, blockId, onUploadProgress);
                }
            }
            if(blockIds.length && url?.length) {
                let xml = '<?xml version="1.0" encoding="utf-8"?><BlockList>';
                for(const blockId of blockIds) {
                    xml+=`<Latest>${blockId}</Latest>`
                }
                xml += '</BlockList>'
                await putBlockList(url, xml, controller);

                const encodedUrl = encodeURI(url);
                ircClient.say(target, encodedUrl);
            }

            if(pendingDialog) pendingDialog.close();
            
        } catch(e:any) {
            if(pendingDialog) pendingDialog.close();;

            if(e.code === "ERR_CANCELED") {
                DialogMgr.addPopup({
                    icon: "info",
                    title: "Canceled",
                    body: "Upload Canceled",
                    closeButton: true
                })
            } else {
                DialogMgr.addPopup({
                    icon: "error",
                    title: "Error",
                    body: "Upload Failed",
                    closeButton: true
                })
            }
            
        }
    }
}

