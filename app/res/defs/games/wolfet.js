
module.exports = function ({}) {

    const path = require('path');
    const fsPromises = require('fs/promises');
    const glob = require('glob')

    const NNCfg_HostFile = "NukemNetHost.cfg";
    const NNCfg_ClientFile = "NukemNetClient.cfg";

    const writeCommandsCfg = [{"cmd":"function", async run(ctx, execDef, result, imHost, players, hostIpDetails, useDedicated, othersIpDetails, channelName, hostTunnelChosenPort) {

        const fname = imHost?NNCfg_HostFile:NNCfg_ClientFile;
        const params = ctx.GameRoom.Params;
        let lineCommands = [];
        for(const p in params) {
            const argsArr = params[p];
            for(const line of argsArr) {
                const trm = line.trim();
                if(trm.startsWith("+") && !trm.startsWith("+exec") && !p.startsWith("_") && ctx?.GameRoom?.ParamDefs?.[p]?.syncOnly) {
                    lineCommands.push({id:p, line:trm.substring(1)});
                }
            }
        }
        lineCommands.sort((a,b) => {
            const aOrder = ctx.GameRoom.ParamDefs[a.id]?.order;
            const bOrder = ctx.GameRoom.ParamDefs[b.id]?.order;
            return (aOrder||0)>(bOrder||0);
        });
        const fileContent = lineCommands.map(r=>r.line).join('\r\n');
        await fsPromises.writeFile(path.resolve(ctx.GameDir, 'etmain', fname), fileContent);
    }}];
    
    const levelsCalc = [
        ["* North Africa Campaign *", "battery"],
        ["Seawall Battery", "battery"],
        ["Gold Rush", "goldrush"],
        ["Siwa Oasis", "oasis"],

        ["* Central Europe Campaign *", "fueldump"],
        ["Fuel Dump", "fueldump"],
        ["Würzburg Radar", "radar"],
        ["Rail Gun", "railgun"]
    ];

    const levels = levelsCalc.map(([name, id]) => ({
        "label": name,
        "value": [id]
    }));

    function getParams() {

        const params = {
            "_dedicated": { 
                order:-102,
                "modeSupport": ["multiplayer"],
                "type": "static",
                "value": [`'+set'`, `'dedicated'`, `'1'`], 
                "for":"host-only-private"
            },
    
            "_netp": { 
                order:-101,
                "modeSupport": ["multiplayer"],
                "type": "static",
                "value": [`'+set'`, `'net_port'`, `GameRoom.MyPort`],
                "for":"host-only-private",
            },
    
            hostbase: {
                order:-101,
                "modeSupport": ["multiplayer"],
                "type": "static",
                "value": [
                    `'+PB_SV_DISABLE'`,
                    `'+set sv_hostname NukemNet'`, `'+set sv_maxclients 16'`, `'+set g_motd "Welcome to NukemNet Server"'`, `'+set rconpassword "nukemnet"'`,
                    `'+set com_hunkMegs 1024'`, `'+set com_soundMegs 32'`, `'+set com_zoneMegs 32'`,
                    `'+set sv_allowDownload 1'`
                ],
                "for":"host-only-private",
                syncOnly:true
            },
    
            "_mod": {
                order:-100,
                "modeSupport": ["multiplayer"],
                "type": "dirfiles",
                "value": ["'+set fs_game '+value[0]"],
                "label": "Mod",
                "path": "!(etmain|legacy)",
                globOpts: {
                    stat: true,
                    ignore: {
                        ignored: (p) => {
                            if(p.isDirectory()) {
                                const files = glob.globSync('*.pk3', {cwd: p.fullpath(), nocase:true});
                                if(files.length>0) {
                                    return false;
                                }
                            }
                            return true;
                        }
                    }
                },
                "stripExt": true,
                "stripPath":true,
                "optional":true
            },
    
            "name": {
                order:-100,
                "modeSupport": ["multiplayer"],
                "type": "static",
                "value": ["'+name '+MyName"],
                "for":"private",
                syncOnly:true,
            },
    
            "_cmdshost": {
                order:-99,
                "modeSupport": ["multiplayer"],
                "type": "static",
                "value": ["'+exec'", `'${NNCfg_HostFile}'`],
                "for":"host-only-private",
            },
            "_cmdsclnt": {
                order:-99,
                "modeSupport": ["multiplayer"],
                "type": "static",
                "value": ["'+exec'", `'${NNCfg_ClientFile}'`],
                "for":"client-only-private",
            },
    
            "_clntcon": {
                order:-99,
                "modeSupport": ["multiplayer"],
                "type": "static",
                "value": ["'+connect'", `GameRoom.DestIp + ':' + GameRoom.DestPort`],
                "for":"client-only-private"
            },

            "gametype": {
                order:-98,
                "modeSupport": ["multiplayer"],
                "type": "choice",
                "label": "Type",
                "value":["'+set g_gametype '+value"],
                "choices": [{
                    "label": "Stopwatch",
                    "value": "3"
                }, {
                    "label": "Campaign",
                    "value": "4"
                }, {
                    "label": "LMS",
                    "value": "5"
                }],
                syncOnly:true,
                "for":"host-only-private"
            },

            "cmpgn": {
                order:-98,
                "modeSupport": ["multiplayer"],
                "type": "choice",
                "label": "Campaign",
                optional:true,
                "value":["'+campaign '+value"],
                "choices": [{
                    "label": "North Africa",
                    "value": "cmpgn_northafrica"
                }, {
                    "label": "Central Europe",
                    "value": "cmpgn_centraleurope"
                }],
                syncOnly:true,
                "for":"host-only-private"
            },

            "_map": {
                "modeSupport": ["multiplayer"],
                "type": "dirfiles",
                "value": ["'+map '+value[0]"],
                "label": "Usermap",
                "path": "etmain/!(*pak*).pk3",
                "stripExt": true,
                "stripPath":true,
                "optional":true,
                "for":"host-only-shared",
                syncOnly: true
            },
            "_level": {
                "modeSupport": ["multiplayer"],
                "type": "choice",
                "label": "Level",
                "value":["'+map '+value[0]"],
                "choices": levels,
                "optional":true,
                "for":"host-only-shared",
                syncOnly:true
            },
            "inactivity": {
                order:-50,
                "modeSupport": ["multiplayer"],
                "type": "numrange",
                "min":0,
                "max":10000,
                "delta":1,
                "label": "Inactivity",
                "description": "Inactivity Seconds before kickout",
                "value": "'+set g_inactivity '+value",
                "optional":true,
                for:'host-only-shared',
                syncOnly:true
            },
            "tmlmt": {
                order:-50,
                "modeSupport": ["multiplayer"],
                "type": "numrange",
                "min":0,
                "max":1000,
                "delta":1,
                "label": "Time Limit",
                "value": "'+set timelimit '+value",
                "optional":true,
                for:'host-only-shared',
                syncOnly:true
            },
            "ff": {
                order:-50,
                "modeSupport": ["multiplayer"],
                "type": "boolean",
                "label": "Friendly Fire",
                "value": `'+set g_friendlyFire '+(value?'1':'0')`,
                addFalse: true,
                "for":"host-only-shared",
                syncOnly: true
            },
            "map": {
                order:-1,
                "modeSupport": ["multiplayer"],
                "type": "static",
                "value": [`GameRoom?.Params?._map?.[0] || GameRoom?.Params?._level?.[0] || (GameRoom?.Params?._mod?.[0]? undefined:"+map oasis")`],
                "for":"host-only-shared",
                syncOnly: true
            }
        }
        return params;
    }

    return {
        "name": "Wolfenstein: Enemy Territory",
        executables: {
            "compat": {
                name:"Compat",
                "maxPlayers":16,
                midGameJoin: true,
                tips: {html: `
                    <div>
                    Using ETLegacy is recommended, simply point to it's directory.<br/>
                    For advanced server setup, refer to https://wolffiles.de/filebase/ET/Stuff/ET-Serverguide.pdf
                    </div>
                `},
                "files": {
                    "dedicated": {glob:["etlded*", "etded*"], runDetachedShell:true}, // first checking for ETLegacy, then vanilla
                    "multi": {glob:["etl.exe", "etl", "et", "et.exe"]},
                    "maindir": {"path":"etmain"}
                },
                "runnables": {
                    "multiplayer": {
                        "file":(ctx)=>{return ctx?.GameRoom?.ImHost?'dedicated':'multi'},
                        beforeRun:writeCommandsCfg,
                        hostAlsoRunsAsClient:true
                    },
                },
                parameters:getParams()
            }
        }
    }
}
