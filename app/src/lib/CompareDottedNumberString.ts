export function compareDottedStringNumbers(val1:string, val2:string):-1|0|1 {
    if (typeof val1 !== 'string') return 0;
    if (typeof val2 !== 'string') return 0;
    const v1:any = val1.split('.');
    const v2:any = val2.split('.');
    const k = Math.min(v1.length, v2.length);
    for (let i = 0; i < k; ++ i) {
        v1[i] = parseInt(v1[i], 10);
        v2[i] = parseInt(v2[i], 10);
        if (v1[i] > v2[i]) return 1;
        if (v1[i] < v2[i]) return -1;        
    }
    return v1.length == v2.length ? 0: (v1.length < v2.length ? -1 : 1);
}