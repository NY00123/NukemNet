import { Client } from "matrix-org-irc";
import sleep from "sleep-promise";
import { Button, Form } from "solid-bootstrap";
import { IrcClientExtra } from "../../server/src/common/ircExtend";
import { IRCEvent_Notice } from "./irc-events/ircEvents";
import { is_ipv4 } from "./lib/GetIp";
import { checkProcessRunSuccess, findFreeUdpTcpPortInRange } from "./lib/utils";
import { child_process, dgram, path, Electron, fsSync, fs } from "./NodeLibs";
import { DialogMgr } from "./Popups";
import { pathOfRes, pathOfUserDir, Settings } from "./Settings";
import { mainStore } from "./store/store";

function addTextToBacklog(text:string) {
    mainStore.addEventTo('backlog', 'backlog', mainStore.backlog, {
        ts: Date.now(),
        type: 'notice',
        text
    } as IRCEvent_Notice)
}

const ircServerAndBroadcastPortNum = 26667

export async function LanMode_Activate(ircClient:Client) {
    await sleep(0); // wait for mainStore to be ready.
    
    addTextToBacklog("** LAN Mode is active **");

    const lanStartedTime = Date.now();
    const socket = dgram.createSocket('udp4');
    let found = false;
    let ihaveStartedMyOwnDaemon = false;
    socket.on('message', async (msg, rinfo) => {
        const str = msg.toString();
        const parts = str.split('connectToMe:');
        if(parts.length===2) {
            found = true;
            // TODO ircClient connect to daemon in rinfo
            const ts = parseInt(parts[1]);

            if(!ihaveStartedMyOwnDaemon) {
                addTextToBacklog(`Found IRC Server, connecting to ${rinfo.address}:${rinfo.port}`);

                // If ts is bigger than lanStartedTime or I don't have a daemon running, connect to the other daemon
                IrcClientExtra.changeIrcConnection(ircClient, rinfo.address, rinfo.port, 4);

                let prevConnected = false;
                IrcClientExtra.customEvents.on('connectionStatus', async (connected)=>{
                    if(connected && !prevConnected) {
                        prevConnected = true;
                        return;
                    }
                    if(prevConnected && !connected) {
                        // Server closed, close app.
                        await DialogMgr.addPopup({title:"Error", body:"LAN IRC server closed, restarting...", closeButton:true, buttons:[{body:"OK"}]});
                        location.reload();
                    }
                });
            } else if(ts<lanStartedTime) {
                // move away to other's IRC daemon.
                await DialogMgr.addPopup({title:"Error", body:"Other IRC Daemon found, restarting to connect to it...", closeButton:true, buttons:[{body:"OK"}]});
                location.reload();
            }
            
        }
    });
    socket.on('listening', async ()=>{
        socket.setBroadcast(true);

        addTextToBacklog("Looking for LAN IRC server...");

        while(true) {
            for(let i=0;i<5;i++) {
                socket.send('getIrcServer',ircServerAndBroadcastPortNum,'255.255.255.255');
                await sleep(1000);
                if(found) {break;}
            }
            if(!found && !IrcClientExtra.isConnected(ircClient)) {

                addTextToBacklog("Other IRC Server not found, please choose server/client");

                let chosenIp:string = '';
                let chosenPort:number = 0;
                let popupClosed = false;
                const popupRef = DialogMgr.addPopup({
                    persistent: true,
                    title: "Client/Server",
                    body:()=>{
                        (async ()=>{
                            while(!popupClosed && !IrcClientExtra.isConnected(ircClient)) {
                                await sleep(1000);
                            }
                            try {popupRef.close();} catch(e) {}
                        })();

                        return <div>
                            Other IRC server not detected on LAN.<br/>
                            Create Server? or Connect by IP?<br/><br/>
                            <div style={{display:"flex", "flex-direction":"row"}}>
                                <Form.Control
                                placeholder="Other IRC Server IPv4"
                                type="input"
                                id="ircServerAddress"/>
                                <Button onClick={()=>{
                                    const value = (document.getElementById('ircServerAddress') as HTMLInputElement)?.value;
                                    const addr_port = value.split(':');
                                    if(addr_port.length === 2) {
                                        chosenIp = addr_port[0];
                                        chosenPort = parseInt(addr_port[1]);
                                        if(isNaN(chosenPort) || chosenPort<1 || chosenPort>65535) {
                                            chosenPort = 0;
                                        }
                                    } else {
                                        chosenIp = value;
                                        chosenPort = ircServerAndBroadcastPortNum;
                                    }

                                    if(!is_ipv4(chosenIp)) {
                                        DialogMgr.addPopup({title:"Error", body:"Invalid IP address", closeButton:true, buttons:[{body:"Close"}]});
                                    } else {
                                        popupRef.close();
                                    }
                                }}>Connect</Button>
                            </div>
                        </div>
                    },
                    buttons: [{body:'Create Server'}, {body:"Keep Searching on LAN"}, {body:"Exit LAN Mode", onClick:()=>{Settings.toggleLanMode()}}]
                });
                if(IrcClientExtra.isConnected(ircClient)) break;

                const dialogResult = await popupRef;
                popupClosed = true;

                if(dialogResult.buttonIndex === 1) {
                    addTextToBacklog(`Looking for IRC server..`);
                    continue
                }

                if(chosenIp?.length) {
                    const port = chosenPort||ircServerAndBroadcastPortNum;
                    addTextToBacklog(`Connecting to IRC server on ${chosenIp}:${port}..`);
                    IrcClientExtra.changeIrcConnection(ircClient, chosenIp, port, 4);
                    let connected = false;
                    for(let i=0;i<5;i++) {
                        await sleep(1000);
                        connected = IrcClientExtra.isConnected(ircClient);
                        if(connected) {break;}
                    }
                    if(!connected) {
                        addTextToBacklog(`Connection failed to ${chosenIp}`);
                        continue;
                    }
                } else if(dialogResult.buttonIndex === 0) {
                    addTextToBacklog(`Creating an IRC server..`);
                    ihaveStartedMyOwnDaemon = true;

                    // create irc server
                    const boardcastServer = dgram.createSocket('udp4');
                    boardcastServer.bind(ircServerAndBroadcastPortNum, '0.0.0.0');
                    boardcastServer.on('message', (msg, rinfo) => {
                        boardcastServer.send('connectToMe:'+lanStartedTime, rinfo.port, rinfo.address);
                    });
                    boardcastServer.on('error', async ()=>{
                        await DialogMgr.addPopup({title:"Error", body:`UDP Binding to ${ircServerAndBroadcastPortNum} failed, cannot create IRC server.`});
                    })
                    
                    // Create your local daemon
                    const ergoLibDir = path.join(pathOfRes, 'libs', 'ergo');
                    const ergoRunDir = path.join(pathOfUserDir, 'ergo');

                    const ergoExec = (function findErgoExec() {
                        const exe = path.resolve(ergoLibDir, 'ergo.exe');
                        if(fsSync.existsSync(exe)) return exe;
                        const altExe = path.resolve(ergoLibDir, 'ergo');
                        if(!fsSync.existsSync(altExe)) {
                            console.error('ergo executable not found');
                        }
                        return altExe;
                    })();
                    
                    if(!fsSync.existsSync(ergoRunDir)) {
                        fsSync.mkdirSync(ergoRunDir, {recursive:true});
                    }
                    const srcErgoYamlPath = path.resolve(ergoLibDir, 'ircd.yaml');
                    const dstErgoYamlPath = path.resolve(ergoRunDir, 'ircd.yaml');
                    fsSync.writeFileSync(dstErgoYamlPath, fsSync.readFileSync(srcErgoYamlPath));

                    const proc = child_process.execFile(ergoExec, ['run' , '--conf', dstErgoYamlPath], {cwd:ergoRunDir});
                    proc.on('close', async (code)=>{
                        await DialogMgr.addPopup({title:"IRC Closed", body:"Local IRC daemon closed (check log file for errors)", closeButton:true, buttons:[
                            {body:"Retry", onClick:()=>{location.reload();}},
                            {body:"OK"}
                        ]});
                    });
                    Electron.ipcRenderer.send('subprocess-pids', {action:"add", value:proc.pid});
                    checkProcessRunSuccess(proc).
                    catch(()=>{
                        DialogMgr.addPopup({title:"Error", body:"Failed to start local IRC daemon.", closeButton:true, buttons:[{body:"OK"}]});
                    }).
                    then(async ()=>{
                        IrcClientExtra.changeIrcConnection(ircClient, mainStore.localhostIp4, ircServerAndBroadcastPortNum, 4);
                    });
        
                    // Check for other older daemons
                    while(true) {
                        socket.send('getIrcServer',ircServerAndBroadcastPortNum,'255.255.255.255');
                        await sleep(5000);
                    }
                }
            }
            break;
        }
    });
    const port = await findFreeUdpTcpPortInRange();
    socket.bind(port, '0.0.0.0');
}