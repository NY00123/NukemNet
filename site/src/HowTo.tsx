


import { Component } from 'solid-js';
const HowTo: Component = () => {
  return <div style="font-size: 12pt; margin-left:auto; margin-right:auto; width:fit-content">
    <h2>Usage guide</h2>
    <p>
      Download NukemNet and extract to any folder on your computer.<br/>
      Run NukemNet.exe to open the program, you'll arrive at the main #lobby chat IRC channel.
    </p>
    <h3>
      Get your games
    </h3>
    <p>
      Although NukemNet has support for many games, they are not bundled with it,<br/>
      Meaning you have to download the games from online sources such as GOG/Steam/Zoom-Platform.<br/>
      NukemNet is not affiliated with any game, and therefore we are not allowed to distribute them.
    </p>
    <h3>
      Configure your games
    </h3>
    <p>
      Once you have obtained the games you want to play, you need to "tell" NukemNet where they are installed.<br/>
      * In NukemNet, click "Setup" on top<br/>
      * On the bottom select your game from the select-box (under "Game Setup")<br/>
      * Use "Locate" to find your game folder, for the game edition you have<br/>
      (Some Games have more than 1 edition, so make sure you "Locate" the right one).<br/>
      * Click on the blinking green button "Click to Save".<br/>
      Do this for all your games.
      * Once all games are configured, scroll to bottom and click "Save Settings"
    </p>
    <h3>
      Configure DosBox
    </h3>
    <p>
      DosBox is a Dos emulator, allowing you to run dos games on modern systems.<br/>
      To play dos games with NukemNet, you must first setup DosBox and configure it with NukemNet<br/>
      * Install DosBox (DosBox-Staging is recommended)<br/>
      * In NukemNet, Click "Setup"<br/>
      * Click "Locate" for the dosbox entry, then click to save, and save your settings.
    </p>
    <h3>
      Choose Nick Name
    </h3>
    <p>
      In "Setup", you can assign a name for chatting on NukemNet IRC,<br/>
      And a different name for the games themselves (shown while playing)
    </p>
    <h3>
      Playing Soure-Ports (Modern Game editions without DosBox)
    </h3>
    <p>
      The steps are a bit different for every game, here are a few examples.<br/>
      <br/>Duke Nukem 3D<br/>
      Most popular source-ports are NetDuke32 and hDuke.<br/>
      Both of them are free to download in the "Downloads" tab.<br/>
      To play them, you must copy a file from your original copy of duke3d.<br/>
      The file name is duke3d.grp, copy it into your NetDuke32 folder, or/and to hDuke folder.<br/>
      It should work now. configure their paths in NukemNet's "Setup" screen.<br/>

      <br/>Shadow Warrior<br/>
      Most popular source-port is voidsw, get in "Downloads" tab.<br/>
      Same step as Duke Nukem 3D, except you need to copy the file sw.grp from your original copy.
    </p>
    <h3>
      How to play on multiplayer
    </h3>
    <p>
      Once you have everything setup on NukemNet, click "Play" on top<br/>
      Select which game to play using the select box.<br/>
      Select game edition (executable) using the select box.<br/>
      In "mode", choose "multiplayer"<br/>
      Set all the parameters for your game (such as level, episode, players..)<br/>
      Click "Create", a new IRC chat room will open, congrats, you have created a game room!<br/>
      Invite your friends to join your room.<br/>
      Once everyone is inside, click "Start Game", the game will launch for everyone.<br/>
      <br/>
      Some dos games (such as duke3d) have settings that must be configured before launching the game.<br/>
      You can do so by clicking "Launch Settings" while in game room, or by clicking "Play" on top, and choose "settings" mode.<br/>
      <br/>
      You can edit game parameters (level, etc.. ) by clicking "Edit Game" in your game room.<br/>
    </p>
    <h2>
      Troubleshooting
    </h2>
    <h5>Port Connectivity Issues</h5>
    <p>
      To host a game on NukemNet, you must forward your game UDP port (23513 by default).<br/>
      You can do so in your home router interface, or right within NukemNet.<br/>
      to use the port-forward feature in NukemNet, click "PortSettings" on top,<br/>
      Wait for the spinner to finish, then click "Just forward port for me".<br/>
      It may or may not work depending on your router configuration.<br/>
      you can click "Check my gameport connectivity" to check if your port os ready.<br/>
      <br/>
      If you cannot forward your port, no worries! there's still a workaround.<br/>
      Once you host a multiplayer game room, click "Try Relay Server", then choose the relay with lowest ping.<br/>
      This will overcome the requirement to port-forward completely, however may/may not add some lag, depending on various situations.
    </p>
    <h5>My dos game is launching and closing immediately</h5>
    <p>
      When that happens, it usually means you need to setup the dos game sound card using DosBox.<br/>
      Games such as duke3d have an additional "setup.exe" file to setup various game settings, and sound cards too<br/>
      You may launch that setup file from dosbox, or using NukemNet.<br/>
      To launch from NukemNet, click "Launch Settings" while in game room, or click "Play", and choose "settings" mode.<br/>
      Ensure that your sound card setting works, then the game should start.
    </p>
    <h5>NBlood Camera Stutters on Multiplayer</h5>
    <p>
      This NBlood bug is rare, but happens on some systems with high refresh rates.<br/>
      To work around it, Enable VSync anc cap the FPS to 30 or 60.
    </p>
  </div>
};

export default HowTo;
