import { Injectable } from '@nestjs/common';
import { makeURLSearchParams, Message } from 'discord.js';
import { BotName_lowercase } from '../common/constants';
import { Room_InMemory } from '../common/room';
import {table} from 'table'
import { REST } from '@discordjs/rest';
import { Routes } from 'discord-api-types/v10';
import * as sleep from 'sleep-promise';
import { truncateWithEllipses } from '../common/StringUtils';

const rest = new REST({ version: '10' }).setToken(process.env.DISCORD_BOT_TOKEN);

const waitUntilSyncIntoDiscordChannels = 5000 // MS

@Injectable()
export class DiscordService {

    public get isDisabled() {
        return process.env.DISABLE_DISCORD?.toLowerCase() == 'true'
    }

    async init() {
        if(this.isDisabled) return;
        
        try {
            this.roomListMessage = await this.eraseMessageInChan(process.env.DISCORD_GAMEROOMS_CHAN_ID, true);
            this.playersListMessage = await this.eraseMessageInChan(process.env.DISCORD_IRCUSERS_CHAN_ID, true);
            
            await this.drawGameRooms([]);
            await this.drawPlayersList([]);
        } catch(e) {
            console.error('Failed to get live irc channels');
            process.exit(1);
        }
    }

    playersListMessage:Message|null = null;
    playersArray = [] as string[];
    isDrawingPlayersArray = false;
    async drawPlayersList(playersArray:string[]) {
        if(this.isDisabled) return;

        playersArray = JSON.parse(JSON.stringify(playersArray)); // must clone to prevent incorrect update logic.

        this.playersArray = playersArray;
        if(this.isDrawingPlayersArray) return;
        this.isDrawingPlayersArray = true;

        do {
            playersArray = this.playersArray; // get latest update
            try {
                const tableStr = (()=>{
                    const rows: any[] = [];
                    playersArray.forEach(p=>p.toLowerCase()!=BotName_lowercase?rows.push([p]):null);
        
                    if(rows.length) {
                        const data = [
                            ...rows
                        ];
                        return "```"+table(data)+"```";
                    } else {
                        return "No players currently in NukemNet IRC."
                    }
                })();
        
                if(this.playersListMessage == null) {
                    this.playersListMessage = await rest.post(Routes.channelMessages(process.env.DISCORD_IRCUSERS_CHAN_ID), {body: {
                        content: tableStr
                    }}) as any;
                } else {
                    await rest.patch(Routes.channelMessage(process.env.DISCORD_IRCUSERS_CHAN_ID, this.playersListMessage.id), {body: {
                        content: tableStr
                    }}) as any;
                }
            } catch(e) {}
            await sleep(waitUntilSyncIntoDiscordChannels);
        } while(playersArray != this.playersArray);
        
        this.isDrawingPlayersArray = false;
    }

    roomListMessage:Message|null = null;
    roomArray:Room_InMemory[] = [];
    isDrawingRoomArray = false;
    async drawGameRooms(roomArray:Room_InMemory[]) {
        if(this.isDisabled) return;

        roomArray = JSON.parse(JSON.stringify(roomArray)); // must clone to prevent incorrect update logic.
        
        this.roomArray = roomArray;
        if(this.isDrawingRoomArray) return;
        this.isDrawingRoomArray = true;

        do {
            roomArray = this.roomArray; // get latest update
            try {
                const tableStr = (()=>{
                    if(roomArray.length) {
                        const rows = roomArray.map(r=>
                            [
                                truncateWithEllipses(r.owner,20)+'\r\n'+truncateWithEllipses(r.name,20)+'\r\n'+r.channel,
                                `${r.playersIn}/${r.maxPlayers}`+(r?.password?.length?'\r\nP':'')+(r.isStarted?'\r\nG':''),
                                r.gameName+'\r\n'+r.execName
                            ]);
                        const data = [
                            ['Room', 'State','Game&Exec'],
                            ...rows
                        ];

                        const roomLinks = roomArray.map(r=>`<nukemnet://join/${r.channel.substring(1)}>`);

                        return "```"+table(data, {
                            columnDefault: {
                                paddingLeft: 0,
                                paddingRight: 0
                            },
                        })+"```\r\n"+roomLinks.join('\r\n');
                    } else {
                        return "No game rooms advertised yet."
                    }
                })();
        
                if(this.roomListMessage == null) {
                    this.roomListMessage = await rest.post(Routes.channelMessages(process.env.DISCORD_GAMEROOMS_CHAN_ID), {body: {
                        content: tableStr
                    }}) as any;
                } else {
                    await rest.patch(Routes.channelMessage(process.env.DISCORD_GAMEROOMS_CHAN_ID, this.roomListMessage.id), {body: {
                        content: tableStr
                    }}) as any;
                }
            } catch(e) {}
            await sleep(waitUntilSyncIntoDiscordChannels);
        } while(roomArray != this.roomArray);
        
        this.isDrawingRoomArray = false;
    }

    async eraseMessageInChan(channel:string, leaveOutOneBotMessage?:boolean):Promise<null|Message> {
        if(this.isDisabled) return;
        
        let currentMessage:Message|null = null;
        let fetched;
        const limit = 100;
        do {
            fetched = await rest.get(Routes.channelMessages(channel), {query:makeURLSearchParams({limit:limit})});
            for(const v of fetched) {
                try {
                    const isMeBot = process.env.DISCORD_BOT_ID === v.author.id;
                    if(currentMessage == null && leaveOutOneBotMessage && isMeBot) {
                        currentMessage = v;
                    } else {
                        await rest.delete(Routes.channelMessage(channel, v.id));
                    }
                } catch(e) {}
            }
        }
        while(fetched.length >= limit);
        return currentMessage;
    }
}
