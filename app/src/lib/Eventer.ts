export class Eventer<T,D> {
    callbacks: {[e:string]: ((data:any)=>void)[]}

    constructor(){
        this.callbacks = {}
    }

    on(event:T, cb:(data:D)=>void){
        if(!this.callbacks[event as any]) this.callbacks[event as any] = [];
        this.callbacks[event as any].push(cb)
    }

    emit(event:string, data:D) {
        let cbs = this.callbacks[event]
        if(cbs){
            cbs.forEach(cb => cb(data))
        }
    }
}