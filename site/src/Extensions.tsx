


import { Component } from 'solid-js';
const Extensions: Component = () => {
  return <div style="font-size: 12pt; margin-left:auto; margin-right:auto; width:fit-content">
    <section>Refer to the "Extensions" tab in the NukemNet application, for documentation on how to use/develop extensions.</section>

    Below is the TypeScript definition for Game and Executables definition.<br/>
    Useful if you want to add support for new games, or new executables within games.<br/>
    <a target="_blank" href="https://gitlab.com/aabluedragon/NukemNet/-/raw/main/app/src/GameDefsTypes.ts?ref_type=heads">Download GameDefsTypes.ts</a><br/>
    The game definition tree is represented by the <pre style={{display:'inline'}}>Game</pre> definition <pre style={{display:'inline'}}>interface</pre>
  </div>
    
};

export default Extensions;
