import { Injectable, OnModuleInit } from '@nestjs/common';
import * as sleep from 'sleep-promise';
import * as NNCmd from '../common/commands';
import * as NNCmd_Meta from '../common/commands-meta';
import { BotMetaChannel } from '../common/constants';
import { IrcClientExtra } from '../common/ircExtend';
import { Room_Advertisement, Room_CreateRequest, Room_InMemory, Room_UpdatePatchFromBot, Room_UpdatePatchFromHost } from 'src/common/room';
import { structuredClone } from '../common/utils';
import { DiscordService } from '../discord/discord.service';
import { IrcService } from '../irc/irc.service';


@Injectable()
export class RoomMgrService implements OnModuleInit {
    
    private rooms: Room_InMemory[] = [];

    constructor(private irc:IrcService, private discordService:DiscordService) {}

    onModuleInit() {
        setImmediate(()=>{
            this.periodicCheck();
        });

        this.irc.client.on('quit', (nick:string, reason, channels, message)=>{            
            // if room owner quit delete his game rooms, and broadcast room deleted to meta channel
            this.deleteRoomsByOwner(nick);
        });

        this.irc.client.on('part', (channel:string, nick:string, reason, message)=>{
            // if room owner leaves meta channel, delete his game rooms, and broadcast room deleted to meta channel
            if(channel.toLowerCase() === BotMetaChannel) {
                this.deleteRoomsByOwner(nick);
            }
        })

        this.irc.client.on('nick', (oldnick, newnick, channels, message)=>{
            this.updateRoomOwner(oldnick, newnick);
        });
    }

    async updateRoomOwner(oldNick:string, newnick:string) {
        for(const room of this.rooms) {
            // if room owner updates his nick, update this.rooms array "owner" property, and broadcast message to meta channel
            if(room.owner.toLowerCase() === oldNick.toLowerCase()) {
                room.owner = newnick;

                // Broadcast room update
                IrcClientExtra.sendNNCmdTo(this.irc.client, BotMetaChannel, {
                    op: 'roomDetailsUpdate',
                    data: {
                        channel: room.channel,
                        patch: {
                            owner: newnick
                        }
                    }
                } as NNCmd_Meta.CmdMeta_RoomDetailsUpdate);
            }
        }
        this.discordService.drawGameRooms(this.rooms);
    }

    async periodicCheck() {
        await sleep(3000);
        while(true) {
            try {
                if(IrcClientExtra.isConnected(this.irc.client)) {
                    await this.removeZombieRooms();
                }
            } catch(e) {}
            await sleep(10000);
        }
    }

    async removeZombieRooms() {
        // TODO: fix issues with this scenario: if a new player joins the room, and the host leaves, the room will remain.
        // Think of a way to detect that, and remove from advertised game list.

        const allChannelsInfo = await IrcClientExtra.getAllChannels(this.irc.client,5000);
        
        const allChannels = new Set(allChannelsInfo.map(c=>c.name.toLowerCase()));
        const userInfoInMetaChannel = Object.fromEntries(this.irc.client.chans.get(BotMetaChannel.toLowerCase()).users);
        const nicksInMetaChannel = new Set(
            userInfoInMetaChannel? Object.keys(userInfoInMetaChannel).map(n=>n.toLowerCase()):[]
        );
        
        const roomsToDelete:string[] = [];

        for(const room of this.rooms) {
            const roomKey = room.channel.toLowerCase();
            if(
                !allChannels.has(roomKey) || // Channel does not exist anymore
                parseInt(allChannelsInfo.find((row)=>row.name.toLowerCase()===roomKey)?.users) < 1 // channel exists with 0 users
            ) {
                // If game room channel is not listed in irc server, or has 0 users, delete it.
                roomsToDelete.push(room.channel); 
            }
            if(!nicksInMetaChannel.has(room.owner.toLowerCase())) {
                // If game room owner is not in meta channel, remove his room
                roomsToDelete.push(room.channel);
            }
        }

        this.deleteRooms(...roomsToDelete);
    }

    deleteRoomsByOwner(nick:string) {
        const roomsToDelete:string[] = [];
        for(const room of this.rooms) {
            if(room.owner.toLowerCase() === nick.toLowerCase()) {
                roomsToDelete.push(room.channel)
            }
        }
        this.deleteRooms(...roomsToDelete);
    }

    deleteRooms(...byChannelIds:string[]) {
        if(!byChannelIds.length) return;

        const channelIdsToDeleteArray = byChannelIds.map(c=>c.toLowerCase());
        const channelIdsToDeleteSet = new Set(channelIdsToDeleteArray);

        for(let i=this.rooms.length-1;i>=0;i--) {
            const room = this.rooms[i];
            if(channelIdsToDeleteSet.has(room.channel)) {
                this.rooms.splice(i,1);
            }
        }

        IrcClientExtra.sendNNCmdTo(this.irc.client, BotMetaChannel, {
            op:'roomsDeleted',
            data: {
                channels:channelIdsToDeleteArray
            }
        } as NNCmd_Meta.CmdMeta_RoomDeleted);
        this.discordService.drawGameRooms(this.rooms);
    }


    getAllRooms() {
        const res = structuredClone(this.rooms) as Room_InMemory[];
        
        for(const room of res) {
            (room as Room_Advertisement).hasPassword = this.hasPassword(room.password);
            delete room.password;
        }

        return res;
    }

    createRoom(roomReq:Room_CreateRequest, owner:string, channel:string) {
        if(this.ownerHasRoom(owner)) {
            throw new Error('Owner has room')
        }

        const newRoom:Room_InMemory = {
            channel,
            owner,
            createdAt: Date.now(),
            playersIn: 1,
            isStarted:false,
            ...roomReq
        };
        
        this.rooms.push(newRoom);

        const newRoomWithoutPassword = structuredClone(newRoom) as Room_InMemory
        delete newRoomWithoutPassword.password;
        const advRoom = {
            ...newRoomWithoutPassword,
            hasPassword: this.hasPassword(newRoom.password)
        } as Room_Advertisement;

        IrcClientExtra.sendNNCmdTo(this.irc.client, BotMetaChannel, {op: 'newRoom', data: advRoom} as NNCmd_Meta.CmdMeta_NewRoom);
        this.discordService.drawGameRooms(this.rooms);

        return newRoom;
    }

    ownerHasRoom(owner:string) {
        for(const room of this.rooms) {
            if(room.owner.toLowerCase() === owner.toLowerCase()) {
                return true;
            }
        }
        return false;
    }

    channelRoomTaken(chan:string) {
        const chanLow = chan.toLowerCase();
        for(const room of this.rooms) {
            if(room.channel.toLowerCase() === chanLow) {
                return true;
            }
        }
        return false;
    }

    getGameRoomByOwnerAndChannel(nick:string, channel:string) {
        const room = this.rooms.filter(r=>r.channel.toLowerCase() === channel.toLowerCase() && r.owner.toLowerCase() === nick.toLowerCase())?.[0];
        return room? room:null;
    }
    
    updateRoomFromUser(owner:string, channel:string, patch:Room_UpdatePatchFromHost) {

        const roomToUpdate = this.rooms.filter(r=>r.channel.toLowerCase() === channel.toLowerCase() && r.owner.toLowerCase() === owner.toLowerCase())?.[0];
        if(!roomToUpdate) throw new Error('Game room not found');

        // Apply patch
        let hasPasswordPatch = null
        let needToBroadcastUpdate = false;
        for(const prop in patch) {
            if(roomToUpdate[prop] !== patch[prop]) {
                // Check if need to broadcase changes
                if(prop === 'password') {
                    if(patch.password === null && roomToUpdate?.password?.length>0) {
                        hasPasswordPatch = false;
                        needToBroadcastUpdate = true;
                    } else if(patch?.password?.length>0 && !(roomToUpdate?.password?.length>0)) {
                        hasPasswordPatch = true;
                        needToBroadcastUpdate = true;
                    }
                } else {
                    needToBroadcastUpdate = true;
                }

                roomToUpdate[prop] = patch[prop];
            }
        }

        const patchFromBot = structuredClone(patch) as Room_UpdatePatchFromBot;
        if(hasPasswordPatch != null) {
            patchFromBot.hasPassword = hasPasswordPatch
        }
        delete patchFromBot.password;

        // delete null props
        for(const prop in roomToUpdate) {
            if(roomToUpdate[prop] == null) delete roomToUpdate[prop];
        }

        if(needToBroadcastUpdate) {
            IrcClientExtra.sendNNCmdTo(this.irc.client, BotMetaChannel, {
                op:'roomDetailsUpdate',
                data: {channel, patch:patchFromBot}
            } as NNCmd_Meta.CmdMeta_RoomDetailsUpdate);
            this.discordService.drawGameRooms(this.rooms);
        }

        const foundChanges = needToBroadcastUpdate || hasPasswordPatch != null;

        return {
            op: 'updateRoomResponse',
            data: {foundChanges}
        } as NNCmd.Cmd_ResponseUpdateRoom
    }

    hasPassword(pass:string):boolean {
        return pass != null && pass.length>0;
    }

}
